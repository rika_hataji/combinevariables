package openCSV.combineVariables

data class Combination(
    val name: String?,
    val textTable: String?,
    val text: String?
)
