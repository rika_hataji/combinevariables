package openCSV.combineVariables.localization

import openCSV.combineVariables.localization.*

/**
 * LocalizationのBaseクラス. 英語の情報を持つ
 *
 * 情報源: PCR4.0.0_TextTable.xlsx
 */
open class Localization {
    open val languageCode = LanguageCode.EN

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    open val commonOn = "ON"
    open val commonOff = "OFF"
    open val commonManual = "Manual"
    open val commonInvalid = "Invalid"
    open val commonValid = "Valid"
    open val commonLow = "LOW"
    open val commonMedium = "MEDIUM"
    open val commonHigh = "HIGH"
    open val commonRecover = "Recover"
    open val commonRecoverEnter = "Recover (Enter)"
    open val commonNotRecover = "Do not recover"
    open val commonOKEnter = "OK(ENTER)"
    open val commonOK = "OK"
    open val commonCancel = "Cancel"
    open val commonCancelEnter = "Cancel (ENTER)"
    open val commonUpdate = "Update (Enter)"
    open val commonNotUpdate = "Do not update"
    open val commonDisconnect = "DISCONNECT"
    open val commonSelect = "Select"
    open val commonYes = "Yes"
    open val commonYesEnter = "Yes (ENTER)"
    open val commonNo = "No"
    open val commonNoEnter = "No (ENTER)"
    open val commonBack = "Back"
    open val commonRestore = "Back"
    open val commonSave = "Save(ENTER)"
    open val commonCancelEnterUppercase = "CANCEL (ENTER)"
    open val commonAbort = "Cancel"
    open val commonApplyEnter = "Reflect (ENTER)"
    open val commonNext = "Next (Enter)"
    open val commonStartEnter = "Start(ENTER)"
    open val commonClear = "Clear"
    open val commonConnecting = "Connecting…"
    open val commonConnected = "Connected"
    open val commonDiagnosing = "Diagnosing…"
    open val commonProgramming = "Programming..."
    open val commonRetrievingUnitData = "Retrieving unit data..."
    open val commonRemainingSeconds = "Remaining:{0} seconds"
    open val commonGetRetrieving = "Loading data..."
    open val commonTimeLimit = "Time limit: {0} sec."
    open val commonCharging = "Charging.."
    open val commonBeingPerformed = "While tasks are being performed"
    open val commonNotRun = "Not performed"
    open val commonNormal = "Normal"
    open val commonAbnormal = "Error"
    open val commonCompletedNormally = "Completed Normally"
    open val commonCompletedWithError = "Completed with Error"
    open val commonEBikeReport = "E-BIKE REPORT"
    open val commonBicycleInformation = "Bicycle information"
    open val commonSkipUppercase = "SKIP"
    open val commonSkipEnter = "SKIP (ENTER)"
    open val commonWaitingForJudgment = "Waiting for judgment"
    open val commonStarting = "Starting {0}."
    open val commonCompleteSettingMessage = "Setting has completed normally."
    open val commonProgrammingErrorMessage = "An error occurred during programming."
    open val commonSettingValueGetErrorMessage =
        "An error occurred while retrieving the setting values."
    open val commonSettingUpMessage = "Do not disconnect until programming is complete."
    open val commonCheckElectricWireMessage =
        "Check whether the electric wire is disconnected or not."
    open val commonElectlicWireIsNotDisconnectedMessage =
        "Perform an error check if the electric wire is not disconnected."
    open val commonErrorCheckResults = "Error check results"
    open val commonErrorCheckCompleteMessage = "Error check of the {0} has been completed."
    open val commonCheckCompleteMessage = "Checking of {0} is complete."
    open val commonFaultCouldNotBeFound = "A fault could not be found."
    open val commonFaultMayExist = "A fault may exist."
    open val commonMayBeFaulty = "{0} may be faulty."
    open val commonItemOfMayBeFaulty = "The {0}th item of {1} may be faulty."
    open val commonSkipped = "Skipped"
    open val commonDefault = "Default"
    open val commonAllUnitErrorCheckCompleteMessage = "Error check is complete for all units."
    open val commonConfirmAbortWork = "Do you want to stop the process?"
    open val commonReturnValue = "The previous values will be restored."
    open val commonManualFileIsNotFound = "Manual file is not found."
    open val commonReinstallMessage = "Uninstall {0} and reinstall it."
    open val commonWaiting = "During standby - STEP {0}"
    open val commonDiagnosisInProgress = "Diagnosis in progress - STEP {0}"
    open val commonStep = "Step {0}/{1}"
    open val commonNotPerformed = "Not performed"
    open val commonDo = "Yes"
    open val commonNotDo = "No"
    open val commonOEMSetting = "OEM setting"
    open val commonTotal = "Total"
    open val commonNow = "Current"
    open val commonReset = "Reset"
    open val commonEnd = "End (ENTER)"
    open val commonComplete = "Complete"
    open val commonNextSwitch = "To the next switch"
    open val commonStart = "Start"
    open val commonCountdown = "Countdown"
    open val commonAdjustmentMethod = "Adjustment method"
    open val commonUnknown = "Unknown"
    open val commonUnitRecognition = "Connection check"

    //region TextTableには記載なし
    open val commonUpdateAll = "UPDATE ALL"
    open val commonDismiss = "Dismiss"
    open val commonDone = "Done"
    //endregion

    // 単位
    open val commonUnitStep = "{0} gears"
    open val commonUnitSecond = "sec"

    // 公式HPの言語設定
    open val commonHPLanguage = "en-EU"

    // アシスト
    open val commonAssistEco = "ECO"
    open val commonAssistNormal = "NORMAL"
    open val commonAssistHigh = "HIGH"
    open val commonAssistTrail = "TRAIL"
    open val commonAssistBoost = "BOOST"
    open val commonAssistAssistOff = "Assist off"

    // 表示言語
    open val commonLanguageEnglish = "English"
    open val commonLanguageGerman = "Deutsch"
    open val commonLanguageFrench = "Français"
    open val commonLanguageItalian = "Italiano"
    open val commonLanguageDutch = "Nederlands"
    open val commonLanguageSpanish = "Español"
    open val commonLanguageJapanise = "日本語"
    open val commonLanguageChinese = "中文"
    open val commonLanguageTaiwanese = "台灣"

    // その他
    open val commonApply = "APPLY"
    open val commonDelete = "Delete"
    open val commonWired = "Wired"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    open val unitCommonFirmwareVersion = "Firmware version"
    open val unitCommonSerialNo = "Serial No."
    open val unitCommonFirmwareUpdate = "Update firmware"
    open val unitCommonUpdateToTheLatestVersion = "Update to the latest version."
    open val unitCommonFirmwareUpdateNecessary = "If necessary, update to the latest version."
    //endregion

    //region 一覧表示
    open val unitUnitBattery = "Battery"
    open val unitUnitDI2Adapter = "DI2 Adapter"
    open val unitUnitInformationDisplay = "Information display"
    open val unitUnitJunctionA = "Junction A"
    open val unitUnitRearSuspension = "Rear suspension"
    open val unitUnitFrontSuspension = "Front suspension"
    open val unitUnitDualControlLever = "Dual control lever"
    open val unitUnitShiftingLever = "Shifting lever"
    open val unitUnitSwitch = "Switch"
    open val unitUnitBatterySwitch = "Battery switch"
    open val unitUnitPowerMeter = "Power meter"
    //endregion

    //region DU
    open val unitDUYes = "Yes"
    open val unitDUNO = "No"
    open val unitDUDestination = "Destination"
    open val unitDUType = "Type {0}"
    open val unitDUPowerTerminalOutputSetting =
        "Light/accessory power supply terminal output setting"
    open val unitDULightONOff = "Light ON/OFF"
    open val unitDULightConnection = "Light connection"
    open val unitDUButtonOperation = "Button operation"
    open val unitDUAlwaysOff = "Always OFF"
    open val unitDUButtonOperations = "Button operation"
    open val unitDUAlwaysOn = "Always ON"
    open val unitDUSetValue = "Set value"
    open val unitDUOutputVoltage = "Output voltage"
    open val unitDURemainingLightCapacity = "Remaining light capacity"
    open val unitDULightOutput = "Light output"
    open val unitDUBatteryCapacity = "Battery capacity for remaining light"
    open val unitDUWalkAssist = "Walk assist"
    open val unitDUTireCircumference = "Tire circumference"
    open val unitDUGearShiftingType = "Gear shifting type"
    open val unitDUShiftingMethod = "Shifting method"
    open val unitDUFrontChainRing = "Number of teeth of the front chain ring"
    open val unitDURearSprocket = "Number of teeth of the rear sprocket"
    open val unitDUToothSelection = "Tooth selection"
    open val unitDUInstallationAngle = "Drive unit installation angle"
    open val unitDULightOffRoad = "LIGHT OFF ROAD"
    open val unitDUAssistCustomize = "Assist customize"
    open val unitDUProfile1 = "Profile 1"
    open val unitDUProfile2 = "Profile 2"
    open val unitDUCostomize = "Customize"
    open val unitDUAssistCharacter = "Assist character"
    open val unitDUPowerful = "POWERFUL"
    open val unitDUMaxTorque = "Max torque"
    open val unitDUAssistStart = "Assist start"
    open val untDUMild = "MILD"
    open val unitDUQuick = "QUICK"
    open val unitDUAssistLv = "Lv.{0:D}"
    open val unitDUAssitLvForMobile = "Lv.{0}"
    open val unitDUAssistLvParen = "Lv.{0} ({1})"
    open val unitDUOther = "Other"
    open val unitDU1stGear = "1st gear"
    open val unitDU2ndGear = "2nd gear"
    open val unitDU3rdGear = "3rd gear"
    open val unitDU4thGear = "4th gear"
    open val unitDU5thGear = "5th gear"
    open val unitDU6thGear = "6th gear"
    open val unitDU7thGear = "7th gear"
    open val unitDU8thGear = "8th gear"
    open val unitDU9thGear = "9th gear"
    open val unitDU10thGear = "10th gear"
    open val unitDU11thGear = "11th gear"
    open val unitDU12thGear = "12th gear"
    open val unitDU13thGear = "13th gear"
    open val unitDUShiftModeAfterDisconnect = "Mode after the app is disconnected"
    open val unitDUAutoGearShiftAdjustment = "Automatic gear shifting adjustment"
    open val unitDUShiftingAdvice = "Shifting advice"
    open val unitDUTravelingDistance = "Traveling distance"
    open val unitDUTravelingDistanceMaintenanceAlert = "Traveling distance (Maintenance alert)"
    open val unitDUDate = "Date"
    open val unitDUDateYear = "Date: Year (Maintenance alert)"
    open val unitDUDateMonth = "Date: Month (Maintenance alert)"
    open val unitDUDateDay = "Date: Day (Maintenance alert)"
    open val unitDUTotalDistance = "Total distance"
    open val unitDUTotalTime = "Total time"
    open val unitDURemedy = "Remedy"
    //endregion

    //region BT
    open val unitBTCycleCount = "Cycle count"
    open val unitBTTimes = "Times"
    open val unitBTRemainingCapacity = "Remaining capacity"
    open val unitBTFullChargeCapacity = "Full charge capacity"
    open val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //

    //region SC, EW
    open val unitSCEWMode = "Mode"
    open val unitSCEWModes = "Modes"
    open val unitSCEWBeep = "Beep : {0}"
    open val unitSCEWBeepSetting = "Beep setting"
    open val unitSCEWDisplayTime = "Display time : {0}"
    open val unitSCEWWirelessCommunication = "Wireless communication"
    open val unitSCEWCommunicationModeOff = "OFF"
    open val unitSCEWAntBle = "ANT+/Bluetooth® LE"
    open val unitSCEWAnt = "ANT+"
    open val unitSCEWBle = "Bluetooth® LE"
    open val unitSCEWBleName = "Bluetooth® LE name\n"
    open val unitSCEWCharacterLimit = "1 to 8 half-width alphanumeric characters"
    open val unitSCEWPasskeyDescription = "6-digit number that starts with a number other than 0"
    open val unitSCEWConfirmation = "Confirmation"
    open val unitSCEWPassKeyInitialization = "PassKey initialization"
    open val unitSCEWEnterPasskey = "Enter a 6-digit number that starts with a number other than 0."
    open val unitSCEWNotMatchPassKey = "Passkey does not match."
    open val unitSCEWDisplayUnits = "Display units"
    open val unitSCEWInternationalUnits = "International units"
    open val unitSCEWDisplaySwitchover = "Shift"
    open val unitSCEWRangeOverview = "Range"
    open val unitSCEWAutoTimeSetting = "Time (auto)"
    open val unitSCEWManualTimeSetting = "Time (manual)"
    open val unitSCEWUsePCTime = "Using the PC time"
    open val unitSCEWTimeSetting = "Time setting"
    open val unitSCEWDoNotSet = "Do not set"
    open val unitSCEWBacklight = "Backlight"
    open val unitSCEWBacklightSetting = "Backlight setting"
    open val unitSCEWManual = "Manual"
    open val unitSCEWBrightness = "Brightness"
    open val unitSCEWFont = "Font"
    open val unitSCEWSet = "Set"
    //endregion
    //endregion

    //region MU
    open val unitMUGearPosition = "Gear position"
    open val unitMUAdjustmentSetting = "Adjustment setting"

    open val unitMU5thGear = "5th gear"
    open val unitMU8thGear = "8th gear"
    open val unitMU11thGear = "11th gear"
    //endregion
    //endregion

    //region SUS
    open val unitSusCD = "CD"
    open val unitSusCtd = "CTD"
    open val unitSusCtdBV = "CTD (BV)"
    open val unitSusCtdOrDps = "CTD (DISH) or DPS"
    open val unitSusPositionFront = "Position{0} front"
    open val unitSusPositionRear = "Position{0} rear"
    open val unitSusPositionDisplayedOnSC = "Position{0} Displayed on SC"
    open val unitSusC = "C"
    open val unitSusT = "T"
    open val unitSusD = "D"
    open val unitSusDUManufacturingSerial = "DU manufacturing serial"
    open val unitSusBackupDateAndTime = "Backup date and time"
    //endregion

    //region ST,SW
    open val unitSTSWUse2ndGgear = "Use 2nd gear"
    open val unitSTSWSwitchMode = "Switch mode"
    open val unitSTSWForAssist = "for Assist"
    open val unitSTSWForShift = "for Shift"
    open val unitSTSWUse = "Use"
    open val unitSTSWDoNotUse = "Do not use"
    //endregion

    //region Shift
    open val unitShiftCopy = "Copy {0}"
    open val unitShiftShiftUp = "Shift up"
    open val unitShiftShiftDown = "Shift down"
    open val unitShiftGearNumberLimit = "Gear number limit"
    open val unitShiftMultiShiftGearNumberLimit = "Gear number limit (Multi-shifting)"
    open val unitShiftInterval = "Gear-shifting interval"
    open val unitShiftMultiShiftGearShiftingInterval = "Gear-shifting interval (Multi-shifting)"
    open val unitShiftAutomaticGearShifting = "Auto shift"
    open val unitShiftMultiShiftFirstGearShiftingPosition =
        "1st gear shifting position (multi-shifting)"
    open val unitShiftMultiShiftSecondGearShiftingPosition =
        "2nd gear shifting position (multi-shifting)"
    open val unitShiftSingleShiftingPosition = "Single shifting position"
    open val unitShiftDoubleShiftingPosition = "Double shifting position"
    open val unitShiftFC = "FC"
    open val unitShiftShiftInterval = "Shift interval"
    open val unitShiftCS = "CS"
    open val unitShiftRemainingBatteryCapacity = "Remaining battery capacity"
    open val unitShiftBatteryMountingForm = "Battery mounting configuration"
    open val unitShiftPowerSupply = "With external power supply"
    open val unitShift20PercentOrLess = "20% or less"
    open val unitShift40PercentOrLess = "40% or less"
    open val unitShift60PercentOrLess = "60% or less"
    open val unitShift80PercentOrLess = "80% or less"
    open val unitShift100PercentOrLess = "100% or less"
    open val unitShiftRearShiftingUnit = "Rear shifting unit"
    open val unitShiftAssistShiftSwitch = "Assist/Shift switch"
    open val unitShiftInner = "Inner"
    open val unitShiftMiddle = "Middle"
    open val unitShiftOuter = "Outer"
    open val unitShiftUp = "Up"
    open val unitShiftDown = "Down"
    open val unitShiftGearShiftingInterval = "Gear-shifting interval"
    //endregion

    //region PC接続機器
    open val unitPCErrorCheck = "Error check"
    open val unitPCBatteryConsumptionCheck = "Battery consumption check"
    open val unitPCBatteryConsumption = "Battery consumption"
    //endregion

    //region エラーチェック
    open val unitErrorCheckMalfunctionInsideProsduct = "Are there any internal failures?"
    open val unitErrorCheckBatteryConnectedProperly = "Is the battery connected properly?"
    open val unitErrorCheckCanDetectTorque = "Can the torque be detected?"
    open val unitErrorCheckCanDetectVehicleSpeed = "Can the vehicle speed be detected?"
    open val unitErrorCheckCanDetectCadence = "Can the cadence be detected?"
    open val unitErrorCheckLightOperateNormally = "Does the light work properly?"
    open val unitErrorCheckOperateNormally = "Does it work properly?"
    open val unitErrorCheckDisplayOperateNormally = "Does the display area work properly?"
    open val unitErrorCheckBackLightOperateNormally = "Does the backlight work properly?"
    open val unitErrorCheckBuzzerOperateNormally = "Does the buzzer work properly?"
    open val unitErrorCheckSwitchOperateNormally = "Do the switches work properly?"
    open val unitErrorCheckwirelessFunctionNormally = "Does the wireless function work properly?"
    open val unitErrorCheckBatteryHasEnoughPower = "Is the built-in battery level high enough?"
    open val unitErrorCheckNormalShiftToEachGear = "Can gears be shifted correctly?"
    open val unitErrorCheckEnoughBatteryForShifting =
        "Is the battery level high enough for gear shifting?"
    open val unitErrorCheckCommunicateNormallyWithBattery =
        "Can communications with the battery be established properly?"
    open val unitErrorCheckLedOperateNormally = "Do LEDs work properly?"
    open val unitErrorCheckSwitchOperation = "Switch operation"
    open val unitErrorCheckCrankArmPperation = "Crank arm operation"
    open val unitErrorCheckLcdCheck = "LCD check"
    open val unitErrorCheckLedCheck = "LED check"
    open val unitErrorCheckAudioCheck = "Audio check"
    open val unitErrorCheckWirelessCommunicationCheck = "Wireless communication check"
    open val unitErrorCheckPleaseWait = "Please wait."
    open val unitErrorCheckCheckLight = "Check light"
    open val unitErrorCheckSprinterSwitch = "Sprinter switch"
    open val unitErrorCheckSwitch = "Switch"
    //endregion
    //endregion

    //region M0 基本構成
    open val m0RecommendationOfAccountRegistration = "Recommendation of account registration"
    open val m0AccountRegistrationMsg =
        "If you create an account, you can use E-TUBE PROJECT in a more convenient manner.\n・Allows you to register your bicycle.\n・Remembers the wireless unit, making it easier to connect your bicycle."
    open val m0SignUp = "SIGN UP"
    open val m0Login = "LOGIN"
    open val m0SuspensionSwitch = "Suspension switch"
    open val m0MayBeFaulty = "{0} may be faulty."
    open val m0ReplaceOrRemoveMessage =
        "Replace or remove the following unit and connect again.\n{0}"
    open val m0UnrecognizableMessage =
        "Shifter or switch is not recognized.\nCheck that the electric wire is not disconnected."
    open val m0UnknownUnit = "Unknown Unit {0}"
    open val m0UnitNotDetectedMessage =
        "Check that the electric wire is not removed.\nIf removed, disconnect the unit and then reconnect."
    open val m0ConnectErrorMessage = "A communication error has occurred. Try connecting again."
    //endregion

    //region M1 起動画面
    open val m1CountryOfUse = "Country of use"
    open val m1Continent = "Continents"
    open val m1Country = "Countries/Regions"
    open val m1OK = "OK"
    open val m1PersonalSettingsMsg = "You can easily set the bike to suit your riding style."
    open val m1NewRegistration = "NEW REGISTRATION"
    open val m1Login = "LOGIN"
    open val m1ForCorporateUsers = "For corporate users"
    open val m1Skip = "Skip"
    open val m1TermsOfService = "Terms of use"
    open val m1ComfirmLinkContentsAndAgree =
        "Click the link below and agree to the content."
    open val m1AgreeToTheAll = "I agree to tha all"
    open val m1TermsOfServicePolicy = "Terms of use"
    open val m1AgreeToTheTermsOfService = "I agree to the terms of use"
    open val m1AppOperationLogAgreement = "App operation log agreement"
    open val m1AgreeToTheAppOperationLogAgreement = "I agree to the App operation log ageement."
    open val m1DataProtecitonNotice = "Data proteciton notice"
    open val m1AgreeUppercase = "AGREE"
    open val m1CorporateLogin = "Corporate login"
    open val m1Email = "Email"
    open val m1Password = "Password"
    open val m1DownloadingLatestFirmware = "The latest firmware is being downloaded."
    open val m1Hi = "Hi, {0}"
    open val m1GetStarted = "Get started"
    open val m1RegisterBikeOrPowerMeter = "Register bike or power meter"
    open val m1ConnectBikeOrPowerMeter = "Connect bike or power meter"
    open val m1BikeList = "My bike"
    open val m1LastConnection = "Last connection"
    open val m1Searching = "Searching.."
    open val m1Download = "DOWNLOAD"
    open val m1SkipUppercase = "SKIP"
    open val m1Copyright = "©SHIMANO INC. ALL RIGHTS RESERVED"
    open val m1CountryArea = "Countries/Regions"
    open val m1ConfirmContentMessage =
        "If you agree to these terms and conditions, place a checkmark and press the \"Next\" button to proceed."
    open val m1Next = "Next"
    open val m1Agree = "Agree"
    open val m1ImageRegistrationFailed = "Failed to register the image."
    open val m1InternetConnectionUnavailable =
        "You are not connected to the Internet. Connect to the Internet and try again."
    open val m1ResumeBikeRegistrationMessage =
        "No applicable bicycle found. Connect to the Internet and register the bicycle again."
    open val m1ResumeImageRegistrationMessage =
        "An unexpected error has occurred. Register the image again."
    open val m1NoStorageSpaceMessage = "There is not enough free space on your smartphone."
    open val m1Powermeter = "Powermeter | {0}"
    //endregion

    //region M2 ペアリング
    open val m2SearchingUnits = "Searching units.."
    open val m2HowToConnectUnits = "How to connect units"
    open val m2Register = "Register"
    open val m2UnitID = "ID:{0}"
    open val m2Passkey = "Passkey"
    open val m2EnterPasskeyToRegisterUnit = "Enter passkey to register unit."
    open val m2OK = "OK"
    open val m2CancelUppercase = "CANCEL"
    open val m2ChangeThePasskey = "Change the passkey?"
    open val m2NotNow = "Not now"
    open val m2Change = "Change"
    open val m2Later = "Later"
    open val m2NewPasskey = "New passkey"
    open val m2PasskeyDescription = "Enter a 6-digit number starting with a number other than 0"
    open val m2Cancel = "Cancel"
    open val m2Nickname = "NICKNAME"
    open val m2ConfirmUnits = "CONFIRM UNITS"
    open val m2SprinterSwitchHasConnected = "Sprinter switch has connected"
    open val m2SkipRegister = "Skip register"
    open val m2RegisterAsNewBike = "REGISTER AS NEW BIKE"
    open val m2Left = "Left"
    open val m2Right = "Right"
    open val m2ID = "ID"
    open val m2Update = "UPDATE"
    open val m2Customize = "CUSTOMIZE"
    open val m2Maintenance = "MAINTENANCE"
    open val m2Connected = "Connected"
    open val m2Connecting = "Connecting.."
    open val m2AddOnRegisteredBike = "Add on registered bike"
    open val m2AddPowermeter = "Add powermeter"
    open val m2Monitor = "MONITOR"
    open val m2ConnectTheBikeAndClickNext = "CONNECT THE BIKE AND CLICK \"NEXT\"."
    open val m2Next = "NEXT"
    open val m2ChangePasskey = "Change passkey"
    open val m2PowerMeter = "Power meter"
    open val m2CrankArmSet = "crank arm set"
    open val m2NewBicycle = "New bicycle"
    open val m2ChangeWirelessUnit = "Change wireless unit"
    open val m2DeleteConnectionInformationMessage =
        "After making changes, delete connection information from [Settings] > [Bluetooth] on your device.\nIf connection information is not deleted, it will not be renewed, preventing a connection from being established."

    // TODO:TextTableには表記がないので要確認
    open val m2CurrentPasskey = "Current passkey"
    open val m2Help = "Help"
    open val m2PairingCompleted = "Pairing complete"
    open val m2AddSwitch = "Add switch"
    open val m2PairDerailleurAndSwitch =
        "Pair a switch with the derailleur. *The derailleur can be operated with the switch once the bicycle is disconnected from E-TUBE PROJECT."
    open val m2HowToPairing = "Pairing method"
    open val m2PairingIdLabel = "ID:{0}({1})"
    open val m2IdInputAreaHint = "Add switch"
    open val m2InvalidQRCodeErrorMsg =
        "Scan the QR code on the switch. A different QR code may have been scanned."
    open val m2QRScanMsg =
        "Tap on the screen to focus. If the QR code cannot be read, enter the 11-digit ID."
    open val m2QRConfirm = "QR code/ID position"
    open val m2SerialManualInput = "Manual ID entry"
    open val m2CameraAccessGuideMsgForAndroid =
        "Access to camera is not allowed. In the app settings, allow access for switch pairing."
    open val m2CameraAccessGuideMsgForIos =
        "Access to camera is not allowed. Allow access in the app setting. When the setting is changed to allow access the app restarts."
    open val m2AddWirelessSwitches = "Add wireless switch"

    //region TODO: 仮
    open val presetFileSavedFailed = "preset save failed"
    //endregion
    //endregion

    //region M3 接続と切断
    open val m3Searching = "Searching..."
    open val m3Connecting = "Connecting..."
    open val m3Connected = "Connected"
    open val m3Detected = "Detected"
    open val m3SearchOff = "OFF"
    open val m3Disconnect = "DISCONNECT"
    open val m3Disconnected = "Disconnected"
    open val m3RecognizedUnits = "Recognized units"
    open val m3Continue = "Continue"

    //region 互換性確認
    open val m3CompatibilityTable = "Compatibility table"
    open val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "For the following units, only 1 of each unit can be connected."
    open val m3CompatibilityErrorUnitSpecMessage1 =
        "The recognized combination of units is not a compatible one. Connect the units according to the compatibility table."
    open val m3CompatibilityErrorUnitSpecMessage2 =
        "Or the compatibility requirements can be satisfied by removing the following red units."
    open val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "The firmware for the following units is not up-to-date."
    open val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "You may be able to resolve this issue by updating the firmware for expanded compatibility.\nTry updating the firmware."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    open val m3CompatibilityErrorDUAndBT =
        "The recognized combination of units is not a compatible one. Connect the units according to the compatibility table."
    //endregion
    //endregion

    //region M4 アップデート
    open val m4Latest = "Latest"
    open val m4UpdateAvailable = "UPDATE AVAILABLE"
    open val m4UpdateAllUnits = "UPDATE ALL"
    open val m4UpdateAnyUnits = "UPDATE | ESTIMATED TIME - {0}"
    open val m4History = "HISTORY"
    open val m4AllUpdateConfirmTitle = "Update all?"
    open val m4AllUpdateConfirmMessage = "Estimated time: {0}"
    open val m4SelectAll = "SELECT ALL (ENTER)"
    open val m4Update = "Update"
    open val m4EstimatedTime = "Estimated time : "
    open val m4UpdateUppercase = "UPDATE"
    open val m4Cancel = "CANCEL"
    open val m4BleVersion = "Bluetooth® LE ver.{0}"
    open val m4OK = "OK (ENTER)"

    // TODO:TextTableには表記がないので要確認
    open val m4Giant = "GIANT"
    open val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    open val m5EBike = "E-BIKE"
    open val m5Assist = "Assist"
    open val m5MaximumAssistSpeed = "Maximum assist speed : {0}"
    open val m5AssistPattern = "Assist pattern : {0}"
    open val m5RidingCharacteristic = "Riding characteristic : {0}"
    open val m5CategoryShift = "Shift"
    open val m5Synchronized = "Synchronized"
    open val m5AutoShift = "Auto shift"
    open val m5Advice = "Advice"
    open val m5Switch = "Switch"
    open val m5ForAssist = "For assist"
    open val m5Suspension = "Suspension"
    open val m5ControlSwitch = "Control switch"
    open val m5SystemInformation = "System information"
    open val m5Information = "Information"
    open val m5CategoryDisplay = "Display"
    open val m5DisplayTime = "Display time: {0}"
    open val m5Mode = "{0} mode"
    open val m5Other = "Other"
    open val m5WirelessSetting = "Wireless settings"
    open val m5Fox = "FOX"
    open val m5ConnectBikeToApplyChangedSettings = "Connect the bike to apply changed settings."
    open val m5CannotBeSetMessage = "There are functions that cannot be set."
    open val m5S1 = "S1 : {0}"
    open val m5S2 = "S2 : {0}"
    open val m5GearShiftingInterval = "Gear-shifting interval : {0}"
    open val m5GearNumberLimit = "Gear number limit : {0}"
    open val m5SusSwitchValue = "{0} / {1}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    //endregion
    open val m5Shift = "shift"
    open val m5Display = "display"
    //endregion

    //region M6 バイク設定
    open val m6Bike = "Bike"
    open val m6Nickname = "Nickname"
    open val m6ChangeWirelessUnit = "Change wireless unit"
    open val m6DeleteUnits = "Delete units"
    open val m6DeleteBike = "Delete bike"
    open val m6Preset = "Preset"
    open val m6SaveCurrentSettings = "Save the current settings"
    open val m6WriteSettings = "Write settings"
    open val m6SettingsCouldNotBeApplied = "Settings could not be applied."
    open val m6DeleteUppercase = "DELETE"
    open val m6Save = "SAVE"
    open val m6ReferenceBike = "Reference bike"
    open val m6PreviousData = "Previous data"
    open val m6SavedSettings = "Saved settings"
    open val m6LatestSetting = "Latest setting"
    open val m6Delete = "Delete"
    open val m6BikeSettings = "Bike settings"
    open val m6WritingSettingIsCompleted = "The setting has been written."
    open val m6Other = "Other"
    //endregion

    //region M7 カスタマイズ詳細
    open val m7TwoStepShiftDescription =
        "Position where multi shift can be enabled"
    open val m7FirstGear = "1st gear"
    open val m7SecondGear = "2nd gear"
    open val m7OtherSwitchMessage =
        "When performing multi-shifting with a switch connected to a component other than SW-M9050, turn ON the 2nd gear."
    open val m7NameDescription = "8 characters maximum"
    open val m7WirelessCommunication = "Wireless communication"
    open val m7Bike = "Bike"
    open val m7Nickname = "Nickname"
    open val m7ChangeWirelessUnit = "Change wireless unit"
    open val m7DeleteUnit = "Delete unit"
    open val m7DeleteBikeRegistration = "Delete bike registration"
    open val m7Setting = "Setting"
    open val m7Mode = "Mode"
    open val m7AlwaysDisplay = "Always show"
    open val m7Auto = "Auto"
    open val m7Manual = "Manual"
    open val m7MaximumAssistSpeed = "Maximum assist speed"
    open val m7ShiftingAdvice = "Shifting advice"
    open val m7ShiftingTiming = "Shifting timing"
    open val m7StartMode = "Start mode"
    open val m7ConfirmConnectSprinterSwitch = "Is the sprinter switch connected?"
    open val m7Reflect = "Reflect (Enter)"
    open val m7Left = "Left"
    open val m7Right = "Right"
    open val m7SettingsCouldNotBeApplied = "Settings could not be applied."
    open val m7Preset = "PRESET"
    open val m7SaveCurrentSettings = "Save current settings"
    open val m7WriteSettings = "Write settings"
    open val m7EditSettings = "Edit settings"
    open val m7ExportSavedSettings = "Export saved settings"
    open val m7Name = "Name"
    open val m7SettingWereNotApplied = "The settings were not applied."
    open val m7DragAndDropFile = "Drag and drop the settings file here."
    open val m7CharactersLimit = "1 to 8 half-width alphanumeric characters"
    open val m7PasskeyDescriptionMessage =
        "Enter in 6 numeric characters.\n0 cannot be set as the first character in the PassKey except during PassKey initialization."
    open val m7Display = "Display"
    open val m7CompleteSettingMessage = "Setting has completed normally."

    open val m7ConfirmCancelSettingsTitle = "Pause setup"
    open val m7ConfirmDefaultTitle = "Reset the settings to default"
    open val m7PasskeyDescription = "Enter a 6-digit number starting with a number other than 0"
    open val m7AssistCharacterWithValue = "Assist character : Lv. {0}"
    open val m7MaxTorqueWithValue = "Max torque : {0} Nm"
    open val m7AssistStartWithValue = "Assist start : Lv. {0}"
    open val m7CustomizeDisplayName = "Customize display name"
    open val m7DisplayNameDescription = "Up to 10 characters"
    open val m7AssistModeDisplay = "Assist mode display"
    open val m7AssistModeType = "{0}/{1}/{2}"
    //endregion

    //region M8 カスタマイズ-スイッチ
    open val m8OptionalSwitchTitle = "Optional switch"
    open val m8SettingColudNotBeApplied = "Setting colud not be applied."
    open val m8MixedAssistShiftCheckMessage =
        "Assist up, assist down, display / lights and rear shift up, rear shift down, display, you will not be able to set D-FLY Ch. into a single switch."
    open val m8EmptyConfigurableListMessage = "There are no configurable items."
    open val m8NotApplySettingMessage = "Some buttons do not apply functions."
    open val m8PressSwitchMessage = "Press the switch on the unit you want to select."
    open val m8SetToGearShifting = "Set to gear shifting"
    open val m8UseX2Y2 = "Use 2nd gear"
    open val m8Model = "Model"
    open val m8CustomizeSusSwitchPosition = "Set to position setting"
    open val m8SetReverseOperation = "Set reverse operation to switch Y1"
    open val m8PortA = "Port A"
    open val m8PortB = "Port B"
    open val m8NoUseMultiShift = "Do not use 2-level gear shifting"
    open val m8SetReverce = "Assign opposite function to {0}"
    open val m8ConfirmReadDcasWSwitchStatusText =
        "Load the current setting. Press any button on {0} ({1})."
    open val m8ConfirmWriteDcasWSwitchStatusText = "Write settings. Press any button on {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    open val m9SynchronizedShiftTitle = "Synchronized shift"
    open val m9TeethPatternSelectorHeaderTitle = "Number of teeth"
    open val m9Cancel = "Cancel"
    open val m9Casette = "Casette"
    open val m9Edit = "Edit"
    open val m9FC = "Crank"
    open val m9CS = "Casette"
    open val m9NoSetting = "No setting"
    open val m9Table = "Table"
    open val m9Animation = "Animation"
    open val m9DisplayConditionConfirmationMessage =
        "Unable to set up synchronized shifting. Refer to the E-TUBE PROJECT website."
    open val m9NotApplicableMessage = "Check the following before applying the settings."
    open val m9NotApplicableUnselectedMessage = "S1 or S2 is not set."
    open val m9NotApplicableUnsettableValueMessage =
        "The shift point of the synchronized shift setting is in the range where it cannot be set."
    open val m9SettingFileUpperLimitMessage = "The setting file storage limit has been exceeded."
    open val m9FrontUp = "Front up"
    open val m9FrontDown = "Front down"
    open val m9RearUp = "Rear up {0} gear"
    open val m9RearDown = "Rear down {0} gear"
    open val m9Crank = "The crank"
    open val m9OK = "OK ({0} | {1})"
    open val m9SynchronizedShift = "SYNCHRONIZED SHIFT"
    open val m9SemiSynchronizedShift = "SEMI-SYNCHRONIZED SHIFT"
    open val m9Option = "Option"
    open val m9FCTeethPattern = "FC {0}"
    open val m9CSTeethPattern = "CS {0}"
    open val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "If the values set for the number of teeth and gear position control are different between S1 and S2, the settings cannot be applied."

    // TODO: TextTableには表記がないので要確認
    open val m9Name = "Name"

    //region M10 メンテナンス
    open val m10Battery = "Battery"
    open val m10DerailleurAdjustment = "Derailleur adjustment"
    open val m10Front = "Front"
    open val m10Rear = "Rear"
    open val m10NotesOnDerailleurAdjustmentMsg =
        "Derailleur adjustment involves the operation of turning the crank by hand. And the like mounting a bike maintenance stand, please create an environment. In addition, so as not to be caught hand to the gear, please be careful."
    open val m10NotAskAgain = "Do not show this next time"
    open val m10Step = "Step {0}/{1}"
    open val m10BeforePerformingElectricAdjustmentMsg =
        "Before performing electrical adjustments using the app, an adjustment must be performed using the adjustment bolt on the front derailleur. See help for details."
    open val m10MovementOfPositionOfDerailleurMsg =
        "Move the derailleur to the position shown in the figure."
    open val m10ShiftStartMesssage = "The shift is started. Please turn the crank."
    open val m10SettingChainAndFrontDerailleurMsg =
        "Adjust the gap between the chain and the front derailleur to 0 to 0.5mm."
    open val m10Adjust = "Adjust : {0}"
    open val m10Gear = "Gear position"
    open val m10ErrorLog = "Error log"
    open val m10AdjustTitle = "Adjustment"

    open val m10Restrictions = "Restrictions"
    open val m10Remedy = "Remedy"

    open val m10Status = "Status"
    open val m10DcasWBatteryLowVoltage = "The battery level is low. Replace the battery."
    open val m10DcasWBatteryUpdateInfo = "Check the battery level. Press any button on {0} ({1})."
    open val m10LeverLeft = "Left lever"
    open val m10LeverRight = "Right lever"

    // Error Message
    open val m10ErrorMessageSensorAbnormality =
        "A Sensor abnormality was detected in the drive unit."
    open val m10ErrorMessageSensorFailure = "Sensor failure was detected in the drive unit."
    open val m10ErrorMessageMotorFailure = "Motor failure detected in the drive unit."
    open val m10ErrorMessageAbnormality = "An abnormality was detected in the drive unit."
    open val m10ErrorMessageNotDetectedSpeedSignal =
        "No vehicle speed signal was detected from the speed sensor."
    open val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "An abnormal vehicle speed signal was detected from the speed sensor."
    open val m10ErrorMessageBadCommunicationBTAndDU =
        "Communication error between the battery and drive unit detected."
    open val m10ErrorMessageDifferentShiftingUnit =
        "Shifting unit different from system configuration is installed."
    open val m10ErrorMessageDUFirmwareAbnormality = "Abnormality detected in drive unit’s firmware."
    open val m10ErrorMessageVehicleSettingsAbnormality =
        "An abnormality was detected in the vehicle settings."
    open val m10ErrorMessageSystemConfiguration = "Error caused by system configuration."
    open val m10ErrorMessageAbnormalHighTemperature =
        "High temperature abnormality detected in the drive unit."
    open val m10ErrorMessageNotCompletedSensorInitialization =
        "Sensor initialization could not be completed normally."
    open val m10ErrorMessageUnexpectBTDisconnected = "Unexpected power disconnection detected."
    open val m10ErrorMessagePowerOffDueToOverTemperature =
        "The power was turned OFF due to the temperature exceeding the guaranteed operating range."
    open val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "The power was turned OFF due to current leakage being detected in the system."

    // Error Restrictions
    open val m10ErrorRestrictionsUnableUseAssistFunction = "Unable to use the assist function."
    open val m10ErrorRestrictionsNotStartAllFunction = "None of the system functions start up."
    open val m10ErrorRestrictionLowerAssistPower = "Assist power becomes lower than usual."
    open val m10ErrorRestrictionLowerMaxAssistSpeed =
        "Maximum assist speed becomes lower than usual."
    open val m10ErrorRestrictionNoRestrictedAssistFunction =
        "There are no restricted assist functions while displayed."
    open val m10ErrorRestrictionUnableToShiftGears = "Unable to shift gears."

    // Error Remedy
    open val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyTurnPowerOffAndThenOn =
        "Turn the power OFF and then ON again. If the error persists, stop using it and contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Have your place of purchase perform the following. \n• Install the speed sensor and magnet in appropriate locations. \n• If the bicycle has been modified, restore it to the factory settings. \nFollow the instructions above and ride the bicycle for a short while to clear the error. If the error persists, or if the information above does not apply, contact your distributor for assistance."
    open val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Have your place of purchase perform the following. \n･ Check to see if the cable between the drive unit and battery is connected correctly."
    open val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Have your place of purchase perform the following. \n• Connect to E-TUBE PROJECT and check the settings. If the settings and vehicle status differ, revise the vehicle status. \nIf the error persists, contact your distributor for assistance."
    open val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Have your place of purchase perform the following. \n• Connect to E-TUBE PROJECT and restore the firmware. \nIf the error persists, contact your distributor for assistance."
    open val m10ErrorRemedyContactBicycleManufacturer = "Contact the bicycle manufacturer."
    open val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "Do not ride the bicycle with the assist mode enabled until the drive unit temperature drops. \nIf the error persists, contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Have your place of purchase perform the following. \n• Install the speed sensor in an appropriate location. \n• Install the magnet in an appropriate location. (Refer to the \"Disc brake\" section in \"General operations\" or to the STEPS series dealer's manual for how to install the removed magnet.) \nIf the error persists, or if the information above does not apply, contact your distributor for assistance."
    open val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Press the battery power button to turn the power OFF and then ON again without placing your feet on the pedals.\nIf the error persists, contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyTorqueSensorOffset =
        "• When the drive unit is the DU-EP800 (the cycle computer displays W103): Turn the cranks in reverse two to three times. \n• When the drive unit is a different model (the cycle computer displays W013): Press the battery power button to turn the power OFF and then ON again without placing your feet on the pedals. \nIf the error persists after performing the above, contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyBTConnecting =
        "Turn the power OFF and then ON again. \nIf W105 is displayed frequently, have your place of purchase perform the following. \n• Eliminate any rattling in the battery mount and ensure that it is secured appropriately in place. \n• Check whether the power cord is damaged. If so, replace it with the required part. \nIf the error persists, contact your distributor for assistance."
    open val m10ErrorRemedyOverTemperature =
        "If it exceeds the temperature where discharge is possible, leave the battery in a cool place away from direct sunlight until the internal temperature of the battery decreases sufficiently. If it is below the temperature where discharge is possible, leave the battery indoors, etc. until its internal temperature is at a suitable temperature. \nIf the error persists, contact your place of purchase or a distributor for assistance."
    open val m10ErrorRemedyReplaceShiftingUnit =
        "Have your place of purchase perform the following. \n• Check the current system status in E-TUBE PROJECT and replace with the appropriate shifting unit.\nIf the error persists, contact your distributor for assistance."
    open val m10ErrorRemedyCurrentLeakage =
        "Request the place of purchase to perform the following.\nRemove the components other than the drive unit one at a time and turn the power ON again.\n• The power is turned OFF during startup if the component causing the problem was not removed.\n• The power is turned ON and W104 continues to be displayed if the component causing the problem was removed.\n• Once the component causing the problem has been identified, replace that component."
    open val m10ErrorTorqueSensorOffsetAdjust =
        "Press the battery power button to turn the power OFF and then ON again without placing your feet on the pedals. If the warning persists after the first attempt, repeat the same operation until it disappears. \n*The sensor also initializes when the bicycle is moving. If you keep riding the bicycle the situation may improve. \n\nIf the situation does not improve, request the following at your place of purchase or a distributor. \n• Adjust the chain tension. \n• With the rear wheel lifted off the ground, turn the pedals to freely spin the rear wheel. Change the stopped position of the pedals while the wheel is spinning until the warning disappears."

    // TODO: TextTableには表記がないので要確認
    open val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    open val m11Name = "Name"
    open val m11Change = "Change"
    open val m11Power = "POWER"
    open val m11Cadence = "CADENCE"
    open val m11Watt = "watt"
    open val m11Rpm = "rpm"
    open val m11Waiting = "Waiting.."
    open val m11CaribrationIsCompleted = "Calibration is completed."
    open val m11CaribrationIsFailed = "Calibration is failed."
    open val m11N = "N"
    open val m11PowerMeter = "Powermeter"
    open val m11ZeroOffsetCaribration = "Zero offset calibration"
    open val m11Set = "Set"
    open val m11ChangeBike = "Change bike"
    open val m11CancelPairing = "Cancel pairing"
    open val m11RegisteredBike = "Registered bicycle"
    open val m11RedLighting = "Power meter battery level: \n1 - 5%"
    open val m11RedBlinking = "Power meter battery level: \nLess than 1％"

    // TODO: このregion内の以下の文言はTextTableには表記がないので要確認
    open val m11NoPhoto = "No photo"
    //endregion

    //region M12 アプリケーション設定
    open val m12Acconut = "Account"
    open val m12ShimanoIDPortal = "SHIMANO ID PORTAL"
    open val m12Setting = "Setting"
    open val m12ApplicationSettings = "Application settings"
    open val m12AutoBikeConnection = "Auto bike connection"
    open val m12Language = "Language"
    open val m12Link = "Link"
    open val m12ETubeProjectWebsite = "E-TUBE PROJECT website"
    open val m12ETubeRide = "E-TUBE RIDE"
    open val m12Other = "Other"
    open val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    open val m12PoweredByShimano = "Powered by SHIMANO"
    open val m12SignUpLogIn = "Sign up/Login"
    open val m12CorporateClient = "Corporate client"
    open val m12English = "English"
    open val m12French = "French"
    open val m12German = "German"
    open val m12Dutch = "Dutch"
    open val m12Spanish = "Spanish"
    open val m12Italian = "Italian"
    open val m12Chinese = "Chinese"
    open val m12Japanese = "Japanese"
    open val m12Change = "Change"
    open val m12AppOperationLogAgreement = "App operation log agreement"
    open val m12AgreeAppOperationLogAgreemenMsg = "I agree to the app operation log ageement."
    open val m12DataProtecitonNotice = "Data proteciton notice"
    open val m12Apache = "Apache"

    // TODO: 仮
    open val m12AppOperationLog = "アプリ操作ログ取得"

    open val m12ConfirmLogout =
        "Do you want to log out? Also, any units currently connected will be disconnected."
    //endregion

    //region M13 ヘルプ
    open val m13Title = "Help"
    open val m13Connection = "Connection"
    open val m13Operation = "Operation"
    open val m13HowToConnectBike = "How to connect bike"
    open val m13SectionCustomize = "Customize"
    open val m13SynchronizedShiftTitle = "Synchronized shift"
    open val m13FrontDerailleurAdjustment = "Front derailleur adjustment"
    open val m13RearDerailleurAdjustment = "Rear derailleur adjustment"
    open val m13ZeroOffsetCalibration = "Zero offset calibration"
    open val m13SystemInformationDisplayTitle = "For information display"
    open val m13JunctionATitle = "For junction A"
    open val m13SCE8000Title = "For SC-E8000"
    open val m13CycleComputerTitle = "For cycle computer/EW-EN100\n(except for SC-E8000)"
    open val m13PowerMeterTitle = "For FC-R9100-P"
    open val m13SystemInformationDisplayDescription =
        "Press and hold the mode switch on the bicycle until \"C\" is displayed on the display."
    open val m13ModeSwitchAnnotationRetention =
        "* Once the bicycle is ready to be connected, release the mode switch or button. If you continue holding the mode switch or button, the system enters a different mode."
    open val m13ShiftMode = "Shift mode"
    open val m13LongPress = "Press and hold"
    open val m13Seconds = "{0} sec. or more"
    open val m13ConnectionMode = "Bluetooth LE connection mode"
    open val m13AdjustMode = "Adjustment mode"
    open val m13RDProtectionResetMode = "RD protection reset mode"
    open val m13ExitModeDescription =
        "Press and hold for 0.5 sec. or more to exit the adjustment mode or RD protection reset mode."
    open val m13JunctionADescription =
        "Press and hold the junction A button until the green LED and red LED start lighting up alternately."
    open val m13HowToConnectWirelessUnit =
        "When you press and hold A while the bicycle is stopped, the Menu List screen is displayed on the cycle computer. Press X1 or Y1 to select Bluetooth LE, then press A to confirm. \nOn the Bluetooth LE screen, select Start, then press A to confirm."
    open val m13CommunicationConditions =
        "Communications are allowed under the following conditions only. \nPlease perform one of the operations."
    open val m13TurningOnStepsMainPower =
        "・ For 15 seconds after the SHIMANO STEPS main power supply is turned ON."
    open val m13AnyButtonOperation =
        "・ For 15 seconds after one of the buttons (except for the SHIMANO STEPS main power supply button) is operated."
    open val m13PowerMeterDescription = "Press the button on the control unit."
    open val m13SynchronizedShift = "Synchronized shift"
    open val m13SynchronizedShiftDetail =
        "This function automatically shifts the front derailleur in synchronization with rear shift up and rear shift down. "
    open val m13SemiSynchronizedShift = "Semi-synchronized shift"
    open val m13SemiSynchronizedShiftDetail =
        "This function automatically shifts the rear derailleur when the front derailleur is shifted in order to obtain optimal gear transition."
    open val m13Characteristics = "Characteristics"
    open val m13CharacteristicsFeature1 =
        "Fast multi shifting is now enabled.\n\n•This feature lets you quickly adjust crank RPM in response to changes in riding conditions.\n•It allows you quickly adjust your speed."
    open val m13CharacteristicsFeature2 = "Enables reliable multi shift operation"
    open val m13Note = "Note"
    open val m13OperatingPrecautionsUse1 =
        "1. It is easy to overshift.\n\n2. If the crank RPM is low, the chain will not be able to keep up with the motion of the rear derailleur.\nAs a result, the chain may fly off the gear teeth instead of engaging the cassette sprocket."
    open val m13OperatingPrecautionsUse2 = "Multi shift operation takes time"
    open val m13Clank = "Crank arm speed to be used"
    open val m13ClankFeature = "At high crank RPM"
    open val m13AutoShiftOverview =
        "SHIMANO STEPS with a DI2 internal geared hub can automatically shift the shifting unit according to the traveling conditions."
    open val m13ShiftTiming = "Shift timing:"
    open val m13ShiftTimingOverview =
        "Changes the cadence standard for automatic gear shifting. Increase the set value for fast pedaling with a light load. Decrease the set value for slow pedaling with a moderate load."
    open val m13StartMode = "Start mode: "
    open val m13StartModeOverview =
        "This function automatically shifts down the gear after stopping at a traffic light, etc., so that you can start from a preset lower gear. This function can be used for automatic or manual gear shifting. "
    open val m13AssistOverview = "There are 2 assist patterns to choose from."
    open val m13Comfort = "COMFORT :"
    open val m13ComfortOverview =
        "Provides a smoother ride and more normal bicycle-like feeling with the maximum torque of 50 Nm."
    open val m13Sportive = "SPORTIVE :"
    open val m13SportiveOverview =
        "Provides assist power that lets you easily climb steep hills with the maximum torque of 60 Nm. (Depending on the internal shifting unit model, the maximum torque may be controlled to 50 Nm.)"
    open val m13RidingCharacteristicOverview = "There are 3 riding characteristics to choose from."
    open val m13Dynamic = "(1) DYNAMIC :"
    open val m13DynamicOverview =
        "There are 3 assist mode levels (ECO/TRAIL/BOOST) that can be changed with a switch. DYNAMIC is a setting where the difference is greater between these 3 assist mode levels. It offers you support when riding on an E-MTB with \"ECO\" that provides more assist power than ECO mode in the EXPLORE setting, \"TRAIL\" for superior control, and \"BOOST\" for powerful acceleration."
    open val m13Explorer = "(2) EXPLORER :"
    open val m13ExplorerOverview =
        "EXPLORER provides both assist power controllability and low battery consumption for the 3 assist mode levels. It is suitable for single track riding."
    open val m13Customize = "(3) CUSTOMIZE :"
    open val m13CustomizeOverview =
        "The desired assist level can be chosen from LOW/MEDIUM/HIGH for each of the 3 assist mode levels."
    open val m13AssistProfileOverview =
        "You can create 2 types of assist profiles to choose from. The profiles can also be switched with SC. A profile adjusts 3 parameters for each of the 3 assist mode levels (ECO/TRAIL/BOOST) that can be changed with a switch."
    open val m13AssistCharacter = "(1) Assist character :"
    open val m13AssistCharacterOverview =
        "With SHIMANO STEPS, the assist torque is applied according to the pedal pressure. When the setting is moved toward POWERFUL, assistance is provided even with low pedal pressure. When the setting is moved toward ECO, the balance between the assist level and low battery consumption can be optimized."
    open val m13MaxTorque = "(2) Max torque :"
    open val m13MaxTorqueOverview =
        "The maximum assist torque output by the drive unit can be changed."
    open val m13AssistStart = "(3) Assist start :"
    open val m13AssistStartOverview =
        "The timing when assistance is provided can be changed. When the setting is toward QUICK, assistance is provided quickly after the crank starts rotating. When the setting is toward MILD, assistance is provided slowly."
    open val m13DisplaySpeedAdjustment = "Display speed adjustment"
    open val m13DisplaySpeedAdjustmentOverview =
        "When there is a difference between the speed displayed on the cycle computer and the speed displayed on your device, adjust the displayed speed.\nWhen you increase the value by pressing Assist-X, the displayed speed value increases.\nWhen you decrease the value by pressing Assist-Y, the displayed speed value decreases."
    open val m13SettingIsNotAvailableMessage =
        "* This setting is not available for some drive unit models."
    open val m13TopSideOfTheAdjustmentMethod = "Performing a top adjustment"
    open val m13SmallestSprocket = "Smallest sprocket"
    open val m13LargestGear = "Largest gear"
    open val m13Chain = "Chain"
    open val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nSet the chain onto the largest chainring and the smallest sprocket."
    open val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nLoosen the stroke fixing bolt with a 2 mm Allen key.\n (A) Stroke adjustment bolt\n (B) Top adjustment bolt"
    open val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nTurn the top adjustment bolt with a 2 mm Allen key to adjust the clearance. Adjust the gap between the chain and the plate inside the chain guide to 0 to 0.5mm."
    open val m13AfterAdujustmentMessge =
        "4.\nAfter adjustment, securely tighten the stroke fixing bolt."
    open val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nMove the chain to the 5th sprocket. Move the guide pulley toward the inside until the chain touches the 4th sprocket and makes a slight noise."
    open val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nTurn the top-side adjustment bolt in a 2mm hex wrench. The gap between the chain and the chain guide outer plate, and then adjusted to 0.5 ~ 1mm."
    open val m13TopSideOfAdjustmentMethod =
        "Before performing electrical adjustments using the app, an adjustment must be performed using the adjustment bolt on the front derailleur. Follow the steps below to perform the adjustment."
    open val m13BoltLocationCheck = "Bolt location check"
    open val m13BoltLocationCheckDescription =
        "The low adjustment bolt, top adjustment bolt, and support bolt are located close to each other. \nMake sure that you are adjusting the correct bolt."
    open val m13Bolts = "(A) Low adjustment bolt\n(B) Top adjustment bolt\n(C) Support bolt"
    open val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nMove the guide pulley toward the outside by 4 steps (5 steps for MTB) to the target position."
    open val m13PerformingZeroOffsetTitle = "Performing zero offset calibration"
    open val m13AdjustmentMethod = "Adjustment method"
    open val m13PerformingZeroOffsetMsg =
        "1.\nPlace the bicycle on level ground.\n\n2.\nPosition the crank arm so that it is perpendicular to the ground as in the illustration.\n\n3.\nPress the \"Zero offset calibration\" button.\n\nDo not place your feet on the pedals or apply load to the crank."
    open val m13ShiftingAdvice = "Shifting advice:"
    open val m13ShiftingAdviceOverview =
        "This function notifies the appropriate shift timing through the cycle computer in manual shifting. The timing when the notification is displayed varies depending on the set value for the shift timing."
    open val m13PerformingLowAdjustment = "How to perform a low adjustment (FD-6870/FD-9070 only)"
    open val m13PerformingLowAdjustmentDescription1 =
        "1.\nSet the chain onto the smallest chainring and the largest sprocket."
    open val m13PerformingLowAdjustmentDescription2 =
        "2.\nRotate the low adjustment bolt with a 2mm hexagon wrench. Adjust the gap between the chain and the chain guide outer plate to 0 to 0.5mm."
    open val m13FrontDerailleurMethod2 =
        "2.\nLoosen the stroke fixing bolt with a 2 mm hexagon wrench.\n(A) Stroke adjustment bolt\n(B) Top adjustment bolt"
    open val m13FrontDerailleurMethod3 =
        "3.\nTurn the top adjustment bolt with a 2 mm hexagon wrench to adjust the clearance. Adjust the gap between the chain and the chain guide inner plate to 0 to 0.5mm."
    open val m13VerySlow = "Very slow"
    open val m13Slow = "Slow"
    open val m13Normal = "Normal"
    open val m13Fast = "Fast"
    open val m13VeryFast = "Very fast"
    open val m13MtbTopSideOfAdjustmentMethod1 =
        "1.\nSet the chain onto the largest chainring and the largest sprocket."
    //endregion

    open val m13Maintenance = "Maintenance"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    open val dialog17SwipeToSwitchTitle = "Swipe to switch"
    open val dialog28BluetoothOffTitle = "Bluetooth OFF"
    open val dialog29LocationInformationOffTitle = "Location information　OFF"
    open val dialog2_3AddBikeTitle = "Add bicycle"
    open val dialog25PasskeyErrorTitle = "Passkey error"
    open val dialog27RegistersBikeTitle = "Registered unit"
    open val dialog2_8RegisterBikeNameTitle = "Register bicycle name"
    open val dialog2_9PasskeyTitle = "Passkey"
    open val dialog2_11ConfirmBikeTitle = "Check bicycle"
    open val dialog69SwitchBikeConnectionTitle = "Disconnect current bicycle"
    open val dialog3_4UnitNotDetectdTitle = "Unit is not detected"
    open val dialog3_5UnitRegisteredAgainTitle = "Re-register unit"
    open val dialog4_1UpdateDetailTitle = "{0} ver.{1}"
    open val dialog4_2AllUpdateConfirmTitle = "Do you want to\nupdate all the units?"
    open val dialog4_3UpdateCompletedTitle = "{0}"
    open val dialog4_4CancelUpdateTitle = "Cancel update"
    open val dialog6_1DeleteBikeTitle = "Delete bicycle"
    open val dialog6_2ConfirmTitle = "Confirm"
    open val dialog6_3DeleteTitle = "Delete"
    open val dialog7_1ConfirmCancelSettingTitle = "Pause setup"
    open val dialog7_2ConfirmDefaultTitle = "Reset the settings to default"
    open val dialog10_2PauseSetupTitle = "Pause setup"
    open val dialog_jira258PushedIntoThe2ndGearTitle =
        "Operation performed when the switch is pushed into the 2nd gear"
    //endregion

    //region 文章
    open val dialog01ConfirmExecuteZeroOffsetMessage = "Do you want to execute zero offset?"
    open val dialog02ConnectedMessage = "Connected."
    open val dialog02CaribrationIsCompleted = "Caribration is completed."
    open val dialog02CaribrationIsFailed = "Caribration is failed."
    open val dialog03FailedToExecuteZeroOffsetMessage = "Failed to execute zero offset."
    open val dialog03DoesNotrespondProperlyMessage = "The power meter does not respond properly."
    open val dialog04UpdatingFirmwareMessage = "Updating the firmware..."
    open val dialog05CompleteSetupMessage = "Setup has been completed."
    open val dialog06ConfirmStartUpdateMessage =
        "Before starting the update, be sure to read the \"NOTICE\" below. Do not perform any other operations during the update."
    open val dialog06AllUpdateConfirmMessage = "Approximate time required: {0}"
    open val dialog07CancelUpdateMessage =
        "Do you want to cancel the firmware update?\n* If you choose \"Cancel\", updates for the next unit onward are cancelled. The update of the unit currently in progress continues."
    open val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Review the following changes to {0} that will result from the firmware restore operation."
    open val dialog13Changes = "Changes"
    open val dialog13MustAgreeToTermsOfUseMessage =
        "You must agree to the software's terms of use in order to restore the firmware."
    open val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Additional Software License Agreement\n (Be sure to read)"
    open val dialog15FirmwareWasUpdateMessage =
        "The firmware of {0} functions normally.\nThe firmware was updated to the latest version."
    open val dialog15FirmwareRestoreMessage = "The firmware of {0} has been restored."
    open val dialog15CanChangeTheSwitchSettingMessage =
        "When using {0}, you can change the Switch setting shown in SC from suspension type in the Customize menu."
    open val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "The firmware of {0} functions normally.\nIt does not need to be restored."
    open val dialog15FirmwareRestorationFailureMessage = "Firmware restoration of {0} failed."
    open val dialog15MayBeFaultyMessage = "{0} may be faulty."
    open val dialog15FirmwareRecoveryAgainMessage = "Firmware recovery for {0} is performed again."
    open val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "The firmware file of {0} is void.\nDownload the latest version of the firmware?"
    open val dialog15FirstUpdateFirmwareMessage = "First, update the firmware for {0}."
    open val diaog15CannotUpdateMessage =
        "Unable to update due to poor signal reception. Try again at a location with better signal reception."
    open val dialog15ConfirmStartUpdateMessage =
        "It will take several minutes to update the firmware. \nIf the battery of your device is low, perform the update after charging it or connect it to a charger. \nStart update?"
    open val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Failed to connect after firmware update.\nPlease connect again."
    open val dialog15ConfirmConnectedUnitMessage = "{0} is recognized.\nIs the connected unit {0}?"
    open val dialog17RegistrationIsCompletedMessage = "Registration is completed."
    open val dialog17ConnectionIsCompletedMessage = "Connection is completed."
    open val dialog19TryAgainWirelessCommunicationMessage =
        "Wireless environment is not stable. Try again at a location where communication is stable."
    open val dialog24PasskeyMessage = "Enter your passkey."
    open val dialog25PasskeyErrorMessage =
        "Unable to authenticate. The PassKey was entered incorrectly, or the PassKey for the wireless unit has been changed. Reconnect, and then try entering the PassKey set in the wireless unit again."
    open val dialog26ChangePasskeyMessage = "Do you want to change your passkey?"
    open val dialog27RegisterdBikeMessage =
        "{0} is already registered as a wireless unit for {1}. Do you want to use it as a wireless unit for the new bike?* {1} will be unregistered."
    open val dialog28BluetoothOffMesssage =
        "Turn ON the Bluetooth setting on the device to connect."
    open val dialog29LocationInformationOffMessage =
        "Enable the location information service to connect."
    open val dialog30DeleteBikeMessage = "When a bike is deleted, its data is also deleted."
    open val dialog31ConfirmDeleteBikeMessage = "Are you sure you want to delete the bike?"
    open val dialog32UpdatedProperlyMessage = "Settings have been updated properly."
    open val dialog32UpdateCompletedMessage = "Update is completed."
    open val dialog36PressSwitchMessage = "Press the switch on the unit you want to select."
    open val dialog37SelectTheDerectionMessage = "Which grip is the control switch installed on?"
    open val dialog38SameMarksCannotBeAssignedMessage =
        "- If the combination pattern differs between the front and rear suspensions, the same mark (CTD) cannot be assigned."
    open val dialog38DifferentMarksCannotBeAssignedMessage =
        "- If the same combination pattern is used for the front and rear suspensions, different marks (CTD) cannot be assigned."
    open val dialog39CheckTheFollowingInformationMessage = "Please check the following information."
    open val dialog39ConfirmContinueWithProgrammingMessage = "OK to continue with programming?"
    open val dialog39SameSettingMessage =
        "- The same setting has been chosen for two or more positions."
    open val dialog39OnlyHasBeenSetMessage = "- Only {1} has been set for {0}."
    open val dialog40CannotSelectSwitchMessage =
        "If you cannot select using any of the switches, check whether any electric wires are disconnected. \nIf so, reconnect them. \nIf not, a switch may be malfunctioning."
    open val dialog41ConfirmDefaultMessage = "Do you want to reset all the settings to default?"
    open val dialog42ConfirmDeleteSettingMessage = "Are you sure you want to delete the settings?"
    open val dialog43ComfirmUnpairPowerMeterMessage = "Do you want to unpair the power meter?"
    open val dialog44ConfirmCancelSettingMessage =
        "Are you sure you want to discard the information you have entered?"
    open val dialog45DisplayConditionConfirmationMessage =
        "Unable to set up synchronized shifting. Refer to the E-TUBE PROJECT website."
    open val dialog46DisplayConditionConfirmationMessage =
        "Unable to set up multi shift mode. Refer to the E-TUBE PROJECT website."
    open val dialog47DuplicateSwitchesMessage =
        "Multiple identical switches are connected. Unable to save the settings."
    open val dialog48FailedToUpdateSettingsMessage = "Failed to update the settings."
    open val dialog48FailedToConnectBicycleMessage = "Failed to connect to bicycle."
    open val dialog49And50CannotOperateOormallyMessage =
        "The shifting point is within the unsettable range and cannot operate normally."
    open val dialog51ConfirmContinueSettingMessage =
        "The following functions are not included. Are you sure you want to continue?\n{0}"
    open val dialog55NoInformationMessage = "No information found on this bike."
    open val dialog56UnpairConfirmationDialogMessage =
        "Are you sure you want to delete the power meter?"
    open val dialog57DisconnectBluetoothDialogMessage =
        "The power meter has been deleted. The wireless unit needs to be deleted also in the Bluetooth setting for OS."
    open val dialog58InsufficientStorageAvailableMessage =
        "Insufficient storage available. Preset file could not be saved."
    open val dialog59ConfirmLogoutMessage = "Do you want to log out?"
    open val dialog59LoggedOutMessage = "You have logged out."
    open val dialog60BeforeApplyingSettingMessage =
        "Check the following before applying the settings."
    open val dialog60S1OrS2IsNotSetMessage = "S1 or S2 is not set."
    open val dialog60AlreadyAppliedSettingMessage = "This setting has been already applied."
    open val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "If the values set for the number of teeth and gear position control are different between S1 and S2, the settings cannot be applied."
    open val dialog60NotApplicableDifferentSettingsMessage =
        "If the values set for the shift interval are different between S1 and S2, the settings cannot be applied."
    open val dialog60ShiftPointCannotAppliedMessage =
        "The shift point of the synchronized shift setting is in the range where it cannot be set."
    open val dialog61CreateNewSettingMessage =
        "To create a new setting, delete one of the existing settings."
    open val dialog62AdjustDerailleurMessage =
        "To adjust the derailleur, the crank needs to be rotated manually. Prepare by placing the bike on the maintenance stand or other device. Also, be careful not to get your hand caught in the gears."
    open val dialog63ConfirmCancellAdjustmentMessage =
        "Are you sure you want to quit?"
    open val dialog64RecoveryFirmwareMessage =
        "The firmware on {0} may not be working correctly. Firmware recovery is performed. Approximate time required: {1}"
    open val dialog64ConfirmRestoreTheFirmwareMessage =
        "A faulty wireless unit was found.\n Restore the firmware?"
    open val dialog64UpdateFirmwareMessage =
        "The wireless unit may be faulty.\nFirmware will be updated."
    open val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "Application or firmware may be outdated. Connect to the Internet and check for the latest version."
    open val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "Do you want to connect to the previously connected unit?"
    open val dialog67BluetoothOffMessage =
        "Bluetooth on the device is not enabled. Turn ON the Bluetooth setting on the device to connect."
    open val dialog68LocationInformationOffMessage =
        "Location information of the device is not available. Enable the location information service to connect."
    open val dialog69SwitchBikeConnectionMessage =
        "Are you sure you want to disconnect the bike currently connected?"
    open val dialog71LanguageChangeCompleteMessage =
        "Language setting has been changed. \nThe language will change once you exit and relaunch the application."
    open val dialog72ConfirmBikeMessage =
        "{0} units are used. Do you want to register as an existing bike? Do you want to register as a new bike?"
    open val dialog73CannnotConnectToNetworkMessage = "Cannot connect to network."
    open val dialog74AccountIsLockedMessage = "This account is locked.Try logging in again later."
    open val dialog75IncorrectIdOrPasswordMessage = "The user ID or password is incorrect."
    open val dialog76PasswordHasExpiredMessage =
        "Your temporary password has expired.\nRe-start the registration process."
    open val dialog77UnexpectedErrorMessage = "An unexpected error has occurred."
    open val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} for assist mode switching is connected. \nThe current bike type supports only setting for gear shifting. \nSet {0} for gear shifting?"
    open val dialog78ShiftModeSwitchingIsConnectedMessage =
        "{0} for gear switching is connected.\nThe current bicycle type only supports the setting for assist mode switching.\nDo you want to set {0} for assist mode switching?"
    open val dialog79WirelessConnectionIsPoorMessage =
        "Wireless connection is poor. \nThe Bluetooth® LE connection may be interrupted."
    open val dialog79ConfirmAjustedChainTensionMessage = "Have you adjusted chain tension?"
    open val dialog79ConfirmAjustedChainTensionDetailMessage =
        "When using an internal geared hub, it is necessary to adjust the chain tension.\nAdjust the chain tension, and then press the Run button."
    open val dialog79ConfirmCrankAngleMessage = "Have you checked the crank angle?"
    open val dialog79ConfirmCrankAngleDetailMessage =
        "The left crank must be installed to the axle at the correct angle. Check the angle of the installed crank, and then press the Run button."
    open val dialog79RecomendUpdateOSVersionMessage =
        "The following unit does not support your smartphone or tablet's operating system.\n{0}\nIt is recommended to update your smartphone or tablet's operating system to the latest version for use with the application."
    open val dialog79CannotUseUnitMessage = "The following unit cannot be used."
    open val dialog79RemoveUnitMessage = "Remove the unit."
    open val dialog79UpdateApplicationMessage =
        "Update the application to the latest version and try again."
    open val dialog79ConnectToTheInternetMessage =
        "Unable to verify a connection with the server. \nConnect to the Internet and try again."
    open val dialog79NewFirmwareVersionWasFoundMessage =
        "A new firmware version was found. \nDownloading new version…"
    open val dialog79DownloadedFileIsCorrupMessage =
        "The downloaded file is corrupt.\nDownload the file again.\nIf the download fails repeatedly, there may be a problem with the Internet connection or the Shimano's web server.\nWait a little while and try again."
    open val dialog79FileDownloadFailedMessage = "Failed to download file."
    open val dialog79ConfirmConnectInternetMessage =
        "The file will be updated. \nIs your device connected to the Internet?"
    open val dialog79UpdateFailedMessage = "Update failed."
    open val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nTake your hand off the switch.\nAn error may have occurred with the switch if the dialog box does not close even after leaving it. In that case, connect the switch by itself and perform an error check."
    open val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Connect to the Internet and check whether there are any updated E-TUBE PROJECT or product versions available. \nUpdating to the latest version will allow you to use new products and features."
    open val dialog82FailedToRegisterTheImageMessage = "Failed to register the image."
    open val dialog83NotConnectedToTheInternetMessage =
        "You are not connected to the Internet. Connect to the Internet and try again."
    open val dialog84RegisterTheBicycleAgainMessage =
        "No applicable bicycle found. Connect to the Internet and register the bicycle again."
    open val dialog85RegisterTheImageAgainMessage =
        "An unexpected error has occurred. Register the image again."
    open val dialog87ProgrammingErrorMessage =
        "An error occurred during programming."
    open val dialog88MoveNextStepMessage =
        "The derailleur is already in the specified gear position. Proceed to the next step."
    open val dialog89ConfirmWhenGoingToETubeRide =
        "Start E-TUBE RIDE. Any units currently connected will be disconnected. Are you sure you want to proceed?"
    open val dialog92Reading = "Now reading."
    open val dialog103BleAutoConnectCancelMessage = "Do you want to cancel connecting?"
    open val dialog107_PairingCompleteMessage =
        "ID:{0}({1})\n\nTo complete pairing, press any button on the switch whose ID was entered."
    open val dialog108_DuplicateMessage =
        "ID:{0}({1})\n\n{2} is already paired. Do you want to unpair the previously paired {2} and proceed to the next step?"
    open val dialog108_LeftLever = "Left lever"
    open val dialog108_RightLever = "Right lever"
    open val dialog109_UnrecognizableMessage =
        "ID:{0}\n\nUnrecognized ID. Check if the ID you entered is correct."
    open val dialog110_WriteFailureMessage = "ID:{0}({1})\n\nFailed to write the ID."
    open val dialog112_PairingDeleteMessage = "Do you want to unpair the switch?"
    open val dialog113GetSwitchFWVersionMessage =
        "Check the firmware version of the switch. Press any button on {0} ({1})."
    open val dialog116HowToUpdateWirelessSwitchMessage = "See \"Details\" for the update procedure."
    open val dialog122ConfirmFirmwareLatestUpdateMessage =
        "Do you want to check for the latest version of the firmware file?"
    open val dialog123AllFirmwareIsUpToDateMessage =
        "The firmware is up to date on all the units on the connected bicycle."
    open val dialog123FoundNewFirmwareVersionMessage =
        "A new firmware version for the unit on the connected bicycle was found."
    open val dialog3_5UnitRegisteredAgainMessage =
        "Bike information is not synchronized. The unit needs to be registered again."
    open val dialog4_1UpdatesMessage = "Updates"
    open val dialog6_3DeleteMessage =
        "The bike has been deleted. The wireless unit needs to be deleted also in the Bluetooth setting for OS."
    open val dialog8_2ConfirmButtonFunction = "Some buttons do not have any functions assigned."
    open val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "Do you want to enable 2-level gear shifting?"
    open val dialog_jira258ConfirmSameOperationMessage =
        "Do you want to specify to perform the same operation as the operation performed when the switch is pressed twice?"
    open val dialog2_2NeedFirmwareUpdateMessage =
        "To use E-TUBE PROJECT, the firmware on {0} needs to be updated. Approximate time required: {1}"
    open val dialog3_1CompleteFirmwareUpdateMessage = "The firmware has been updated."
    open val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "Do you want to cancel the firmware update? \n*Updates for the next unit onward are cancelled. \nThe update of the unit currently in progress continues."
    open val dialog3_3ConfirmRewriteFirmwareMessage =
        "The firmware on {0} is the latest version. Do you want to rewrite?"
    open val dialog3_4UpdateForNewFirmwareMessage =
        "New firmware for {0} exists, but cannot be updated. The Bluetooth® LE version is not supported. Update with the mobile version."
    open val dialog4_1ConfirmDisconnectMessage =
        "Do you want to disconnect?\nCustomized settings exist. If disconnected, the settings will not be applied."
    open val dialog4_2ConfirmResetAllChangedSettingMessage =
        "Do you want to reset all the changed settings?"
    open val dialog4_3ConfirmDefaultMessage =
        "Do you want to reset the selected settings to default?"
    open val dialog12_2CanNotErrorCheckMessage = "Error check cannot be performed with SM-BCR2."
    open val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "Do you want to stop charging and disconnect?"
    open val dialog13_1ConfirmDiscaresAdjustmentsMessage =
        "Are you sure you want to discard the adjustments you have made?"
    open val dialog_jira258BrightnessSettingMessage =
        "The display brightness setting will take effect after disconnection."
    open val dialogUsingSM_Pce02Message =
        "Using SM-PCE02 makes E-TUBE PROJECT more useful.\n\n・ Battery consumption check that can check for leakage current in any part of the connected unit.\n・Improved communication stability\n・Faster update speed "
    open val dialogLocationServiceIsNotPermittedMessage =
        "If access to Location service is not permitted, a Bluetooth® LE connection cannot be established due to the usage restrictions of Android OS. Turn on Location service for this application, then restart the application."
    open val dialogStorageServiceIsNotPermittedMessage =
        "If access to storage is not permitted, images cannot be registered due to the usage restrictions of Android OS. Turn on storage for this application, then restart the application."
    open val dialog114AcquisitionFailure = "Failed to retrieve."
    open val dialog114WriteFailure = "Failed to write."
    open val dialog129_DuplicateMessage = "{0}\n\nThis switch is already paired."
    open val dialogUnsupportedFetchErrorLogMessage =
        "Error logs are not viewable with the firmware version of your unit."
    open val dialogAcquisitionFailedMessage = "Failed to retrieve."
    //endregion

    //region 選択肢
    open val dialog06TopButtonTitle = "NOTICE"
    open val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Agree and update"
    open val dialog20And21Yes = "YES"
    open val dialog20And21No = "NO"
    open val dialog22OK = "OK"
    open val dialog28BluetoothOffOption1 = "Set"
    open val dialog69SwitchBikeConnectionOption1 = "Disconnect"
    open val dialog25PasskeyErrorOption1 = "Re-enter"
    open val dialog27RegisterBikeOption2 = "Use"
    open val dialog26ChangePasskeyOption1 = "Later"
    open val dialog26ChangePasskeyOption2 = "Change"
    open val dialog39ConfirmContinueOption2 = "Continue"
    open val dialog45SecondButtonTitle = "More"
    open val dialog72ConfirmBikeOption1 = "Register as\na new bike"
    open val dialog72ConfirmBikeOption2 = "Register as\nan existing bike"
    open val dialog55NoInformationOption1 = "Delete"
    open val dialog55NoInformationOption2 = "Register Unit"
    open val dialog88MoveNextStepOption = "Next"
    open val dialog3_5UnitRegisteredAgainOption1 = "Register"
    open val dialog4_1UpdateDetailOption2 = "Update history"
    open val dialog6_3DeleteOption1 = "Set"
    open val dialog41ConfirmDefaultOption2 = "Back"
    open val dialog2_1RecoveryFirmwareOption1 = "Recover (Enter)"
    open val dialog2_1RecoveryFirmwareOption2 = "Do not recover"
    open val dialog2_2NeedFirmwareUpdateOption1 = "Update (Enter)"
    open val dialog2_2NeedFirmwareUpdateOption2 = "Do not update"
    open val dialog3_1CompleteFirmwareUpdateOption = "OK (Enter)"
    open val dialog3_3ConfirmRewriteFirmwareOption2 = "Select"
    open val dialogPhotoLibraryAuthorizationSet = "Set"
    open val dialog109_Retry = "Re-enter"

    //region 文章
    open val dialog20BetaVersionMsg =
        "This software is a beta version.\n Not yet evaluation is complete, and there is a possibility that various problems may occur.\n After having agreed to these things, whether you will use this software?"
    open val dialog21BetaVersionMsg =
        "This software is a beta version.\n Not yet evaluation is complete, and there is a possibility that various problems may occur.\n After having agreed to these things, whether you will use this software?\n \n Expiration date of the software : {0}"
    open val dialog22BetaVersionMsg =
        "This software is a beta version.\n This software is past the expiration date is.\n Please re-install genuine software."
    open val dialogPhotoLibraryAuthorizationMessage =
        "This app uses Photo Library to upload images."

    // TODO: 英語の確認
    open val dialog47DuplicateSwitchesTitle = "同一スイッチ複数確認"
    //endregion
    //endregion

    //region ver3.4
    open val commonsConnectBle = "Bluetooth® LE connection"
    open val commonsOperation = "Operation counter"
    open val commonsBleSettings = "Bluetooth® LE settings"
    open val commonsYes = "Yes"
    open val commonsNo = "No"
    open val commonsBikeType = "Bike type"
    open val commonsRoad10 = "ROAD10"
    open val commonsRoad11 = "ROAD11"
    open val commonsMtb = "MTB"
    open val commonsUrbancity = "URBAN/CITY"
    open val commonsUnitName = "Unit name"
    open val commonsOnStr = "ON"
    open val commonsOffStr = "OFF"
    open val commonsLumpShiftTimeSuperFast = "Super Fast"
    open val commonsLumpShiftTimeVeryFast = "Very Fast"
    open val commonsLumpShiftTimeFast = "Fast"
    open val commonsLumpShiftTimeNormal = "Normal"
    open val commonsLumpShiftTimeSlow = "Slow"
    open val commonsLumpShiftTimeVerySlow = "Very Slow"
    open val commonsRetry = "Retry"
    open val commonsUse = "Use"
    open val commonsNoUse = "Spare"
    open val commonsDone = "Complete"
    open val commonsDecide = "Confirm"
    open val commonsCancel = "Cancel"
    open val commonsClose = "Close"
    open val commonsCategoryBmr = "Battery mount"
    open val commonsCategoryBtr2 = "Built-in battery"
    open val commonsCategorySt = "Shift lever"
    open val commonsCategorySw = "Switch unit"
    open val commonsCategoryEw = "Junction"
    open val commonsCategoryFd = "Front derailleur"
    open val commonsCategoryRd = "Rear derailleur"
    open val commonsCategoryMu = "Motor unit"
    open val commonsCategoryDu = "Drive unit"
    open val commonsCategoryBt = "Battery"
    open val commonsCategoryCj = "Cassette joint"
    open val commonsGroupMaster = "Master unit"
    open val commonsGroupJunction = "junction [A]"
    open val commonsGroupFshift = "Front shifting unit"
    open val commonsGroupRshift = "Rear shifting unit"
    open val commonsGroupSus = "Suspension control switch"
    open val commonsGroupStsw = "Shifter/Switch"
    open val commonsGroupEww = "Wireless unit"
    open val commonsGroupBattery = "Battery"
    open val commonsGroupPowerswitch = "Power switch"
    open val commonsGroupUnknown = "Unknown unit"
    open val commonsSusControlSw = "Suspension control switch"
    open val commonsBleUnit = "Wireless unit"
    open val commonsPresetName = "Preset file name"
    open val commonsComment = "Comment"
    open val commonsOptional = "(optional)"
    open val commonsTargetUnitMayBeBroken = "{0} may be faulty."
    open val commonsElectricWires = "Check that the electric wire is not disconnected."
    open val commonsAlreadyRecognizedUnit = "The unit has already been recognized."
    open val commonsNumberOfTeethFront = "Number of front gear teeth"
    open val commonsNumberOfTeethRear = "Number of rear gear teeth"
    open val commonsMaxGear = "Maximum number of gears"
    open val commonsLamp = "Light connection"
    open val commonsAgree = "Agree"
    open val commonsFirmwareUpdateTerminated = "The firmware update of {0} was interrupted."
    open val commonsBeginFwRestoration = "The firmware of {0} is restored."
    open val commonsPleaseConnectBle = "Make Bluetooth® LE connection."
    open val commonsDownLoadFailed = "Failed to download file."
    open val commonsQDeleteSetting = "The settings will be lost. Continue?"
    open val commonsDisplay = "Display"
    open val commonsSwitch = "Switch unit"
    open val commonsSwitchA = "Switch A"
    open val commonsSwitchX = "Switch X"
    open val commonsSwitchY = "Switch Y"
    open val commonsSwitchZ = "Switch Z"
    open val commonsSwitchX1 = "Switch X1"
    open val commonsSwitchX2 = "Switch X2"
    open val commonsSwitchY1 = "Switch Y1"
    open val commonsSwitchY2 = "Switch Y2"
    open val commonsFirmer = "Firmer"
    open val commonsSofter = "Softer"
    open val commonsPosition = "Position {0}"
    open val commonsShiftCountPlural = "{0} gears"
    open val commonsLocked = "CLIMB(FIRM)"
    open val commonsUnlocked = "DESCEND(OPEN)"
    open val commonsFirm = "TRAIL(MEDIUM)"
    open val commonsSuspensionType = "Suspension type"
    open val commonsSprinterSwitch = "Satellite shifter"
    open val commonsMultiShiftMode = "Multi shift"
    open val commonsShiftMode = "Shift mode"
    open val commonsLogin = "Login"
    open val commonsSettingUsername = "User ID"
    open val commonsSettingPassword = "Password"
    open val commonsUnexpectedError = "An unexpected error has occurred."
    open val commonsAccessKeyAuthenticationError =
        "Login session has become invalid. Please login again."
    open val commonsSoftwareLicenseAgreement = "Terms of use"
    open val commonsReInput = "Enter it again."
    open val commonsPerson = "Person responsible"
    open val commonsCompany = "Company name"
    open val commonsPhoneNumber = "Telephone number"
    open val commonsErrorNotice =
        "There is a problem with the entered content. Please review the below items."
    open val commonsFirmwareVersion = "Firmware version"
    open val commonsNotConnected = "Unconnected"
    open val commonsBeep = "Beep"
    open val commonsDisplayTime = "Display time"
    open val commonsTheSameMarkCtdCannotBeAssigned =
        "- If the combination pattern differs between the front and rear suspensions, the same mark (CTD) cannot be assigned."
    open val commonsDifferentMarksCtdCannotBeAssigned =
        "- If the same combination pattern is used for the front and rear suspensions, different marks (CTD) cannot be assigned."
    open val commonsInternetConnectionUnavailable =
        "Not connected to the Internet.\nConnect to the Internet and try again."
    open val commonsNotAvailableApp =
        "This is the {0} edition of the app. \nIt cannot be used on a {1}."
    open val commonsNotAvailableAppLink = "Download {0} edition of the app"
    open val commonsSmartPhone = "smartphone"
    open val commonsTablet = "tablet"
    open val commonsCycleComputerRight = "Cyclecomputer right"
    open val commonsCycleComputerLeft = "Cyclecomputer left"
    open val commonsError = "Error"
    open val commonsPleaseConnectAgain =
        "If the unit has been disconnected, check the connection again."
    open val commonsPleasePerformErrorCheckIfNotDisconnected =
        "If it is not disconnected, consult a distributor."
    open val commonsPleasePerformErrorCheck = "Contact a distributor."
    open val commonsErrorC =
        "There is no battery, or the battery does not have a sufficient charge. \nUse a battery with sufficient charge, or charge and then reconnect the battery."
    open val commonsErrorIfNoSwitchResponse =
        "If no switches have any effect, check whether any electric wires are disconnected."
    open val commonsNetworkError = "Network error"
    open val commonsNext = "Next"
    open val commonsErrorOccurdDuringSetting = "An error occurred during programming."
    open val commonsFrontShiftUp = "Front shift up"
    open val commonsFrontShiftDown = "Front shift down"
    open val commonsRearShiftUp = "Rear shift up"
    open val commonsRearShiftDown = "Rear shift down"
    open val commonsGroupFsus = "Front fork suspension"
    open val commonsErrorBattery =
        "Remaining battery capacity could not be confirmed.\nCheck that the master unit and battery are properly connected."
    open val commonsUpdateCheckUpdateSetting = "Checking for E-TUBE PROJECT updates."
    open val commonsApply = "Apply"
    open val commonsFinish = "Finish"
    open val commonsDoNotUseProhibitionCharacter = "Do not use forbidden characters ({1}) in {0}."
    open val commonsInputError = "Enter {0}."
    open val commonsInputNumberError = "Enter {0} in half-width numerals."
    open val commonsDi2 = "DI2"
    open val commonsSteps = "STePS"
    open val commonsSwitchForAssist = "the switch for changing the assist mode"
    open val commonsUsinBleUnitError = "An wireless unit error occurred while in use."
    open val commonsCommunicationMode = "Wireless communication mode"
    open val errorBleDisconnect =
        "The Bluetooth® LE connection was interrupted.\nTry connecting again."
    open val errorOccurredAbnormalCommunication =
        "A communication error has occurred. Try connecting again."
    open val commonsDrawerMenu = "Drawer menu"
    open val drawerMenuUnitList = "Unit list"
    open val drawerMenuDisconnectBle = "Disconnect Bluetooth® LE"
    open val drawerMenuTutorial = "Tutorial"
    open val drawerMenuApplicationSettings = "Application settings"
    open val drawerMenuLanguageSettings = "Language setting"
    open val drawerMenuVersionInfo = "Version information"
    open val drawerMenuLogin = "Login"
    open val drawerMenuLogout = "Logout"
    open val drawerMenuChangePassword = "Change password"
    open val drawerMenuRegistered = "User Information Query"
    open val commonsBetaMessageMsg1 =
        "This software is beta version.\nIt has not yet been completely validated and therefore may cause various troubles.\nOK?"
    open val commonsBetaMessageMsg2 = "Expiration date of the software"
    open val commonsBetaMessageMsg3 = "This software has passed its expiration date."
    open val launchLicenseMsg1 = "Review the license terms before using the application."
    open val launchLicenseMsg2 =
        "Click the \"Agree\" button after reviewing the agreement's provisions to continue the process. You must accept the terms of the agreement in order to use this application."
    open val launchFreeSize =
        "There is insufficient free disk space ({1} MB) in order to run E-TUBE PROJECT. \nThe required files will be downloaded the next time the application is run. \nBe sure that there is at least {0} MB of disk space available before running the application next time."
    open val connectionErrorRepairMsg2 = "{0} may be faulty."
    open val connectionErrorRepairMsg3 = "Select undetected unit group."
    open val connectionErrorRepairMsg4 = "A unit may be defective."
    open val connectionErrorRepairMsg6 = "Unit of {0}"
    open val connectionErrorRepairMsg7 = "The firmware for this unit is recovered."
    open val connectionErrorRepairMsg8 =
        "The following units are connected properly."
    open val connectionErrorUnknownUnit =
        "One or more units that are not supported by the current version of E-TUBE PROJECT are connected.\nInstall the latest app from the {0}."
    open val connectionErrorOverMsg1 = "A total of up to only {1} units of {0} can be connected."
    open val connectionErrorOverMsg2 = "Unit connected"
    open val connectionErrorNoCompatibleUnitsMsg1 =
        "An incompatible combination was found. Verify that a unit that is compatible with your bike type is connected and repeat from the beginning."
    open val connectionErrorNoCompatibleUnitsMsg2 =
        "You may be able to resolve this issue by updating the firmware for expanded compatibility. \nTry updating the firmware."
    open val connectionErrorNoCompatibleUnitsLink = "View a more detailed compatibility chart"
    open val connectionErrorFirmwareBroken =
        "The firmware of {0} may not be working correctly.\nThe firmware will be restored."
    open val connectionDialogMessage1 =
        "The {0} for assist mode switching is not connected.\nWhen running the switch mode setting, the setting of {1} can be changed from gear switching to assist mode switching."
    open val connectionDialogMessage3 = "{0} is set for suspension.\nChange the setting."
    open val connectionDialogMessage4 =
        "The \"{1}\" setting of {0} does not work with the bicycle in use.\nPlease change the setting."
    open val connectionDialogMessage5 =
        "The system will not function normally with the recognized combination of units.\nTo enable the system to function properly, connect the required units and check the connections again. Continue?"
    open val connectionDialogMessage6 = "Required unit (one of following)"
    open val connectionDialogMessage7 =
        "The system will not function normally with the recognized combination of units.\nTo enable the system to function properly, connect the required units and check the connections again."
    open val connectionDescription1 =
        "Set the unit on the bicycle to Bluetooth® LE connection mode."
    open val connectionDescription2 = "Select the wireless unit you wish to connect."
    open val connectionWhatIsPairing = "Enabling connection mode on the wireless unit."
    open val connectionDisconnectDescription =
        "To disconnect, select \"Disconnect Bluetooth® LE\" from the menu in the top right."
    open val connectionBluetoothInitialPasskey =
        "This app utilizes Bluetooth® LE for wireless communication.\nThe initial PassKey for Bluetooth® LE is \"000000\"."
    open val connectionPasskeyMessage =
        "Change the initial PassKey.\nThere is a risk of connection from a third party."
    open val connectionPasskey = "PassKey"
    open val connectionPasswordConfirm = "PassKey (for confirmation)"
    open val connectionPleaseInputPassword = "Enter your passkey."
    open val connectionDisablePassword =
        "Unable to authenticate. The PassKey was entered incorrectly, or the PassKey for the wireless unit has been changed. Reconnect, and then try entering the PassKey set in the wireless unit again."
    open val connectionValidityPassword = "0 cannot be set as the first character in the PassKey."
    open val connectionBleFirmwareRestore =
        "Firmware recovery will be performed on the wireless unit."
    open val connectionBleFirmwareRestoreFailed =
        "Reconnect the unit.\nIf the error persists, use the PC version of E-TUBE PROJECT."
    open val connectionBleWeakWaves =
        "Wireless connection is poor. \nThe Bluetooth® LE connection may be interrupted."
    open val connectionBleNotRequiredRestore =
        "The wireless unit is operating normally.\nFirmware recovery is not necessary."
    open val connectionFoundNewBleFirmware =
        "New firmware for the wireless unit has been found.\nUpdating will begin."
    open val connectionScanReload = "Reload"
    open val connectionUnmatchPassword = "PassKey does not match."
    open val connectionBikeTypeDi2Type1 = "ROAD"
    open val connectionBikeTypeDi2Type2 = "MTB"
    open val connectionBikeTypeDi2Type3 = "URBAN/CITY"
    open val connectionBikeTypeEbikeType1 = "MTB"
    open val connectionNeedPairingBySetting =
        "Set the wireless unit for connection and perform pairing from [Settings] > [Bluetooth] on the device."
    open val connectionSwE6000Recognize =
        "Press and hold either one of the switches of {1} set in {0}."
    open val commonsGroupDU = "Drive unit"
    open val commonsGroupSC = "Cyclecomputer"
    open val commonsGroupRSus = "Rear suspension"
    open val commonsGroupForAssist = "Shifting switch for assist"
    open val commonsGroupForShift = "Shifting switch for shift"
    open val connectionMultipleChangeShiftToAssist =
        "{0} {1} is connected. \nThe current bike type supports only {2}. \nDo you want to change all {0} to {2}?"
    open val connectionNewFirmwareFileNotFound = "The firmware file is not the most up-to-date."
    open val commonsGroupMasterCap = "Master unit"
    open val commonsGroupJunctionCap = "Junction [A]"
    open val commonsGroupFshiftCap = "Front shifting unit"
    open val commonsGroupRshiftCap = "Rear shifting unit"
    open val commonsGroupSusCap = "Suspension control switch"
    open val commonsGroupStswCap = "Shifter/switch"
    open val commonsGroupFsusCap = "Front fork suspension"
    open val commonsGroupRsusCap = "Rear suspension"
    open val commonsGroupDuCap = "Drive unit"
    open val commonsGroupForassistforshiftCap =
        "Shifting switch for assist/shifting switch for shift"
    open val commonsGroupForassistforshift = "shifting switch for assist/shifting switch for shift"
    open val connectionErrorNoSupportedMaster =
        "{0} is not supported by this application.\n{0} may be used with the PC version of E-TUBE PROJECT."
    open val connectionErrorNoSupportedMaster2 = "Not compatible."
    open val connectionErrorUnknownMaster = "An unknown master unit is connected."
    open val connectionErrorPleaseConfirmMasterUnit = "Check the master unit."
    open val connectionErrorUnSupportedMsg1 =
        "A unit that does not support Bluetooth® LE connection is connected.\nDisconnect all the below units and then reconnect."
    open val connectionErrorPcLinkageDevice = "PC linkage device"
    open val connectionErrorUnSupportedMsg2 =
        "Multiple wireless units are connected.\nRemove all but one of the units below and connect again."
    open val connectionErrorBroken =
        "The application was unable to connect to E-TUBE PROJECT because one of the connected units may be faulty. \nContact your distributor or dealer."
    open val connectionChargeMsg1 = "Check the battery's status."
    open val connectionChargeMsg2 =
        "Cannot be used while charging. Connect again after charging is complete."
    open val connectionChargeMsg3 =
        "If not charging, the battery may be faulty. \nContact a distributor."
    open val connectionChargeRdMsg =
        "Charging stopped. To charge again, disconnect from the app and then connect the charging cable again."
    open val connectionBikeTypePageTitle = "Bike type selection"
    open val connectionBikeTypeMsg =
        "The application was unable to detect the bike type. Select the bike type."
    open val connectionBikeTypeTitle1 = "DI2 system"
    open val connectionBikeTypeTitle2 = "E-bike system"
    open val connectionBikeTypeError =
        "The bike type for the settings being written differs from the bike type for the connected unit. \nCheck the settings or connected unit before writing the settings."
    open val connectionDialogMessage9 = "{0} for assist mode switching is connected."
    open val connectionDialogMessage12 =
        "The current bike type supports only setting for gear shifting."
    open val connectionDialogMessage13 = "Set {0} for gear shifting?"
    open val connectionDialogMessage14 = "Set all {0} for gear shifting?"
    open val connectionDialogMessage19 =
        "The firmware on the device in use may be out-of-date.\nConnect to the Internet and select \"Download all\" from \"Version information\" to download the latest firmware ."
    open val connectionChangeShiftToAssist =
        "{0} {1} is connected. \nThe current bike type supports only {2}. \nDo you want to change {0} to {2}?"
    open val connectionDialogMessage11 = "The following unit cannot be used."
    open val connectionDialogMessage16 =
        "Update the application to the latest version and try again."
    open val connectionDialogMessage15 = "Remove the unit."
    open val connectionDialogMessage17 =
        "The following unit does not support your smartphone or tablet's operating system."
    open val connectionDialogMessage18 =
        "It is recommended to update your smartphone or tablet's operating system to the latest version for use with the application."
    open val connectionSprinterMsg1 = "Are you using the sprinter switch on {0}?"
    open val connectionSprinterMsg2 = "Multiple {0} are connected.\nUse all sprinter switches?"
    open val commonsSelect = "Select"
    open val connectionSprinterContinue = "Continue selecting?"
    open val connectionAppVersion = "Ver.{0}"
    open val commonsVersionInfoEtubeVersion = "E-TUBE PROJECT version"
    open val commonsVersionInfoBleFirmwareVersion = "Bluetooth® LE firmware version"
    open val commonsVersionInfoMessage1 =
        "The checker function utilizes the wireless unit and E-TUBE PROJECT to detect broken wires and system errors. It cannot detect problems with all units and operational instability.\nContact the place of purchase or a dealer."
    open val commonsVersionInfoMessage2 =
        "A new version of E-TUBE PROJECT is available but it does not support your OS. For information on supported OS, see the SHIMANO website."
    open val commonsVersionInfoUpdateCheck = "Download all"
    open val settingMsg3 = "Verify that the screen for changing the initial PassKey displays."
    open val settingMsg5 = "Check the latest applications for different operating systems."
    open val settingMsg6 = "Verify that the shift mode animation guide is displayed."
    open val settingMsg4 = "Verify that the animation guide on multi shift is displayed."
    open val settingMsg1 = "Check the firmware update during the preset."
    open val settingMsg2 = "Proxy server requires authentication."
    open val settingServer = "Server"
    open val settingPort = "Port"
    open val settingUseProxy = "Use proxy server"
    open val settingUsername = "User ID"
    open val settingPassword = "Password"
    open val settingFailed = "An error occurred when saving application settings."
    open val commonsEnglish = "English"
    open val commonsJapanese = "Japanese"
    open val commonsChinese = "Chinese"
    open val commonsFrench = "French"
    open val commonsItalian = "Italian"
    open val commonsDutch = "Dutch"
    open val commonsSpanish = "Spanish"
    open val commonsGerman = "German"
    open val commonsLanguageChangeComplete =
        "Language setting has been changed. \nThe language will change once you exit and relaunch the application."
    open val commonsDiagnosisResult3 = "Replace or remove the following unit and connect again."
    open val connectionErrorSelectNoDetectedMsg = "Select an undetected unit."
    open val connectionErrorSelectNoDetectedAlert =
        "If you select the wrong unit, the wrong firmware may be written to the unit, rendering it inoperative. Ensure that you have chosen the correct unit and proceed to the next step."
    open val commonsCheckElectricWires2 = "{0} is not recognized."
    open val commonsDiagnosisCheckMsg = "{0} is recognized.\nIs the connected unit {0}?"
    open val commonsUpdateCheckUpdateError =
        "Unable to verify a connection with the server. \nConnect to the Internet and try again."
    open val commonsUpdateCheckWebServerError =
        "There may be a problem with the Internet connection or the Shimano's web server. \nWait a little while and try again."
    open val commonsUpdateCheckUpdateSettingIOs = "Now reading."
    open val firmwareUpdateRecognizeUnitDialogMsg =
        "Keep pressing one of the {0} switches for the unit you would like to update the firmware for."
    open val customizeSwitchFunctionDialogMsg1 =
        "Keep pressing one of the {0} switches for the unit you would like to set."
    open val connectionSprinterRecognize =
        "Keep pressing one of the {0} switches for the unit you would like to select."
    open val commonsConfirmPressedSwitch = "Did you press the switch?"
    open val connectionSprinterRelease =
        "OK. Take your hand off the switch. \n\nIf this dialog box fails to close after you take your hand off the switch, a switch may be malfunctioning. In that case, contact a distributor."
    open val commonsLeft = "Left"
    open val commonsRight = "Right"
    open val commonsUpdateCheckIsInternetConnected =
        "The file will be updated. \nIs your device connected to the Internet?"
    open val commonsUpdateCheckUpdateFailed = "Update failed."
    open val commonsUpdateFailed =
        "The application was unable to find one or more files required in order to run E-TUBE PROJECT. \nUninstall and then reinstall E-TUBE PROJECT."
    open val customizeBleBleName = "Wireless unit name"
    open val customizeBlePassKeyRule = "(6 half-width alphanumeric characters)"
    open val customizeBlePassKeyDisplay = "Display"
    open val customizeBlePassKeyPlaceholder = "Enter it again."
    open val customizeBleErrorBleName = "Enter in 1 to 8 half-width alphanumeric characters."
    open val customizeBleErrorPassKey = "Enter in 6 half-width numerals."
    open val customizeBleErrorPassKeyConsistency = "PassKey does not match."
    open val customizeBleAlphanumeric = "half-width alphanumeric characters"
    open val customizeBleHalfWidthNumerals = "Half-width numerals"
    open val commonsCustomize = "Customize"
    open val customizeFunctionDriveUnit = "Drive unit"
    open val customizeFunctionNonUnit = "No customizable unit is connected."
    open val customizeCadence = "Cadence"
    open val customizeAssistSetting = "Assist"
    open val commonsAssistPattern = "Assist pattern"
    open val commonsElectricType = "Electrical"
    open val commonsExteriorTransmission = "Derailleur type (chain)"
    open val commonsMechanical = "Mechanical"
    open val commonsAssistPatternDynamic = "DYNAMIC"
    open val commonsAssistPatternExplorer = "EXPLORER"
    open val commonsAssistModeHigh = "HIGH"
    open val commonsAssistModeMedium = "MEDIUM"
    open val commonsAssistModeLow = "LOW"
    open val commonsRidingCharacteristic = "Riding characteristic"
    open val commonsFunctionRidingCharacteristic = "Riding characteristic"
    open val commonsAssistPatternCustomize = "CUSTOMIZE"
    open val commonsAssistModeBoost = "BOOST"
    open val commonsAssistModeTrail = "TRAIL"
    open val commonsAssistModeEco = "ECO"
    open val customizeSceKm = "m\nkg"
    open val customizeSceMile = "y\nlb"
    open val commonsLanguage = "Language"
    open val customizeDisplayNowTime = "Current time"
    open val customizeDisplaySet = "Set"
    open val customizeDisplayDontSet = "Do not set"
    open val commonsDisplayLight = "Display/Light"
    open val customizeSwitchSettingViewControllerMsg1 = "Shifting switch"
    open val customizeSwitchSettingViewControllerMsg8 = "Use {0}"
    open val customizeSwitchSettingViewControllerMsg2 = "Change to suspension setting"
    open val customizeSwitchSettingViewControllerMsg3 = "About Synchronized shift"
    open val customizeSwitchSettingViewControllerMsg11 =
        "The unit's connection status has changed. Do not disconnect or change units while configuring the settings. \nRepeat the process from the beginning."
    open val customizeSwitchSettingViewControllerMsg4 =
        "The currently selected information will be lost. \nChange to suspension setting?"
    open val customizeSwitchSettingViewControllerMsg9 = "What is Synchronized shift?"
    open val customizeSwitchSettingViewControllerMsg5 =
        "Synchronized shift automatically shifts FD in synchronization with Rear Shift Up and Rear Shift Down."
    open val customizeSwitchSettingViewControllerMsg6 =
        "The following functions are not included. Continue processing?"
    open val customizeSwitchSettingViewControllerMsg7 = "Switch {0}"
    open val customizeSwitchSettingViewControllerMsg10 = "Set switch {0} to reverse operation"
    open val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift is a function that automatically shifts the rear derailleur when the front derailleur is shifted in order to obtain optimal gear transition."
    open val customizeSwitchSettingViewControllerMsg13 = "Shift points can be selected."
    open val customizeSwitchSettingViewControllerMsg14 =
        "At that time, the rear derailleur shift position can be selected from 0 to 4. (Depending on the combination, there may be a non-selectable shift position.)"
    open val commonsSwitchType = "Switch mode"
    open val commonsAssistUp = "Assist up"
    open val commonsAssistDown = "Assist down"
    open val customizeSwitchModeChangeAssist =
        "Change the mode of this switch to [for Assist].\nContinue?"
    open val customizeSwitchModeChangeShift =
        "Change the mode of this switch to [for Shift].\nContinue?"
    open val customizeSwitchModeLostAssist =
        "When the switch mode is changed to [for Shift], the switch for assist mode switching will disappear.\nProceed to change?"
    open val commonsDFlyCh1 = "D-FLY Ch. 1"
    open val commonsDFlyCh2 = "D-FLY Ch. 2"
    open val commonsDFlyCh3 = "D-FLY Ch. 3"
    open val commonsDFlyCh4 = "D-FLY Ch. 4"
    open val commonsDFly = "D-FLY"
    open val commonsFunction = "Function"
    open val customizeGearShifting = "Gear shifting"
    open val customizeManualSelect = "Select the switch manually"
    open val commonsUnit = "Unit"
    open val customizeScSetting = "Cyclecomputer"
    open val customizeSusSusTypeTitle = "Suspension control switch"
    open val customizeSusSusPositionInform = "Which grip is the control switch installed on?"
    open val customizeSusSusPositionLeft = "Left-hand side"
    open val customizeSusSusPositionRight = "Right-hand side"
    open val customizeSusPosition = "Position"
    open val customizeSusFront = "Front"
    open val customizeSusRear = "Rear"
    open val customizeFooterMsg1 = "Restore default values"
    open val customizeFooterMsg2 = "Restores the default settings {0}"
    open val customizeSusConsistencyCheckMsg1 = "Please check the following information."
    open val customizeSusConsistencyCheckMsg6 = "OK to continue with programming?"
    open val customizeSusConsistencyCheckMsg3 =
        "- The same setting has been chosen for two or more positions."
    open val customizeSusConsistencyCheckMsg2 = "- Only {1} has been set for {0}."
    open val customizeSusConsistencyCheckMsg4 =
        "- The same setting has been applied to two or more positions in CTD."
    open val customizeSusClimb = "CLIMB(FIRM)"
    open val customizeSusDescend = "DESCEND(OPEN)"
    open val customizeSusTrail = "TRAIL(MEDIUM)"
    open val customizeSusSwVerCheck =
        "Switch cannot be set to suspension because the firmware version of {0} is below {1}.\nIn order to set the switch to suspension, connect to a network and update to the latest version ({1} or later)."
    open val customizeSusSusConnectMsg1 =
        "One of the following units is required in order to set the switch to suspension."
    open val customizeSusSusConnectMsg2 = "Reconnect the required unit and try again."
    open val customizeSusSusConnectMsg3 = "It is also possible to change it to shifting setting."
    open val customizeSusSusConnectMsg4 =
        "To change it to shifting setting, press [Yes]. To connect required units and start over without changing the setting, press [No]."
    open val customizeSusSusModeSelect =
        "The \"→(T)\" setting does not work with the suspension in use. \nYou can hide the \"→(T)\" setting via the setup screen. \nHide \"→(T)\"?"
    open val customizeSusSwitchShift = "Change to shifting setting"
    open val customizeSusTransitionShift =
        "The currently selected information will be lost. \nChange to shifting setting?"
    open val customizeMuajstTitle = "Derailleur adjustment"
    open val customizeMuajstAdjustment = "Adjustment"
    open val customizeMuajstGearPosition = "Gear position change"
    open val customizeMuajstInfo1 = "The switches installed to the bicycle do not operate."
    open val customizeMuajstInfo2 = "See here for more about the adjustment method."
    open val customizeMultiShiftModalTitle = "Multi-shift speed setting"
    open val customizeMultiShiftModalDescription =
        "The initial settings use standard default values. \nAfter ensuring that you understand the characteristics of multi shift, select multi shift settings that suit the conditions in which you will be using the bicycle (terrain, riding style, etc.)."
    open val customizeMultiShiftModalVeryFast = "Very Fast"
    open val customizeMultiShiftModalFast = "Fast"
    open val customizeMultiShiftModalNormal = "Normal"
    open val customizeMultiShiftModalSlow = "Slow"
    open val customizeMultiShiftModalVerySlow = "Very Slow"
    open val customizeMultiShiftModalFeatures = "Characteristics"
    open val customizeMultiShiftModalFeature1 =
        "Fast multi shifting is now enabled. \n\n•This feature lets you quickly adjust crank RPM in response to changes in riding conditions. \n•It allows you quickly adjust your speed."
    open val customizeMultiShiftModalFeature2 = "Enables reliable multi shift operation"
    open val customizeMultiShiftModalNoteForUse = "Operating precautions"
    open val customizeMultiShiftModalUse1 =
        "1. It is easy to overshift. \n\n2. If the crank RPM is low, the chain will not be able to keep up with the motion of the rear derailleur. \nAs a result, the chain may fly off the gear teeth instead of engaging the cassette sprocket."
    open val customizeMultiShiftModalUse2 = "Multi shift operation takes time"
    open val customizeMultiShiftModalCrank = "Crank RPM required for multi shift use"
    open val customizeMultiShiftModalCrank1 = "At high crank RPM"
    open val customizeMultiShiftModeViewControllerMsg3 = "Gear-shifting interval"
    open val customizeMultiShiftModeViewControllerMsg4 = "Gear number limit"
    open val commonsLumpShiftUnlimited = "No limit"
    open val commonsLumpShiftLimit2 = "2 gears"
    open val commonsLumpShiftLimit3 = "3 gears"
    open val customizeMultiShiftModeViewControllerMsg5 = "Other Shifting Switch"
    open val customizeShiftModeRootMsg1 =
        "One unit from each of (1) and (2) below is required in order to set shift mode."
    open val customizeShiftModeRootMsg3 = "Reconnect the required unit and try again."
    open val customizeShiftModeRootMsg4 =
        "One of the following units is required in order to set shift mode."
    open val customizeShiftModeRootMsg5 =
        "Even if shift mode settings are made, they will only be effective if the firmware version of {0} is {1} or higher."
    open val customizeShiftModeRootMsg6 = "Firmware version is {0} or higher"
    open val customizeShiftModeSettingMsg1 = "Delete?"
    open val customizeShiftModeSettingMsg2 = "The current bike settings will be lost. Continue?"
    open val customizeShiftModeSettingMsg3 =
        "Settings cannot be configured because the unit is not properly connected."
    open val customizeShiftModeSettingMsg4 =
        "The bike has a gear structure that cannot be configured. Check the settings."
    open val customizeShiftModeSettingMsg5 =
        "Unable to read the setting value from stock file {0}. \n{0} will be deleted."
    open val customizeShiftModeSettingMsg7 = "Which function would you like to assign?"
    open val customizeShiftModeSettingMsg8 = "The settings file name is already in use."
    open val customizeShiftModeTeethMsg1 = "Select the number of gear teeth for your current bike."
    open val customizeShiftModeGuideMsg1 =
        "To use the settings file on your device to overwrite your bike's settings, drag the file to the slot (S1 or S2) you wish to overwrite."
    open val customizeShiftModeGuideMsg2 =
        "The icon shown above the settings indicates the shift mode."
    open val customizeShiftModeGuideMsg3 =
        "Synchronized shift is a function for automatically shifting the front derailleur in synchronization with the rear derailleur. You can configure Synchronized shift operation by selecting points on the map."
    open val customizeShiftModeGuideMsg4 =
        "Move the green cursor to set UP (from the inside to the outside) and the blue cursor to set DOWN (from the outside to the inside)."
    open val customizeShiftModeGuideMsg5 =
        "Synchronization points \nPoints at which the front derailleur shifts in concert with the rear derailleur. \nThe requirements for the arrows indicating the synchronization points are as follows: \nUP: Pointing up or sideways \nDOWN: Pointing down or sideways"
    open val customizeShiftModeGuideMsg6 =
        "Available gear ratios \nUP: You can select any gear ratio up to that one lower than the synchronization point gear ratio. \nDOWN: You can select any gear ratio up to that one higher than the synchronization point gear ratio."
    open val customizeShiftModeTeethMsg6 =
        "If front chainwheel (FC) is changed from {0} to {1}, all set shift points will be reset to default values.\nMake change?"
    open val customizeShiftModeTeethDouble = "double"
    open val customizeShiftModeTeethTriple = "triple"
    open val customizeShiftModeTeethMsg2 = "Synchronized shift interval"
    open val customizeShiftModeTeethSettingTitle = "Select the number of teeth"
    open val customizeShiftModeGuideTitle1 = "Synchronized shift"
    open val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    open val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    open val customizeShiftModeSynchroMsg1 =
        "The shifting point is within the unsettable range and cannot operate normally."
    open val customizeShiftModeTeethMsg5 = "Settings file name"
    open val customizeShiftModeBtn1 = "Up"
    open val customizeShiftModeBtn2 = "Down"
    open val customizeShiftModeTeethMsg8 = "Gear position control "
    open val customizeShiftModeSettingMsg9 =
        "The information below is also updated for both S1 and S2."
    open val customizeShiftModeTeethInward1 = "Rear up on front down"
    open val customizeShiftModeTeethOutward1 = "Rear down on front up"
    open val customizeShiftModeTeethInward2 = "Rear shifts up\non Front shift down"
    open val customizeShiftModeTeethOutward2 = "Rear shifts down\non Front shift up"
    open val customizeShiftModeTypeSelectTitle = "Selection of functions to assign"
    open val customizeShiftModeSettingMsg10 = "Continue processing?"
    open val customizeShiftModeTeethMsg7 =
        "Select the number of gear teeth for the current bike. \n*Fixed as {0} is connected."
    open val commonsErrorCheck = "Error check"
    open val commonsAll = "All"
    open val errorCheckResultError = "{0} may be faulty."
    open val errorCheckContactDealer = "Please inquire at the retail store or distributor."
    open val commonsStart = "Start"
    open val commonsUpdate = "Update firmware"
    open val firmwareUpdateUpdateAll = "Update all"
    open val firmwareUpdateCancelAll = "Cancel all"
    open val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Connect to the Internet and check whether there are any updated E-TUBE PROJECT or product versions available. \nUpdating to the latest version will allow you to use new products and features."
    open val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "The application or firmware may be out of date. \nConnect to the Internet and check for the latest version."
    open val firmwareUpdateConnectionLevelIsLow =
        "Cannot be updated due to a poor wireless connection. \nUpdate the firmware after establishing a good connection."
    open val firmwareUpdateIsUpdatingFw = "Firmware for {0} is being updated."
    open val firmwareUpdateNotEnoughBattery =
        "It will take several minutes to update the firmware. \nIf the battery of your device is low, perform the update after charging it or connect it to a charger. \nStart update?"
    open val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "The application was unable to update the firmware. \nPerform the firmware update process again. \nIf the update fails after repeated attempts, the {0} may be faulty."
    open val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "A unit that is not working normally is connected.\n\nIf the firmware of {0} is updated under this condition, the new firmware for {0} may overwrite the firmware of other units, possibly resulting in a fault.\nDisconnect all units other than {0}."
    open val firmwareUpdateLatest = "Latest"
    open val firmwareUpdateWaitingForUpdate = "Waiting for update"
    open val firmwareUpdateCompleted = "Complete"
    open val firmwareUpdateProtoType = "prototype"
    open val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "Firmware update of {0} failed. \nProceed to the restore process."
    open val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "Download the latest version of the firmware?"
    open val firmwareUpdateInvalidFirmwareFileSingle = "The firmware file of {0} is void."
    open val firmwareUpdateStopFwUpdate = "Cancel the firmware update."
    open val firmwareUpdateStopFwRestoring = "Cancel the restoration of the firmware."
    open val firmwareUpdateRestoreComplete =
        "When using {0}, you can change the Switch setting shown in SC from suspension type in the Customize menu."
    open val firmwareUpdateRestoreCompleteSc = "SC"
    open val firmwareUpdateSystemUpdateFailed = "Failed to update the system."
    open val firmwareUpdateAfterUnitRecognitionFailed = "Failed to connect after firmware update."
    open val firmwareUpdateUpdateBleUnitFirst = "First, update the firmware for {0}."
    open val firmwareUpdateSystemUpdateFailure = "Unable to update the system."
    open val firmwareUpdateSystemUpdateFinishedWithoutUpdate = "The system was not updated."
    open val firmwareUpdateBleUpdateErrorOccuredConnectAgain = "Please connect again."
    open val firmwareUpdateFwRestoring =
        "The firmware is currently being overwritten. \nDo not disconnect until overwriting is complete."
    open val firmwareUpdateFwRestorationUnitIsNormal = "The firmware of {0} functions normally."
    open val firmwareUpdateNoNeedForFwRestoration = "It does not need to be restored."
    open val firmwareUpdateFwRestorationError = "Firmware restoration of {0} failed."
    open val firmwareUpdateFinishedFirmwareUpdate =
        "The firmware was updated to the latest version."
    open val firmwareUpdateFinishedFirmwareRestoration = "The firmware of {0} has been restored."
    open val firmwareUpdateRetryFirmwareRestoration =
        "Firmware recovery for {0} is performed again."
    open val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "You must agree to the software's terms of use in order to update the firmware for the following units."
    open val functionThresholdAdditionalSoftwareUsageAgreement =
        "Additional Software License Agreement"
    open val functionThresholdMustRead = "(Be sure to read)"
    open val functionThresholdContentUpdateDetail = "Changes"
    open val functionThresholdBtnTitle = "Agree and update"
    open val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "You must agree to the software's terms of use in order to restore the firmware."
    open val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "You must agree to the software's terms of use in order to restore the unit's firmware."
    open val commonsPreset = "Preset"
    open val presetTopMenuLoadFileBtn = "Loading a settings file"
    open val presetTopMenuLoadFromBikeBtn = "Loading settings from the bike"
    open val presetDflyCantSet =
        "The connected unit configuration does not support D-FLY settings.\nThe D-FLY settings cannot be written."
    open val presetSkipUnitBelow = "The following unit will be skipped."
    open val presetNoSupportedFileVersion =
        "A preset file created in a version earlier than {0} cannot be used with the current bicycle type."
    open val presetPresetTitleShiftMode = "Shift mode"
    open val presetConnectedUnit = "Connect a unit."
    open val presetEndConnected = "Connected"
    open val presetErrorWhileReading = "Error occurred while reading the settings."
    open val presetOverConnect =
        "{1} units of {0} are connected.\nConnect just the selected number of {0}."
    open val presetChangeSwitchType = "{0} is recognized. Change to {1}?"
    open val presetLackOfTargetUnits =
        "A unit required in the current configuration of units is missing.\nReview the unit configuration and change the settings again."
    open val presetLoadFileTopMessage =
        "Select the preset settings file to load or delete from the following list of preset settings files."
    open val presetLoadFileModifiedOn = "Date of update"
    open val presetLoadFileConfimDeleteFile = "The selected {0} file will be deleted. \nContinue?"
    open val presetLoadFileErrorDeleteFile = "The application was unable to delete the preset file."
    open val presetLoadFileErrorLoadFile = "Reading of the preset file failed."
    open val presetLoadFileErrorNoSetting =
        "The settings of a unit supported by the current bicycle type are not included."
    open val presetLoadFileErrorIncompatibleSetting =
        "The settings of a unit unsupported by the current bicycle type are included.\nDo you want to ignore the settings of unsupported units?"
    open val presetLoadFileErrorShortageSetting =
        "Setting items have been added/deleted.\nRecheck the settings of the preset file, save the file, and retry writing."
    open val presetLoadFileErrorUnanticipatedSetting =
        "An unexpected value was read when reading the set value.\nRecheck the settings of the preset file, save the file, and retry writing."
    open val presetWriteToBikeBan =
        "The Synchronized shift settings cannot be written because the Synchronized shift settings and rear derailleur settings do not match. \nContinue?"
    open val presetWriteToBikePoint =
        "The Synchronized shift settings cannot be written because the gear-shifting point is outside the valid setting range. \nContinue?"
    open val presetWriteToBikeNoSettingToWrite = "There is no setting information to write."
    open val presetQEndPreset = "Disable the connection and complete setup?"
    open val commonsSynchromap = "Shift mode{0}"
    open val commonsSetting = "Setting"
    open val commonsDirection = "Directions"
    open val commonsChangePointFd = "Switching point for FD"
    open val commonsChangePointRd = "Switching point for RD"
    open val commonsAimPoint = "Target gear position for RD"
    open val commonsDirectionUp = "UP direction"
    open val commonsDirectionDown = "DOWN direction"
    open val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} is set in the multiple buttons of {0}."
    open val switchFunctionSettingDuplicationCheckMsg3 =
        "Assign a different setting to each button."
    open val presetSaveFileOk = "Saving of the preset file completed successfully."
    open val presetSaveFileError = "Saving of the preset file failed."
    open val presetFile = "File"
    open val presetSaveCheckErrorChangeSetting = "Change the settings."
    open val presetSaveCheckErrorSemiSynchro = "Semi-Synchronized Shift is set to an invalid value."
    open val presetConnectionNotPresetUnitConnected = "Unable to recognize unit."
    open val presetConnectionSameUnitsConnectionError =
        "Identical units were detected. The preset function does not support identical units."
    open val presetNeedFirmwareUpdateToPreset =
        "The settings cannot be written to the following unit(s) because the firmware is not the latest version."
    open val presetUpdateFirmwareToContinuePreset = "To continue making presets, please update."
    open val presetOldFirmwareUnitsExist =
        "The firmware for the following units is not up-to-date. \nUpdate to the latest firmware now?"
    open val presetOldFirmwareUnitsExistAndStopPreset =
        "The settings cannot be written to the following unit(s) because the firmware is not the latest version.\nUpdate the firmware of the unit(s) to write to."
    open val presetDonotShowNextTimeCheckTitle =
        "Do not show this screen in the future (select only when the same settings will be reused)."
    open val presetUpdateBtnTitle = "Update"
    open val presetPresetOntyRecognizedUnit = "Write the settings to the recognized unit only?"
    open val presetNewFirmwareUnitsExist =
        "The preset cannot be configured because the unit's firmware is newer than the application. \nUpdate the application's firmware to the new version."
    open val presetNewFirmwareUnitsExistAndStopPreset =
        "It may not be possible to configure the settings normally because the unit's firmware is newer than the application. \nThe process is being canceled."
    open val presetSynchronizedShiftSettings = "Synchronized shift settings"
    open val presetSemiSynchronizedShiftSettings = "Semi-Synchronized Shift settings"
    open val presetQContinue = "Continue?"
    open val presetWriteToBikeNoUnit = "{1} cannot be written because {0} is not connected."
    open val presetWriteToBikeToothSettingNotMatch =
        "{0} cannot be written to S1 or S2 because the tooth structure in the setting file differs from the tooth structure of the connected unit."
    open val loginViewForgetPwdTitle = "If you have forgotten your password"
    open val loginViewForgetIdTitle = "Forgot your ID?"
    open val loginViewCreateUserContent =
        "*If user registration by the OEM is preferred,contact a local distributor or Shimano staff."
    open val loginInternetConnection = "Cannot connect to network."
    open val loginAccountIsExist = "This account is locked.Try logging in again later."
    open val loginCorrectIdOrPassword = "The user ID or password is incorrect."
    open val loginPasswordErrorMessage = "The password is incorrect."
    open val loginPasswordExpired =
        "Your temporary password has expired.\nRe-start the registration process."
    open val loginPasswordExpiredChange =
        "Your password has not been changed for {0} days.\nChange your password."
    open val loginChange = "Change"
    open val loginLater = "Later"
    open val loginViewFinishLogin = "You have logged in."
    open val loginErrorAll =
        "There is a problem with the entered content. Please review the below items."
    open val loginViewUserId = "User ID"
    open val loginViewEmail = "Email address"
    open val loginViewPassword = "Password"
    open val loginViewCountry = "Country"
    open val loginLoginMove = "To login"
    open val loginMailSendingError = "Unable to send email. Wait a moment and try again."
    open val loginAlphanumeric = "half-width alphanumeric characters"
    open val loginTitleForgetPassword = "Reset your password"
    open val loginViewForgetPasswordTitleFir =
        "You will be sent information you need to set a new password in an email."
    open val loginViewForgetMailSettingTitle =
        "Verify settings, in advance, so that \"{0}\" can be received."
    open val loginTitleForgetMailAdress = "Registered email address"
    open val loginUserIdAndEmailDataNotExist =
        "The user ID or registered email address is incorrect."
    open val loginViewForgotPassword = "Password reset e-mail sent."
    open val loginTitleChangePassword = "Change password"
    open val loginViewCurrentPassword = "Current password"
    open val loginViewNewPassword = "New password"
    open val loginErrorCurrentPasswordNonFormat =
        "Current password may only contain alphanumeric characters."
    open val loginErrorCurrentPasswordLength =
        "Current password is between {0} and {1} characters long."
    open val loginErrorNewPasswordNonFormat =
        "New password may only contain alphanumeric characters."
    open val loginErrorNewPasswordLength =
        "New password must be between {0} and {1} characters long."
    open val loginErrorPasswordNoDifferent =
        "Your new password cannot be the same as your current password."
    open val loginErrorPasswordNoConsistent =
        "New password does not match the confirmation password."
    open val loginViewFinishPasswordChange = "Your password was changed."
    open val loginTitleLogOut = "Logout"
    open val loginViewLogOut = "You have logged out."
    open val loginViewForgetIdTitleFir = "Your user ID will be sent to you by email."
    open val loginTitleForgetId = "User ID notification"
    open val loginViewForgetTitleSec =
        "*If you are unable to use the registered email address, it will not be possible to reissue your {0} since there will be no way to confirm your identity. Register as a new user."
    open val loginEmailDataNotExist = "Email address has not been registered."
    open val loginViewForgotUserId = "Your user ID has been sent to you by email."
    open val loginTitleUserInfo = "User Information Query"
    open val loginViewAddress = "Address"
    open val loginUserDataNotExist = "Failed to acquire user information."
    open val commonsSet = "Set"
    open val commonsAverageVelocity = "Average speed"
    open val commonsBackLightBrightness = "Brightness"
    open val commonsBackLightBrightnessLevel = "Level {0}"
    open val commonsCadence = "Cadence"
    open val commonsDistanceUnit = "Unit"
    open val commonsDrivingTime = "Traveling time"
    open val commonsForAssist = "for Assist"
    open val commonsForShift = "for Shift"
    open val commonsInvalidate = "No"
    open val commonsKm = "International units"
    open val commonsMaxGearUnit = "gears"
    open val commonsMaximumVelocity = "Maximum speed"
    open val commonsMile = "Yard & pound method"
    open val commonsNotShow = "Do not display"
    open val commonsShow = "Display"
    open val commonsSwitchDisplay = "Display switchover"
    open val commonsTime = "Time setting"
    open val commonsValidate = "Yes"
    open val commonsNowTime = "Current time"
    open val commonsValidateStr = "Valid"
    open val commonsInvalidateStr = "Invalid"
    open val commonsAssistModeBoostRatio = "Assist mode BOOST"
    open val commonsAssistModeTrailRatio = "Assist mode TRAIL"
    open val commonsAssistModeEcoRatio = "Assist mode ECO"
    open val commonsStartMode = "Start mode"
    open val commonsAutoGearChangeModeLog = "Automatic gear shifting function"
    open val commonsAutoGearChangeAdjustLog = "Shifting timing"
    open val commonsBackLight = "Backlight setting"
    open val commonsFontColor = "Font color"
    open val commonsFontColorBlack = "Black"
    open val commonsFontColorWhite = "White"
    open val commonsInteriorTransmission = "Internal geared hub (chain)"
    open val commonsInteriorTransmissionBelt = "Internal geared hub  (belt)"
    open val commonsRangeOverview = "Range overview"
    open val commonsUnknown = "Unknown"
    open val commonsOthers = "Others"
    open val commonsBackLightManual = "MANUAL"
    open val commonsStartApp = "Start"
    open val commonsSkipTutorial = "Skip tutorial"
    open val powerMeterMonitorPower = "Power"
    open val powerMeterMonitorCadence = "Cadence"
    open val powerMeterMonitorPowerUnit = "W"
    open val powerMeterMonitorCadenceUnit = "rpm"
    open val powerMeterMonitorCycleComputerConnected = "Return to cycle computer"
    open val powerMeterMonitorGuideTitle = "Performing zero offset calibration"
    open val powerMeterMonitorGuideMsg =
        "1.Place the bicycle on level ground.\n2.Position the crank arm so that it is perpendicular to the ground as in the illustration.\n3.Press the \"Zero offset calibration\" button.\n\nDo not place your feet on the pedals or apply load to the crank."
    open val powerMeterLoadCheckTitle = "Load check mode"
    open val powerMeterLoadCheckUnit = "N"
    open val powerMeterFewCharged = "Battery level is low. Charge the battery."
    open val powerMeterZeroOffsetErrorTimeout =
        "Failed to connect to power meter due to poor wireless connection.\nPerform in a better wireless reception environment."
    open val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Insufficient battery charge. Charge the battery and try again."
    open val powerMeterZeroOffsetErrorSensorValueOver =
        "The crank may be loaded. Release any load on the crank and try again."
    open val powerMeterZeroOffsetErrorCadenceSignalChange =
        "The crank may have moved. Release your hands from the crank and try again."
    open val powerMeterZeroOffsetErrorSwitchOperation =
        "The switch may have been operated. Release your hands from the switch and try again."
    open val powerMeterZeroOffsetErrorCharging = "Remove the charging cable and try again."
    open val powerMeterZeroOffsetErrorCrankCommunication =
        "The left crank connector may be detached. Remove the outer cap, check that the connector is connected and try again."
    open val powerMeterZeroOffsetErrorInfo = "Refer to the Shimano manual for details."
    open val drawerMenuLoadCheck = "Load check mode"
    open val commonsMonitor = "Monitoring mode"
    open val commonsZeroOffsetSetting = "zero offset calibration"
    open val commonsGroupPowermeter = "power meter"
    open val commonsPresetError = "Power meter cannot be preset.\nConnect to a different unit."
    open val commonsWebPageTitleAdditionalFunction =
        "E-TUBE PROJECT|Information on additional functions"
    open val commonsWebPageTitleFaq = "E-TUBE PROJECT|FAQ"
    open val commonsWebPageTitleGuide = "E-TUBE PROJECT|How to use E-TUBE PROJECT"
    open val connectionReconnectingBle =
        "The Bluetooth® LE connection was interrupted.\nTrying to reconnect."
    open val connectionCompleteReconnect = "Reconnection complete."
    open val connectionFirmwareWillBeUpdated = "Firmware will be updated."
    open val connectionFirmwareUpdatedFailed =
        "Reconnect the unit.\nIf you encounter the error again, there may be a fault.\nContact your distributor or dealer."
    open val settingMsg7 = "Automatically synchronize the time setting of the cycle computer."
    open val commonsDestinationType = "Type {0}"
    open val customizeSwitchSettingViewControllerMsg15 = "Assist switch"
    open val presetNeedToChangeSettings =
        "A function that cannot be used has been configured. Modify the functions that can be used in Customize, and then try again."
    open val connectionErrorSelectNoDetectedMsg1 =
        "Cannot restore if there is no unit name to select."
    open val connectionTurnOnGps =
        "To connect to a wireless unit, turn device location information ON."
    open val drawerMenuAboutEtube = "About E-TUBE PROJECT"
    open val commonsX2Y2Notice =
        "* If the setting for X2/Y2 is set to \"Do not use\" in {0}, recognition will not be performed even if X2/Y2 is pressed."
    open val powerMeterZeroOffsetComplete = "Zero offset result"
    open val commonsMaxAssistSpeed = "Maximum assist speed"
    open val commonsShiftingAdvice = "Shifting advice"
    open val commonsSpeedAdjustment = "Display Speed"
    open val customizeSpeedAdjustGuideMsg = "Adjust if different from other speed indicators."
    open val commonsMaintenanceAlertDistance = "Maintenance alert traveling distance"
    open val commonsMaintenanceAlertDate = "Maintenance alert date"
    open val commonsAssistPatternComfort = "COMFORT"
    open val commonsAssistPatternSportive = "SPORTIVE"
    open val commonsEbike = "E-BIKE"
    open val connectionDialogMessage20 = "{0} has a setting switch for assist. Change the settings."
    open val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "If {0} is set to assist function only\nonly the following may be set:\nSwitch X: Assist up\nSwitch Y: Assist down"
    open val switchFunctionSettingDuplicationCheckMsg5 =
        "The suspension function cannot be set simultaneously with another function."
    open val switchFunctionSettingDuplicationCheckMsg6 =
        "Only the following combinations may be set\nfor SW-E6000\nRear shift up\nRear shift down\nDisplay\nor\nAssist up\nAssist down\nDisplay/light"
    open val commonsGroupSw = "switch"
    open val customizeWarningMaintenanceAlert =
        "This maintenance alert setting causes alerts to be displayed. Continue?"
    open val customizeCommunicationModeSetting = "Wireless communication mode settings"
    open val customizeCommunicationModeCustomizeItemTitle =
        "Wireless communication mode (for cycle computers)"
    open val customizeCommunicationModeAntAndBle = "ANT/Bluetooth® LE mode"
    open val customizeCommunicationModeAnt = "ANT mode"
    open val customizeCommunicationModeBle = "Bluetooth® LE mode"
    open val customizeCommunicationModeOff = "OFF mode"
    open val customizeCommunicationModeGuideMsg =
        "Conserve the battery consumption amount by aligning the settings with the cycle computers communication system. If connecting to a cycle computer with a different communication system, reset the E-TUBE PROJECT.\nE-TUBE PROJECT for tablets and smartphones can be used with any configured communication system."
    open val presetCantSet = "The following cannot be set for {0}."
    open val presetWriteToBikeInvalidSettings = "{0} is connected."
    open val presetDuMuSettingNotice =
        "The settings of the drive unit do not match those of the motor unit. "
    open val presetDuSettingDifferent = "The drive unit is not set for {0}."
    open val commonsAssistLockChainTension = "Have you adjusted chain tension?"
    open val commonsAssistLockInternalGear =
        "When using an internal geared hub, it is necessary to adjust the chain tension."
    open val commonsAssistLockInternalGear2 =
        "Adjust the chain tension, and then press the Run button."
    open val commonsAssistLockCrank = "Have you checked the crank angle?"
    open val commonsAssistLockCrankManual =
        "The left crank must be installed to the axle at the correct angle. Check the angle of the installed crank, and then press the Run button."
    open val switchFunctionSettingDuplicationCheckMsg7 =
        "The same function cannot be assigned to multiple buttons."
    open val commonsMaintenanceAlertUnitKm = "km"
    open val commonsMaintenanceAlertUnitMile = "mile"
    open val commonsMaxAssistSpeedUnitKm = "km/h"
    open val commonsMaxAssistSpeedUnitMph = "mph"
    open val firmwareUpdateBlefwUpdateFailed = "The firmware update of {0} failed."
    open val firmwareUpdateShowFaq = "View detailed information"
    open val commonsMaintenanceAlert = "Maintenance alert"
    open val commonsOutOfSettableRange = "{0} is outside the allowable setting range."
    open val commonsCantWriteToUnit =
        "It is not possible to write to the {0} with the current settings."
    open val connectionScanModalText =
        "A connection cannot be made while using E-TUBE RIDE. Disconnect E-TUBE RIDE and the wireless unit in the application, and then try connecting again.\n Otherwise, set the wireless unit on the bicycle to Bluetooth® LE connection mode."
    open val connectionUsingEtubeRide = "When using E-TUBE RIDE."
    open val connectionScanModalTitle = "When using E-TUBE RIDE."
    open val firmwareUpdateCommunicationIsUnstable =
        "The wireless reception environment is unstable.\n Bring the wireless unit close to the device and improve the wireless reception environment."
    open val connectionQBleFirmwareRestore =
        "A faulty wireless unit was found.\n Restore the firmware?"
    open val commonsAutoGearChange = "Automatic gear shifting"
    open val firmwareUpdateBlefwUpdateRestoration =
        "Reconnect via Bluetooth® LE, and then proceed to the restore process."
    open val firmwareUpdateBlefwUpdateReconnectFailed =
        "Unable to reconnect Bluetooth® LE automatically after updating {0}."
    open val firmwareUpdateBlefwResotreReconnectFailed =
        "Unable to reconnect Bluetooth® LE automatically after restoring {0}."
    open val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Update complete. To continue use, first reconnect Bluetooth® LE."
    open val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Restore complete. To continue use, first reconnect Bluetooth® LE."
    open val connectionConnectionToOsConnectingUnitSucceeded =
        "E-TUBE PROJECT connected to {0} not displayed on the screen."
    open val connectionSelectYesToContinue =
        "To continue connection processing, select \"Confirm\"."
    open val connectionSelectNoToConnectOtherUnit =
        "To disconnect the current connection and connect to another unit, select \"Cancel\"."
    open val commonsCommunicationIsNotStable =
        "The wireless reception environment is unstable.\n Bring {0} close to the device to improve the wireless reception environment, and then change the settings again."
    //endregion

    companion object {
        fun fromLanguageCode(languageCode: LanguageCode): Localization {
            return when (languageCode) {
                LanguageCode.EN -> Localization()
                LanguageCode.FR -> LocalizationFR()
                LanguageCode.DE -> LocalizationDE()
                LanguageCode.NL -> LocalizationNL()
                LanguageCode.ES -> LocalizationES()
                LanguageCode.IT -> LocalizationIT()
                LanguageCode.ZH -> LocalizationZH()
                LanguageCode.JA -> LocalizationJA()
            }
        }

        /** ユニットログ用のLocalizationを返す */
        fun forUnitLog(languageCode: LanguageCode): Localization {
            return when (languageCode) {
                LanguageCode.ZH -> Localization()
                else -> fromLanguageCode(languageCode)
            }
        }

        /** FoxUnit用のLocalizationを返す */
        fun forFoxUnit(languageCode: LanguageCode): Localization {
            return when (languageCode) {
                LanguageCode.NL -> Localization()
                else -> fromLanguageCode(languageCode)
            }
        }
    }
}
