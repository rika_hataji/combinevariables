package openCSV.combineVariables.localization

import openCSV.combineVariables.localization.*

class LocalizationDE : Localization() {
    override val languageCode = LanguageCode.DE

    override val commonsConnectBle = "Bluetooth® LE-Verbindung"
    override val commonsOperation = "Arbeitsgangzähler"
    override val commonsBleSettings = "Bluetooth® LE-Einstellungen"
    override val commonsYes = "Ja"
    override val commonsNo = "Nein"
    override val commonsBikeType = "Fahrrad-Typ"
    override val commonsRoad10 = "RENNRAD10"
    override val commonsRoad11 = "RENNRAD11"
    override val commonsMtb = "MTB"
    override val commonsUrbancity = "URBAN/CITY"
    override val commonsUnitName = "Bezeichnung des Bauteils"
    override val commonsOnStr = "Ein"
    override val commonsOffStr = "Aus"
    override val commonsLumpShiftTimeSuperFast = "Extrem schnell"
    override val commonsLumpShiftTimeVeryFast = "Sehr schnell"
    override val commonsLumpShiftTimeFast = "Schnell"
    override val commonsLumpShiftTimeNormal = "Normal"
    override val commonsLumpShiftTimeSlow = "Langsam"
    override val commonsLumpShiftTimeVerySlow = "Sehr langsam"
    override val commonsRetry = "Wiederholen"
    override val commonsUse = "Verwendung"
    override val commonsNoUse = "Ersatz"
    override val commonsDone = "Beenden"
    override val commonsDecide = "Bestätigen"
    override val commonsCancel = "Abbrechen"
    override val commonsClose = "Schließen"
    override val commonsCategoryBmr = "Batteriehalter"
    override val commonsCategoryBtr2 = "Integrierte Batterie"
    override val commonsCategorySt = "Schalthebel"
    override val commonsCategorySw = "Schalter"
    override val commonsCategoryEw = "Mehrfachverbinder"
    override val commonsCategoryFd = "Vorderer Umwerfer"
    override val commonsCategoryRd = "Hinteres Schaltwerk"
    override val commonsCategoryMu = "Motoreinheit"
    override val commonsCategoryDu = "Antriebseinheit"
    override val commonsCategoryBt = "Akku"
    override val commonsCategoryCj = "Schalteinheit"
    override val commonsGroupMaster = "Mastereinheit"
    override val commonsGroupJunction = "kontaktstelle (A)"
    override val commonsGroupFshift = "Schalteinheit vorn"
    override val commonsGroupRshift = "Schalteinheit hinten"
    override val commonsGroupSus = "Federung Bedienschalter"
    override val commonsGroupStsw = "Schalthebel/schalter"
    override val commonsGroupEww = "Drahtloses Gerät"
    override val commonsGroupBattery = "Akku"
    override val commonsGroupPowerswitch = "Netzschalter"
    override val commonsGroupUnknown = "Unbekannte Einheit"
    override val commonsSusControlSw = "Federung Bedienschalter"
    override val commonsBleUnit = "Drahtloses Gerät"
    override val commonsPresetName = "Name der Voreinstellungsdatei"
    override val commonsComment = "Anmerkung"
    override val commonsOptional = "(optional)"
    override val commonsTargetUnitMayBeBroken = "{0} ist möglicherweise defekt."
    override val commonsElectricWires =
        "Prüfen Sie, ob sämtliche elektrische Leitungen ordnungsgemäß angeschlossen sind."
    override val commonsAlreadyRecognizedUnit = "Die Einheit wurde bereits erkannt."
    override val commonsNumberOfTeethFront = "Zähnezahl des vorderen Kettenblatts"
    override val commonsNumberOfTeethRear = "Zähnezahl des hinteren Ritzels"
    override val commonsMaxGear = "Maximalzahl der gänge:"
    override val commonsLamp = "Lichtanschluss"
    override val commonsAgree = "Ich stimme zu"
    override val commonsFirmwareUpdateTerminated =
        "Die Firmware-Aktualisierung von {0} wurde unterbrochen."
    override val commonsBeginFwRestoration = "Die Firmware von {0} ist wieder hergestellt."
    override val commonsPleaseConnectBle = "Stellen Sie eine Bluetooth® LE-Verbindung her."
    override val commonsDownLoadFailed = "Die Datei konnte nicht heruntergeladen werden."
    override val commonsQDeleteSetting = "Die Einstellungen gehen verloren. Fortfahren?"
    override val commonsDisplay = "Display"
    override val commonsSwitch = "Schalter"
    override val commonsSwitchA = "Schalter A"
    override val commonsSwitchX = "Schalter X"
    override val commonsSwitchY = "Schalter Y"
    override val commonsSwitchZ = "Schalter Z"
    override val commonsSwitchX1 = "Schalter X1"
    override val commonsSwitchX2 = "Schalter X2"
    override val commonsSwitchY1 = "Schalter Y1"
    override val commonsSwitchY2 = "Schalter Y2"
    override val commonsFirmer = "Firmer"
    override val commonsSofter = "Softer"
    override val commonsPosition = "Position {0}"
    override val commonsShiftCountPlural = "{0} Gänge"
    override val commonsLocked = "CLIMB(FIRM)"
    override val commonsUnlocked = "DESCEND(OPEN)"
    override val commonsFirm = "TRAIL(MEDIUM)"
    override val commonsSuspensionType = "Federungstyp"
    override val commonsSprinterSwitch = "Satelliten-Schalter"
    override val commonsMultiShiftMode = "Multi-Shift-Modus"
    override val commonsShiftMode = "Schaltmodus"
    override val commonsLogin = "Login"
    override val commonsSettingUsername = "Benutzer-ID"
    override val commonsSettingPassword = "Passwort"
    override val commonsUnexpectedError = "Ein unerwarteter Fehler ist aufgetreten."
    override val commonsAccessKeyAuthenticationError =
        "Die Login-Sitzung ist abgelaufen. Melden Sie sich bitte erneut an."
    override val commonsSoftwareLicenseAgreement = "Nutzungsbedingungen"
    override val commonsReInput = "Geben Sie diese erneut ein."
    override val commonsPerson = "Verantwortliche Person"
    override val commonsCompany = "Name des Unternehmens"
    override val commonsPhoneNumber = "Telefonnummer"
    override val commonsErrorNotice =
        "Es gibt ein Problem mit den eingegebenen Inhalt. Bitte prüfen Sie die unten aufgeführten Punkte."
    override val commonsFirmwareVersion = "Firmware-Version"
    override val commonsNotConnected = "Nicht verbunden"
    override val commonsBeep = "Signalgeräusch"
    override val commonsDisplayTime = "Anzeigenzeit"
    override val commonsTheSameMarkCtdCannotBeAssigned =
        "- Falls sich die Kombinationsmuster der vorderen und hinteren Aufhängung unterscheiden, ist es nicht möglich, die gleiche Markierung (CTD) zuzuweisen."
    override val commonsDifferentMarksCtdCannotBeAssigned =
        "- Wird das gleiche Kombinationsmuster für die vordere und hintere Aufhängung verwendet, ist es nicht möglich, unterschiedliche Markierungen (CTD) zuzuweisen."
    override val commonsInternetConnectionUnavailable =
        "Nicht mit dem Internet verbunden.\nStellen Sie eine Internetverbindung her und wiederholen Sie den Versuch."
    override val commonsNotAvailableApp =
        "Dies ist die Edition der App für {0}. \nSie kann auf einem {1} nicht verwendet werden."
    override val commonsNotAvailableAppLink = "Laden Sie die Edition der App für {0} herunter"
    override val commonsSmartPhone = "Smartphone"
    override val commonsTablet = "Tablet"
    override val commonsCycleComputerRight = "Fahrradcomputer rechts"
    override val commonsCycleComputerLeft = "Fahrradcomputer links"
    override val commonsError = "Fehler"
    override val commonsPleaseConnectAgain =
        "Falls die Einheit getrennt wurde, überprüfen Sie die Verbindung erneut."
    override val commonsPleasePerformErrorCheckIfNotDisconnected =
        "Falls alles ordnungsgemäß angeschlossen ist, wenden Sie sich an einen Händler."
    override val commonsPleasePerformErrorCheck = "Wenden Sie sich an einen Vertriebspartner."
    override val commonsErrorC =
        "Es ist kein Akku vorhanden oder der Akku ist nicht ausreichend geladen. \nBenutzen Sie einen ausreichend geladenen Akku oder laden Sie ihn auf und schließen Sie dann den Akku wieder an."
    override val commonsErrorIfNoSwitchResponse =
        "Wenn keiner der Schalter etwas bewirkt, prüfen Sie, ob Stromkabel getrennt wurden."
    override val commonsNetworkError = "Netzwerkfehler"
    override val commonsNext = "Weiter"
    override val commonsErrorOccurdDuringSetting = "Beim Speichern ist ein Fehler aufgetreten."
    override val commonsFrontShiftUp = "Vorn hochschalten"
    override val commonsFrontShiftDown = "Vorn herunterschalten"
    override val commonsRearShiftUp = "Hinten hochschalten"
    override val commonsRearShiftDown = "Hinten herunterschalten"
    override val commonsGroupFsus = "Vorderradgabel Federung"
    override val commonsErrorBattery =
        "Die verbleibende Akkukapazität konnte nicht bestätigt werden.\nStellen Sie sicher, dass die Mastereinheit und der Akku richtig angeschlossen sind."
    override val commonsUpdateCheckUpdateSetting =
        "Es wird nach Aktualisierungen für E-TUBE PROJECT gesucht."
    override val commonsApply = "Anwenden"
    override val commonsFinish = "Beenden"
    override val commonsDoNotUseProhibitionCharacter =
        "Verwenden Sie keine unzulässigen Zeichen ({1}) in {0}."
    override val commonsInputError = "Geben Sie {0} ein."
    override val commonsInputNumberError =
        "Verwenden Sie für die Eingabe von {0} Ziffern mit halber Breite."
    override val commonsDi2 = "DI2"
    override val commonsSteps = "STePS"
    override val commonsSwitchForAssist = "Schalter zum Ändern des Unterstützungsmodus"
    override val commonsUsinBleUnitError =
        "Während der Verwendung trat ein Fehler an der Einheit für drahtlose Signalübertragung auf."
    override val commonsCommunicationMode = "Drahtloskommunikationsmodus"
    override val errorBleDisconnect =
        "Die Bluetooth® LE-Verbindung wurde unterbrochen.\nVersuchen Sie, die Verbindung wieder herzustellen."
    override val errorOccurredAbnormalCommunication =
        "Ein Kommunikationsfehler ist aufgetreten. Versuchen Sie erneut, eine Verbindung herzustellen."
    override val commonsDrawerMenu = "Drawer-Menü"
    override val drawerMenuUnitList = "Einheitenliste"
    override val drawerMenuDisconnectBle = "Bluetooth® LE trennen"
    override val drawerMenuTutorial = "Lernprogramm"
    override val drawerMenuApplicationSettings = "Programmeinstellungen"
    override val drawerMenuLanguageSettings = "SprachenEinstellung"
    override val drawerMenuVersionInfo = "Versionsinfo"
    override val drawerMenuLogin = "Login"
    override val drawerMenuLogout = "Logout"
    override val drawerMenuChangePassword = "Passwort ändern"
    override val drawerMenuRegistered = "Abfrage der Benutzerdaten"
    override val commonsBetaMessageMsg1 =
        "Diese Software ist eine Betaversion.\nSie wurde noch nicht vollständig geprüft und kann daher verschiedene Probleme verursachen.\nOK?"
    override val commonsBetaMessageMsg2 = "Ablaufdatum der Software"
    override val commonsBetaMessageMsg3 = "Diese Software hat ihr Ablaufdatum überschritten."
    override val launchLicenseMsg1 =
        "Lesen Sie die Lizenzvereinbarung, bevor Sie das Programm verwenden."
    override val launchLicenseMsg2 =
        "Um fortzufahren, klicken Sie auf die Schaltfläche \"Ich stimme zu\", nachdem Sie die Vereinbarung gelesen haben. Sie müssen den Bedingungen der Vereinbarung zustimmen, um dieses Programm nutzen zu können."
    override val launchFreeSize =
        "Es ist nicht genügend Speicherplatz ({1} MB) vorhanden, um E-TUBE PROJECT auszuführen. \nDie benötigten Dateien werden beim nächsten Ausführen des Programms heruntergeladen. \nStellen Sie sicher, dass freier Speicherplatz von mindestens {0} MB verfügbar ist, bevor Sie das Programm das nächste Mal starten."
    override val connectionErrorRepairMsg2 = "{0} ist möglicherweise defekt."
    override val connectionErrorRepairMsg3 = "Wählen Sie die unerkannte Einheitengruppe."
    override val connectionErrorRepairMsg4 = "Eine Einheit ist eventuell defekt."
    override val connectionErrorRepairMsg6 = "Einheit von {0}"
    override val connectionErrorRepairMsg7 =
        "Die Firmware für diese Einheit wird wiederhergestellt."
    override val connectionErrorRepairMsg8 =
        "Die folgenden Einheiten sind ordnungsgemäß angeschlossen."
    override val connectionErrorUnknownUnit =
        "Es sind eine oder mehrere Einheiten angeschlossen, die nicht von der aktuellen Version von E-TUBE PROJECT unterstützt werden.\nInstallieren Sie die neueste App aus {0}."
    override val connectionErrorOverMsg1 =
        "Insgesamt können nur bis zu {1} Einheiten von {0} angeschlossen werden."
    override val connectionErrorOverMsg2 = "Angeschlossene Einheit"
    override val connectionErrorNoCompatibleUnitsMsg1 =
        "Es wurde eine inkompatible Kombination gefunden. Stellen Sie sicher, dass eine mit Ihrem Fahrrad-Typ kompatible Einheit angeschlossen ist, und wiederholen Sie den gesamten Vorgang."
    override val connectionErrorNoCompatibleUnitsMsg2 =
        "Möglicherweise können Sie dieses Problem lösen, indem Sie die Firmware aktualisieren und so eine erweiterte Kompatibilität erhalten. \nVersuchen Sie, die Firmware zu aktualisieren."
    override val connectionErrorNoCompatibleUnitsLink =
        "Detailliertere Kompatibilitätstabelle anzeigen"
    override val connectionErrorFirmwareBroken =
        "Die Firmware von {0} funktioniert möglicherweise nicht korrekt.\nDie Firmware wird wiederhergestellt."
    override val connectionDialogMessage1 =
        "Der {0} für Unterstützungsmodus ist nicht verbunden.\nWenn die Zuweisung der Schalterfunktion durchgeführt wird, kann die Zuweisung von {1} von Gangwahl auf Unterstützungsmodus umgestellt werden."
    override val connectionDialogMessage3 =
        "{0} wird für Federung eingestellt.\nÄndern Sie die Einstellung."
    override val connectionDialogMessage4 =
        "Die Einstellung \"{1}\" für {0} funktioniert bei dem verwendeten Fahrrad nicht.\nBitte ändern Sie die Einstellung."
    override val connectionDialogMessage5 =
        "Das System wird mit der erkannten Kombination von Einheiten nicht normal funktionieren.\nSchließen Sie die benötigten Einheiten an und überprüfen Sie die Verbindungen erneut, damit das System ordnungsgemäß funktionieren kann. Fortfahren?"
    override val connectionDialogMessage6 = "Benötigte Einheit (eine der folgenden)"
    override val connectionDialogMessage7 =
        "Das System wird mit der erkannten Kombination von Einheiten nicht normal funktionieren.\nSchließen Sie die benötigten Einheiten an und überprüfen Sie die Verbindungen erneut, damit das System ordnungsgemäß funktionieren kann."
    override val connectionDescription1 =
        "Stellen Sie die Einheit am Fahrrad auf Bluetooth® LE-Verbindungsmodus ein."
    override val connectionDescription2 =
        "Wählen Sie die Einheit für drahtlose Signalübertragung, die verbunden werden soll."
    override val connectionWhatIsPairing =
        "Der Verbindungsmodus an der Einheit für drahtlose Signalübertragung wird aktiviert."
    override val connectionDisconnectDescription =
        "Wählen Sie zum Trennen die Option \"Bluetooth® LE trennen\" im Menü rechts oben."
    override val connectionBluetoothInitialPasskey =
        "Diese App verwendet für die drahtlose Datenübertragung  Bluetooth® LE.\nDer erste PassKey für Bluetooth® LE ist \"000000\"."
    override val connectionPasskeyMessage =
        "Ändern Sie den ursprünglichen PassKey.\nAnsonsten besteht die Gefahr, dass ein Dritter eine Verbindung herstellt."
    override val connectionPasskey = "PassKey"
    override val connectionPasswordConfirm = "PassKey (zur Bestätigung)"
    override val connectionPleaseInputPassword = "Geben Sie Ihr PassKey ein."
    override val connectionDisablePassword =
        "Authentifizierung nicht möglich. Der eingegebene PassKey ist falsch oder der PassKey für die Funkeinheit wurde geändert. Stellen Sie die Verbindung wieder her und versuchen Sie, den in der Funkeinheit festgelegten PassKey erneut einzugeben."
    override val connectionValidityPassword =
        "0 kann nicht als erstes Zeichen im PassKey verwendet werden."
    override val connectionBleFirmwareRestore =
        "An der Einheit für drahtlose Signalübertragung wird die Firmware wiederhergestellt."
    override val connectionBleFirmwareRestoreFailed =
        "Schließen Sie die Einheit wieder an.\nFalls der Fehler weiterhin auftritt, verwenden Sie die PC-Version von E-TUBE PROJECT."
    override val connectionBleWeakWaves =
        "Schlechte drahtlose Verbindung \nDie Bluetooth® LE-Verbindung wurde möglicherweise unterbrochen."
    override val connectionBleNotRequiredRestore =
        "Die Einheit für drahtlose Signalübertragung funktioniert ordnungsgemäß.\nEine Wiederherstellung der Firmware ist nicht erforderlich."
    override val connectionFoundNewBleFirmware =
        "Es wurde eine neue Firmware für die Einheit für drahtlose Signalübertragung gefunden.\nDie Aktualisierung wird gestartet."
    override val connectionScanReload = "Erneut laden"
    override val connectionUnmatchPassword = "Der PassKey stimmt nicht überein."
    override val connectionBikeTypeDi2Type1 = "RENNRAD"
    override val connectionBikeTypeDi2Type2 = "MTB"
    override val connectionBikeTypeDi2Type3 = "URBAN/CITY"
    override val connectionBikeTypeEbikeType1 = "MTB"
    override val connectionNeedPairingBySetting =
        "Stellen Sie die Einheit für drahtlose Signalübertragung für die Verbindung ein und führen Sie die Kopplung unter [Einstellungen] > [Bluetooth] am Gerät durch."
    override val connectionSwE6000Recognize =
        "Halten Sie einen der von {1} in {0} eingestellten Schalter gedrückt."
    override val commonsGroupDU = "Antriebseinheit"
    override val commonsGroupSC = "Fahrradcomputer"
    override val commonsGroupRSus = "Hinterrad-Federung"
    override val commonsGroupForAssist = "Schalter für Antriebsunterstützung"
    override val commonsGroupForShift = "Schalter für Gangschaltung"
    override val connectionMultipleChangeShiftToAssist =
        "{0} {1} ist verbunden. \nDer aktuelle Fahrrad-Typ unterstützt nur {2}. \nMöchten Sie alle {0} auf {2} umstellen?"
    override val connectionNewFirmwareFileNotFound =
        "Die Firmwaredatei ist nicht auf dem aktuellsten Stand."
    override val commonsGroupMasterCap = "Mastereinheit"
    override val commonsGroupJunctionCap = "Kontaktstelle (A)"
    override val commonsGroupFshiftCap = "Schalteinheit vorn"
    override val commonsGroupRshiftCap = "Schalteinheit hinten"
    override val commonsGroupSusCap = "Federung Bedienschalter"
    override val commonsGroupStswCap = "Schalthebel/schalter"
    override val commonsGroupFsusCap = "Vorderradgabel Federung"
    override val commonsGroupRsusCap = "Hinterrad-Federung"
    override val commonsGroupDuCap = "Antriebseinheit"
    override val commonsGroupForassistforshiftCap =
        "Schalter für Unterstützung/schalthebel für Gangschaltung"
    override val commonsGroupForassistforshift =
        "schalter für Unterstützung/schalthebel für Gangschaltung"
    override val connectionErrorNoSupportedMaster =
        "{0} wird nicht von dieser Anwendung unterstützt.\n{0} kann mit der PC-Version von E-TUBE PROJECT verwendet werden."
    override val connectionErrorNoSupportedMaster2 = "Nicht kompatibel."
    override val connectionErrorUnknownMaster = "Eine unbekannte Mastereinheit ist angeschlossen."
    override val connectionErrorPleaseConfirmMasterUnit = "Überprüfen Sie die Mastereinheit."
    override val connectionErrorUnSupportedMsg1 =
        "Eine Einheit, die eine Bluetooth® LE Verbindungnicht unterstützt, wurde angeschlossen.\nTrennen Sie alle nachstehenden Einheiten und schließen Sie sie erneut an."
    override val connectionErrorPcLinkageDevice = "PC-Verbindungskabel"
    override val connectionErrorUnSupportedMsg2 =
        "Es sind mehrere Einheiten für drahtlose Signalübertragung angeschlossen.\nTrennen Sie alle nachstehenden Einheiten  bis auf eine und stellen Sie die Verbindung erneut her."
    override val connectionErrorBroken =
        "Das Programm konnte sich nicht mit E-TUBE PROJECT verbinden, da eine der angeschlossenen Einheiten möglicherweise defekt ist. \nWenden Sie sich an Ihren Vertriebspartner oder Händler."
    override val connectionChargeMsg1 = "Überprüfen Sie den Zustand des Akkus."
    override val connectionChargeMsg2 =
        "Benutzung während des Ladevorgangs nicht möglich. Stellen Sie die Verbindung erneut her, nachdem der Ladevorgang abgeschlossen ist."
    override val connectionChargeMsg3 =
        "Wenn der Akku nicht lädt, ist er möglicherweise defekt. \nWenden Sie sich an einen Vertriebspartner."
    override val connectionChargeRdMsg =
        "Ladevorgang angehalten. Um den Ladevorgang wieder zu starten, von der App trennen und das Ladekabel wieder anschließen."
    override val connectionBikeTypePageTitle = "Auswahl des Fahrrad-Typs"
    override val connectionBikeTypeMsg =
        "Der Fahrrad-Typ konnte nicht erkannt werden. Wählen Sie den Fahrrad-Typ aus."
    override val connectionBikeTypeTitle1 = "DI2-System"
    override val connectionBikeTypeTitle2 = "E-bike system"
    override val connectionBikeTypeError =
        "Der Fahrrad-Typ für die zu speichernden Einstellungen weicht vom Fahrrad-Typ für die angeschlossene Einheit ab. \nÜberprüfen Sie vor dem Speichern der Einstellungen die Einstellungen oder die angeschlossene Einheit."
    override val connectionDialogMessage9 = "{0} für den Unterstützungsmodus ist verbunden."
    override val connectionDialogMessage12 =
        "Der aktuelle Fahrrad-Typ unterstützt nur die Einstellung der Gangschaltung."
    override val connectionDialogMessage13 = "{0} für Gangschaltung einstellen?"
    override val connectionDialogMessage14 = "Alle {0} für Gangschaltung einstellen?"
    override val connectionDialogMessage19 =
        "Die Firmware  des verwendeten Gerätes ist möglicherweise veraltet.\nStellen Sie eine Verbindung mit dem Internet her und wählen Sie \"Alles herunterladen\" aus \"Versionsinfo\", um die neueste Firmware  herunterzuladen."
    override val connectionChangeShiftToAssist =
        "{0} {1} ist verbunden. \nDer aktuelle Fahrrad-Typ unterstützt nur {2}. \nMöchten Sie {0} auf {2} umstellen?"
    override val connectionDialogMessage11 = "Die folgende Einheit kann nicht verwendet werden."
    override val connectionDialogMessage16 =
        "Aktualisieren Sie das Programm auf die neueste Version und versuchen Sie es erneut."
    override val connectionDialogMessage15 = "Entfernen Sie die Einheit."
    override val connectionDialogMessage17 =
        "Die folgende Einheit unterstützt das Betriebssystem Ihres Smartphones oder Tablets nicht."
    override val connectionDialogMessage18 =
        "Zum Einsatz mit dem Programm wird empfohlen, dass Sie das Betriebssystem Ihres Smartphones oder Tablets auf die neueste Version aktualisieren."
    override val connectionSprinterMsg1 = "Verwenden Sie den Sprintschalter für {0}?"
    override val connectionSprinterMsg2 =
        "Es sind mehrere {0} angeschlossen.\nAlle Sprinter-Schalter verwenden?"
    override val commonsSelect = "Wählen"
    override val connectionSprinterContinue = "Die Auswahl fortsetzen?"
    override val connectionAppVersion = "Ver.{0}"
    override val commonsVersionInfoEtubeVersion = "E-TUBE PROJECT Version"
    override val commonsVersionInfoBleFirmwareVersion = "Bluetooth® LE Firmware-Version"
    override val commonsVersionInfoMessage1 =
        "Die Prüffunktion verwendet die Einheit für drahtlose Signalübertragung und E-TUBE PROJECT , um Leistungsbrüche und Systemfehler zu erkennen. Sie kann nicht an allen Einheiten Probleme und Betriebsschwankungen erkennen.\nKontaktieren Sie den Ort des Kaufs oder einen Händler."
    override val commonsVersionInfoMessage2 =
        "Eine neue Version des E-TUBE PROJECT ist verfügbar, jedoch wird Ihr Betriebssystem nicht unterstützt. Für weitere Informationen zu unterstützten Betriebssystemen, siehe die SHIMANO-Webseite."
    override val commonsVersionInfoUpdateCheck = "Alles herunterladen"
    override val settingMsg3 =
        "Stellen Sie sicher, dass der Bildschirm zum Ändern des ursprünglichen PassKey angezeigt wird."
    override val settingMsg5 =
        "Überprüfen Sie die neuesten Programme auf unterschiedliche Betriebssysteme."
    override val settingMsg6 =
        "Stellen Sie sicher, dass die animierte Anleitung für den Schaltmodus angezeigt wird."
    override val settingMsg4 =
        "Stellen Sie sicher, dass die animierte Anleitung für Multi-Shift angezeigt wird."
    override val settingMsg1 = "Überprüfen sie das Firmware-Update während der Voreinstellung."
    override val settingMsg2 = "Für den Proxy-Server ist eine Authentifizierung erforderlich."
    override val settingServer = "Server"
    override val settingPort = "Anschluss"
    override val settingUseProxy = "Verwenden Sie einen Proxy-Server"
    override val settingUsername = "Benutzer-ID"
    override val settingPassword = "Passwort"
    override val settingFailed = "Beim Speichern der Einstellungen trat ein Fehler auf."
    override val commonsEnglish = "Englisch"
    override val commonsJapanese = "Japanisch"
    override val commonsChinese = "Chinesisch"
    override val commonsFrench = "Französisch"
    override val commonsItalian = "Italienisch"
    override val commonsDutch = "Niederländisch"
    override val commonsSpanish = "Spanisch"
    override val commonsGerman = "Deutsch"
    override val commonsLanguageChangeComplete =
        "Die Spracheinstellung wurde geändert. \nDie Sprache wird erst nach Beenden und Neustarten des Programms geändert."
    override val commonsDiagnosisResult3 =
        "Ersetzen oder entfernen Sie die folgende Einheit und stellen Sie die Verbindung erneut her."
    override val connectionErrorSelectNoDetectedMsg = "Wählen Sie eine unerkannte Einheit."
    override val connectionErrorSelectNoDetectedAlert =
        "Wenn Sie die falsche Einheit auswählen, wird möglicherweise die falsche Firmware auf die Einheit gespeichert, wodurch sie funktionsunfähig werden kann. Stellen Sie sicher, dass Sie die richtige Einheit ausgewählt haben, und fahren Sie dann mit dem nächsten Schritt fort."
    override val commonsCheckElectricWires2 = "{0} wird nicht erkannt."
    override val commonsDiagnosisCheckMsg = "{0} wird erkannt.\nIst {0} die angeschlossene Einheit?"
    override val commonsUpdateCheckUpdateError =
        "Verbindung mit dem Server kann nicht verifiziert werden. \nStellen Sie eine Internetverbindung her und wiederholen Sie den Versuch."
    override val commonsUpdateCheckWebServerError =
        "Die Internet-Verbindung oder der Shimano-Webserver könnte ein Problem haben.\nWarten Sie eine Weile und versuchen Sie es erneut."
    override val commonsUpdateCheckUpdateSettingIOs = "Lesevorgang läuft."
    override val firmwareUpdateRecognizeUnitDialogMsg =
        "Halten Sie einen der {0}-Schalter für das Bauteil gedrückt, deren Firmware Sie aktualisieren wollen."
    override val customizeSwitchFunctionDialogMsg1 =
        "Halten Sie einen der {0}-Schalter für das einzustellende Bauteil gedrückt."
    override val connectionSprinterRecognize =
        "Halten Sie einen der {0}-Schalter des Bauteils, das Sie wählen möchten, gedrückt."
    override val commonsConfirmPressedSwitch = "Haben Sie den Schalter betätigt?"
    override val connectionSprinterRelease =
        "OK. Nehmen Sie Ihre Hand vom Schalter. \n\nFalls dieses Dialogfeld nicht ausgeblendet wird, nachdem Sie Ihre Hand vom Schalter genommen haben, ist möglicherweise ein Schalter defekt. Sie in diesem Fall wenden Sie sich an einen Vertriebspartner."
    override val commonsLeft = "Links"
    override val commonsRight = "Rechts"
    override val commonsUpdateCheckIsInternetConnected =
        "Die Datei wird aktualisiert. \nIst Ihr Gerät mit dem Internet verbunden?"
    override val commonsUpdateCheckUpdateFailed = "Aktualisierung fehlgeschlagen."
    override val commonsUpdateFailed =
        "Eine oder mehrere Dateien, die für die Ausführung von E-TUBE PROJECT erforderlich sind, konnten nicht gefunden werden. \nDeinstallieren Sie E-TUBE PROJECT und installieren Sie es anschließend erneut."
    override val customizeBleBleName = "Name der Einheit für drahtlose Signalübertragung"
    override val customizeBlePassKeyRule = "(6 alphanumerisch Zeichen mit halber Breite)"
    override val customizeBlePassKeyDisplay = "Anzeigen"
    override val customizeBlePassKeyPlaceholder = "Geben Sie diese erneut ein."
    override val customizeBleErrorBleName =
        "Verwenden Sie für die Eingabe 1 bis 8 alphanumerische Zeichen mit halber Breite."
    override val customizeBleErrorPassKey =
        "Verwenden Sie für die Eingabe maximal 6 halb-breite Ziffern."
    override val customizeBleErrorPassKeyConsistency = "Der PassKey stimmt nicht überein."
    override val customizeBleAlphanumeric = "halb-breite alphanumerische Zeichen"
    override val customizeBleHalfWidthNumerals = "Halb-breiten Ziffern"
    override val commonsCustomize = "Anpassen"
    override val customizeFunctionDriveUnit = "Antriebseinheit"
    override val customizeFunctionNonUnit = "Es ist keine anpassbare Einheit vorhanden."
    override val customizeCadence = "Trittfrequenz"
    override val customizeAssistSetting = "Unterstützen"
    override val commonsAssistPattern = "Unterstützungsmuster"
    override val commonsElectricType = "Elektrisch"
    override val commonsExteriorTransmission = "Kettenschaltung(chain)"
    override val commonsMechanical = "Mechanisch"
    override val commonsAssistPatternDynamic = "DYNAMIC"
    override val commonsAssistPatternExplorer = "EXPLORER"
    override val commonsAssistModeHigh = "HOCH"
    override val commonsAssistModeMedium = "MEDIUM"
    override val commonsAssistModeLow = "LOW"
    override val commonsRidingCharacteristic = "Fahrcharakteristik "
    override val commonsFunctionRidingCharacteristic = "Fahrcharakteristik "
    override val commonsAssistPatternCustomize = "CUSTOMIZE"
    override val commonsAssistModeBoost = "BOOST"
    override val commonsAssistModeTrail = "TRAIL"
    override val commonsAssistModeEco = "ECO"
    override val customizeSceKm = "m\nkg"
    override val customizeSceMile = "y\nlb"
    override val commonsLanguage = "Sprache"
    override val customizeDisplayNowTime = "Aktuelle uhrzeit"
    override val customizeDisplaySet = "Einstellen"
    override val customizeDisplayDontSet = "Nicht einstellen"
    override val commonsDisplayLight = "Anzeige/Licht"
    override val customizeSwitchSettingViewControllerMsg1 = "Schalthebel"
    override val customizeSwitchSettingViewControllerMsg8 = "Verwenden Sie {0}"
    override val customizeSwitchSettingViewControllerMsg2 = "Zur Einstellung der Federung wechseln"
    override val customizeSwitchSettingViewControllerMsg3 = "Über Synchronized shift"
    override val customizeSwitchSettingViewControllerMsg11 =
        "Der Verbindungsstatus hat sich geändert. Trennen oder ändern Sie die Einheiten nicht, während die Einstellungen konfiguriert werden. \nWiederholen Sie den gesamten Vorgang."
    override val customizeSwitchSettingViewControllerMsg4 =
        "Die aktuell ausgewählten Informationen gehen verloren. \nZur Einstellung der Federung wechseln?"
    override val customizeSwitchSettingViewControllerMsg9 = "Was ist Synchronized shift?"
    override val customizeSwitchSettingViewControllerMsg5 =
        "Synchronized shift schaltet den vorderen Umwerfer automatisch synchron mit Hinten hochschalten und Hinten herunterschalten."
    override val customizeSwitchSettingViewControllerMsg6 =
        "Die folgenden Funktionen sind nicht eingeschlossen. Mit dem Verarbeiten fortfahren?"
    override val customizeSwitchSettingViewControllerMsg7 = "Taste {0}"
    override val customizeSwitchSettingViewControllerMsg10 =
        "Schalter {0} in Rückwärtsbetrieb stellen"
    override val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift ist eine Einstellung, mit der beim Schalten des Umwerfers automatisch auch das Schaltwerk geschaltet wird, um einen optimalen Gangwechsel zu erzielen."
    override val customizeSwitchSettingViewControllerMsg13 = "Die Schaltpositionen sind wählbar."
    override val customizeSwitchSettingViewControllerMsg14 =
        "Nun kann eine Schaltstellung des Schaltwerks von 0 bis 4 gewählt werden. (Je nach der Kombination ist die Schaltstellung möglicherweise nicht wählbar.)"
    override val commonsSwitchType = "Schaltermodus"
    override val commonsAssistUp = "Unterstützung erhöhen"
    override val commonsAssistDown = "Unterstützung reduzieren"
    override val customizeSwitchModeChangeAssist =
        "Ändern Sie den Modus dieses Schalters auf [für Unterstützung].\nFortfahren?"
    override val customizeSwitchModeChangeShift =
        "Ändern Sie den Modus dieses Schalters auf [für Gangschaltung].\nFortfahren?"
    override val customizeSwitchModeLostAssist =
        "Wenn der Schalter auf [für Gangschaltung] geändert wird, verschwindet der Schalter für die Unterstützung.\nZum Ändern fortfahren?"
    override val commonsDFlyCh1 = "D-FLY Kan 1"
    override val commonsDFlyCh2 = "D-FLY Kan 2"
    override val commonsDFlyCh3 = "D-FLY Kan 3"
    override val commonsDFlyCh4 = "D-FLY Kan 4"
    override val commonsDFly = "D-FLY"
    override val commonsFunction = "Funktion"
    override val customizeGearShifting = "Schalten"
    override val customizeManualSelect = "Schalter manuell auswählen"
    override val commonsUnit = "Einheit"
    override val customizeScSetting = "Fahrradcomputer"
    override val customizeSusSusTypeTitle = "Federung Bedienschalter"
    override val customizeSusSusPositionInform =
        "Auf welchem Griff ist der Bedienschalter installiert?"
    override val customizeSusSusPositionLeft = "Linke Seite"
    override val customizeSusSusPositionRight = "Rechte Seite"
    override val customizeSusPosition = "Position"
    override val customizeSusFront = "Vorn"
    override val customizeSusRear = "Hinten"
    override val customizeFooterMsg1 = "Standardwert\nwiederherstellen"
    override val customizeFooterMsg2 = "Stellt die Vorgabeeinstellungen {0} wieder her"
    override val customizeSusConsistencyCheckMsg1 = "Bitte überprüfen Sie die folgende Nachricht."
    override val customizeSusConsistencyCheckMsg6 =
        "Ist es OK, um mit der Programmierung fortzufahren?"
    override val customizeSusConsistencyCheckMsg3 =
        "- Die gleiche Einstellung wurde für zwei oder mehr Positionen gewählt."
    override val customizeSusConsistencyCheckMsg2 = "- Nur {1} wurde für {0} eingestellt."
    override val customizeSusConsistencyCheckMsg4 =
        "- Die gleiche Einstellung wurde für zwei oder mehr Positionen im CTD angewendet."
    override val customizeSusClimb = "CLIMB(FIRM)"
    override val customizeSusDescend = "DESCEND(OPEN)"
    override val customizeSusTrail = "TRAIL(MEDIUM)"
    override val customizeSusSwVerCheck =
        "Schalter kann nicht auf Federung gestellt werden, da die Firmware-Version von {0} niedriger ist als {1}.\nUm den Schalter auf Federung zu stellen, stellen Sie eine Verbindung zu einem Netzwerk her und führen eine Aktualisierung auf die neueste Version ({1} oder später) durch."
    override val customizeSusSusConnectMsg1 =
        "Eine der folgenden Einheiten wird benötigt, um den Schalter auf Federung zu stellen."
    override val customizeSusSusConnectMsg2 =
        "Schließen Sie die benötigte Einheit wieder an und versuchen Sie es erneut."
    override val customizeSusSusConnectMsg3 =
        "Eine Umstellung auf Schalteinstellung ist ebenfalls möglich."
    override val customizeSusSusConnectMsg4 =
        "Um auf Schalteinstellung umzustellen, [Ja] drücken. Um die gewünschten Einheiten anzuschließen ohne die Einstellung zu verändern, [Nein] drücken."
    override val customizeSusSusModeSelect =
        "Die Einstellung \"→(T)\" funktioniert nicht bei der verwendeten Federung. \nSie können die Einstellung \"→(T)\" über den Einstellungsbildschirm verbergen. \n\"→(T)\" verbergen?"
    override val customizeSusSwitchShift = "Zur Schalteinstellung wechseln"
    override val customizeSusTransitionShift =
        "Die aktuell ausgewählten Informationen gehen verloren. \nZur Einstellung der Schaltung wechseln?"
    override val customizeMuajstTitle = "Einstellen von Umwerfer und Schaltwerk"
    override val customizeMuajstAdjustment = "Einstellung"
    override val customizeMuajstGearPosition = "Wechsel des Ganges"
    override val customizeMuajstInfo1 = "Die Schalter am Fahrrad funktionieren nicht."
    override val customizeMuajstInfo2 =
        "Hier finden Sie weitere Informationen über die Einstellmethode."
    override val customizeMultiShiftModalTitle = "Multi-Shift-Geschwindigkeitseinstellung"
    override val customizeMultiShiftModalDescription =
        "Für die Initialeinstellungen werden Standardwerte verwendet. \nWenn Sie sich sicher sind, dass Sie das Konzept von Multi-Shift verstehen, wählen Sie die Multi-Shift-Einstellungen für die Bedingungen, unter denen Sie das Fahrrad benutzen (Gelände, Fahrstil usw.)."
    override val customizeMultiShiftModalVeryFast = "Sehr schnell"
    override val customizeMultiShiftModalFast = "Schnell"
    override val customizeMultiShiftModalNormal = "Normal"
    override val customizeMultiShiftModalSlow = "Langsam"
    override val customizeMultiShiftModalVerySlow = "Sehr langsam"
    override val customizeMultiShiftModalFeatures = "Merkmale"
    override val customizeMultiShiftModalFeature1 =
        "Schneller Multi-Shift-Modus ist nun aktiviert. \n\n・Diese Funktion ermöglicht eine schnelle Anpassung der Kurbel-RPM auf veränderte Fahrbedingungen. \n・Sie ermöglicht eine schnelle Anpassung Ihrer Geschwindigkeit."
    override val customizeMultiShiftModalFeature2 =
        "Ermöglicht einen zuverlässigen Multi-Shift-Betrieb"
    override val customizeMultiShiftModalNoteForUse = "Sicherheitshinweise"
    override val customizeMultiShiftModalUse1 =
        "1. Es besteht die Gefahr des Überschaltens. \n\n2. Bei zu niedriger Kurbel-RPM kann die Kette nicht mit der Bewegung des hinteren Schaltwerks mithalten. \nAls Folge kann die Kette vom Kettenblatt abspringen, anstatt in den Kassettenzahnkranz einzugreifen."
    override val customizeMultiShiftModalUse2 = "Der Multi-Shift-Betrieb dauert einen Moment"
    override val customizeMultiShiftModalCrank =
        "Benötigte Kurbel-RPM für den Einsatz von Multi-Shift"
    override val customizeMultiShiftModalCrank1 = "Bei hoher Kurbel-RPM"
    override val customizeMultiShiftModeViewControllerMsg3 = "Intervall des Gangwechsels"
    override val customizeMultiShiftModeViewControllerMsg4 = "Limit der Gänge"
    override val commonsLumpShiftUnlimited = "keine beschränkung"
    override val commonsLumpShiftLimit2 = "2 gänge"
    override val commonsLumpShiftLimit3 = "3 gänge"
    override val customizeMultiShiftModeViewControllerMsg5 = "Anderer Schalthebel"
    override val customizeShiftModeRootMsg1 =
        "Zum Einstellen des Schaltmodus ist jeweils ein des unter (1) und (2) genannten Geräte erforderlich."
    override val customizeShiftModeRootMsg3 =
        "Schließen Sie die benötigte Einheit wieder an und versuchen Sie es erneut."
    override val customizeShiftModeRootMsg4 =
        "Zum Einstellen des Schaltmodus wird eines der folgenden Geräte benötigt."
    override val customizeShiftModeRootMsg5 =
        "Auch wenn im Schaltmodus Einstellungen vorgenommen werden, so werden diese nur wirksam, wenn die Firmware-Version von {0} {1} oder höher ist."
    override val customizeShiftModeRootMsg6 = "Die Firmware-Version ist {0} oder höher"
    override val customizeShiftModeSettingMsg1 = "Löschen?"
    override val customizeShiftModeSettingMsg2 =
        "Die aktuellen Einstellungen des Fahrrads gehen verloren. Fortfahren?"
    override val customizeShiftModeSettingMsg3 =
        "Die Einstellungen können nicht konfiguriert werden, da die Einheit nicht korrekt angeschlossen ist."
    override val customizeShiftModeSettingMsg4 =
        "Das Fahrrad hat eine Gangstruktur, die nicht konfiguriert werden kann. Überprüfen Sie die Einstellungen."
    override val customizeShiftModeSettingMsg5 =
        "Einstellwert kann in Bestandsdatei {0} nicht gelesen werden. \n{0} wird gelöscht."
    override val customizeShiftModeSettingMsg7 = "Welche Funktion möchten Sie zuordnen?"
    override val customizeShiftModeSettingMsg8 =
        "Der Name der Einstellungsdatei ist bereits vergeben."
    override val customizeShiftModeTeethMsg1 =
        "Wählen Sie die Zähneanzahl des Ritzels/Kettenblatts für Ihr aktuelles Fahrrad."
    override val customizeShiftModeGuideMsg1 =
        "Um die Einstellungsdatei an Ihrem Gerät zum Überschreiben der Fahrradeinstellungen zu verwenden, ziehen Sie die Datei in den Slot (S1 oder S2), der überschrieben werden soll."
    override val customizeShiftModeGuideMsg2 =
        "Das Symbol über den Einstellungen zeigt den Schaltmodus an."
    override val customizeShiftModeGuideMsg3 =
        "Synchronized shift ist eine Funktion, mit der vordere Umwerfer automatisch synchron zum hinteren Schaltwerk umgeschaltet wird. Sie können die Funktionsweise von Synchronized shift durch Auswahl von Punkten auf der Karte konfigurieren."
    override val customizeShiftModeGuideMsg4 =
        "Bewegen Sie den grünen Cursor zum Einstellen von AUF (von innen nach außen) und den blauen Cursor zum Einstellen von AB (von außen nach innen)."
    override val customizeShiftModeGuideMsg5 =
        "Synchronisationspunkte \nPunkte, an denen der vordere Umwerfer im Zusammenspiel mit dem hinteren Schaltwerk schaltet. \nDie Anforderungen für die Pfeile, die auf die Synchronisationspunkte hinweisen, sind wie folgt: \nAUF: Nach oben oder zur Seite weisend \nAB: Nach unten oder zur Seite weisend"
    override val customizeShiftModeGuideMsg6 =
        "Verfügbare Übersetzungen \nAUF: Sie können jede Übersetzung bis zu diejenigen wählen, die unter der Übersetzung des Synchronisationspunktes liegt. \nAB: Sie können jede Übersetzung bis zu diejenigen wählen, die über der Übersetzung des Synchronisationspunktes liegt."
    override val customizeShiftModeTeethMsg6 =
        "Wird die Kurbelgarnitur (FC) von {0} auf {1} geändert, werden alle eingestellten Schaltpunkte auf Standardwerte zurückgesetzt.\nÄnderung vornehmen?"
    override val customizeShiftModeTeethDouble = "2-fach"
    override val customizeShiftModeTeethTriple = "3-fach"
    override val customizeShiftModeTeethMsg2 = "Synchronized shift-Intervall"
    override val customizeShiftModeTeethSettingTitle = "Anzahl der Zähne wählen"
    override val customizeShiftModeGuideTitle1 = "Synchronized shift"
    override val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    override val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    override val customizeShiftModeSynchroMsg1 =
        "Der Schaltpunkt liegt im nicht einstellbaren Bereich und funktioniert nicht normal."
    override val customizeShiftModeTeethMsg5 = "Name der Einstellungsdatei"
    override val customizeShiftModeBtn1 = "Auf"
    override val customizeShiftModeBtn2 = "Ab"
    override val customizeShiftModeTeethMsg8 = "Steuerung der gangstellung"
    override val customizeShiftModeSettingMsg9 =
        "Die nachfolgenden Informationen werden ebenfalls sowohl für S1 als auch S2 aktualisiert."
    override val customizeShiftModeTeethInward1 = "Hinten auf bei vorne ab"
    override val customizeShiftModeTeethOutward1 = "Hinten ab bei vorne auf"
    override val customizeShiftModeTeethInward2 = "Hinten hochschalten\nbeim Herunterschalten vorn"
    override val customizeShiftModeTeethOutward2 = "Hinten herunterschalten\nbeim Hochschalten vorn"
    override val customizeShiftModeTypeSelectTitle = "Auswahl der Funktionen zum Zuordnen"
    override val customizeShiftModeSettingMsg10 = "Mit dem Verarbeiten fortfahren?"
    override val customizeShiftModeTeethMsg7 =
        "Wählen Sie die Zähneanzahl für Ihr aktuelles Fahrrad. \n*Fest als {0} angeschlossen."
    override val commonsErrorCheck = "Störungsdiagnose"
    override val commonsAll = "Alles"
    override val errorCheckResultError = "{0} ist möglicherweise defekt."
    override val errorCheckContactDealer =
        "Wenden Sie sich an den Einzelhändler oder den Vertriebspartner."
    override val commonsStart = "Start"
    override val commonsUpdate = "Firmware aktualisieren"
    override val firmwareUpdateUpdateAll = "Alles aktualisieren"
    override val firmwareUpdateCancelAll = "Alles löschen"
    override val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Stellen Sie eine Internetverbindung her und prüfen Sie, ob Aktualisierungen für E-TUBE PROJECT oder Produktversionen verfügbar sind. \nDurch eine Aktualisierung auf die neueste Version können Sie neue Produkte und Funktionen verwenden."
    override val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "Das Programm oder die Firmware ist möglicherweise veraltet. \nStellen Sie eine Internetverbindung her und suchen Sie nach der neuesten Version."
    override val firmwareUpdateConnectionLevelIsLow =
        "Aktualisierung aufgrund der schlechten drahtlosen Verbindung nicht möglich. \nAktualisieren Sie die Firmware, nachdem Sie eine gute Verbindung hergestellt haben."
    override val firmwareUpdateIsUpdatingFw = "Die Firmware für {0} wird aktualisiert."
    override val firmwareUpdateNotEnoughBattery =
        "Die Aktualisierung der Firmware wird einige Minuten in Anspruch nehmen. \nWenn der Ladestand des Akkus für Ihr Gerät zu niedrig ist, führen Sie die Aktualisierung nach dem Aufladen durch oder schließen Sie den Akku an ein Ladegerät an. \nAktualisierung beginnen?"
    override val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "Die Firmware konnte nicht aktualisiert werden. \nWiederholen Sie die Aktualisierung der Firmware. \nFalls die Aktualisierung auch nach wiederholten Versuchen fehlschlägt, ist möglicherweise {0} defekt."
    override val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "Ein Bauteil, das nicht normal arbeitet, ist angeschlossen.\n\nWird die Firmware von {0} unter dieser Bedingung aktualisiert, könnte die neue Firmware für {0} die Firmware von anderen Bauteilen überschreiben, was zu einem Fehler führen kann.\nTrennen Sie alle Bauteile außer {0}."
    override val firmwareUpdateLatest = "Neueste"
    override val firmwareUpdateWaitingForUpdate = "Warten auf die Aktualisierung"
    override val firmwareUpdateCompleted = "Beenden"
    override val firmwareUpdateProtoType = "Prototyp"
    override val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "Firmware-Aktualisierung von {0} fehlgeschlagen. \nFahren Sie mit der Wiederherstellung fort."
    override val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "Neueste Version der Firmware herunterladen?"
    override val firmwareUpdateInvalidFirmwareFileSingle =
        "Die Firmware-Datei von {0} ist ungültig."
    override val firmwareUpdateStopFwUpdate = "Brechen Sie die Firmware-Aktualisierung ab."
    override val firmwareUpdateStopFwRestoring = "Abbruch der Wiederherstellung der Firmware."
    override val firmwareUpdateRestoreComplete =
        "Bei Verwendung von {0} können Sie die Einstellung des Federungsstatus, die in SC angezeigt wird, über den Schaltereinstellung im Menü „Anpassen“ ändern."
    override val firmwareUpdateRestoreCompleteSc = "SC"
    override val firmwareUpdateSystemUpdateFailed = "Das System konnte nicht aktualisiert werden."
    override val firmwareUpdateAfterUnitRecognitionFailed =
        "Verbindung nach Firmware-Aktualisierung fehlgeschlagen."
    override val firmwareUpdateUpdateBleUnitFirst =
        "Aktualisieren Sie zunächst die Firmware für {0}."
    override val firmwareUpdateSystemUpdateFailure = "Das System kann nicht aktualisiert werden."
    override val firmwareUpdateSystemUpdateFinishedWithoutUpdate =
        "Das System wurde nicht aktualisiert."
    override val firmwareUpdateBleUpdateErrorOccuredConnectAgain =
        "Bitte stellen Sie die Verbindung erneut her."
    override val firmwareUpdateFwRestoring =
        "Die Firmware wird überschrieben. \nDen Anschluss nicht lösen bis das Überschreiben abgeschlossen ist."
    override val firmwareUpdateFwRestorationUnitIsNormal =
        "Die Firmware von {0} funktioniert normal."
    override val firmwareUpdateNoNeedForFwRestoration = "Sie muss nicht wiederhergestellt werden."
    override val firmwareUpdateFwRestorationError =
        "Firmware-Wiederherstellung von {0} ist fehlgeschlagen."
    override val firmwareUpdateFinishedFirmwareUpdate =
        "Die Firmware wurde auf die neueste Version aktualisiert."
    override val firmwareUpdateFinishedFirmwareRestoration =
        "Die Firmware von {0} wurde wiederhergestellt."
    override val firmwareUpdateRetryFirmwareRestoration =
        "Firmware-Wiederherstellung für {0} wird erneut durchgeführt."
    override val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "Sie müssen den Nutzungsbedingungen für die Software zustimmen, damit Sie die Firmware aktualisieren können."
    override val functionThresholdAdditionalSoftwareUsageAgreement =
        "Ergänzende Software-Lizenzvereinbarung"
    override val functionThresholdMustRead = "(Bitte unbedingt lesen)"
    override val functionThresholdContentUpdateDetail = "Änderungen"
    override val functionThresholdBtnTitle = "Zustimmen und aktualisieren"
    override val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "Sie müssen den Nutzungsbedingungen für die Software zustimmen, um die Firmware wiederherstellen zu können."
    override val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "Sie müssen den Nutzungsbedingungen für die Software zustimmen, um die Firmware der Einheit wiederherstellen zu können."
    override val commonsPreset = "Voreinstellung"
    override val presetTopMenuLoadFileBtn = "Einstellungsdatei wird geladen"
    override val presetTopMenuLoadFromBikeBtn = "Lade Einstellungen vom Fahrrad"
    override val presetDflyCantSet =
        "Die angeschlossene Gerätekonfiguration unterstützt keine D-FLY-Einstellungen.\nDie D-FLY-Einstellungen können nicht gespeichert werden."
    override val presetSkipUnitBelow = "Die folgende Einheit wird übersprungen."
    override val presetNoSupportedFileVersion =
        "Für den aktuellen Fahrrad-Typ kann eine in einer früheren Version als {0} erstellte Voreinstellungsdatei nicht verwendet werden."
    override val presetPresetTitleShiftMode = "Schaltmodus"
    override val presetConnectedUnit = "Schließen Sie eine Einheit an."
    override val presetEndConnected = "Verbunden"
    override val presetErrorWhileReading =
        "Beim Auslesen der Einstellungen ist ein Fehler aufgetreten."
    override val presetOverConnect =
        "{1} Bauteile von {0} sind verbunden.\nVerbinden Sie nur die ausgewählte Anzahl von {0}."
    override val presetChangeSwitchType = "{0} wird erkannt. Ändern in {1}?"
    override val presetLackOfTargetUnits =
        "Eine in der aktuellen Einheitenkonfiguration erforderliche Einheit fehlt.\nÜberprüfen Sie die Einheitenkonfiguration und ändern Sie die Einstellungen erneut."
    override val presetLoadFileTopMessage =
        "Wählen Sie die Voreinstellungsdatei, die geladen oder gelöscht werden soll, aus der folgenden Liste der Voreinstellungsdateien."
    override val presetLoadFileModifiedOn = "Aktualisierungsdatum"
    override val presetLoadFileConfimDeleteFile =
        "Die ausgewählte {0} Datei wird gelöscht. \nFortfahren?"
    override val presetLoadFileErrorDeleteFile =
        "Die Voreinstellungsdatei konnte nicht gelöscht werden."
    override val presetLoadFileErrorLoadFile =
        "Lesen der Datei für die Voreinstellungen fehlgeschlagen."
    override val presetLoadFileErrorNoSetting =
        "Die Einstellungen eines vom aktuellen Fahrrad-Typ unterstützten Bauteils sind nicht beinhaltet."
    override val presetLoadFileErrorIncompatibleSetting =
        "Die Einstellungen eines vom aktuellen Fahrrad-Typ nicht unterstützten Bauteils sind beinhaltet.\nMöchten Sie die Einstellung nicht unterstützter Bauteile ignorieren?"
    override val presetLoadFileErrorShortageSetting =
        "Einstellungselemente wurden hinzugefügt/gelöscht.\nPrüfen Sie erneut die Einstellungen der Voreinstellungsdatei, speichern Sie die Datei und versuchen Sie erneut zu schreiben."
    override val presetLoadFileErrorUnanticipatedSetting =
        "Beim Auslesen des Sollwerts wurde ein unerwarteter Wert gelesen.\nPrüfen Sie erneut die Einstellungen der Voreinstellungsdatei, speichern Sie die Datei und versuchen Sie erneut zu schreiben."
    override val presetWriteToBikeBan =
        "Die Einstellungen für Synchronized shift können nicht gespeichert werden, da die Einstellungen für Synchronized shift und für das hintere Schaltwerk nicht übereinstimmen. \nFortfahren?"
    override val presetWriteToBikePoint =
        "Die Einstellungen für Synchronized shift können nicht gespeichert werden, da der Schaltpunkt für die Gangschaltung außerhalb des zulässigen Einstellbereichs liegt. \nFortfahren?"
    override val presetWriteToBikeNoSettingToWrite =
        "Es sind keine Einstellungsdaten zum Speichern vorhanden."
    override val presetQEndPreset = "Die Verbindung deaktivieren und den Setup abschließen?"
    override val commonsSynchromap = "Schaltmodus{0}"
    override val commonsSetting = "Einstellung"
    override val commonsDirection = "Richtungen"
    override val commonsChangePointFd = "Schaltpunkt für FD"
    override val commonsChangePointRd = "Schaltpunkt für RD"
    override val commonsAimPoint = "Zielgangstellung für RD"
    override val commonsDirectionUp = "AUF-Richtung"
    override val commonsDirectionDown = "AB-Richtung"
    override val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} ist in mehreren Tasten von {0} eingestellt."
    override val switchFunctionSettingDuplicationCheckMsg3 =
        "Weisen Sie jeder Taste eine andere Einstellung zu."
    override val presetSaveFileOk = "Speichern der Voreinstellungen erfolgreich abgeschlossen."
    override val presetSaveFileError = "Speichern der Voreinstellungen fehlgeschlagen."
    override val presetFile = "Datei"
    override val presetSaveCheckErrorChangeSetting = "Ändern Sie die Einstellungen."
    override val presetSaveCheckErrorSemiSynchro =
        "Semi-Synchronized Shift ist auf einen ungültigen Wert eingestellt."
    override val presetConnectionNotPresetUnitConnected = "Einheit kann nicht erkannt werden."
    override val presetConnectionSameUnitsConnectionError =
        "Identische Bauteile wurden erkannt. Die Voreinstellungsfunktion unterstützt keine identischen Bauteile."
    override val presetNeedFirmwareUpdateToPreset =
        "Die Einstellungen der nachfolgenden Bauteile können nicht geschrieben werden, da die Firmware nicht aktualisiert ist."
    override val presetUpdateFirmwareToContinuePreset =
        "Um mit der Konfiguration von Voreinstellungen fortfahren zu können, führen Sie bitte eine Aktualisierung durch."
    override val presetOldFirmwareUnitsExist =
        "Die Firmware der folgenden Einheiten ist nicht auf dem aktuellen Stand. \nJetzt auf die neueste Firmware aktualisieren?"
    override val presetOldFirmwareUnitsExistAndStopPreset =
        "Die Einstellungen der nachfolgenden Bauteile können nicht geschrieben werden, da die Firmware nicht aktualisiert ist.\nAktualisieren Sie die Firmware der Bauteile, auf die geschrieben werden soll."
    override val presetDonotShowNextTimeCheckTitle =
        "Diesen Bildschirm nicht mehr anzeigen (nur wählen, wenn dieselben Einstellungen wiederverwendet werden)."
    override val presetUpdateBtnTitle = "Aktualisieren"
    override val presetPresetOntyRecognizedUnit =
        "Einstellungen nur an der erkannten Einheit speichern?"
    override val presetNewFirmwareUnitsExist =
        "Die Voreinstellung kann nicht konfiguriert werden, da die Firmware der Einheit neuer als das Programm ist. \nAktualisieren Sie die Firmware des Programms auf die neuste Version."
    override val presetNewFirmwareUnitsExistAndStopPreset =
        "Die Einstellungen können möglicherweise nicht normal konfiguriert werden, da die Firmware der Einheit neuer als das Programm ist. \nDer Vorgang wird abgebrochen."
    override val presetSynchronizedShiftSettings = "Einstellungen für Synchronized shift"
    override val presetSemiSynchronizedShiftSettings = "Einstellungen für Semi-Synchronized Shift"
    override val presetQContinue = "Fortfahren?"
    override val presetWriteToBikeNoUnit =
        "{1} können nicht gespeichert werden, da {0} nicht verbunden ist."
    override val presetWriteToBikeToothSettingNotMatch =
        "{0} können nicht auf S1 bzw. S2 gespeichert werden, da die Zahnstruktur in der Einstellungsdatei von der Zahnstruktur der angeschlossenen Einheit abweicht."
    override val loginViewForgetPwdTitle = "Wenn Sie Ihr Passwort vergessen haben"
    override val loginViewForgetIdTitle = "Haben Sie Ihre ID vergessen?"
    override val loginViewCreateUserContent =
        "*Wenn Sie eine Benutzerregistrierung durch den OEM bevorzugen, wenden Sie sich an den lokalen Vertriebspartner oder an das Personal von Shimano."
    override val loginInternetConnection = "Kann keine Verbindung zum Netzwerk aufbauen."
    override val loginAccountIsExist =
        "Dieses Benutzerkonto ist gesperrt. Versuchen Sie später sich erneut anzumelden."
    override val loginCorrectIdOrPassword = "Benutzer-ID oder Passwort falsch."
    override val loginPasswordErrorMessage = "Das Passwort ist falsch."
    override val loginPasswordExpired =
        "Ihr vorläufiges Passwort ist abgelaufen.\nStarten Sie den Registrierungsvorgang erneut."
    override val loginPasswordExpiredChange =
        "Sie haben Ihr Passwort seit {0} Tagen nicht geändert.\nÄndern Sie Ihr Passwort."
    override val loginChange = "Ändern"
    override val loginLater = "Später"
    override val loginViewFinishLogin = "Sie haben sich angemeldet."
    override val loginErrorAll =
        "Es gibt ein Problem mit den eingegebenen Inhalt. Bitte prüfen Sie die unten aufgeführten Punkte."
    override val loginViewUserId = "Benutzer-ID"
    override val loginViewEmail = "E-Mail-Adresse"
    override val loginViewPassword = "Passwort"
    override val loginViewCountry = "Land"
    override val loginLoginMove = "Zur Anmeldung"
    override val loginMailSendingError =
        "E-Mail kann nicht gesendet werden. Warten Sie einen Moment und versuchen Sie es dann erneut."
    override val loginAlphanumeric = "halb-breite alphanumerische Zeichen"
    override val loginTitleForgetPassword = "Setzen Sie Ihr Passwort zurück"
    override val loginViewForgetPasswordTitleFir =
        "Sie erhalten die Informationen zum Einstellen eines neuen Passwortes in einer E-Mail."
    override val loginViewForgetMailSettingTitle =
        "Überprüfen Sie die Einstellungen im Voraus, so dass \"{0}\" empfangen werden kann."
    override val loginTitleForgetMailAdress = "Registrierte E-Mail-Adresse"
    override val loginUserIdAndEmailDataNotExist =
        "Benutzer-ID oder registrierte E-Mail-Adresse ist falsch."
    override val loginViewForgotPassword =
        "Die E-Mail zum Zurücksetzen des Passworts wurde abgesendet."
    override val loginTitleChangePassword = "Passwort ändern"
    override val loginViewCurrentPassword = "Aktuelles Passwort"
    override val loginViewNewPassword = "Neues Passwort"
    override val loginErrorCurrentPasswordNonFormat =
        "Aktuelles Passwort darf nur alphanumerische Zeichen enthalten."
    override val loginErrorCurrentPasswordLength =
        "Aktuelles Passwort ist zwischen {0} und {1} Zeichen lang."
    override val loginErrorNewPasswordNonFormat =
        "Neues Passwort darf nur alphanumerische Zeichen enthalten."
    override val loginErrorNewPasswordLength =
        "Neues Passwort muss zwischen  {0} und {1} Zeichen lang sein."
    override val loginErrorPasswordNoDifferent =
        "Ihr neues Passwort kann nicht dasselbe sein, wie Ihr aktuelles Passwort."
    override val loginErrorPasswordNoConsistent =
        "Neues Passwort stimmt nicht mit dem hinterlegten Passwort überein."
    override val loginViewFinishPasswordChange = "Ihr Passwort wurde geändert."
    override val loginTitleLogOut = "Logout"
    override val loginViewLogOut = "Sie haben sich abgemeldet."
    override val loginViewForgetIdTitleFir = "Ihre Benutzer-ID wird Ihnen per E-Mail gesendet."
    override val loginTitleForgetId = "Benachrichtigung über Benutzer-ID"
    override val loginViewForgetTitleSec =
        "*Falls Sie die registrierte E-Mail-Adresse nicht benutzen können, können wir Ihr {0} nicht neu ausstellen, da keine Möglichkeit besteht, dass Sie Ihre Identität bestätigen. Melden Sie sich als neuer Benutzer an."
    override val loginEmailDataNotExist = "Diese E-Mail-Adresse wurde noch nicht registriert."
    override val loginViewForgotUserId = "Ihre Benutzer-ID wurde Ihnen per E-Mail gesendet."
    override val loginTitleUserInfo = "Abfrage der Benutzerdaten"
    override val loginViewAddress = "Adresse"
    override val loginUserDataNotExist = "Fehler bei der Erfassung der Benutzerinformationen."
    override val commonsSet = "Einstellen"
    override val commonsAverageVelocity = "Durchschnittsgeschwindigkeit"
    override val commonsBackLightBrightness = "Helligkeit"
    override val commonsBackLightBrightnessLevel = "Stufe {0}"
    override val commonsCadence = "Trittfrequenz"
    override val commonsDistanceUnit = "Einheit"
    override val commonsDrivingTime = "Fahrzeit"
    override val commonsForAssist = "für Unterstützung"
    override val commonsForShift = "für Gangschaltung"
    override val commonsInvalidate = "Nein"
    override val commonsKm = "Metrische einheiten"
    override val commonsMaxGearUnit = "Gänge"
    override val commonsMaximumVelocity = "Maximale geschwindigkeit"
    override val commonsMile = "Meilen (US)"
    override val commonsNotShow = "Nicht anzeigen"
    override val commonsShow = "Anzeigen"
    override val commonsSwitchDisplay = "Menüpunkt anzeigen"
    override val commonsTime = "Zeiteinstellung"
    override val commonsValidate = "Ja"
    override val commonsNowTime = "Aktuelle Uhrzeit"
    override val commonsValidateStr = "Gültig"
    override val commonsInvalidateStr = "Ungültig"
    override val commonsAssistModeBoostRatio = "Unterstützungsmodus BOOST"
    override val commonsAssistModeTrailRatio = "Unterstützungsmodus TRAIL"
    override val commonsAssistModeEcoRatio = "Unterstützungsmodus ECO"
    override val commonsStartMode = "Start mode (Startmodus)"
    override val commonsAutoGearChangeModeLog = "Funktion der automatischen Gangschaltung"
    override val commonsAutoGearChangeAdjustLog = "Timing für das Schalten"
    override val commonsBackLight = "Hintergrundbeleuchtung"
    override val commonsFontColor = "Farboption"
    override val commonsFontColorBlack = "Schwarz"
    override val commonsFontColorWhite = "Weiß"
    override val commonsInteriorTransmission = "Nabenschaltung (Kette)"
    override val commonsInteriorTransmissionBelt = "Nabenschaltung (Riemen)"
    override val commonsRangeOverview = "Reichweitenübersicht"
    override val commonsUnknown = "Unbekannt"
    override val commonsOthers = "Sonstiges"
    override val commonsBackLightManual = "Manuell"
    override val commonsStartApp = "Start"
    override val commonsSkipTutorial = "Anleitung überspringen"
    override val powerMeterMonitorPower = "Leistung"
    override val powerMeterMonitorCadence = "Trittfrequenz"
    override val powerMeterMonitorPowerUnit = "W"
    override val powerMeterMonitorCadenceUnit = "rpm"
    override val powerMeterMonitorCycleComputerConnected = "Zurück zu Fahrradcomputer"
    override val powerMeterMonitorGuideTitle = "Nullpunkt-Kalibrierung durchführen"
    override val powerMeterMonitorGuideMsg =
        "1.Stellen Sie das Fahrrad auf einer ebenen Fläche ab.\n2.Stellen Sie den Kurbelarm so, dass er im rechten Winkel zum Boden steht, wie in er Abbildung gezeigt.\n3.Drücken Sie die Schaltfläche \"Nullpunkt-Kalibrierung\".\n\nSetzen Sie Ihre Füße nicht auf die Pedale und belasten Sie die Kurbel nicht."
    override val powerMeterLoadCheckTitle = "Lastprüfmodus"
    override val powerMeterLoadCheckUnit = "N"
    override val powerMeterFewCharged = "Der Akkuladestand ist niedrig. Laden Sie den Akku auf."
    override val powerMeterZeroOffsetErrorTimeout =
        "Die Verbindung zum Leistungsmesser konnte wegen schlechter Drahtlosverbindung nicht hergestellt werden.\nDen Schritt unter besseren Empfangsbedingungen für drahtlose Datenübertragung ausführen. "
    override val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Unzureichender Akkuladestand. Den Akku laden und erneut versuchen."
    override val powerMeterZeroOffsetErrorSensorValueOver =
        "Die Kurbel ist eventuell belastet. Die Kurbel entlasten und erneut versuchen."
    override val powerMeterZeroOffsetErrorCadenceSignalChange =
        "Die Kurbel hat sich eventuell bewegt. Die Kurbel nicht berühren und erneut versuchen."
    override val powerMeterZeroOffsetErrorSwitchOperation =
        "Der Schalter wurde eventuell betätigt. Den Schalter nicht berühren und erneut versuchen."
    override val powerMeterZeroOffsetErrorCharging = "Das Ladekabel entfernen und erneut versuchen."
    override val powerMeterZeroOffsetErrorCrankCommunication =
        "Der Stecker am linken Kurbelarm ist eventuell nicht angeschlossen. Entfernen Sie die äußere Kappe, prüfen Sie, ob der Stecker angeschlossen ist, und versuchen Sie es erneut."
    override val powerMeterZeroOffsetErrorInfo =
        "Weitere Informationen finden Sie im Shimano Handbuch."
    override val drawerMenuLoadCheck = "Lastprüfmodus"
    override val commonsMonitor = "Überwachungsmodus"
    override val commonsZeroOffsetSetting = "Nullpunkt-Kalibrierung"
    override val commonsGroupPowermeter = "leistungsmesser"
    override val commonsPresetError =
        "Der Leistungsmesser kann nicht voreingestellt werden.\nVerbinden Sie eine andere Einheit."
    override val commonsWebPageTitleAdditionalFunction =
        "E-TUBE PROJECT|Information zu Zusatzfunktionen"
    override val commonsWebPageTitleFaq = "E-TUBE PROJECT|FAQ"
    override val commonsWebPageTitleGuide = "E-TUBE PROJECT|Nutzungshinweise für E-TUBE PROJECT"
    override val connectionReconnectingBle =
        "Die Bluetooth® LE-Verbindung wurde unterbrochen.\nVersuche, die Verbindung wiederherszustellen."
    override val connectionCompleteReconnect = "Die Verbindung ist wiederhergestellt."
    override val connectionFirmwareWillBeUpdated = "Die Firmware wird aktualisiert."
    override val connectionFirmwareUpdatedFailed =
        "Schließen Sie die Einheit wieder an.\nFalls der Fehler wieder auftritt, könnte eine Störung vorliegen.\nWenden Sie sich an Ihren Vertriebspartner oder Händler."
    override val settingMsg7 =
        "Synchronisiert die Zeiteinstellung des Fahrradcomputers automatisch."
    override val commonsDestinationType = "Typ {0}"
    override val customizeSwitchSettingViewControllerMsg15 = "Unterstützungsschalter"
    override val presetNeedToChangeSettings =
        "Es wurde eine Funktion konfiguriert, die nicht verwendet werden kann. Modifizieren Sie die verfügbaren Funktionen unter „Anpassen“ und versuchen Sie es erneut."
    override val connectionErrorSelectNoDetectedMsg1 =
        "Kann nicht wiederhergestellt werden, wenn kein Gerätename ausgewählt werden kann."
    override val connectionTurnOnGps =
        "Um eine Verbindung zu einem drahtlosen Gerät herzustellen, schalten Sie die Standortinformationen ein."
    override val drawerMenuAboutEtube = "Über E-TUBE PROJECT"
    override val commonsX2Y2Notice =
        "*Wenn die Einstellung für X2/Y2 in {0} auf „Nicht verwenden“ gesetzt ist, wird die Erkennung nicht durchgeführt, auch wenn X2/Y2 gedrückt wird."
    override val powerMeterZeroOffsetComplete = "Ergebnis der Nullpunktverschiebung"
    override val commonsMaxAssistSpeed = "Begrenzung der maximalen Geschwindigkeit"
    override val commonsShiftingAdvice = "Schaltvorschlag"
    override val commonsSpeedAdjustment = "Anzeigegeschwindigkeit"
    override val customizeSpeedAdjustGuideMsg =
        "Nehmen Sie Einstellungen vor, wenn sich die Anzeige von anderen Geschwindigkeitsanzeigen unterscheidet."
    override val commonsMaintenanceAlertDistance = "Distanz bis zum nächsten Service"
    override val commonsMaintenanceAlertDate = "Datum für Service"
    override val commonsAssistPatternComfort = "COMFORT"
    override val commonsAssistPatternSportive = "SPORTIVE"
    override val commonsEbike = "E-BIKE"
    override val connectionDialogMessage20 =
        "{0} ist eingestellt auf Unterstützungsschalter. Ändern Sie die Einstellungen."
    override val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "Ist {0} lediglich auf Unterstützungsfunktion eingestellt\nkann nur Folgendes eingestellt werden:\nTaste X: Unterstützung auf\nTaste Y: Unterstützung ab"
    override val switchFunctionSettingDuplicationCheckMsg5 =
        "Die Federungsfunktion kann nicht gleichzeitig mit einer anderen Funktion eingestellt werden."
    override val switchFunctionSettingDuplicationCheckMsg6 =
        "Nur folgende Kombinationen können eingestellt werden\nfür SW-E6000\nHinten hochschalten\nHinten herunterschalten\nAnzeige\noder\nUnterstützung auf\nUnterstützung ab\nAnzeige/Licht"
    override val commonsGroupSw = "Schalter"
    override val customizeWarningMaintenanceAlert =
        "Mit dieser Einstellung der Wartungsanzeige werden Warnungen angezeigt. Fortfahren?"
    override val customizeCommunicationModeSetting = "Einstellmenü Drahtloskommunikation"
    override val customizeCommunicationModeCustomizeItemTitle =
        "Drahtloskommunikationsmodus (für Fahrradcomputer)"
    override val customizeCommunicationModeAntAndBle = "Modus ANT/Bluetooth® LE"
    override val customizeCommunicationModeAnt = "Modus ANT"
    override val customizeCommunicationModeBle = "Modus Bluetooth® LE"
    override val customizeCommunicationModeOff = "Modus AUS"
    override val customizeCommunicationModeGuideMsg =
        "Senken Sie den Akkuverbrauch, indem Sie die Einstellungen mit dem Kommunikationssystem des Fahrradcomputers abgleichen. Wenn Sie den Fahrradcomputer mit einem anderen Kommunikationssystem verbinden, setzen Sie E-TUBE PROJECT zurück.\nE-TUBE PROJECT für Tablets und Smartphones kann mit jedem konfigurierten Kommunikationssystem verwendet werden."
    override val presetCantSet = "Folgendes kann nicht für {0} eingestellt werden."
    override val presetWriteToBikeInvalidSettings = "{0} ist angeschlossen."
    override val presetDuMuSettingNotice =
        "Die Einstellungen der Antriebseinheit entsprechen nicht denen der Motoreinheit. "
    override val presetDuSettingDifferent = "Die Antriebseinheit ist nicht für die {0} eingestellt."
    override val commonsAssistLockChainTension = "Haben Sie die Kettenspannung eingestellt?"
    override val commonsAssistLockInternalGear =
        "Bei Einsatz einer Nabenschaltung ist es erforderlich, die Kettenspannung einzustellen."
    override val commonsAssistLockInternalGear2 =
        "Passen Sie die Kettenspannung an und drücken Sie dann die Taste „Ausführen“."
    override val commonsAssistLockCrank = "Haben Sie den Winkel der Kurbel überprüft?"
    override val commonsAssistLockCrankManual =
        "Der linke Kurbelarm muss im korrekten Winkel  auf die Achse gesetzt werden. Prüfen Sie den Winkel der aufgesetzten Kurbel und drücken Sie dann die Taste „Ausführen“."
    override val switchFunctionSettingDuplicationCheckMsg7 =
        "Dieselbe Funktion kann maximal einer Taste zugewiesen werden."
    override val commonsMaintenanceAlertUnitKm = "km"
    override val commonsMaintenanceAlertUnitMile = "mile"
    override val commonsMaxAssistSpeedUnitKm = "km/h"
    override val commonsMaxAssistSpeedUnitMph = "mph"
    override val firmwareUpdateBlefwUpdateFailed =
        "Die Aktualisierung der Firmware von {0} ist fehlgeschlagen."
    override val firmwareUpdateShowFaq = "Nähere Informationen anzeigen"
    override val commonsMaintenanceAlert = "Wartungsanzeige"
    override val commonsOutOfSettableRange =
        "{0} befindet sich außerhalb des zulässigen Einstellungsbereichs."
    override val commonsCantWriteToUnit =
        "Mit den aktuellen Einstellungen kann nicht in die {0} geschrieben werden."
    override val connectionScanModalText =
        "Während der Verwendung von E-TUBE RIDE kann keine weitere Verbindung hergestellt werden. Trennen Sie E-TUBE RIDE in der Anwendung und versuchen Sie erneut, die andere Verbindung herzustellen.\n Andernfalls stellen Sie die Einheit am Fahrrad auf Bluetooth® LE-Verbindungsmodus ein."
    override val connectionUsingEtubeRide = "Bei Verwendung von E-TUBE RIDE."
    override val connectionScanModalTitle = "Bei Verwendung von E-TUBE RIDE."
    override val firmwareUpdateCommunicationIsUnstable =
        "Die Drahtlosumgebung ist nicht stabil.\n Verringern Sie den Abstand zwischen den beiden Verbindungsgeräten, um den Drahtlosempfang zu verbessern."
    override val connectionQBleFirmwareRestore =
        "Es wurde ein Fehler an der Einheit für drahtlose Signalübertragung festgestellt.\n Firmware wiederherstellen?"
    override val commonsAutoGearChange = "Automatische Gangschaltung"
    override val firmwareUpdateBlefwUpdateRestoration =
        "Stellen Sie die Verbindung über Bluetooth® LE erneut her und fahren Sie dann mit der Wiederherstellung fort."
    override val firmwareUpdateBlefwUpdateReconnectFailed =
        "Nach dem Aktualisieren von {0} konnte Bluetooth® LE nicht automatisch erneut verbunden werden."
    override val firmwareUpdateBlefwResotreReconnectFailed =
        "Nach dem Wiederherstellen von {0} konnte Bluetooth® LE nicht automatisch erneut verbunden werden."
    override val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Update abgeschlossen. Stellen Sie vor der Weiterverwendung zunächst eine Verbindung über Bluetooth® LE her."
    override val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Wiederherstellung abgeschlossen. Stellen Sie vor der Weiterverwendung zunächst eine Verbindung über Bluetooth® LE her."
    override val connectionConnectionToOsConnectingUnitSucceeded =
        "E-TUBE PROJECT ist mit {0} verbunden, was nicht auf dem Bildschirm angezeigt wird."
    override val connectionSelectYesToContinue =
        "Um Die Verbindung fortzusetzen, wählen Sie „Bestätigen“ ."
    override val connectionSelectNoToConnectOtherUnit =
        "Wählen Sie „Abbrechen“, um die aktuelle Verbindung zu trennen und eine Verbindung zu einer anderen Einheit herzustellen."
    override val commonsCommunicationIsNotStable =
        "Der Funkempfang ist nicht stabil.\n {0} in der Nähe des Geräts positionieren, um den Funkempfang zu verbessern und dann die Einstellungen erneut ändern."

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    override val commonOn = "EIN"
    override val commonOff = "AUS"
    override val commonManual = "Manuell"
    override val commonInvalid = "Ungültig"
    override val commonValid = "Gültig"
    override val commonRecover = "Wiederherstellen"
    override val commonRecoverEnter = "Wiederherstellen (Eingabe)"
    override val commonNotRecover = "Nicht wiederherstellen"
    override val commonOKEnter = "OK(EINGABE)"
    override val commonCancel = "Abbrechen"
    override val commonCancelEnter = "Abbrechen (EINGABE)"
    override val commonUpdate = "Aktualisieren (Eingabe)"
    override val commonNotUpdate = "Nicht aktualisieren"
    override val commonDisconnect = "TRENNEN"
    override val commonSelect = "Wählen"
    override val commonYes = "Ja"
    override val commonYesEnter = "Ja (EINGABE)"
    override val commonNo = "Nein"
    override val commonNoEnter = "Nein (EINGABE)"
    override val commonBack = "Zurück"
    override val commonRestore = "Zurück"
    override val commonSave = "Speichern (Eingabe)"
    override val commonCancelEnterUppercase = "ABBRECHEN (EINGABE)"
    override val commonAbort = "Abbrechen"
    override val commonApplyEnter = "Reflektieren (Eingabe)"
    override val commonNext = "Weiter (Eingabe)"
    override val commonStartEnter = "Start (Eingabe)"
    override val commonClear = "Löschen"
    override val commonConnecting = "Wird verbunden..."
    override val commonConnected = "Verbunden"
    override val commonDiagnosing = "Diagnose wird ausgeführt..."
    override val commonProgramming = "Einstellung wird ausgeführt..."
    override val commonRetrievingUnitData = "Abrufen von Daten wird ausgeführt..."
    override val commonGetRetrieving = "Lade Daten…"
    override val commonRemainingSeconds = "Verbleiben: {0} Sekunden"
    override val commonTimeLimit = "Zeitbegrenzung: {0} Sek."
    override val commonCharging = "Lädt…"
    override val commonBeingPerformed = "Während der Ausführung von Aufgaben"
    override val commonNotRun = "Nicht ausgeführt"
    override val commonAbnormal = "Fehler"
    override val commonCompletedNormally = "Normal abgeschlossen"
    override val commonCompletedWithError = "Mit Fehler abgeschlossen"
    override val commonEBikeReport = "E-BIKE-REPORT"
    override val commonBicycleInformation = "Fahrradinformationen"
    override val commonSkipUppercase = "ÜBERSPRINGEN"
    override val commonSkipEnter = "ÜBERSPRINGEN (ENTER)"
    override val commonWaitingForJudgment = "Warten auf die Beurteilung"
    override val commonStarting = "{0} wird gestartet."
    override val commonCompleteSettingMessage = "Die Einstellungen wurden normal abgeschlossen."
    override val commonProgrammingErrorMessage = "Beim Speichern ist ein Fehler aufgetreten."
    override val commonSettingValueGetErrorMessage =
        "Beim Abrufen der Einstellungen ist ein Fehler aufgetreten."
    override val commonSettingUpMessage =
        "Einstellung wird ausgeführt. Bis zum Abschluss der Einstellung darf die Spannungsversorgung nicht getrennt werden."
    override val commonCheckElectricWireMessage =
        "Prüfen Sie, ob sämtliche elektrischen Leitungen ordnungsgemäß angeschlossen sind."
    override val commonElectlicWireIsNotDisconnectedMessage =
        "Führen Sie eine Fehlerprüfung durch, falls das Stromkabel nicht gelöst ist."
    override val commonErrorCheckResults = "Ergebnisse der Störungsdiagnose"
    override val commonErrorCheckCompleteMessage =
        "Die Störungsdiagnose von {0} wurde abgeschlossen."
    override val commonCheckCompleteMessage = "Die Überprüfung von {0} ist abgeschlossen."
    override val commonFaultCouldNotBeFound = "Es konnte kein Fehler gefunden werden."
    override val commonFaultMayExist = "Ein Fehler kann vorliegen."
    override val commonMayBeFaulty = "{0} ist möglicherweise defekt."
    override val commonItemOfMayBeFaulty = "Das {0}te Teil von {1} ist möglicherweise defekt."
    override val commonSkipped = "Übersprungen"
    override val commonAllUnitErrorCheckCompleteMessage =
        "Die Fehlerprüfung ist für alle Einheiten abgeschlossen."
    override val commonConfirmAbortWork = "Möchten Sie den Vorgang beenden?"
    override val commonReturnValue = "Die vorherigen Werte werden wiederhergestellt."
    override val commonManualFileIsNotFound = "Die Handbuch-Datei wurde nicht gefunden."
    override val commonReinstallMessage = "Deinstallieren Sie {0} und installieren Sie erneut."
    override val commonWaiting = "Bei Standby – SCHRITT {0}"
    override val commonDiagnosisInProgress = "Diagnose wird ausgeführt - STEP {0}"
    override val commonNotPerformed = "Nicht ausgeführt"
    override val commonDefault = "Voreinstellung"
    override val commonDo = "Ja"
    override val commonNotDo = "Nein"
    override val commonOEMSetting = "OEM-Einstellungen"
    override val commonTotal = "Insgesamt"
    override val commonNow = "Aktuell"
    override val commonReset = "Zurücksetzen"
    override val commonEnd = "Ende (Eingabe)"
    override val commonComplete = "Beenden"
    override val commonNextSwitch = "Zum nächsten Schalter"
    override val commonStart = "Start"
    override val commonAdjustmentMethod = "Einstellmethode"
    override val commonUnknown = "Unbekannt"
    override val commonUnitRecognition = "Verbindungsprüfung"

    // 単位
    override val commonUnitStep = "{0} gänge"
    override val commonUnitSecond = "s"

    // 公式HPの言語設定
    override val commonHPLanguage = "de-DE"

    // アシスト
    override val commonAssistAssistOff = "Unterstützung Aus"

    // その他
    override val commonDelete = "Löschen"
    override val commonApply = "ANWENDEN"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    override val unitCommonFirmwareVersion = "Firmware-Version"
    override val unitCommonSerialNo = "Serien-Nr."
    override val unitCommonFirmwareUpdate = "Firmware aktualisieren"
    override val unitCommonUpdateToTheLatestVersion = "Aktualisierung auf die neueste Version."
    override val unitCommonFirmwareUpdateNecessary =
        "Falls erforderlich, Aktualisierung auf die neueste Version."
    //endregion

    //region 一覧表示
    override val unitUnitBattery = "Akku"
    override val unitUnitDI2Adapter = "DI2-Adapter"
    override val unitUnitInformationDisplay = "Informationsdisplay"
    override val unitUnitJunctionA = "Kontaktstelle A"
    override val unitUnitRearSuspension = "Hinterrad-Federung"
    override val unitUnitFrontSuspension = "Vorderrad-Federung"
    override val unitUnitDualControlLever = "Dual-Control-Schalt-/Bremshebel"
    override val unitUnitShiftingLever = "Schaltgriff"
    override val unitUnitSwitch = "Schalter"
    override val unitUnitBatterySwitch = "Akku-Schalter"
    override val unitUnitPowerMeter = "Leistungsmesser"
    //endregion

    //region DU
    override val unitDUYes = "Ja"
    override val unitDUNO = "Nein"
    override val unitDUType = "Typ {0}"
    override val unitDUPowerTerminalOutputSetting =
        "Ausgabeeinstellung des Stromversorgungsanschlusses für Licht/Zubehör"
    override val unitDULightONOff = "Licht EIN/AUS"
    override val unitDUDestination = "Bestimmungsort/Zielland"
    override val unitDURemainingLightCapacity = "Rest brenndauer"
    override val unitDULightConnection = "Lichtanschluss"
    override val unitDUButtonOperation = "Betätigung per Taste"
    override val unitDUAlwaysOff = "Immer AUS"
    override val unitDUButtonOperations = "Betätigung per Taste"
    override val unitDUAlwaysOn = "Immer EIN"
    override val unitDUSetValue = "Eingestellter Wert"
    override val unitDUOutputVoltage = "Ausgangsspannung"
    override val unitDULightOutput = "Lichtleistungg"
    override val unitDUBatteryCapacity = "Batterie Restkapazität für Beleuchtung"
    override val unitDUWalkAssist = "Schiebehilfe"
    override val unitDUTireCircumference = "Reifenumfang"
    override val unitDUGearShiftingType = "Schaltungstyp"
    override val unitDUShiftingMethod = "Wechselmethode"
    override val unitDUFrontChainRing = "Anzahl der zähne auf dem vorderen kettenblatt"
    override val unitDURearSprocket = "Anzahl der zähne auf dem ritzel/ritzelpaket"
    override val unitDUToothSelection = "Anzahl der zähne"
    override val unitDUInstallationAngle = "Einbauwinkel der antriebseinheit"
    override val unitDUAssistCustomize = "Fahrcharakteristik anpassen"
    override val unitDUProfile1 = "Profil 1"
    override val unitDUProfile2 = "Profil 2"
    override val unitDUCostomize = "Anpassen"
    override val unitDUAssistCharacter = "Fahrcharakteristik"
    override val unitDUMaxTorque = "Maximales drehmoment"
    override val unitDUAssistStart = "Fahrcharakteristik beim start"
    override val unitDUOther = "Sonstige"
    override val unitDU1stGear = "1. gang"
    override val unitDU2ndGear = "2. gang"
    override val unitDU3rdGear = "3. gang"
    override val unitDU4thGear = "4. gang"
    override val unitDU5thGear = "5. gang"
    override val unitDU6thGear = "6. gang"
    override val unitDU7thGear = "7. gang"
    override val unitDU8thGear = "8. gang"
    override val unitDU9thGear = "9. gang"
    override val unitDU10thGear = "10. gang"
    override val unitDU11thGear = "11. gang"
    override val unitDU12thGear = "12. gang"
    override val unitDU13thGear = "13. gang"
    override val unitDUShiftModeAfterDisconnect = "Modus nach dem Trennen der App"
    override val unitDUAutoGearShiftAdjustment = "Einstellung der automatischen gangschaltung"
    override val unitDUShiftingAdvice = "Schalthinweis"
    override val unitDUTravelingDistance = "Fahrtstrecke"
    override val unitDUTravelingDistanceMaintenanceAlert = "Fahrtstrecke (Wartungsanzeige)"
    override val unitDUDate = "Datum"
    override val unitDUDateYear = "Datum: Jahr (Wartungsanzeige)"
    override val unitDUDateMonth = "Datum: Monat (Wartungsanzeige)"
    override val unitDUDateDay = "Datum: Tag (Wartungsanzeige)"
    override val unitDUTotalDistance = "Gesamtstrecke"
    override val unitDUTotalTime = "Gesamtzeit"
    override val unitDURemedy = "Abhilfemaßnahme"
    //endregion

    //region BT
    override val unitBTCycleCount = "Zykluszähler"
    override val unitBTTimes = "Zyklen"
    override val unitBTRemainingCapacity = "Ladezustand des Akkus"
    override val unitBTFullChargeCapacity = "Verbleibende Akkukapazität"

    override val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //endregion

    //region SC, EW
    override val unitSCEWMode = "Modus"
    override val unitSCEWModes = "Modi"
    override val unitSCEWDisplayTime = "Anzeigenzeit : {0}"
    override val unitSCEWBeep = "Signal : {0}"
    override val unitSCEWBeepSetting = "Systemton"
    override val unitSCEWWirelessCommunication = "Drahtlose Kommunikation"
    override val unitSCEWCommunicationModeOff = "AUS"
    override val unitSCEWBleName = "Bluetooth® LE-Name\n"
    override val unitSCEWCharacterLimit = "1 bis 8 alphanumerisch Zeichen mit halber Breite"
    override val unitSCEWPasskeyDescription =
        "6-digit number that starts with a number other than 0"
    override val unitSCEWConfirmation = "Bestätigung"
    override val unitSCEWPassKeyInitialization = "PassKey-Initialisierung"
    override val unitSCEWEnterPasskey =
        "Geben Sie eine sechsstellige Nummer ein, die nicht mit 0 beginnt."
    override val unitSCEWNotMatchPassKey = "Der Passkey stimmt nicht überein."
    override val unitSCEWDisplayUnits = "Maßeinheit"
    override val unitSCEWInternationalUnits = "Metrische einheiten"
    override val unitSCEWDisplaySwitchover = "Schalten"
    override val unitSCEWRangeOverview = "Radius"
    override val unitSCEWAutoTimeSetting = "Zeit (automatisch)"
    override val unitSCEWManualTimeSetting = "Zeit (manuell)"
    override val unitSCEWUsePCTime = "PC-zeit übernehmen"
    override val unitSCEWTimeSetting = "Zeiteinstellung"
    override val unitSCEWDoNotSet = "Nicht einstellen"
    override val unitSCEWBacklight = "Hintergrundbeleuchtung"
    override val unitSCEWBacklightSetting = "Hintergrundbeleuchtung"
    override val unitSCEWManual = "Manuell"
    override val unitSCEWBrightness = "Helligkeit"
    override val unitSCEWFont = "Schriftart"
    override val unitSCEWSet = "Einstellen"
    //endregion

    //region MU
    override val unitMUGearPosition = "Gangstellung"
    override val unitMUAdjustmentSetting = "Einstellung"

    override val unitMU5thGear = "5. gang"
    override val unitMU8thGear = "8. gang"
    override val unitMU11thGear = "11. gang"
    //endregion
    //endregion

    //region SUS
    override val unitSusCtdBV = "CTD (BV)"
    override val unitSusCtdOrDps = "CTD (DISH) oder DPS"
    override val unitSusPositionFront = "Position{0} Vorne"
    override val unitSusPositionRear = "Position{0} Hinten"
    override val unitSusPositionDisplayedOnSC = "Position{0} im SC angezeigt"
    override val unitSusDUManufacturingSerial = "Seriennummer der Antriebseinheit"
    override val unitSusBackupDateAndTime = "Datum und Uhrzeit des Backups"
    //endregion

    //region ST,SW
    override val unitSTSWUse2ndGgear = "2. Gang verwenden"
    override val unitSTSWSwitchMode = "Schaltermodus"
    override val unitSTSWForAssist = "für Unterstützung"
    override val unitSTSWForShift = "für Gangschaltung"
    override val unitSTSWUse = "Verwenden"
    override val unitSTSWDoNotUse = "Nicht verwenden"

    //endregion

    //region Shift
    override val unitShiftCopy = "{0} kopieren"
    override val unitShiftShiftUp = "Hochschalten"
    override val unitShiftShiftDown = "Herunterschalten"
    override val unitShiftGearNumberLimit = "Grenze der Ganganzahl "
    override val unitShiftMultiShiftGearNumberLimit = "Grenze der Ganganzahl (Multi-Shifting)"
    override val unitShiftInterval = "Geschwindigkeit des gangwechsels"
    override val unitShiftMultiShiftGearShiftingInterval = "Gangschaltintervall (Multi-Shifting)"
    override val unitShiftMultiShiftFirstGearShiftingPosition =
        "Schaltposition 1. Gang (Multi-Shifting)"
    override val unitShiftMultiShiftSecondGearShiftingPosition =
        "Schaltposition 2. Gang (Multi-Shifting)"
    override val unitShiftSingleShiftingPosition = "Einfache Schaltposition"
    override val unitShiftDoubleShiftingPosition = "Zweifache Schaltposition"
    override val unitShiftAutomaticGearShifting = "Automatische Schaltung"
    override val unitShiftShiftInterval = "Schaltintervall"
    override val unitShiftRemainingBatteryCapacity = "Verbleibende Akkukapazität"
    override val unitShiftBatteryMountingForm = "Konfiguration der Akkuhalterung"
    override val unitShiftPowerSupply = "Mit externer Stromversorgung"
    override val unitShift20PercentOrLess = "20 % oder weniger"
    override val unitShift40PercentOrLess = "40 % oder weniger"
    override val unitShift60PercentOrLess = "60 % oder weniger"
    override val unitShift80PercentOrLess = "80 % oder weniger"
    override val unitShift100PercentOrLess = "100 % oder weniger"
    override val unitShiftRearShiftingUnit = "Schalteinheit hinten"
    override val unitShiftAssistShiftSwitch = "Schalter für Unterstützung/Schaltung"
    override val unitShiftInner = "Innen"
    override val unitShiftMiddle = "Mitte"
    override val unitShiftOuter = "Außen"
    override val unitShiftUp = "Auf"
    override val unitShiftDown = "Ab"
    override val unitShiftGearShiftingInterval = "Geschwindigkeit des gangwechsels"
    //endregion

    //region PC接続機器
    override val unitPCErrorCheck = "Störungsdiagnose"
    override val unitPCBatteryConsumptionCheck = "Überprüfung des Akkuverbrauchs"
    override val unitPCBatteryConsumption = "Akkuverbrauch"
    //endregion

    override val unitErrorCheckBatteryConnectedProperly =
        "Ist der Akku ordnungsgemäß angeschlossen?"
    override val unitErrorCheckCanDetectTorque = "Kann das Drehmoment festgestellt werden?"
    override val unitErrorCheckCanDetectVehicleSpeed =
        "Kann die Fahrzeuggeschwindigkeit festgestellt werden?"
    override val unitErrorCheckCanDetectCadence = "Kann die Trittfrequenz festgestellt werden?"
    override val unitErrorCheckLightOperateNormally = "Funktioniert das Licht ordnungsgemäß?"
    override val unitErrorCheckOperateNormally = "Funktioniert es ordnungsgemäß?"
    override val unitErrorCheckDisplayOperateNormally =
        "Funktioniert der Displaybereich ordnungsgemäß?"
    override val unitErrorCheckBackLightOperateNormally =
        "Funktioniert die Hintergrundbeleuchtung ordnungsgemäß?"
    override val unitErrorCheckBuzzerOperateNormally =
        "Funktioniert der Signaltongeber ordnungsgemäß?"
    override val unitErrorCheckSwitchOperateNormally = "Funktionieren die Schalter ordnungsgemäß?"
    override val unitErrorCheckwirelessFunctionNormally =
        "Funktioniert die Drahtlosfunktion ordnungsgemäß?"
    override val unitErrorCheckBatteryHasEnoughPower =
        "Ist der Ladestand des eingebauten Akkus ausreichend?"
    override val unitErrorCheckNormalShiftToEachGear = "Lassen sich die Gänge fehlerfrei schalten?"
    override val unitErrorCheckEnoughBatteryForShifting =
        "Ist der Ladestand des Akkus ausreichend zum Schalten?"
    override val unitErrorCheckCommunicateNormallyWithBattery =
        "Lässt sich die Kommunikation mit dem Akku ordnungsgemäß herstellen?"
    override val unitErrorCheckLedOperateNormally = "Funktionieren die LEDs ordnungsgemäß?"
    override val unitErrorCheckSwitchOperation = "Betätigung per Schalter"
    override val unitErrorCheckCrankArmPperation = "Betrieb der Kurbel"
    override val unitErrorCheckLcdCheck = "LCD-Überprüfung"
    override val unitErrorCheckLedCheck = "LED-Überprüfung"
    override val unitErrorCheckAudioCheck = "Audio-Überprüfung"
    override val unitErrorCheckWirelessCommunicationCheck =
        "Überprüfung der drahtlosen Kommunikation"
    override val unitErrorCheckPleaseWait = "Bitte warten."
    override val unitErrorCheckCheckLight = "Licht prüfent"
    override val unitErrorCheckSprinterSwitch = "Sprintschalter"
    override val unitErrorCheckSwitch = "Schalter"
    //endregion
    //endregion

    //region M0 基本構成
    override val m0RecommendationOfAccountRegistration =
        "Empfehlung zur Registrierung eines Benutzerkontos"
    override val m0AccountRegistrationMsg =
        "Die Bedienung von E-TUBE PROJECT vereinfacht sich, wenn Sie ein Konto erstellen.\n･Dadurch können Sie Ihr Fahrrad registrieren.\n･Die Einheit für drahtlose Signalübertragung wird gespeichert, was das Verbinden mit Ihrem Fahrrad vereinfacht."
    override val m0SignUp = "REGISTRIEREN"
    override val m0Login = "LOGIN"
    override val m0SuspensionSwitch = "Suspension-Schalter"
    override val m0MayBeFaulty = "{0} ist möglicherweise defekt."
    override val m0ReplaceOrRemoveMessage =
        "Ersetzen oder entfernen Sie die folgende Einheit und stellen Sie die Verbindung erneut her.\n{0}"
    override val m0UnrecognizableMessage =
        "Schaltgriff oder Schalter wird nicht erkannt.\nPrüfen Sie, ob das Stromkabel angeschlossen ist."
    override val m0UnknownUnit = "Unbekannte Einheit {0}"
    override val m0UnitNotDetectedMessage =
        "Stellen Sie sicher, dass das Stromkabel nicht entfernt wurde.\nFalls es entfernt wurde, trennen Sie es und schließen Sie es erneut an."
    override val m0ConnectErrorMessage =
        "Ein Kommunikationsfehler ist aufgetreten. Versuchen Sie erneut, eine Verbindung herzustellen."
    //endregion

    //region M1 起動画面
    override val m1CountryOfUse = "Verwendungsland"
    override val m1Continent = "Kontinente"
    override val m1Country = "Länder/Regionen"
    override val m1OK = "OK"
    override val m1PersonalSettingsMsg =
        "Sie können das Fahrrad ganz einfach Ihrem Fahrstil entsprechend einstellen."
    override val m1NewRegistration = "ERSTREGISTRIERUNG"
    override val m1Login = "LOGIN"
    override val m1ForCorporateUsers = "Für Firmenanwender"
    override val m1Skip = "Überspringen"
    override val m1TermsOfService = "Nutzungsbedingungen"
    override val m1ComfirmLinkContentsAndAgree =
        "Klicken Sie auf den nachfolgenden Link und stimmen Sie dem Inhalt zu."
    override val m1TermsOfServicePolicy = "Nutzungsbedingungen"
    override val m1AgreeToTheTermsOfService = "Ich stimme den Nutzungsbedingungen zu"
    override val m1AgreeToTheAll = "Ich stimme allen Punkten zu"
    override val m1AppOperationLogAgreement = "Vereinbarung über das Betriebsprotokoll der App"
    override val m1AgreeToTheAppOperationLogAgreement =
        "Ich stimme der Vereinbarung über das Betriebsprotokoll der App zu."
    override val m1DataProtecitonNotice = "Datenschutzerklärung"
    override val m1AgreeUppercase = "ICH STIMME ZU"
    override val m1CorporateLogin = "Firmen-Login"
    override val m1Email = "E-Mail-Adresse"
    override val m1Password = "Passwort"
    override val m1DownloadingLatestFirmware = "Die neueste Firmware wird heruntergeladen."
    override val m1Hi = "Hallo, {0}"
    override val m1GetStarted = "Erste Schritte"
    override val m1RegisterBikeOrPowerMeter = "Fahrrad oder Leistungsmesser registrieren"
    override val m1ConnectBikeOrPowerMeter = "Fahrrad oder Leistungsmesser verbinden"
    override val m1BikeList = "Mein Fahrrad"
    override val m1LastConnection = "Letzte Verbindung"
    override val m1Searching = "Suchen..."
    override val m1Download = "HERUNTERLADEN"
    override val m1SkipUppercase = "ÜBERSPRINGEN"
    override val m1Copyright = "©SHIMANO INC. ALLE RECHTE VORBEHALTEN"
    override val m1CountryArea = "Länder/Regionen"
    override val m1ConfirmContentMessage =
        "Setzen Sie ein Häkchen, wenn Sie mit diesen Bedingungen einverstanden sind. Drücken Sie dann auf die Taste „Weiter“, um fortzufahren."
    override val m1Next = "Weiter"
    override val m1Agree = "Zustimmen"
    override val m1ImageRegistrationFailed = "Das Bild konnte nicht registriert werden."
    override val m1InternetConnectionUnavailable =
        "Sie sind nicht mit dem Internet verbunden. Stellen Sie eine Internetverbindung her und wiederholen Sie den Versuch."
    override val m1ResumeBikeRegistrationMessage =
        "Es wurden keine gültigen Fahrraddaten gefunden. Stellen Sie eine Internetverbindung her und registrieren Sie das Fahrrad erneut."
    override val m1ResumeImageRegistrationMessage =
        "Ein unerwarteter Fehler ist aufgetreten. Registrieren Sie das Bild erneut."
    override val m1NoStorageSpaceMessage =
        "Auf Ihrem Smartphone ist nicht genügend Speicherplatz frei."
    override val m1Powermeter = "Leistungsmesser | {0}"
    //endregion

    //region M2 ペアリング
    override val m2SearchingUnits = "Einheiten werden gesucht …"
    override val m2HowToConnectUnits = "So verbinden Sie Einheiten"
    override val m2Register = "Registrierung"
    override val m2UnitID = "ID:{0}"
    override val m2Passkey = "Passkey"
    override val m2EnterPasskeyToRegisterUnit =
        "Geben Sie den Passkey ein, um die Einheit zu registrieren."
    override val m2OK = "OK"
    override val m2CancelUppercase = "ABBRECHEN"
    override val m2ChangeThePasskey = "Passkey ändern?"
    override val m2NotNow = "Nicht jetzt"
    override val m2Change = "Ändern"
    override val m2Later = "Später"
    override val m2NewPasskey = "Neuer Passkey"
    override val m2PasskeyDescription =
        "Geben Sie eine sechsstellige Nummer ein, die nicht mit 0 beginnt"
    override val m2Cancel = "Abbrechen"
    override val m2Nickname = "SPITZNAME"
    override val m2ConfirmUnits = "EINHEITEN BESTÄTIGEN"
    override val m2SprinterSwitchHasConnected = "Sprintschalter wurde angeschlossen"
    override val m2SkipRegister = "Registrierung überspringen"
    override val m2RegisterAsNewBike = "ALS NEUES FAHRRAD REGISTRIEREN"
    override val m2Left = "Links"
    override val m2Right = "Rechts"
    override val m2ID = "ID"
    override val m2Update = "AKTUALISIEREN"
    override val m2Customize = "EINSTELLUNGEN"
    override val m2Maintenance = "WARTUNG"
    override val m2Connected = "Verbunden"
    override val m2Connecting = "Wird verbunden..."
    override val m2AddOnRegisteredBike = "Registriertes Fahrrad hinzufügen"
    override val m2AddPowermeter = "Leistungsmesser hinzufügen"
    override val m2Monitor = "ÜBERWACHEN"
    override val m2ConnectTheBikeAndClickNext =
        "VERBINDEN SIE DAS FAHRRAD UND KLICKEN SIE AUF „WEITER“."
    override val m2Next = "WEITER"
    override val m2ChangePasskey = "Passkey ändern"
    override val m2PowerMeter = "Leistungsmesser"
    override val m2CrankArmSet = "Kurbel eingestellt"
    override val m2NewBicycle = "Neues Fahrrad"
    override val m2ChangeWirelessUnit = "Drahtlose Einheit ändern"
    override val m2DeleteConnectionInformationMessage =
        "Löschen Sie die Verbindungsinformationen auf Ihrem Gerät unter [Einstellungen] > [Bluetooth], nachdem Sie Änderungen vorgenommen haben.\nWerden die Verbindungsinformationen nicht gelöscht, werden diese nicht erneuert, wodurch der Aufbau einer Verbindung verhindert wird."
    override val m2Help = "Hilfe"
    override val m2PairingCompleted = "Koppelung abgeschlossen"
    override val m2AddSwitch = "Schalter hinzufügen"
    override val m2PairDerailleurAndSwitch =
        "Koppeln Sie einen Schalter mit dem Schaltwerk. *Das Schaltwerk kann mit dem Schalter betätigt werden, sobald das Fahrrad von E-TUBE PROJECT getrennt ist."
    override val m2HowToPairing = "Koppelungsmethode"
    override val m2IdInputAreaHint = "Schalter hinzufügen"
    override val m2InvalidQRCodeErrorMsg =
        "Scannen Sie den QR code auf dem Schalter. Unter Umständen wurde ein anderer QR code gescannt."
    override val m2QRScanMsg =
        "Zum Fokussieren auf den Bildschirm tippen. Wenn der QR code nicht gelesen werden kann, geben Sie die 11-stellige ID ein."
    override val m2QRConfirm = "QR code/ID-Position"
    override val m2SerialManualInput = "Manuelle Eingabe der Kennung"
    override val m2CameraAccessGuideMsgForAndroid =
        "Kamerazugriff ist nicht erlaubt. Erlauben Sie in den App-Einstellungen den Zugriff für die Schalterkoppelung."
    override val m2CameraAccessGuideMsgForIos =
        "Kamerazugriff ist nicht erlaubt. Zugriff in den App-Einstellungen erlauben. Wenn die Einstellung geändert wird, um den Zugriff zu erlauben, startet die App neu."
    override val m2AddWirelessSwitches = "Drahtlosen Schalter hinzufügen"
    //endregion

    //region M3 接続と切断
    override val m3Searching = "Suchen..."
    override val m3Connecting = "Wird verbunden..."
    override val m3Connected = "Verbunden"
    override val m3Detected = "Erkannt"
    override val m3SearchOff = "AUS"
    override val m3Disconnect = "TRENNEN"
    override val m3Disconnected = "Nicht verbunden"
    override val m3RecognizedUnits = "Erkannte Einheiten"
    override val m3Continue = "Fortsetzen"

    //region 互換性確認
    override val m3CompatibilityTable = "Kompatibilitätstabelle"
    override val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "Von den folgenden Einheit kann nur jeweils eine Einheit angeschlossen werden."
    override val m3CompatibilityErrorUnitSpecMessage1 =
        "Die erkannte Einheitenkombination ist nicht kompatibel. Schließen Sie die Einheiten entsprechend der Kompatibilitätstabelle an."
    override val m3CompatibilityErrorUnitSpecMessage2 =
        "Ansonsten können die Kompatibilitätsanforderungen erfüllt werden, indem Sie die folgenden roten Einheiten entfernen."
    override val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "Die Firmware der folgenden Einheiten ist nicht auf dem aktuellen Stand."
    override val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "Möglicherweise können Sie dieses Problem lösen, indem Sie die Firmware aktualisieren und so eine erweiterte Kompatibilität erhalten.\nVersuchen Sie, die Firmware zu aktualisieren."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    override val m3CompatibilityErrorDUAndBT =
        "Die erkannte Einheitenkombination ist nicht kompatibel. Schließen Sie die Einheiten entsprechend der Kompatibilitätstabelle an."
    //endregion
    //endregion

    //region M4 アップデート
    override val m4Latest = "Neueste"
    override val m4UpdateAvailable = "AKTUALISIEREN"
    override val m4UpdateAllUnits = "ALLES AKTUALISIEREN"
    override val m4UpdateAnyUnits = "UPDATE | GESCHÄTZTE ZEIT - {0}"
    override val m4History = "VERLAUF"
    override val m4AllUpdateConfirmTitle = "Alles aktualisieren?"
    override val m4AllUpdateConfirmMessage = "Geschätzte Zeit: {0}"
    override val m4SelectAll = "ALLE AUSWÄHLEN (EINGABE)"
    override val m4EstimatedTime = "Geschätzte Zeit: "
    override val m4Cancel = "ABBRECHEN"
    override val m4BleVersion = "Bluetooth® LE Ver. {0}"
    override val m4OK = "OK (EINGABE)"
    override val m4Update = "Update"
    override val m4UpdateUppercase = "UPDATE"

    // TODO:TextTableには表記がないので要確認
    override val m4Giant = "GIANT"
    override val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    override val m5EBike = "E-BIKE"
    override val m5Assist = "Unterstützen"
    override val m5MaximumAssistSpeed = "Begrenzung der maximalen Geschwindigkeit {0}"
    override val m5AssistPattern = "unterstützungsmuster : {0}"
    override val m5RidingCharacteristic = "fahrcharakteristik : {0}"
    override val m5CategoryShift = "Schalten"
    override val m5Synchronized = "Synchronized"
    override val m5AutoShift = "Automatische Schaltung"
    override val m5Advice = "Tipp"
    override val m5Switch = "Schalter"
    override val m5ForAssist = "Für Unterstützung"
    override val m5Suspension = "Federung"
    override val m5ControlSwitch = "Bedienschalter"
    override val m5CategoryDisplay = "Anzeige"
    override val m5SystemInformation = "Systeminformationen"
    override val m5Information = "Informations-"
    override val m5DisplayTime = "Anzeigedauer des Displays: {0}"
    override val m5Mode = "Modus {0}"
    override val m5Other = "Sonstiges"
    override val m5WirelessSetting = "Drahtloseinstellungen"
    override val m5Fox = "FOX"
    override val m5ConnectBikeToApplyChangedSettings =
        "Fahrrad anschließen, um geänderte Einstellungen zu übernehmen."
    override val m5CannotBeSetMessage = "Einige Funktionen können nicht eingestellt werden."
    override val m5GearShiftingInterval = "Geschwindigkeit des gangwechsels : {0}"
    override val m5GearNumberLimit = "Grenze der Ganganzahl : {0}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    override val m5Shift = "Shift"
    override val m5Display = "display"
    //endregion
    //endregion

    //region M6 バイク設定
    override val m6Bike = "Fahrrad"
    override val m6Nickname = "Spitzname"
    override val m6ChangeWirelessUnit = "Drahtlose Einheit ändern"
    override val m6DeleteUnits = "Einheiten löschen"
    override val m6DeleteBike = "Fahrrad löschen"
    override val m6Preset = "Voreinstellung"
    override val m6SaveCurrentSettings = "Die aktuellen Einstellungen speichern"
    override val m6WriteSettings = "Einstellungen schreiben"
    override val m6SettingsCouldNotBeApplied = "Einstellungen konnten nicht übernommen werden."
    override val m6DeleteUppercase = "LÖSCHEN"
    override val m6Save = "SPEICHERN"
    override val m6ReferenceBike = "Referenzfahrrad"
    override val m6PreviousData = "Vorherige Daten"
    override val m6SavedSettings = "Gespeicherte Einstellungen"
    override val m6LatestSetting = "Aktuelle Einstellung"
    override val m6Delete = "Löschen"
    override val m6BikeSettings = "Fahrradeinstellungen"
    override val m6WritingSettingIsCompleted = "Die Einstellung wurde geschrieben."
    override val m6Other = "Sonstiges"

    //region M7 カスタマイズ詳細
    override val m7TwoStepShiftDescription =
        "Position, in der Multi-Shift aktiviert werden kann"
    override val m7FirstGear = "1. Gang"
    override val m7SecondGear = "2. Gang"
    override val m7OtherSwitchMessage =
        "Beim Ausführen von Multi-Shifting mit einem Schalter, der mit einer Komponente verbunden ist, die nicht SW-M9050 entspricht, wechseln Sie in den 2. Gang."
    override val m7NameDescription = "Maximal acht Zeichen"
    override val m7WirelessCommunication = "Drahtlose Kommunikation"
    override val m7Bike = "Fahrrad"
    override val m7Nickname = "Spitzname"
    override val m7ChangeWirelessUnit = "Drahtlose Einheit ändern"
    override val m7DeleteUnit = "Einheit löschen"
    override val m7DeleteBikeRegistration = "Fahrradregistrierung löschen"
    override val m7Auto = "Automatisch"
    override val m7Manual = "Manuell"
    override val m7Setting = "Voreinstellungen"
    override val m7Mode = "Modus"
    override val m7AlwaysDisplay = "Immer anzeigen"
    override val m7MaximumAssistSpeed = "Begrenzung der maximalen Geschwindigkeit"
    override val m7ShiftingAdvice = "Schalthinweis"
    override val m7ShiftingTiming = "Timing für das Schalten"
    override val m7StartMode = "Startmodus"
    override val m7ConfirmConnectSprinterSwitch = "Ist der Sprintschalter verbunden?"
    override val m7Reflect = "Reflektieren (Eingabe)"
    override val m7Left = "Links"
    override val m7Right = "Rechts"
    override val m7SettingsCouldNotBeApplied = "Einstellungen konnten nicht übernommen werden."
    override val m7Preset = "VOREINSTELLUNG"
    override val m7SaveCurrentSettings = "Aktuelle Einstellungen speichern"
    override val m7WriteSettings = "Einstellungen schreiben"
    override val m7EditSettings = "Einstellungen bearbeiten"
    override val m7ExportSavedSettings = "Gespeicherte Einstellungen exportieren"
    override val m7Name = "Name"
    override val m7SettingWereNotApplied = "Die Einstellungen wurden nicht übernommen."
    override val m7DragAndDropFile = "Ziehen Sie die Einstellungsdatei über Drag-and-Drop hierhin."
    override val m7CharactersLimit = "1 bis 8 alphanumerisch Zeichen mit halber Breite"
    override val m7PasskeyDescriptionMessage =
        "Verwenden Sie für die Eingabe 6 Ziffern.\n0 kann nicht als erstes Zeichen im PassKey verwendet werden, außer während der PassKey-Initialisierung."
    override val m7Display = "Anzeigen"
    override val m7CompleteSettingMessage = "Die Einstellungen wurden normal abgeschlossen."

    override val m7ConfirmCancelSettingsTitle = "Einrichtung unterbrechen"
    override val m7ConfirmDefaultTitle = "Die Einstellungen auf die Standardwerte zurücksetzen"
    override val m7PasskeyDescription =
        "Geben Sie eine sechsstellige Nummer ein, die nicht mit 0 beginnt"
    override val m7AssistCharacterWithValue = "Fahrcharakteristik : Lv. {0}"
    override val m7MaxTorqueWithValue = "Maximales drehmoment : {0} Nm"
    override val m7AssistStartWithValue = "Fahrcharakteristik beim start : Lv. {0}"
    override val m7CustomizeDisplayName = "Anzeigenamen anpassen"
    override val m7DisplayNameDescription = "Bis zu 10 Zeichen"
    override val m7AssistModeDisplay = "Assistenzmodusanzeige"
    //endregion

    //region M8 カスタマイズ-スイッチ
    override val m8OptionalSwitchTitle = "Optionaler Schalter"
    override val m8SettingColudNotBeApplied = "Einstellung konnte nicht übernommen werden."
    override val m8MixedAssistShiftCheckMessage =
        "Unterstützung auf, Unterstützung ab, Anzeige/Lichter und hinten hochschalten, hinten herunterschalten, Anzeigen, Sie werden nicht in der Lage sein, D-FLY Kan. in einen einfachen Schalter einzustellen."
    override val m8EmptyConfigurableListMessage = "Es gibt keine konfigurierbaren Elemente."
    override val m8NotApplySettingMessage = "Einigen Tasten sind keine Funktionen zugeordnet."
    override val m8PressSwitchMessage =
        "Drücken Sie auf der Einheit auf den Schalter, den Sie auswählen möchten."
    override val m8SetToGearShifting = "Eingestellt auf Schalten"
    override val m8UseX2Y2 = "2. Gang verwenden"
    override val m8Model = "Modell"
    override val m8CustomizeSusSwitchPosition = "Eingestellt auf Positionseinstellung"
    override val m8SetReverseOperation = "Rückwärtsbetrieb auf Schalter Y1 eingestellt"
    override val m8PortA = "Anschluss A"
    override val m8PortB = "Anschluss B"
    override val m8NoUseMultiShift = "Verwenden Sie nicht die 2-Stufen-Gangschaltung."
    override val m8SetReverce = "Weisen Sie {0} die gegensätzliche Funktion zu"
    override val m8ConfirmReadDcasWSwitchStatusText =
        "Laden Sie die aktuelle Einstellung. Drücken Sie eine beliebige Taste auf {0} ({1})."
    override val m8ConfirmWriteDcasWSwitchStatusText = "Einstellungen schreiben. Drücken Sie eine beliebige Taste auf {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    override val m9SynchronizedShiftTitle = "Synchronized shift"
    override val m9TeethPatternSelectorHeaderTitle = "Anzahl der Zähne"
    override val m9Cancel = "Abbrechen"
    override val m9Casette = "Kassette"
    override val m9Edit = "Bearbeiten"
    override val m9FC = "Kurbel"
    override val m9CS = "Kassette"
    override val m9NoSetting = "Keine Einstellung"
    override val m9Table = "Tabelle"
    override val m9Animation = "Animation"
    override val m9DisplayConditionConfirmationMessage =
        "Zum Einstellen des Schaltmodus ist eine kompatible Umwerfer-/Schaltwerk-Verbindung erforderlich."
    override val m9NotApplicableMessage =
        "Prüfen Sie die folgenden Punkte, bevor Sie die Einstellungen übernehmen."
    override val m9NotApplicableUnselectedMessage = "S1 oder S2 ist nicht eingestellt."
    override val m9NotApplicableUnsettableValueMessage =
        "Der Schaltpunkt der synchronisierten Schalteinstellung ist in einem Bereich, in dem er nicht eingestellt werden kann."
    override val m9SettingFileUpperLimitMessage =
        "Die Speicherbegrenzung der Einstellungsdatei wurde überschritten."
    override val m9FrontUp = "Vorne auf"
    override val m9FrontDown = "Vorne ab"
    override val m9RearUp = "Hinten auf {0} Gang"
    override val m9RearDown = "Hinten ab {0} Gang"
    override val m9Crank = "die Kurbel"
    override val m9SynchronizedShift = "SYNCHRONIZED SHIFT"
    override val m9SemiSynchronizedShift = "SEMI-SYNCHRONIZED SHIFT"
    override val m9Option = "Option"
    override val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "Wenn sich die eingestellten Werte für die Anzahl der Zähne und die Steuerung der Gangstellung zwischen S1 und S2 unterscheiden, können die Einstellungen nicht übernommen werden."

    // TODO: TextTableには表記がないので要確認
    override val m9Name = "Name"

    //region M10 メンテナンス
    override val m10Battery = "Akku"
    override val m10DerailleurAdjustment = "Umwerfereinstellung"
    override val m10Front = "Vorn"
    override val m10Rear = "Hinten"
    override val m10NotesOnDerailleurAdjustmentMsg =
        "Die Einstellung des Umwerfers umfasst das Drehen der Kurbel von Hand. Zum Befestigen eines Fahrradwartungsständers und Ähnlichem, schaffen Sie bitte eine entsprechende Umgebung. Achten Sie zudem darauf, dass Sie Ihre Hand nicht zwischen dem Ritzel einklemmen."
    override val m10NotAskAgain = "Nicht wieder anzeigen"
    override val m10Step = "Schritt {0}/{1}"
    override val m10BeforePerformingElectricAdjustmentMsg =
        "Bevor mithilfe der App elektrische Einstellungen vorgenommen werden, muss mithilfe der Einstellschraube auf dem vorderen Umwerfer die Einstellung des größten Gangs vorgenommen werden. Genauere Angaben finden Sie unter „Hilfe“."
    override val m10MovementOfPositionOfDerailleurMsg =
        "Bringen Sie den Umwerfer in die in der Abbildung gezeigte Position."
    override val m10ShiftStartMesssage =
        "Die Gangschaltung ist gestartet. Bitte drehen Sie die Kurbel."
    override val m10SettingChainAndFrontDerailleurMsg =
        "Stellen Sie den Spalt zwischen der Kette und dem Umwerfer auf 0 bis 0,5 mm ein."
    override val m10Adjust = "Einstellung : {0}"
    override val m10Gear = "Gangposition"
    override val m10ErrorLog = "Fehlerprotokoll"
    override val m10AdjustTitle = "Einstellung"

    override val m10Restrictions = "BESCHRÄNKUNGEN"
    override val m10Remedy = "Abhilfemaßnahme"

    override val m10Status = "Status"
    override val m10DcasWBatteryLowVoltage = "Der Akkuladestand ist niedrig. Akku wechseln."
    override val m10DcasWBatteryUpdateInfo =
        "Prüfen Sie den Akkuladestand. Drücken Sie eine beliebige Taste auf {0} ({1})."
    override val m10LeverLeft = "Linker Hebel"
    override val m10LeverRight = "Rechter Hebel"

    // Error Message
    override val m10ErrorMessageSensorAbnormality =
        "Sensorabweichung in der Antriebseinheit erkannt."
    override val m10ErrorMessageSensorFailure = "Sensorfehler in der Antriebseinheit erkannt."
    override val m10ErrorMessageMotorFailure = "Motorfehler in der Antriebseinheit erkannt."
    override val m10ErrorMessageAbnormality =
        "Eine Abweichung wurde in der Antriebseinheit erkannt."
    override val m10ErrorMessageNotDetectedSpeedSignal =
        "Es wurde kein Fahrzeuggeschwindigkeitssignal vom Geschwindigkeitssensor erkannt."
    override val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "Es wurde ein abweichendes Fahrzeuggeschwindigkeitssignal vom Geschwindigkeitssensor erkannt."
    override val m10ErrorMessageBadCommunicationBTAndDU =
        "Kommunikationsfehler zwischen Akku und Antriebseinheit erkannt."
    override val m10ErrorMessageDifferentShiftingUnit =
        "Eine Schalteinheit, die von der Systemkonfiguration abweicht, wurde montiert."
    override val m10ErrorMessageDUFirmwareAbnormality =
        "Unregelmäßigkeit in der Firmware der Antriebseinheit erkannt."
    override val m10ErrorMessageVehicleSettingsAbnormality =
        "Es wurde eine Abweichung in den Fahrzeugeinstellungen erkannt."
    override val m10ErrorMessageSystemConfiguration = "Fehler aufgrund der Systemkonfiguration."
    override val m10ErrorMessageAbnormalHighTemperature =
        "Hohe Temperaturabweichung in der Antriebseinheit erkannt."
    override val m10ErrorMessageNotCompletedSensorInitialization =
        "Die Initialisierung des Sensors konnte nicht normal abgeschlossen werden."
    override val m10ErrorMessageUnexpectBTDisconnected = "Unerwartete Stromabschaltung erkannt."
    override val m10ErrorMessagePowerOffDueToOverTemperature =
        "Der Strom wurde AUSgeschaltet, da die Temperatur den garantierten Betriebsbereich überschritten hatte."
    override val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "Der Strom wurde AUSgeschaltet, da ein Stromverlust im System erkannt wurde."

    // Error Restrictions
    override val m10ErrorRestrictionsUnableUseAssistFunction =
        "Die Unterstützungsfunktion kann nicht verwendet werden."
    override val m10ErrorRestrictionsNotStartAllFunction = "Keine der Systemfunktionen startet."
    override val m10ErrorRestrictionLowerAssistPower =
        "Die Unterstützungsleistung wird niedriger als üblich."
    override val m10ErrorRestrictionLowerMaxAssistSpeed =
        "Die maximale Geschwindigkeit mit Unterstützung wird niedriger als üblich."
    override val m10ErrorRestrictionNoRestrictedAssistFunction =
        "Während der Anzeige sind die Funktionen der Motorunterstützung nicht eingeschränkt."
    override val m10ErrorRestrictionUnableToShiftGears = "Gangwechsel nicht möglich."

    // Error Remedy
    override val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Wenden Sie sich für weitere Unterstützung an Ihren Verkäufer oder einen Vertriebspartner in Ihrer Nähe."
    override val m10ErrorRemedyTurnPowerOffAndThenOn =
        "Schalten Sie den Strom AUS und wieder EIN. Wenn der Fehler weiterhin besteht, stellen Sie die Verwendung ein und wenden Sie sich an Ihre Verkaufsstelle oder an eine Vertretung in Ihrer Nähe."
    override val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n• Den Geschwindigkeitssensor und den Magneten an den entsprechenden Einbaupositionen montieren. \n• Wurde das Fahrrad modifiziert, sollte es auf die Standardeinstellung zurückgesetzt werden. \nGehen Sie vor, wie oben beschrieben, und fahren Sie das Fahrrad für eine kurze Weile, um den Fehler zu beheben. Wenn der Fehler weiterhin besteht oder die oben genannten Informationen nicht zutreffen, wenden Sie sich für weitere Unterstützung an Ihren Service-Partner."
    override val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n･ Stellen Sie sicher, dass das Kabel zwischen Antriebseinheit und Akku ordnungsgemäß angeschlossen ist."
    override val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n• Eine Verbindung mit E-TUBE PROJECT herstellen, um die Einstellungen zu überprüfen. Wenn Einstellungen und Fahrzeugstatus nicht übereinstimmen, ändern Sie den Fahrzeugstatus. \nBesteht der Fehler weiterhin, kontaktieren Sie für weitere Unterstützung Ihren Service-Partner."
    override val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n• Eine Verbindung mit E-TUBE PROJECT herstellen, um die Firmware wiederherzustellen. \nBesteht der Fehler weiterhin, kontaktieren Sie für weitere Unterstützung Ihren Service-Partner."
    override val m10ErrorRemedyContactBicycleManufacturer =
        "Kontaktieren Sie den Fahrradhersteller."
    override val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "Fahren Sie das Fahrrad nicht mit aktiviertem Unterstützungsmodus, bis die Temperatur der Antriebseinheit sinkt. \nWenden Sie sich für weitere Unterstützung an Ihren Verkäufer oder eine Vertretung in Ihrer Nähe, wenn der Fehler weiterhin besteht."
    override val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n• Den Geschwindigkeitssensor an der entsprechenden Einbauposition montieren. \n• Den Magneten an der entsprechenden Einbauposition montieren. (Im Abschnitt „Scheibenbremse“ unter „Allgemeine Bedienungsvorgänge“ bzw. in der Händlerbetriebsanleitung der STEPS-Serie wird erläutert, wie der entfernte Magnet wieder montiert wird.) \nWenn der Fehler weiterhin besteht oder die oben genannten Informationen nicht zutreffen, wenden Sie sich für weitere Unterstützung an Ihren Service-Partner."
    override val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Drücken Sie die Ein-/Ausschalttaste des Akkus und schalten Sie den Strom AUS. Schalten Sie den Strom anschließend wieder EIN, ohne währenddessen die Pedale mit Ihren Füßen zu berühren.\nWenden Sie sich für weitere Unterstützung an Ihren Verkäufer oder eine Vertretung in Ihrer Nähe, wenn der Fehler weiterhin besteht."
    override val m10ErrorRemedyTorqueSensorOffset =
        "• Wenn es sich bei der Antriebseinheit um den DU-EP800 handelt (der Fahrradcomputer zeigt W103 an): Drehen Sie die Kurbeln zwei oder drei Mal rückwärts. \n• Wenn es sich bei der Antriebseinheit um ein anderes Modell handelt (die Fahrradcomputer zeigt W013 an): Drücken Sie die Ein-/Ausschalttaste des Akkus und schalten Sie den Strom AUS. Schalten Sie den Strom anschließend wieder EIN, ohne währenddessen die Pedale mit Ihren Füßen zu berühren. \nWenden Sie sich für weitere Unterstützung an Ihre Verkaufsstelle oder an eine Vertretung, wenn der Fehler nach Durchführung des oben Genannten weiterhin besteht."
    override val m10ErrorRemedyBTConnecting =
        "Schalten Sie den Strom AUS und wieder EIN.\nWenn W105 dauerhaft angezeigt wird, bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen. \n• Beseitigen Sie klappernde Geräusche an der Akkuhalterung und stellen Sie sicher, dass die Akkuhalterung korrekt befestigt ist. \n• Prüfen Sie, ob das Netzkabel beschädigt ist.Falls ja, sollte es ausgetauscht werden. \nBesteht der Fehler weiterhin, kontaktieren Sie für weitere Unterstützung Ihren Service-Partner."
    override val m10ErrorRemedyOverTemperature =
        "Wenn die Temperatur den Bereich überschritten hat, in dem eine Entladung möglich ist, lassen Sie den Akku an einem kühlen Ort ohne direkte Sonneneinstrahlung liegen, bis die interne Temperatur des Akkus ausreichend gesenkt wurde. Liegt die Temperatur unterhalb der Entladetemperatur, den Akku in einem Innenraum belassen, bis die interne Akkutemperatur einen geeigneten Wert erreicht hat. \nWenden Sie sich für weitere Unterstützung an Ihren Verkäufer oder eine Vertretung in Ihrer Nähe, wenn der Fehler weiterhin besteht."
    override val m10ErrorRemedyReplaceShiftingUnit =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen: \n• Den aktuellen Systemstatus in E-TUBE PROJECT prüfen und die mit der passenden Schalteinheit ersetzen. \nBesteht der Fehler weiterhin, kontaktieren Sie für weitere Unterstützung Ihren Service-Partner."
    override val m10ErrorRemedyCurrentLeakage =
        "Bitten Sie die Verkaufsstelle darum, die folgenden Schritte durchzuführen. \nEntfernen Sie die Komponenten außer der Antriebseinheit nacheinander. Schalten Sie danach den Akkustrom wieder EIN. \n･ Der Strom wird beim Starten AUSgeschaltet, wenn die Komponente, die das Problem verursacht, nicht entfernt wurde.  \n･ Der Strom wird EINgeschaltet und W104 wird weiterhin angezeigt, wenn die das Problem verursachende Komponente entfernt wurde.  \n･ Ersetzen Sie die Komponente, sobald die Komponente, die das Problem verursacht, identifiziert wurde."
    override val m10ErrorTorqueSensorOffsetAdjust =
        "Drücken Sie die Ein-/Ausschalttaste des Akkus und schalten Sie den Strom AUS. Schalten Sie den Strom anschließend wieder EIN, ohne dass Sie die Füße auf den Pedalen haben. Sollte die Warnung nach dem ersten Versuch noch immer angezeigt werden, dann wiederholen Sie den gleichen Vorgang, bis sie nicht mehr angezeigt wird. \n*Der Sensor initialisiert auch dann, wenn das Fahrrad in Bewegung ist. Wenn Sie das Fahrrad weiter fahren, verbessert sich die Situation gegebenenfalls. \n\n Wenn sich die Situation nicht verbessert, dann bitten Sie die Verkaufsstelle oder eine Vertretung, Folgendes vorzunehmen: \n• Stellen Sie die Kettenspannung ein. \n• Betätigen Sie die Pedale und heben Sie dabei das Hinterrad an, so dass sich das Hinterrad frei drehen kann. Ändern Sie die Stopp-Position der Pedale, während sich das Laufrad dreht, bis die Warnung nicht mehr angezeigt wird."

    // TODO: ドイツ語訳の確認
    override val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    override val m11Name = "Name"
    override val m11Change = "Ändern"
    override val m11Power = "STROM"
    override val m11Cadence = "TRITTFREQUENZ"
    override val m11Watt = "Watt"
    override val m11Rpm = "U/min"
    override val m11Waiting = "Warten..."
    override val m11CaribrationIsCompleted = "Die Kalibrierung ist abgeschlossen."
    override val m11CaribrationIsFailed = "Die Kalibrierung ist fehlgeschlagen."
    override val m11N = "N"
    override val m11PowerMeter = "Leistungsmesser"
    override val m11ZeroOffsetCaribration = "Nullpunkt-Kalibrierung"
    override val m11ChangeBike = "Fahrrad ändern"
    override val m11CancelPairing = "Koppelung abbrechen"
    override val m11Set = "Einstellen"
    override val m11RegisteredBike = "Registriertes Fahrrad"
    override val m11RedLighting = "Leistungsmesser-Akkuladestand: \n1 - 5 %"
    override val m11RedBlinking = "Leistungsmesser-Akkuladestand: \nWeniger als 1 ％"
    //endregion

    //region M12 アプリケーション設定
    override val m12Acconut = "Konto"
    override val m12ShimanoIDPortal = "SHIMANO ID-PORTAL"
    override val m12Setting = "Voreinstellungen"
    override val m12ApplicationSettings = "Programmeinstellungen"
    override val m12AutoBikeConnection = "Automatische Fahrradverbindung"
    override val m12Language = "Sprache"
    override val m12Link = "Link"
    override val m12ETubeProjectWebsite = "E-TUBE PROJECT Website"
    override val m12ETubeRide = "E-TUBE RIDE"
    override val m12Other = "Sonstiges"
    override val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    override val m12PoweredByShimano = "Powered by SHIMANO"
    override val m12SignUpLogIn = "Registrieren/Anmelden"
    override val m12CorporateClient = "Firmenkunde"
    override val m12English = "Englisch"
    override val m12French = "Französisch"
    override val m12German = "Deutsch"
    override val m12Dutch = "Niederländisch"
    override val m12Spanish = "Spanisch"
    override val m12Italian = "Italienisch"
    override val m12Chinese = "Chinesisch"
    override val m12Japanese = "Japanisch"
    override val m12Change = "Ändern"
    override val m12AppOperationLogAgreement = "Vereinbarung über das Betriebsprotokoll der App"
    override val m12AgreeAppOperationLogAgreemenMsg =
        "Ich stimme der Vereinbarung über das Betriebsprotokoll der App zu."
    override val m12DataProtecitonNotice = "Datenschutzerklärung"
    override val m12Apache = "Apache"

    // TODO: 仮
    override val m12AppOperationLog = "アプリ操作ログ取得"

    override val m12ConfirmLogout =
        "Möchten Sie sich abmelden? Außerdem werden alle momentan verbundenen Bauteile getrennt."
    //endregion

    //region M13 ヘルプ
    override val m13Connection = "Verbindung"
    override val m13Title = "Hilfe"
    override val m13Operation = "Betrieb"
    override val m13HowToConnectBike = "So verbinden Sie ein Fahrrad"
    override val m13SectionCustomize = "Individuell anpassen"
    override val m13SynchronizedShiftTitle = "Synchronized shift"
    override val m13Maintenance = "Wartung"
    override val m13FrontDerailleurAdjustment = "Einstellung des vorderen Umwerfers"
    override val m13RearDerailleurAdjustment = "Einstellung des hinteren Schaltwerks"
    override val m13ZeroOffsetCalibration = "Nullpunkt-Kalibrierung"
    override val m13SystemInformationDisplayTitle = "Für das Informationsdisplay"
    override val m13JunctionATitle = "Für Kontaktstelle (A)"
    override val m13SCE8000Title = "Für SC-E8000"
    override val m13CycleComputerTitle = "Für Fahrradcomputer/EW-EN100\n(außer SC-E8000)"
    override val m13PowerMeterTitle = "Für FC-R9100-P"
    override val m13SystemInformationDisplayDescription =
        "Halten Sie den Modusschalter am Fahrrad gedrückt, bis „C“ auf dem Display erscheint."
    override val m13ModeSwitchAnnotationRetention =
        "* Sobald das Fahrrad für die Verbindung bereit ist, lassen Sie den Modusschalter oder die Taste los. Wenn Sie den Modusschalter oder die Taste weiterhin gedrückt halten, wechselt das System in einen anderen Modus."
    override val m13ShiftMode = "Schaltmodus"
    override val m13LongPress = "Drücken und gedrückt halten"
    override val m13Seconds = "{0} S. oder länger"
    override val m13ConnectionMode = "Bluetooth LE-Verbindungsmodus"
    override val m13AdjustMode = "Einstellungsmodus"
    override val m13RDProtectionResetMode = "Schaltwerk-Reset-Modus"
    override val m13ExitModeDescription =
        "Halten Sie den Schalter mindestens 0,5 Sekunden lang gedrückt, um den Einstellungsmodus oder den Schaltwerk-Reset-Modus zu beenden."
    override val m13JunctionADescription =
        "Halten Sie die Taste für Kontaktstelle A gedrückt, bis die grüne und die rote LED abwechselnd aufleuchten."
    override val m13HowToConnectWirelessUnit =
        "Wenn Sie A gedrückt halten, während sich das Fahrrad nicht bewegt, wird auf dem Fahrradcomputer der Menülistenbildschirm angezeigt. Drücken Sie X1 oder Y1, um Bluetooth LE auszuwählen und drücken Sie dann A, um zu bestätigen. \nWählen Sie auf dem Bluetooth LE-Bildschirm „Start“ und drücken Sie dann A, um zu bestätigen."
    override val m13CommunicationConditions =
        "Kommunikation ist nur unter folgenden Bedingungen erlaubt. \nBitte führen Sie eine der folgenden Aktionen aus."
    override val m13TurningOnStepsMainPower =
        "・ 15 Sekunden lang, nachdem die Hauptstromversorgung für SHIMANO STEPS EINgeschaltet wurde."
    override val m13AnyButtonOperation =
        "・ 15 Sekunden lang, nachdem eine der Tasten (außer die Taste für die Hauptstromversorgung für SHIMANO STEPS) bedient wurde."
    override val m13PowerMeterDescription = "Drücken Sie dann die Taste an der Steuereinheit."
    override val m13SynchronizedShift = "Synchronisierte Schaltung"
    override val m13SynchronizedShiftDetail =
        "Diese Funktion schaltet den Umwerfer automatisch synchron mit „Hinten hochschalten“ und „Hinten herunterschalten“. "
    override val m13SemiSynchronizedShift = "Halbsynchronisierte Schaltung"
    override val m13SemiSynchronizedShiftDetail =
        "Mit dieser Funktion wird beim Schalten des Umwerfers automatisch auch das Schaltwerk geschaltet, um einen optimalen Gangwechsel zu erzielen."
    override val m13Characteristics = "Merkmale"
    override val m13CharacteristicsFeature1 =
        "Schneller Multi-Shift-Modus ist nun aktiviert.\n\n•Diese Funktion ermöglicht eine schnelle Anpassung der Kurbel-RPM auf veränderte Fahrbedingungen.\n•Sie ermöglicht eine schnelle Anpassung Ihrer Geschwindigkeit."
    override val m13CharacteristicsFeature2 = "Ermöglicht einen zuverlässigen Multi-Shift-Betrieb"
    override val m13Note = "Hinweis"
    override val m13OperatingPrecautionsUse1 =
        "1. Es besteht die Gefahr des Überschaltens.\n\n2. Bei zu niedriger Kurbel-RPM kann die Kette nicht mit der Bewegung des hinteren Schaltwerks mithalten.\nAls Folge kann die Kette vom Kettenblatt abspringen, anstatt in den Kassettenzahnkranz einzugreifen."
    override val m13OperatingPrecautionsUse2 = "Der Multi-Shift-Betrieb dauert einen Moment"
    override val m13Clank = "Zu verwendende Geschwindigkeit des Kurbelarms"
    override val m13ClankFeature = "Bei hoher Kurbel-RPM"
    override val m13AutoShiftOverview =
        "SHIMANO STEPS mit einer DI2-Nabenschaltung kann die Gangschaltung entsprechend der Fahrbedingung automatisch schalten."
    override val m13ShiftTiming = "Schalteinstellung:"
    override val m13ShiftTimingOverview =
        "Ändert den Trittfrequenzstandard für die automatische Gangschaltung. Den eingestellten Wert für schnelles Treten unter niedriger Last erhöhen. Verringern Sie den eingestellten Wert für langsames Treten unter mäßiger Last."
    override val m13StartMode = "Startmodus: "
    override val m13StartModeOverview =
        "Mit dieser Funktion wird automatisch heruntergeschaltet, sobald Sie an einer roten Ampel anhalten usw., sodass Sie mit einem voreingestellten niedrigeren Gang wieder anfahren können. Diese Funktion dient der automatischen oder manuellen Gangschaltung."
    override val m13AssistOverview = "Es stehen zwei Unterstützungsmuster zur Auswahl."
    override val m13Comfort = "COMFORT :"
    override val m13ComfortOverview =
        "Sorgt für eine geschmeidigere Fahrt und ein typisches Fahrradgefühl mit einem maximalen Drehmoment von 50 Nm."
    override val m13Sportive = "SPORTIVE :"
    override val m13SportiveOverview =
        "Bietet Ihnen Unterstützungsleistung, mit der Sie große Steigungen mit dem maximalen Drehmoment von 60 Nm leicht bezwingen können. (Je nach Modell der eingebauten Schalteinheit kann das maximale Drehmoment auf 50 Nm beschränkt sein.)"
    override val m13RidingCharacteristicOverview =
        "Es stehen drei Fahrcharakteristiken zur Auswahl."
    override val m13Dynamic = "(1) DYNAMIC :"
    override val m13DynamicOverview =
        "Der Unterstützungsmodus verfügt über drei Stufen (ECO/TRAIL/BOOST), die mithilfe eines Schalters geändert werden können. Bei der Einstellung DYNAMIC ist der Unterschied zwischen diesen drei Stufen des Unterstützungsmodus am größten. Es bietet Ihnen Unterstützung beim Fahren auf einem E-MTB mit „ECO“, der für mehr Unterstützungsleistung sorgt als der ECO-Modus in der Einstellung EXPLORER; „TRAIL“ bietet überragende Steuerung und „BOOST“ leistungsstarke Beschleunigung."
    override val m13Explorer = "(2) EXPLORER :"
    override val m13ExplorerOverview =
        "EXPLORER bietet für die drei Stufen des Unterstützungsmodus eine hohe Regulierbarkeit der Unterstützungsleistung und einen niedrigen Akkuverbrauch. Dieser Modus eignet sich zum Fahren auf einspurigen Wegen. "
    override val m13Customize = "(3) CUSTOMIZE :"
    override val m13CustomizeOverview =
        "Die gewünschte Unterstützungsstufe kann für jede der drei Stufen des Unterstützungsmodus zwischen LOW/MEDIUM/HIGH gewählt werden."
    override val m13AssistProfileOverview =
        "Sie können zwei Typen von Unterstützungsprofilen erstellen, die Ihnen dann zur Auswahl stehen. Die Profile können auch mit SC geschaltet werden. Für ein Profil können für jede der drei Stufen des Unterstützungsmodus (ECO/TRAIL/BOOST) drei Parameter angepasst werden, die mithilfe eines Schalters geändert werden können."
    override val m13AssistCharacter = "(1) Unterstützungsmerkmal :"
    override val m13AssistCharacterOverview =
        "Mit SHIMANO STEPS wird das Unterstützungsdrehmoment auf den Pedaldruck angepasst angewendet. Wenn die Einstellung auf POWERFUL gesetzt wird, wird selbst bei niedrigem Pedaldruck Unterstützung bereitgestellt. Mit der Einstellung ECO kann die Balance zwischen der Unterstützungsstufe und dem niedrigen Akkuverbrauch optimiert werden. "
    override val m13MaxTorque = "(2) Max. Drehmoment :"
    override val m13MaxTorqueOverview =
        "Die maximale Leistung des Unterstützungsdrehmoments der Antriebseinheit kann geändert werden."
    override val m13AssistStart = "(3) Unterstützungsstart :"
    override val m13AssistStartOverview =
        "Der Zeitpunkt, an dem Unterstützung bereitgestellt wird, kann geändert werden. Wenn die Einstellung auf QUICK gesetzt ist, wird die Unterstützung schnell bereitgestellt, nachdem sich die Kurbel zu drehen beginnt. Befindet sich die Einstellung auf MILD, wird die Unterstützung langsam bereitgestellt."
    override val m13DisplaySpeedAdjustment = "Einstellung der Anzeigegeschwindigkeit"
    override val m13DisplaySpeedAdjustmentOverview =
        "Wenn sich die auf dem Fahrradcomputer angezeigte Geschwindigkeit von der auf Ihrem Gerät angezeigten Geschwindigkeit unterscheidet, passen Sie die angezeigte Geschwindigkeit an.\nWenn Sie den Wert durch Drücken von Unterstützung-X erhöhen, erhöht sich der Wert für die angezeigte Geschwindigkeit.\nWenn Sie den Wert durch Drücken von Unterstützung-Y senken, sinkt der Wert für die angezeigte Geschwindigkeit."
    override val m13SettingIsNotAvailableMessage =
        "*Diese Einstellung ist für einige Antriebseinheitsmodelle nicht verfügbar."
    override val m13TopSideOfTheAdjustmentMethod = "Einstellen des größten Gangs"
    override val m13SmallestSprocket = "Kleinstes Ritzel"
    override val m13LargestGear = "Größte Übersetzung"
    override val m13Chain = "Kette"
    override val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nDrehen Sie die obere Stellschraube mit einem 2-mm-Innensechskantschlüssel. Der Abstand zwischen der Kette und der Kettenführungs-Außenplatte sollte 0, 5 bis 1 mm betragen."
    override val m13TopSideOfAdjustmentMethod =
        "Bevor mithilfe der App elektrische Einstellungen vorgenommen werden, muss mithilfe der Einstellschraube auf dem vorderen Umwerfer die Einstellung des größten Gangs vorgenommen werden. Folgen Sie den nachfolgenden Schritten, um die Einstellung vorzunehmen."
    override val m13BoltLocationCheck = "Überprüfung der Schraubenposition"
    override val m13BoltLocationCheckDescription =
        "Die innere Einstellschrauben, die obere Einstellschraube und die Stützschraube befinden sich nahe beieinander.\nStellen Sie sicher, dass Sie die richtige Schraube einstellen."
    override val m13Bolts =
        "(A) Untere Einstellschraube\n(B) Obere Einstellschraube\n(C) Stützschraube"
    override val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nStellen Sie die Kette auf das größte Kettenblatt und das kleinste Ritzel."
    override val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nLösen Sie die Hub-Befestigungsschraube mit einem 2 mm-Inbusschlüssel.\n (A) Hub-Einstellschraube\n (B) Obere Einstellschraube"
    override val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nDrehen Sie die obere Einstellschraube mit einem 2-mm-Inbusschlüssel und passen Sie den Abstand an. Stellen Sie die Lücke zwischen der Kette und der Platte im Inneren der Kettenführung auf 0 bis 0, 5 mm ein."
    override val m13AfterAdujustmentMessge =
        "4.\nZiehen Sie nach der Einstellung die Hub-Befestigungsschraube fest."
    override val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nBewegen Sie die Kette auf das fünfte Ritzel. Stellen Sie die Führungsrolle nach innen, bis die Kette am vierten Zahnkranz streift und ein leises Geräusch verursacht."
    override val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nStellen Sie die Leitrolle um vier Stufen (fünf Stufen bei MTB) nach außen in die Zielposition."
    override val m13PerformingZeroOffsetTitle = "Nullpunkt-Kalibrierung durchführen"
    override val m13PerformingZeroOffsetMsg =
        "1.\nStellen Sie das Fahrrad auf einer ebenen Fläche ab.\n\n2.\nStellen Sie den Kurbelarm so, dass er im rechten Winkel zum Boden steht, wie in er Abbildung gezeigt.\n\n3.\nDrücken Sie die Schaltfläche \"Nullpunkt-Kalibrierung\".\n\nSetzen Sie Ihre Füße nicht auf die Pedale und belasten Sie die Kurbel nicht."
    override val m13ShiftingAdvice = "Schaltvorschlag:"
    override val m13ShiftingAdviceOverview =
        "Mithilfe dieser Funktion wird auf dem Fahrradcomputer beim manuellen Schalten über die angemessene Schalteinstellung informiert. Der Zeitpunkt, an dem die Meldung angezeigt wird, variiert je nach eingestelltem Wert der Schalteinstellung."
    override val m13PerformingLowAdjustment =
        "So wird der kleinste Gang eingestellt (nur FD-6870/FD-9070)"
    override val m13PerformingLowAdjustmentDescription1 =
        "1.\nStellen Sie die Kette auf das kleinste Kettenblatt und den größten Zahnkranz."
    override val m13PerformingLowAdjustmentDescription2 =
        "2.\nDrehen Sie die untere Einstellschraube mit einem 2-mm-Innensechskantschlüssel. Stellen Sie den Abstand zwischen Kette und äußerem Kettenleitblech auf 0 bis 0,5mm ein."
    override val m13FrontDerailleurMethod2 =
        "2.\nLösen Sie die Hub-Befestigungsschraube mit einem 2-mm-Innensechskantschlüssel.\n(A) Hub-Einstellschraube\n(B) Obere Einstellschraube"
    override val m13FrontDerailleurMethod3 =
        "3.\nDrehen Sie die obere Einstellschraube mit einem 2-mm-Innensechskantschlüssel und passen Sie den Abstand an. Stellen Sie den Abstand zwischen Kette und innerem Kettenleitblech auf 0 bis 0,5 mm ein."
    override val m13VerySlow = "sehr langsam"
    override val m13Slow = "langsam"
    override val m13Normal = "normal"
    override val m13Fast = "schnell"
    override val m13VeryFast = "sehr schnell"
    override val m13MtbTopSideOfAdjustmentMethod1 =
        "1.\nStellen Sie die Kette auf das größte Kettenblatt und das größte Ritzel."
    override val m13AdjustmentMethod = "調整方法"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    override val dialog17SwipeToSwitchTitle = "Wischen Sie zum Umschalten"
    override val dialog28BluetoothOffTitle = "Bluetooth AUS"
    override val dialog29LocationInformationOffTitle = "Standortinformationen　AUS"
    override val dialog2_3AddBikeTitle = "Fahrrad hinzufügen"
    override val dialog25PasskeyErrorTitle = "Passkey-Fehler"
    override val dialog27RegistersBikeTitle = "Registrierte Einheit"
    override val dialog2_8RegisterBikeNameTitle = "Registrieren Sie die Fahrradbezeichnung"
    override val dialog2_9PasskeyTitle = "Passkey"
    override val dialog2_11ConfirmBikeTitle = "Fahrrad prüfen"
    override val dialog69SwitchBikeConnectionTitle = "Aktuelles Fahrrad trennen"
    override val dialog3_4UnitNotDetectdTitle = "Es wurde keine Einheit erkannt"
    override val dialog3_5UnitRegisteredAgainTitle = "Einheit erneut registrieren"
    override val dialog4_1UpdateDetailTitle = "{0} ver.{1}"
    override val dialog4_2AllUpdateConfirmTitle = "Möchten Sie alle Einheiten\naktualisieren?"
    override val dialog4_4CancelUpdateTitle = "Update abbrechen"
    override val dialog6_1DeleteBikeTitle = "Fahrrad löschen"
    override val dialog6_2ConfirmTitle = "Bestätigen"
    override val dialog6_3DeleteTitle = "Löschen"
    override val dialog7_1ConfirmCancelSettingTitle = "Einrichtung unterbrechen"
    override val dialog7_2ConfirmDefaultTitle =
        "Die Einstellungen auf die Standardwerte zurücksetzen"
    override val dialog10_2PauseSetupTitle = "Einstellung unterbrechen"
    override val dialog_jira258PushedIntoThe2ndGearTitle =
        "Durchgeführter Vorgang, wenn der Schalter im 2. Gang gedrückt wird"
    //endregion

    //region 文章
    override val dialog01ConfirmExecuteZeroOffsetMessage =
        "Möchten Sie die Nullpunktverschiebung aktivieren?"
    override val dialog02ConnectedMessage = "Verbunden."
    override val dialog02CaribrationIsCompleted = "Die Kalibrierung ist abgeschlossen."
    override val dialog02CaribrationIsFailed = "Die Kalibrierung ist fehlgeschlagen."
    override val dialog03FailedToExecuteZeroOffsetMessage =
        "Ausführen der Nullpunktverschiebung fehlgeschlagen."
    override val dialog03DoesNotrespondProperlyMessage =
        "Der Leistungsmesser reagiert nicht ordnungsgemäß."
    override val dialog04UpdatingFirmwareMessage = "Firmware wird aktualisiert …"
    override val dialog05CompleteSetupMessage = "Einrichtung wurde abgeschlossen."
    override val dialog06ConfirmStartUpdateMessage =
        "Vor der Aktualisierung unbedingt den „HINWEIS“ unten durchlesen. Während der Aktualisierung keine anderen Vorgänge durchführen."
    override val dialog06AllUpdateConfirmMessage = "Ungefähr erforderliche Zeit: {0}"
    override val dialog07CancelUpdateMessage =
        "Möchten Sie das Firmware-Update abbrechen?\n* Wenn Sie „Abbrechen“ auswählen, werden die Updates ab der nächsten Einheit abgebrochen. Das Update, das für die aktuelle Einheit gerade ausgeführt wird, wird fortgeführt."
    override val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Überprüfen Sie die folgenden Änderungen an {0}, die sich aus der Wiederherstellung der Firmware ergeben."
    override val dialog13Changes = "Änderungen"
    override val dialog13MustAgreeToTermsOfUseMessage =
        "Sie müssen den Nutzungsbedingungen für die Software zustimmen, um die Firmware wiederherstellen zu können."
    override val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Contrat de licence logiciel supplémentaire\n (À lire impérativement)"
    override val dialog15FirmwareWasUpdateMessage =
        "Die Firmware von {0} funktioniert normal.\nDie Firmware wurde auf die neueste Version aktualisiert."
    override val dialog15FirmwareRestoreMessage = "Die Firmware von {0} wurde wiederhergestellt."
    override val dialog15CanChangeTheSwitchSettingMessage =
        "Bei Verwendung von {0} können Sie die Einstellung des Federungsstatus, die in SC angezeigt wird, über den Schaltereinstellung im Menü „Anpassen“ ändern."
    override val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "Die Firmware von {0} funktioniert normal.\nSie muss nicht wiederhergestellt werden."
    override val dialog15FirmwareRestorationFailureMessage =
        "Firmware-Wiederherstellung von {0} ist fehlgeschlagen."
    override val dialog15MayBeFaultyMessage = "{0} ist möglicherweise defekt."
    override val dialog15FirmwareRecoveryAgainMessage =
        "Firmware-Wiederherstellung für {0} wird erneut durchgeführt."
    override val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "Die Firmware-Datei von {0} ist ungültig.\nNeueste Version der Firmware herunterladen?"
    override val dialog15FirstUpdateFirmwareMessage =
        "Aktualisieren Sie zunächst die Firmware für {0}."
    override val diaog15CannotUpdateMessage =
        "Update kann aufgrund von schlechtem Signalempfang nicht durchgeführt werden. Versuchen Sie es erneut an einem Ort, an dem der Signalempfang besser ist."
    override val dialog15ConfirmStartUpdateMessage =
        "Die Aktualisierung der Firmware wird einige Minuten in Anspruch nehmen. \nWenn der Ladestand des Akkus für Ihr Gerät zu niedrig ist, führen Sie die Aktualisierung nach dem Aufladen durch oder schließen Sie den Akku an ein Ladegerät an. \nAktualisierung beginnen?"
    override val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Verbindung nach Firmware-Aktualisierung fehlgeschlagen.\nBitte stellen Sie die Verbindung erneut her."
    override val dialog15ConfirmConnectedUnitMessage =
        "{0} wird erkannt.\nIst {0} die angeschlossene Einheit?"
    override val dialog17RegistrationIsCompletedMessage = "Registrierung ist abgeschlossen."
    override val dialog17ConnectionIsCompletedMessage = "Verbindung ist abgeschlossen."
    override val dialog19TryAgainWirelessCommunicationMessage =
        "Die Drahtlosumgebung ist nicht stabil. Versuchen Sie es erneut an einem Ort, an dem die Verbindung stabil ist."
    override val dialog20BetaVersionMsg =
        "Diese Software ist eine Betaversion.\n Die Evaluation ist noch nicht abgeschlossen und es besteht die Möglichkeit, dass verschiedene Probleme auftreten können.\n Werden Sie diese Software verwenden, nachdem Sie diesen Punkten zugestimmt haben?"
    override val dialog21BetaVersionMsg =
        "Diese Software ist eine Betaversion.\n Die Evaluation ist noch nicht abgeschlossen und es besteht die Möglichkeit, dass verschiedene Probleme auftreten können.\n Werden Sie diese Software verwenden, nachdem Sie diesen Punkten zugestimmt haben?\n \n Ablaufdatum der Software : {0}"
    override val dialog22BetaVersionMsg =
        "Diese Software ist eine Betaversion.\n Das Ablaufdatum dieser Software ist abgelaufen.\n Bitte installieren Sie die Original-Software neu."
    override val dialog24PasskeyMessage = "Geben Sie Ihren Passkey ein."
    override val dialog25PasskeyErrorMessage =
        "Authentifizierung nicht möglich. Der eingegebene PassKey ist falsch oder der PassKey für die Funkeinheit wurde geändert. Stellen Sie die Verbindung wieder her und versuchen Sie, den in der Funkeinheit festgelegten PassKey erneut einzugeben."
    override val dialog26ChangePasskeyMessage = "Möchten Sie Ihren Passkey ändern?"
    override val dialog27RegisterdBikeMessage =
        "{0} ist bereits als drahtlose Einheit für {1} registriert. Möchten Sie es als drahtlose Einheit für das neue Fahrrad verwenden?* Die Registrierung für {1} wird aufgehoben."
    override val dialog28BluetoothOffMesssage =
        "Schalten Sie die Bluetooth-Einstellung auf dem Gerät EIN, um eine Verbindung herzustellen."
    override val dialog29LocationInformationOffMessage =
        "Aktivieren Sie den Standortinformationsdienst, um eine Verbindung herzustellen."
    override val dialog30DeleteBikeMessage =
        "Wenn ein Fahrrad gelöscht wird, werden auch seine zugehörigen Daten gelöscht."
    override val dialog31ConfirmDeleteBikeMessage = "Möchten Sie das Fahrrad wirklich löschen?"
    override val dialog32UpdatedProperlyMessage =
        "Die Einstellungen wurden ordnungsgemäß aktualisiert."
    override val dialog32UpdateCompletedMessage = "Update ist abgeschlossen."
    override val dialog36PressSwitchMessage =
        "Drücken Sie auf der Einheit auf den Schalter, den Sie auswählen möchten."
    override val dialog37SelectTheDerectionMessage =
        "Auf welchem Griff ist der Bedienschalter installiert?"
    override val dialog38SameMarksCannotBeAssignedMessage =
        "- Falls sich die Kombinationsmuster der vorderen und hinteren Aufhängung unterscheiden, ist es nicht möglich, die gleiche Markierung (CTD) zuzuweisen."
    override val dialog38DifferentMarksCannotBeAssignedMessage =
        "- Wird das gleiche Kombinationsmuster für die vordere und hintere Aufhängung verwendet, ist es nicht möglich, unterschiedliche Markierungen (CTD) zuzuweisen."
    override val dialog39CheckTheFollowingInformationMessage =
        "Bitte überprüfen Sie die folgende Nachricht."
    override val dialog39ConfirmContinueWithProgrammingMessage =
        "Ist es OK, um mit der Programmierung fortzufahren?"
    override val dialog39SameSettingMessage =
        "- Die gleiche Einstellung wurde für zwei oder mehr Positionen gewählt."
    override val dialog39OnlyHasBeenSetMessage = "- Nur {1} wurde für {0} eingestellt."
    override val dialog40CannotSelectSwitchMessage =
        "Wenn Sie einen oder mehrere der Schalter nicht zur Auswahl verwenden können, prüfen Sie, ob irgendwelche Stromkabel getrennt sind. \nWenn dies der Fall ist, schließen Sie sie wieder an. \nAndernfalls ist möglicherweise ein Schalter defekt."
    override val dialog41ConfirmDefaultMessage =
        "Möchten Sie alle Einstellungen auf die Standardwerte zurücksetzen?"
    override val dialog42ConfirmDeleteSettingMessage =
        "Möchten Sie die Einstellungen wirklich löschen?"
    override val dialog43ComfirmUnpairPowerMeterMessage =
        "Möchten Sie den Leistungsmesser entkoppeln?"
    override val dialog44ConfirmCancelSettingMessage =
        "Möchten Sie die eingegebenen Informationen wirklich verwerfen?"
    override val dialog45DisplayConditionConfirmationMessage =
        "Synchronized Shift konnte nicht eingerichtet werden. Weitere Informationen finden Sie auf der E-TUBE PROJECT-Website."
    override val dialog46DisplayConditionConfirmationMessage =
        "Multi-Shift-Modus konnte nicht eingerichtet werden. Weitere Informationen finden Sie auf der E-TUBE PROJECT-Website."
    override val dialog47DuplicateSwitchesMessage =
        "Mehrere identische Schalter sind angeschlossen. Die Einstellungen konnten nicht gespeichert werden."
    override val dialog48FailedToUpdateSettingsMessage =
        "Das Aktualisieren der Einstellungen ist fehlgeschlagen."
    override val dialog48FailedToConnectBicycleMessage =
        "Das Verbinden mit dem Fahrrad ist fehlgeschlagen."
    override val dialog49And50CannotOperateOormallyMessage =
        "Der Schaltpunkt liegt im nicht einstellbaren Bereich und funktioniert nicht normal."
    override val dialog51ConfirmContinueSettingMessage =
        "Die folgenden Funktionen sind nicht eingeschlossen. Möchten Sie wirklich fortfahren?\n{0}"
    override val dialog55NoInformationMessage =
        "Es wurden keine Informationen für dieses Fahrrad gefunden."
    override val dialog56UnpairConfirmationDialogMessage =
        "Sind Sie sicher, dass Sie den Leistungsmesser löschen möchten?"
    override val dialog57DisconnectBluetoothDialogMessage =
        "Der Leistungsmesser wurde gelöscht. Die drahtlose Einheit muss auch in der Bluetooth-Einstellung des Betriebssystems gelöscht werden."
    override val dialog58InsufficientStorageAvailableMessage =
        "Es ist nicht genug Speicherplatz verfügbar. Die Voreinstellungsdatei konnte nicht gespeichert werden."
    override val dialog59ConfirmLogoutMessage = "Möchten Sie sich abmelden?"
    override val dialog59LoggedOutMessage = "Sie haben sich abgemeldet."
    override val dialog60BeforeApplyingSettingMessage =
        "Prüfen Sie die folgenden Punkte, bevor Sie die Einstellungen übernehmen."
    override val dialog60S1OrS2IsNotSetMessage = "S1 oder S2 ist nicht eingestellt."
    override val dialog60AlreadyAppliedSettingMessage =
        "Diese Einstellung wurde bereits angewendet."
    override val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "Wenn sich die eingestellten Werte für die Anzahl der Zähne und die Steuerung der Gangstellung zwischen S1 und S2 unterscheiden, können die Einstellungen nicht übernommen werden."
    override val dialog60NotApplicableDifferentSettingsMessage =
        "Wenn sich die eingestellten Werte für die Schaltintervalle zwischen S1 und S2 unterscheiden, können die Einstellungen nicht übernommen werden."
    override val dialog60ShiftPointCannotAppliedMessage =
        "Der Schaltpunkt der synchronisierten Schalteinstellung ist in einem Bereich, in dem er nicht eingestellt werden kann."
    override val dialog61CreateNewSettingMessage =
        "Löschen Sie eine der bestehenden Einstellungen, um eine neue Einstellung zu erstellen."
    override val dialog62AdjustDerailleurMessage =
        "Zum Anpassen des Umwerfers muss die Kurbel manuell gedreht werden. Bereiten Sie das Fahrrad vor, indem Sie es auf den Wartungsständer oder auf eine andere Vorrichtung platzieren. Achten Sie auch darauf, Ihre Hand nicht zwischen dem Ritzel einzuklemmen."
    override val dialog63ConfirmCancellAdjustmentMessage =
        "Möchten Sie wirklich beenden?"
    override val dialog64RecoveryFirmwareMessage =
        "Die Firmware auf {0} arbeitet möglicherweise nicht korrekt. Firmware-Wiederherstellung wird durchgeführt. Ungefähr erforderliche Zeit: {1}"
    override val dialog64ConfirmRestoreTheFirmwareMessage =
        "Es wurde ein Fehler an der Einheit für drahtlose Signalübertragung festgestellt.\n Firmware wiederherstellen?"
    override val dialog64UpdateFirmwareMessage =
        "Die Einheit für drahtlose Signalübertragung ist möglicherweise gestört.\nDie Firmware wird aktualisiert."
    override val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "Die Anwendung oder Firmware ist möglicherweise nicht aktuell. Stellen Sie eine Internetverbindung her und suchen Sie nach der neuesten Version."
    override val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "Möchten Sie eine Verbindung zur vorher angeschlossenen Einheit herstellen?"
    override val dialog67BluetoothOffMessage =
        "Bluetooth ist auf dem Gerät nicht aktiviert. Schalten Sie die Bluetooth-Einstellung auf dem Gerät EIN, um eine Verbindung herzustellen."
    override val dialog68LocationInformationOffMessage =
        "Die Standortinformationen des Geräts sind nicht verfügbar. Aktivieren Sie den Standortinformationsdienst, um eine Verbindung herzustellen."
    override val dialog69SwitchBikeConnectionMessage =
        "Möchten Sie das aktuell verbundene Fahrrad wirklich trennen?"
    override val dialog71LanguageChangeCompleteMessage =
        "Die Spracheinstellung wurde geändert. \nDie Sprache wird erst nach Beenden und Neustarten des Programms geändert."
    override val dialog72ConfirmBikeMessage =
        "{0} Einheiten werden verwendet. Möchten Sie als ein bereits vorhandenes Fahrrad registrieren? Möchten Sie als ein neues Fahrrad registrieren?"
    override val dialog73CannnotConnectToNetworkMessage =
        "Kann keine Verbindung zum Netzwerk aufbauen."
    override val dialog74AccountIsLockedMessage =
        "Dieses Benutzerkonto ist gesperrt. Versuchen Sie später sich erneut anzumelden."
    override val dialog75IncorrectIdOrPasswordMessage = "Benutzer-ID oder Passwort falsch."
    override val dialog76PasswordHasExpiredMessage =
        "Ihr vorläufiges Passwort ist abgelaufen.\nStarten Sie den Registrierungsvorgang erneut."
    override val dialog77UnexpectedErrorMessage = "Ein unerwarteter Fehler ist aufgetreten."
    override val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} für den Unterstützungsmodus ist verbunden. \nDer aktuelle Fahrrad-Typ unterstützt nur die Einstellung der Gangschaltung. \n{0} für Gangschaltung einstellen?"
    override val dialog78ShiftModeSwitchingIsConnectedMessage =
        "{0} für die Gangwahl ist verbunden.\nDer derzeitige Fahrradtyp unterstützt nur die Einstellung für den Unterstützungsmodus.\nMöchten Sie {0} auf den Unterstützungsmodus einstellen?"
    override val dialog79WirelessConnectionIsPoorMessage =
        "Schlechte drahtlose Verbindung \nDie Bluetooth® LE-Verbindung wurde möglicherweise unterbrochen."
    override val dialog79ConfirmAjustedChainTensionMessage =
        "Haben Sie die Kettenspannung eingestellt?"
    override val dialog79ConfirmAjustedChainTensionDetailMessage =
        "Bei Einsatz einer Nabenschaltung ist es erforderlich, die Kettenspannung einzustellen.\nPassen Sie die Kettenspannung an und drücken Sie dann die Taste „Ausführen“."
    override val dialog79ConfirmCrankAngleMessage = "Haben Sie den Winkel der Kurbel überprüft?"
    override val dialog79ConfirmCrankAngleDetailMessage =
        "Der linke Kurbelarm muss im korrekten Winkel  auf die Achse gesetzt werden. Prüfen Sie den Winkel der aufgesetzten Kurbel und drücken Sie dann die Taste „Ausführen“."
    override val dialog79RecomendUpdateOSVersionMessage =
        "Die folgende Einheit unterstützt das Betriebssystem Ihres Smartphones oder Tablets nicht.\n{0}\nZum Einsatz mit dem Programm wird empfohlen, dass Sie das Betriebssystem Ihres Smartphones oder Tablets auf die neueste Version aktualisieren."
    override val dialog79CannotUseUnitMessage = "Die folgende Einheit kann nicht verwendet werden."
    override val dialog79RemoveUnitMessage = "Entfernen Sie die Einheit."
    override val dialog79UpdateApplicationMessage =
        "Aktualisieren Sie das Programm auf die neueste Version und versuchen Sie es erneut."
    override val dialog79ConnectToTheInternetMessage =
        "Verbindung mit dem Server kann nicht verifiziert werden. \nStellen Sie eine Internetverbindung her und wiederholen Sie den Versuch."
    override val dialog79NewFirmwareVersionWasFoundMessage =
        "Es wurde eine neue Firmware-Version gefunden. \nNeue Version wird heruntergeladen …"
    override val dialog79DownloadedFileIsCorrupMessage =
        "Die heruntergeladene Datei ist beschädigt.\nLaden Sie die Datei erneut herunter.\nSollte der Download wiederholt fehlschlagen, könnte die Internet-Verbindung oder der Shimano-Webserver ein Problem haben.\nWarten Sie eine Weile und versuchen Sie es erneut."
    override val dialog79FileDownloadFailedMessage =
        "Die Datei konnte nicht heruntergeladen werden."
    override val dialog79ConfirmConnectInternetMessage =
        "Die Datei wird aktualisiert. \nIst Ihr Gerät mit dem Internet verbunden?"
    override val dialog79UpdateFailedMessage = "Aktualisierung fehlgeschlagen."
    override val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nNehmen Sie Ihre Hand vom Schalter.\nEventuell ist ein Fehler am Schalter aufgetreten, falls das Dialogfeld offen bleibt, nachdem Sie es verlassen haben. Schließen Sie in diesem Fall den Schalter allein an und führen Sie eine Fehlerprüfung durch."
    override val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Stellen Sie eine Internetverbindung her und prüfen Sie, ob Aktualisierungen für E-TUBE PROJECT oder Produktversionen verfügbar sind. \nDurch eine Aktualisierung auf die neueste Version können Sie neue Produkte und Funktionen verwenden."
    override val dialog82FailedToRegisterTheImageMessage =
        "Das Bild konnte nicht registriert werden."
    override val dialog83NotConnectedToTheInternetMessage =
        "Sie sind nicht mit dem Internet verbunden. Stellen Sie eine Internetverbindung her und wiederholen Sie den Versuch."
    override val dialog84RegisterTheBicycleAgainMessage =
        "Es wurden keine gültigen Fahrraddaten gefunden. Stellen Sie eine Internetverbindung her und registrieren Sie das Fahrrad erneut."
    override val dialog85RegisterTheImageAgainMessage =
        "Ein unerwarteter Fehler ist aufgetreten. Registrieren Sie das Bild erneut."
    override val dialog87ProgrammingErrorMessage =
        "Beim Speichern ist ein Fehler aufgetreten."
    override val dialog88MoveNextStepMessage =
        "Der Umwerfer befindet sich bereits in der angegebenen Gangstufe. Fahren Sie mit dem nächsten Schritt fort."
    override val dialog89ConfirmWhenGoingToETubeRide =
        "Starten Sie E-TUBE RIDE. Alle momentan verbundenen Bauteile werden getrennt. Möchten Sie wirklich fortfahren?"
    override val dialog92Reading = "Lesevorgang läuft."
    override val dialog103BleAutoConnectCancelMessage =
        "Möchten Sie den Verbindungsvorgang beenden?"
    override val dialog107_PairingCompleteMessage =
        "ID:{0}({1})\n\nDrücken Sie eine beliebige Taste auf dem Schalter, dessen Kennung eingegeben wurde, um die Koppelung abzuschließen."
    override val dialog108_DuplicateMessage =
        "ID:{0}({1})\n\n{2} wurde bereits gekoppelt. Möchten Sie das zuvor gekoppelte Gerät {2} entkoppeln und mit dem nächsten Schritt fortfahren?"
    override val dialog108_LeftLever = "Linker Hebel"
    override val dialog108_RightLever = "Rechter Hebel"
    override val dialog109_UnrecognizableMessage =
        "ID:{0}\n\nUnbekannte Kennung. Prüfen Sie, ob die von Ihnen eingegebene Kennung stimmt."
    override val dialog110_WriteFailureMessage =
        "ID:{0}({1})\n\nSchreiben der Kennung fehlgeschlagen."
    override val dialog112_PairingDeleteMessage = "Möchten Sie den Schalter entkoppeln?"
    override val dialog113GetSwitchFWVersionMessage =
        "Prüfen Sie die Firmware-Version des Schalters. Drücken Sie eine beliebige Taste auf {0} ({1})."
    override val dialog116HowToUpdateWirelessSwitchMessage =
        "Siehe „Details“ für den Aktualisierungsvorgang."
    override val dialog122ConfirmFirmwareLatestUpdateMessage =
        "Wollen Sie nach der neuesten Version der Firmware-Datei suchen?"
    override val dialog123AllFirmwareIsUpToDateMessage =
        "Die Firmware ist auf allen Einheiten des verbundenen Fahrrads aktuell."
    override val dialog123FoundNewFirmwareVersionMessage =
        "Für die Einheit auf dem verbundenen Fahrrad wurde eine neue Firmware-Version gefunden."
    override val dialog3_5UnitRegisteredAgainMessage =
        "Die Fahrradinformationen wurden nicht synchronisiert. Die Einheit muss erneut registriert werden."
    override val dialog4_1UpdatesMessage = "Updates"
    override val dialog6_3DeleteMessage =
        "Das Fahrrad wurde gelöscht. Die drahtlose Einheit muss auch in der Bluetooth-Einstellung des Betriebssystems gelöscht werden."
    override val dialog8_2ConfirmButtonFunction = "Einigen Tasten sind keine Funktionen zugewiesen."
    override val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "Möchten Sie die 2-Stufen-Gangschaltung aktivieren?"
    override val dialog_jira258ConfirmSameOperationMessage =
        "Möchten Sie festlegen, dass der gleiche Vorgang ausgeführt wird, wenn der Schalter zweimal betätigt wird?"
    override val dialog2_2NeedFirmwareUpdateMessage =
        "Um E-TUBE PROJECT verwenden zu können, muss die Firmware auf {0} aktualisiert werden. Ungefähr erforderliche Zeit: {1}"
    override val dialog3_1CompleteFirmwareUpdateMessage = "Die Firmware wurde aktualisiert."
    override val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "Möchten Sie das Firmware-Update abbrechen?\n*Die Updates ab der nächsten Einheit werden abgebrochen. \nDas Update, das für die aktuelle Einheit gerade ausgeführt wird, wird fortgeführt."
    override val dialog3_3ConfirmRewriteFirmwareMessage =
        "Auf {0} ist die neueste Firmware-Version installiert. Möchten Sie sie umschreiben?"
    override val dialog3_4UpdateForNewFirmwareMessage =
        "Für {0} ist eine neue Firmware vorhanden, aber sie kann nicht aktualisiert werden. Die Bluetooth® LE-Version wird nicht unterstützt. Mit der mobilen Version aktualisieren."
    override val dialog4_1ConfirmDisconnectMessage =
        "Möchten Sie die Verbindung trennen?\nEs sind individuelle Einstellungen vorhanden. Bei getrennter Verbindung werden die Einstellungen nicht übernommen."
    override val dialog4_2ConfirmResetAllChangedSettingMessage =
        "Möchten Sie alle geänderten Einstellungen auf zurücksetzen?"
    override val dialog4_3ConfirmDefaultMessage =
        "Möchten Sie die ausgewählten Einstellungen auf die Standardwerte zurücksetzen?"
    override val dialog12_2CanNotErrorCheckMessage =
        "Die Fehlerprüfung kann mit SM-BCR2 nicht durchgeführt werden."
    override val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "Möchten Sie das Laden unterbrechen und die Verbindung trennen?"
    override val dialog13_1ConfirmDiscaresAdjustmentsMessage =
        "Möchten Sie die vorgenommenen Einstellungen wirklich verwerfen?"
    override val dialog_jira258BrightnessSettingMessage =
        "Die Einstellung der Anzeigehelligkeit wird erst nach Trennen der Verbindung wirksam."
    override val dialogUsingSM_Pce02Message =
        "Mit SM-PCE02 ist E-TUBE PROJECT noch nützlicher.\n\n・Bei der Überprüfung des Akkuverbrauchs werden die Teile der angeschlossenen Einheit auf Verluststrom geprüft.\n\n・Verbesserte Stabilität der Kommunikation\n・Schnellere Update-Geschwindigkeit"
    override val dialogLocationServiceIsNotPermittedMessage =
        "Wenn der Zugriff auf die Ortungsdienste nicht erlaubt ist, kann die Bluetooth® LE-Verbindung aufgrund der Nutzungsbeschränkungen von Android OS nicht hergestellt werden. Schalten Sie die Ortungsdienste für diese Anwendung ein und starten Sie die Anwendung dann neu."
    override val dialogStorageServiceIsNotPermittedMessage =
        "Wenn der Zugriff auf den Speicher nicht gestattet ist, können Bilder aufgrund der Nutzungsbeschränkungen von Android OS nicht registriert werden. Schalten Sie den Speicher für diese Anwendung ein und starten Sie die Anwendung dann neu."
    override val dialog114AcquisitionFailure = "Abrufen fehlgeschlagen."
    override val dialog114WriteFailure = "Schreiben fehlgeschlagen."
    override val dialog129_DuplicateMessage = "{0}\n\nDer Schalter wurde bereits gekoppelt."
    override val dialogUnsupportedFetchErrorLogMessage =
        "Fehlerprotokolle sind mit der Firmware-Version Ihres Gerätes nicht einsehbar."
    override val dialogAcquisitionFailedMessage = "Abrufen fehlgeschlagen."
    //endregion

    //region 選択肢
    override val dialog06TopButtonTitle = "HINWEIS"
    override val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Zustimmen und aktualisieren"
    override val dialog20And21Yes = "JA"
    override val dialog20And21No = "NEIN"
    override val dialog22OK = "OK"
    override val dialog25PasskeyErrorOption1 = "Erneut eingeben"
    override val dialog26ChangePasskeyOption1 = "Später"
    override val dialog26ChangePasskeyOption2 = "Ändern"
    override val dialog27RegisterBikeOption2 = "Verwenden"
    override val dialog28BluetoothOffOption1 = "Einstellen"
    override val dialog39ConfirmContinueOption2 = "Weiter"
    override val dialog41ConfirmDefaultOption2 = "Zurück"
    override val dialog45SecondButtonTitle = "Mehr"
    override val dialog55NoInformationOption1 = "Löschen"
    override val dialog55NoInformationOption2 = "Einheit registrieren"
    override val dialog69SwitchBikeConnectionOption1 = "Trennen"
    override val dialog72ConfirmBikeOption1 = "Als\nneues Fahrrad registrieren"
    override val dialog72ConfirmBikeOption2 = "Als\nein bereits vorhandenes Fahrrad registrieren"
    override val dialog88MoveNextStepOption = "Weiter"
    override val dialog3_5UnitRegisteredAgainOption1 = "Registrieren"
    override val dialog4_1UpdateDetailOption2 = "Update-Verlauf"
    override val dialog6_3DeleteOption1 = "Einstellen"
    override val dialog2_1RecoveryFirmwareOption1 = "Wiederherstellen (Eingabe)"
    override val dialog2_1RecoveryFirmwareOption2 = "Nicht wiederherstellen"
    override val dialog2_2NeedFirmwareUpdateOption1 = "Aktualisieren (Eingabe)"
    override val dialog2_2NeedFirmwareUpdateOption2 = "Nicht aktualisieren"
    override val dialog3_1CompleteFirmwareUpdateOption = "OK (Eingabe)"
    override val dialog3_3ConfirmRewriteFirmwareOption2 = "Wählen"
    override val dialogPhotoLibraryAuthorizationSet = "Einstellen"
    override val dialogPhotoLibraryAuthorizationMessage =
        "Diese App verwendet die Fotobibliothek für das Hochladen von Bildern."
    override val dialog109_Retry = "Erneut eingeben"
    // endregion
    //endregion
}
