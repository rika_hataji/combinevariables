package openCSV.combineVariables.localization

import openCSV.combineVariables.localization.*

class LocalizationIT : Localization() {
    override val languageCode = LanguageCode.IT

    override val commonsConnectBle = "Collegamento Bluetooth® LE"
    override val commonsOperation = "Contatore operativo"
    override val commonsBleSettings = "Impostazioni Bluetooth® LE"
    override val commonsYes = "Sì"
    override val commonsNo = "No"
    override val commonsBikeType = "Tipo bicicletta"
    override val commonsRoad10 = "STRADA10"
    override val commonsRoad11 = "STRADA11"
    override val commonsMtb = "MTB"
    override val commonsUrbancity = "AREE URBANE/CITTÀ"
    override val commonsUnitName = "Nome unità"
    override val commonsOnStr = "ACCESO"
    override val commonsOffStr = "SPENTO"
    override val commonsLumpShiftTimeSuperFast = "Super Fast"
    override val commonsLumpShiftTimeVeryFast = "Estremamente veloce"
    override val commonsLumpShiftTimeFast = "Veloce"
    override val commonsLumpShiftTimeNormal = "Normale"
    override val commonsLumpShiftTimeSlow = "Lento"
    override val commonsLumpShiftTimeVerySlow = "Estremamente lento"
    override val commonsRetry = "Ritenta"
    override val commonsUse = "Uso"
    override val commonsNoUse = "Ricambio"
    override val commonsDone = "Fine"
    override val commonsDecide = "Conferma"
    override val commonsCancel = "Annulla"
    override val commonsClose = "Chiudi"
    override val commonsCategoryBmr = "Supporto batteria"
    override val commonsCategoryBtr2 = "Batteria integrata"
    override val commonsCategorySt = "Leva del cambio"
    override val commonsCategorySw = "Unità interruttore"
    override val commonsCategoryEw = "Giunzione"
    override val commonsCategoryFd = "Deragliatore anteriore"
    override val commonsCategoryRd = "Deragliatore posteriore"
    override val commonsCategoryMu = "Unità motore"
    override val commonsCategoryDu = "Unità di trasmissione"
    override val commonsCategoryBt = "Batteria"
    override val commonsCategoryCj = "Giunto a cassetta"
    override val commonsGroupMaster = "Unità master"
    override val commonsGroupJunction = "giunzione (A)"
    override val commonsGroupFshift = "Unità cambio anteriore"
    override val commonsGroupRshift = "Unità cambio posteriore"
    override val commonsGroupSus = "Interruttore di controllo sospensioni"
    override val commonsGroupStsw = "Comando cambio/Interruttore"
    override val commonsGroupEww = "Unità wireless"
    override val commonsGroupBattery = "Batteria"
    override val commonsGroupPowerswitch = "interruttore di alimentazione"
    override val commonsGroupUnknown = "Unità sconosciuta"
    override val commonsSusControlSw = "Interruttore di controllo sospensioni"
    override val commonsBleUnit = "Unità wireless"
    override val commonsPresetName = "Nome del file delle preimpostazioni"
    override val commonsComment = "Commento"
    override val commonsOptional = "(opzionale)"
    override val commonsTargetUnitMayBeBroken = "{0} potrebbe essere guasto."
    override val commonsElectricWires =
        "Controllare che i fili elettrici siano stati collegati adeguatamente."
    override val commonsAlreadyRecognizedUnit = "L'unità è già stata riconosciuta."
    override val commonsNumberOfTeethFront = "Dentatura della corona anteriore"
    override val commonsNumberOfTeethRear = "Dentatura della corona posteriore"
    override val commonsMaxGear = "Numero massimo di marce"
    override val commonsLamp = "Collegamento della luce"
    override val commonsAgree = "Acconsento"
    override val commonsFirmwareUpdateTerminated =
        "L'aggiornamento del firmware di {0} è stato interrotto."
    override val commonsBeginFwRestoration = "Il firmware di {0} è ripristinato."
    override val commonsPleaseConnectBle = "Effettuare il collegamento Bluetooth® LE."
    override val commonsDownLoadFailed = "Impossibile scaricare il file."
    override val commonsQDeleteSetting = "Le impostazioni verranno perse. Continuare?"
    override val commonsDisplay = "Visualizza"
    override val commonsSwitch = "Unità interruttore"
    override val commonsSwitchA = "Interruttore A"
    override val commonsSwitchX = "Interruttore X"
    override val commonsSwitchY = "Interruttore Y"
    override val commonsSwitchZ = "Interruttore Z"
    override val commonsSwitchX1 = "Interruttore X1"
    override val commonsSwitchX2 = "Interruttore X2"
    override val commonsSwitchY1 = "Interruttore Y1"
    override val commonsSwitchY2 = "Interruttore Y2"
    override val commonsFirmer = "Firmer"
    override val commonsSofter = "Softer"
    override val commonsPosition = "Posizione {0}"
    override val commonsShiftCountPlural = "{0} azionamenti"
    override val commonsLocked = "CLIMB(FIRM)"
    override val commonsUnlocked = "DESCEND(OPEN)"
    override val commonsFirm = "TRAIL(MEDIUM)"
    override val commonsSuspensionType = "Tipo di sospensione"
    override val commonsSprinterSwitch = "Comando cambio satellitare"
    override val commonsMultiShiftMode = "Modalità multirapporto"
    override val commonsShiftMode = "Modalità cambio"
    override val commonsLogin = "Login"
    override val commonsSettingUsername = "ID utente"
    override val commonsSettingPassword = "Password"
    override val commonsUnexpectedError = "Si è verificato un errore imprevisto."
    override val commonsAccessKeyAuthenticationError =
        "La sessione di accesso è scaduta. Eseguire nuovamente l'accesso."
    override val commonsSoftwareLicenseAgreement = "Termini di utilizzo"
    override val commonsReInput = "Inserirlo nuovamente."
    override val commonsPerson = "Persona responsabile"
    override val commonsCompany = "Nome azienda"
    override val commonsPhoneNumber = "Numero di telefono"
    override val commonsErrorNotice =
        "Problema nei contenuti immessi. Rivedere le voci immesse qui di seguito."
    override val commonsFirmwareVersion = "Versione firmware"
    override val commonsNotConnected = "Scollegato"
    override val commonsBeep = "Bip"
    override val commonsDisplayTime = "Tempo di visualizzazione"
    override val commonsTheSameMarkCtdCannotBeAssigned =
        "- Se il modello di combinazione è diverso tra le sospensioni anteriore e posteriore, non è possibile assegnare la stessa indicazione (CTD)."
    override val commonsDifferentMarksCtdCannotBeAssigned =
        "- Se viene utilizzato lo stesso modello di combinazione per le sospensioni anteriore e quella posteriore, non è possibile assegnare indicazioni diverse (CTD)."
    override val commonsInternetConnectionUnavailable =
        "Non collegato a Internet.\nCollegarsi a Internet e riprovare."
    override val commonsNotAvailableApp =
        "Questa è l'edizione {0} dell'app. \nImpossibile utilizzarla su un dispositivo {1}."
    override val commonsNotAvailableAppLink = "Scaricare l'edizione {0} dell'app"
    override val commonsSmartPhone = "smartphone"
    override val commonsTablet = "tablet"
    override val commonsCycleComputerRight = "Ciclocomputer lato destro"
    override val commonsCycleComputerLeft = "Ciclocomputer lato sinistro"
    override val commonsError = "Errore"
    override val commonsPleaseConnectAgain =
        "Se l'unità è stata scollegata, controllare nuovamente il collegamento."
    override val commonsPleasePerformErrorCheckIfNotDisconnected =
        "Se non è scollegato, consultare un distributore."
    override val commonsPleasePerformErrorCheck = "Rivolgersi al distributore."
    override val commonsErrorC =
        "La batteria è assente o la batteria presente non ha carica sufficiente. \nUsare una batteria con carica sufficiente oppure caricare la batteria e ricollegarla."
    override val commonsErrorIfNoSwitchResponse =
        "Se nessun interruttore funziona, controllare se sono presenti cavi elettrici scollegati."
    override val commonsNetworkError = "Errore di rete"
    override val commonsNext = "Avanti"
    override val commonsErrorOccurdDuringSetting =
        "Si è verificato un errore durante l'elaborazione dell'impostazione."
    override val commonsFrontShiftUp = "Cambio corona superiore"
    override val commonsFrontShiftDown = "Cambio corona inferiore"
    override val commonsRearShiftUp = "Cambio superiore"
    override val commonsRearShiftDown = "Cambio inferiore"
    override val commonsGroupFsus = "Sospensione forcella anteriore"
    override val commonsErrorBattery =
        "Impossibile confermare la capacità residua della batteria.\nControllare che l'unità master e la batteria siano collegate correttamente."
    override val commonsUpdateCheckUpdateSetting =
        "Verifica degli aggiornamenti di E-TUBE PROJECT in corso."
    override val commonsApply = "Applica"
    override val commonsFinish = "Fine"
    override val commonsDoNotUseProhibitionCharacter =
        "Non utilizzare caratteri non consentiti ({1}) in {0}."
    override val commonsInputError = "Immettere {0}."
    override val commonsInputNumberError = "Immettere {0} in valori numerici ridotti."
    override val commonsDi2 = "DI2"
    override val commonsSteps = "STePS"
    override val commonsSwitchForAssist = "Interruttore modificare la modalità di servoassistenza"
    override val commonsUsinBleUnitError =
        "Si è verificato un errore mentre l'unità wireless era in uso."
    override val commonsCommunicationMode = "Modalità comunicazione wireless"
    override val errorBleDisconnect =
        "Il collegamento Bluetooth® LE è stato interrotto.\nRiprovare."
    override val errorOccurredAbnormalCommunication =
        "Si è verificato un errore di comunicazione. Provare a riconnettersi."
    override val commonsDrawerMenu = "Menu a comparsa"
    override val drawerMenuUnitList = "Elenco unità"
    override val drawerMenuDisconnectBle = "Scollegare il Bluetooth® LE"
    override val drawerMenuTutorial = "Lezione"
    override val drawerMenuApplicationSettings = "Impostazioni applicazione"
    override val drawerMenuLanguageSettings = "Selezione lingua"
    override val drawerMenuVersionInfo = "Informazioni versione"
    override val drawerMenuLogin = "Login"
    override val drawerMenuLogout = "Logout"
    override val drawerMenuChangePassword = "Cambia password"
    override val drawerMenuRegistered = "Query informazioni utente"
    override val commonsBetaMessageMsg1 =
        "Questo software è in versione beta.\nNon è stato ancora convalidato e pertanto potrebbe provocare vari problemi.\nOK?"
    override val commonsBetaMessageMsg2 = "Data di scadenza del software"
    override val commonsBetaMessageMsg3 = "Questo software è scaduto."
    override val launchLicenseMsg1 = "Riesaminare i termini della licenza prima dell'applicazione."
    override val launchLicenseMsg2 =
        "Fare clic sul pulsante \"Acconsento\" dopo aver riesaminato le disposizioni dell'accordo per continuare il processo. È necessario accettare i termini dell'accordo per poter usare questa applicazione."
    override val launchFreeSize =
        "Lo spazio libero su disco ({1} MB) non è sufficiente per eseguire E-TUBE PROJECT. \nI file necessari verranno scaricati alla successiva esecuzione dell'applicazione. \nAccertarsi che vi siano almeno {0} MB di spazio su disco disponibile prima di eseguire nuovamente l'applicazione."
    override val connectionErrorRepairMsg2 = "{0} può essere difettoso."
    override val connectionErrorRepairMsg3 = "Selezionare il gruppo unità non rilevate."
    override val connectionErrorRepairMsg4 = "Un'unità potrebbe essere difettosa."
    override val connectionErrorRepairMsg6 = "Unità di {0}"
    override val connectionErrorRepairMsg7 =
        "Il firmware per questa unità viene ripristinato."
    override val connectionErrorRepairMsg8 =
        "Le unità di seguito sono collegate correttamente."
    override val connectionErrorUnknownUnit =
        "Sono collegate una o più unità non supportate dalla versione corrente di  E-TUBE PROJECT.\nInstallare l'app più recente dall'{0}."
    override val connectionErrorOverMsg1 =
        "Fino a un totale di solo {1} unità su {0} può essere collegato."
    override val connectionErrorOverMsg2 = "Unità collegata"
    override val connectionErrorNoCompatibleUnitsMsg1 =
        "È stata trovata una combinazione non compatibile. Verificare che sia collegata un'unità compatibile con il tipo di bici e ripetere la procedura dall'inizio."
    override val connectionErrorNoCompatibleUnitsMsg2 =
        "Il problema potrebbe essere risolto aggiornando il firmware per una compatibilità estesa. \nProvare ad aggiornare il firmware."
    override val connectionErrorNoCompatibleUnitsLink =
        "Visualizza tabella compatibilità più dettagliata"
    override val connectionErrorFirmwareBroken =
        "Il firmware di {0} potrebbe non funzionare correttamente.\nIl firmware verrà ripristinato."
    override val connectionDialogMessage1 =
        "L'unità {0} per il cambio in modalità assistita non è collegata.\nDurante l'impostazione della modalità dell'interruttore, l'impostazione dell'unità {1} può essere cambiata da interruttore del cambio a cambio in modalità assistita."
    override val connectionDialogMessage3 =
        "L'unità {0} è impostata per le sospensioni.\nModificare l'impostazione."
    override val connectionDialogMessage4 =
        "L'impostazione \"{1}\" di {0} non funziona quando la bici è in uso.\nModificare l'impostazione."
    override val connectionDialogMessage5 =
        "Il sistema non funzionerà normalmente con la combinazione di unità riconosciuta.\nPer abilitare il sistema e farlo funzionare correttamente, collegare le unità richieste e controllare nuovamente i collegamenti. Continuare?"
    override val connectionDialogMessage6 = "Unità richiesta (una delle seguenti)"
    override val connectionDialogMessage7 =
        "Il sistema non funzionerà normalmente con la combinazione di unità riconosciuta.\nPer abilitare il sistema e farlo funzionare correttamente, collegare le unità richieste e controllare nuovamente i collegamenti."
    override val connectionDescription1 =
        "Impostare l'unità sulla bici sulla modalità di collegamento Bluetooth® LE."
    override val connectionDescription2 = "Selezionare l'unità wireless che si desidera collegare."
    override val connectionWhatIsPairing =
        "Abilitazione della modalità di collegamento dell'unità wireless."
    override val connectionDisconnectDescription =
        "Per scollegarsi, selezionare \"Scollegare il Bluetooth® LE\" dal menu in alto a destra."
    override val connectionBluetoothInitialPasskey =
        "Questa app utilizza Bluetooth® LE per la comunicazione wireless.\nLa PassKey iniziale per Bluetooth® LE è \"000000\"."
    override val connectionPasskeyMessage =
        "Modificare la PassKey iniziale.\nÈ possibile che venga effettuato il collegamento da soggetti terzi."
    override val connectionPasskey = "PassKey"
    override val connectionPasswordConfirm = "PassKey (per conferma)"
    override val connectionPleaseInputPassword = "Inserire la PassKey."
    override val connectionDisablePassword =
        "Impossibile autenticare. PassKey inserita in modo errato, oppure la PassKey per l’unità wireless è stata modificata. Riconnettersi e provare a reimmettere la PassKey impostata nell’unità wireless."
    override val connectionValidityPassword =
        "0 non può essere impostato come primo carattere della PassKey."
    override val connectionBleFirmwareRestore =
        "Il ripristino del firmware verrà eseguito sull'unità wireless."
    override val connectionBleFirmwareRestoreFailed =
        "Ricollegare l'unità.\nSe l'errore persiste, utilizzare la versione PC dell'E-TUBE PROJECT."
    override val connectionBleWeakWaves =
        "Il collegamento wireless è scadente. \nIl collegamento Bluetooth® LE potrebbe essere interrotto."
    override val connectionBleNotRequiredRestore =
        "L'unità wireless funziona normalmente.\nIl ripristino del firmware non è necessario."
    override val connectionFoundNewBleFirmware =
        "È stato trovato un nuovo firmware per l'unità wireless.\nL'aggiornamento avrà inizio."
    override val connectionScanReload = "Ricarica"
    override val connectionUnmatchPassword = "PassKey errata."
    override val connectionBikeTypeDi2Type1 = "STRADA"
    override val connectionBikeTypeDi2Type2 = "MTB"
    override val connectionBikeTypeDi2Type3 = "AREE URBANE/CITTÀ"
    override val connectionBikeTypeEbikeType1 = "MTB"
    override val connectionNeedPairingBySetting =
        "Impostare l'unità wireless per il collegamento ed eseguire l'appaiamento da [Impostazioni] > [Bluetooth] sul dispositivo."
    override val connectionSwE6000Recognize =
        "Tenere premuto l'interruttore {1} dell'unità {0} per impostarlo."
    override val commonsGroupDU = "Unità di trasmissione"
    override val commonsGroupSC = "Ciclocomputer"
    override val commonsGroupRSus = "Sospensione posteriore"
    override val commonsGroupForAssist = "Interruttore cambio per servoassistenza"
    override val commonsGroupForShift = "Interruttore cambio"
    override val connectionMultipleChangeShiftToAssist =
        "{0} {1} collegate. \nL'attuale tipo di bicicletta supporta solo {2}. \nImpostare tutte le unità {0} su {2}?"
    override val connectionNewFirmwareFileNotFound =
        "Il file del firmware non è aggiornato all'ultima versione."
    override val commonsGroupMasterCap = "Unità master"
    override val commonsGroupJunctionCap = "Giunzione (A)"
    override val commonsGroupFshiftCap = "Unità cambio anteriore"
    override val commonsGroupRshiftCap = "Unità cambio posteriore"
    override val commonsGroupSusCap = "Interruttore di controllo sospensioni"
    override val commonsGroupStswCap = "Comando cambio/interruttore"
    override val commonsGroupFsusCap = "Sospensione forcella anteriore"
    override val commonsGroupRsusCap = "Sospensione posteriore"
    override val commonsGroupDuCap = "Unità di trasmissione"
    override val commonsGroupForassistforshiftCap =
        "Interruttore cambio per servoassistenza/interruttore cambio"
    override val commonsGroupForassistforshift =
        "interruttore cambio per servoassistenza/interruttore cambio"
    override val connectionErrorNoSupportedMaster =
        "{0} non è supportato da questa applicazione.\n{0} può essere utilizzato con la versione PC di E-TUBE PROJECT."
    override val connectionErrorNoSupportedMaster2 = "Non compatibile."
    override val connectionErrorUnknownMaster = "È collegata un'unità master sconosciuta."
    override val connectionErrorPleaseConfirmMasterUnit = "Controllare l'unità master."
    override val connectionErrorUnSupportedMsg1 =
        "È collegata un'unità che non supporta il collegamento Bluetooth® LE.\nScollegare tutte le unità indicate di seguito e ricollegarle."
    override val connectionErrorPcLinkageDevice = "Dispositivo di collegamento al PC"
    override val connectionErrorUnSupportedMsg2 =
        "Sono collegate più unità wireless.\nRimuovere tutte le unità indicate di seguito tranne una e ricollegarle."
    override val connectionErrorBroken =
        "L'applicazione non è riuscita a collegarsi all'E-TUBE PROJECT perché una delle unità collegate potrebbe essere guasta. \nRivolgersi al distributore o al rivenditore."
    override val connectionChargeMsg1 = "Controllare lo stato della batteria."
    override val connectionChargeMsg2 =
        "Impossibile usare la batteria mentre è in carica. Effettuare nuovamente il collegamento una volta completata la carica."
    override val connectionChargeMsg3 =
        "Se non si carica, la batteria potrebbe essere guasta. \nRivolgersi al distributore."
    override val connectionChargeRdMsg =
        "Ricarica interrotta Per ricaricare, scollegare dall’app e quindi ricollegare il cavo di carica."
    override val connectionBikeTypePageTitle = "Selezione del tipo bicicletta"
    override val connectionBikeTypeMsg =
        "L'applicazione non è riuscita a rilevare il tipo di bicicletta. Selezionare il tipo bicicletta."
    override val connectionBikeTypeTitle1 = "Sistema DI2"
    override val connectionBikeTypeTitle2 = "Sistema E-bike"
    override val connectionBikeTypeError =
        "Il tipo di bici per le impostazioni da scrivere differisce dal tipo di bici per l'unità collegata. \nControllare le impostazioni o l'unità collegata prima di scrivere le impostazioni."
    override val connectionDialogMessage9 = "{0} per cambio in modalità assistita collegato."
    override val connectionDialogMessage12 =
        "Il tipo di bici corrente supporta solo l'impostazione per il cambio rapporti."
    override val connectionDialogMessage13 = "Impostare tutte le unità {0} sul cambio rapporti?"
    override val connectionDialogMessage14 = "Impostare tutte le unità {0} per il cambio rapporti?"
    override val connectionDialogMessage19 =
        "Il firmware del dispositivo in uso potrebbe essere non aggiornato.\nCollegarsi a Internet e selezionare \"Scarica tutto\" da \"Informazioni versione\" per scaricare la versione più recente del firmware ."
    override val connectionChangeShiftToAssist =
        "{0} {1} collegate. \nL'attuale tipo di bicicletta supporta solo {2}. \nImpostare le unità {0} su {2}?"
    override val connectionDialogMessage11 = "Impossibile usare l'unità seguente."
    override val connectionDialogMessage16 =
        "Aggiornare l'applicazione alla versione più recente e riprovare."
    override val connectionDialogMessage15 = "Rimuovere l'unità."
    override val connectionDialogMessage17 =
        "L'unità indicata di seguito non supporta il sistema operativo dello smartphone o del tablet."
    override val connectionDialogMessage18 =
        "Si consiglia di aggiornare il sistema operativo dello smartphone o tablet alla versione più recente per poterlo usare con l'applicazione."
    override val connectionSprinterMsg1 = "Si sta utilizzando l'interruttore velocista su {0}?"
    override val connectionSprinterMsg2 =
        "Sono collegate più unità {0}.\nUtilizzare tutti gli interruttori velocista?"
    override val commonsSelect = "Seleziona"
    override val connectionSprinterContinue = "Continuare la selezione?"
    override val connectionAppVersion = "Ver.{0}"
    override val commonsVersionInfoEtubeVersion = "Versione E-TUBE PROJECT"
    override val commonsVersionInfoBleFirmwareVersion = "Versione firmware Bluetooth® LE"
    override val commonsVersionInfoMessage1 =
        "La funzione di verifica utilizza l'unità wireless e il software  E-TUBE PROJECT per rilevare rotture del filo ed errori del sistema. Impossibile rilevare i problemi di tutte le unità o le instabilità di funzionamento.\nRivolgersi presso il luogo di acquisto o un rivenditore."
    override val commonsVersionInfoMessage2 =
        "È disponibile una nuova versione di E-TUBE PROJECT , ma il sistema operativo installato non è supportato. Per informazioni sui sistemi operativi supportati, consultare il sito Web SHIMANO."
    override val commonsVersionInfoUpdateCheck = "Scarica tutto"
    override val settingMsg3 =
        "Verificare che sia visualizzata la schermata per modificare la PassKey iniziale."
    override val settingMsg5 =
        "Controllare le applicazioni più recenti dei diversi sistemi operativi."
    override val settingMsg6 =
        "Verificare che la guida animata per la modalità di cambiata sia visualizzata."
    override val settingMsg4 =
        "Verificare che la guida animata della modalità multirapporto sia visualizzata."
    override val settingMsg1 = "Controllare lʼaggiornamento firmware durante la preimpostazione."
    override val settingMsg2 = "Il server proxy richiede l'autenticazione."
    override val settingServer = "Server"
    override val settingPort = "Porta"
    override val settingUseProxy = "Utilizzare il server proxy"
    override val settingUsername = "ID utente"
    override val settingPassword = "Password"
    override val settingFailed =
        "Si è verificato un errore durante il salvataggio delle impostazioni."
    override val commonsEnglish = "Inglese"
    override val commonsJapanese = "Giapponese"
    override val commonsChinese = "Cinese"
    override val commonsFrench = "Francese"
    override val commonsItalian = "Italiano"
    override val commonsDutch = "Olandese"
    override val commonsSpanish = "Spagnolo"
    override val commonsGerman = "Tedesco"
    override val commonsLanguageChangeComplete =
        "L'impostazione della lingua è stata modificata. \nLa lingua cambierà dopo aver chiuso e riavviato l'applicazione."
    override val commonsDiagnosisResult3 =
        "Sostituire o rimuovere l'unità seguente ed effettuare nuovamente il collegamento."
    override val connectionErrorSelectNoDetectedMsg = "Selezionare un'unità non rilevata."
    override val connectionErrorSelectNoDetectedAlert =
        "Se si seleziona l'unità sbagliata, il firmware sbagliato potrebbe essere scritto sull'unità, rendendola non operativa. Accertarsi di aver scelto l'unità corretta e procedere al passo successivo."
    override val commonsCheckElectricWires2 = "{0} non riconosciuto."
    override val commonsDiagnosisCheckMsg = "{0} riconosciuto.\nL'unità collegata è {0}?"
    override val commonsUpdateCheckUpdateError =
        "Impossibile verificare un collegamento al server. \nCollegarsi a Internet e riprovare."
    override val commonsUpdateCheckWebServerError =
        "Ci potrebbe essere un problema con la connessione Internet o il web server di Shimano. \nAspettare un po' e riprovare."
    override val commonsUpdateCheckUpdateSettingIOs = "Lettura in corso."
    override val firmwareUpdateRecognizeUnitDialogMsg =
        "Continuare a premere uno degli interruttori {0} per l'unità di cui si desidera effettuare l'aggiornamento del firmware."
    override val customizeSwitchFunctionDialogMsg1 =
        "Tenere premuto uno degli interruttori {0} per l'unità che si desidera impostare."
    override val connectionSprinterRecognize =
        "Tenere premuto uno degli interruttori {0} per l'unità che si desidera selezionare."
    override val commonsConfirmPressedSwitch = "È stato premuto l'interruttore?"
    override val connectionSprinterRelease =
        "OK. Togliere le mani dall'interruttore. \n\nSe questa finestra di dialogo non si chiude dopo aver tolto le mani dall'interruttore, è possibile che un interruttore non funzioni correttamente. In questo caso, rivolgersi al distributore."
    override val commonsLeft = "Sinistra"
    override val commonsRight = "Destra"
    override val commonsUpdateCheckIsInternetConnected =
        "Il file sarà aggiornato. \nIl dispositivo è collegato a Internet?"
    override val commonsUpdateCheckUpdateFailed = "Aggiornamento non riuscito."
    override val commonsUpdateFailed =
        "L'applicazione non è riuscita a trovare uno o più file necessari per eseguire E-TUBE PROJECT. \nDisinstallare e reinstallare E-TUBE PROJECT."
    override val customizeBleBleName = "Nome dell'unità wireless"
    override val customizeBlePassKeyRule = "(da 6 caratteri alfanumerici ridotti)"
    override val customizeBlePassKeyDisplay = "Visualizzare"
    override val customizeBlePassKeyPlaceholder = "Inserirlo nuovamente."
    override val customizeBleErrorBleName = "Immettere da 1 a 8 caratteri alfanumerici ridotti."
    override val customizeBleErrorPassKey = "Immettere 6 valori numerici ridotti."
    override val customizeBleErrorPassKeyConsistency = "PassKey errata."
    override val customizeBleAlphanumeric = "caratteri alfanumerici a mezza larghezza"
    override val customizeBleHalfWidthNumerals = "Valori numerici ridotti"
    override val commonsCustomize = "Personalizza"
    override val customizeFunctionDriveUnit = "Unità di trasmissione"
    override val customizeFunctionNonUnit = "Nessuna unità personalizzabile collegata."
    override val customizeCadence = "Cadenza"
    override val customizeAssistSetting = "Servoassistenza"
    override val commonsAssistPattern = "Modello servoassistenza"
    override val commonsElectricType = "Elettrico"
    override val commonsExteriorTransmission = "Tipo deragliatore (catena)"
    override val commonsMechanical = "Meccanico"
    override val commonsAssistPatternDynamic = "DYNAMIC"
    override val commonsAssistPatternExplorer = "EXPLORER"
    override val commonsAssistModeHigh = "HIGH"
    override val commonsAssistModeMedium = "MEDIUM"
    override val commonsAssistModeLow = "LOW"
    override val commonsRidingCharacteristic = "Caratteristiche"
    override val commonsFunctionRidingCharacteristic = "Caratteristiche"
    override val commonsAssistPatternCustomize = "CUSTOMIZE"
    override val commonsAssistModeBoost = "BOOST"
    override val commonsAssistModeTrail = "TRAIL"
    override val commonsAssistModeEco = "ECO"
    override val customizeSceKm = "m\nkg"
    override val customizeSceMile = "y\nlb"
    override val commonsLanguage = "Lingua"
    override val customizeDisplayNowTime = "Ora esatta"
    override val customizeDisplaySet = "Impostare"
    override val customizeDisplayDontSet = "Non impostare"
    override val commonsDisplayLight = "Visualizza/Luce"
    override val customizeSwitchSettingViewControllerMsg1 = "Interruttore cambio"
    override val customizeSwitchSettingViewControllerMsg8 = "Utilizzare {0}"
    override val customizeSwitchSettingViewControllerMsg2 =
        "Passare all'impostazione delle sospensioni"
    override val customizeSwitchSettingViewControllerMsg3 = "Informazioni su Synchronized shift"
    override val customizeSwitchSettingViewControllerMsg11 =
        "Lo stato del collegamento dell'unità è cambiato. Non scollegare o cambiare unità mentre si configurano le impostazioni. \nRipetere il processo dall'inizio."
    override val customizeSwitchSettingViewControllerMsg4 =
        "Le informazioni correntemente selezionate verranno perse. \nPassare all'impostazione delle sospensioni?"
    override val customizeSwitchSettingViewControllerMsg9 = "Cos'è il Synchronized shift?"
    override val customizeSwitchSettingViewControllerMsg5 =
        "Synchronized shift cambia automaticamente FD in sincronizzazione con Cambio superiore e Cambio inferiore."
    override val customizeSwitchSettingViewControllerMsg6 =
        "Le seguenti funzioni non sono incluse. Continuare l'elaborazione?"
    override val customizeSwitchSettingViewControllerMsg7 = "Interruttore {0}"
    override val customizeSwitchSettingViewControllerMsg10 =
        "Impostare l'interruttore{0} sul funzionamento inverso"
    override val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift  è una funzione che consente di cambiare automaticamente il rapporto del deragliatore posteriore quando si cambia il rapporto del deragliatore anteriore allo scopo di ottenere una transizione ottimale da un rapporto all'altro."
    override val customizeSwitchSettingViewControllerMsg13 =
        "È possibile selezionare i punti di cambiata."
    override val customizeSwitchSettingViewControllerMsg14 =
        "A questo punto, è possibile selezionare la posizione del cambio da 0 a 4. (A seconda della combinazione, potrebbe esserci una posizione non selezionabile.)"
    override val commonsSwitchType = "Modalità dell'interruttore"
    override val commonsAssistUp = "Servoassistenza rapporto superiore"
    override val commonsAssistDown = "Servoassistenza rapporto inferiore"
    override val customizeSwitchModeChangeAssist =
        "Cambiare la modalità di questo interruttore a [per Servoassistenza].\nContinuare?"
    override val customizeSwitchModeChangeShift =
        "Cambiare la modalità di questo interruttore a [per Cambio].\nContinuare?"
    override val customizeSwitchModeLostAssist =
        "Quando la modalità interruttore viene cambiata a  [per Cambio], l'interruttore per il cambio in modalità assistita scomparirà.\nProcedere alla modifica?"
    override val commonsDFlyCh1 = "D-FLY Can. 1"
    override val commonsDFlyCh2 = "D-FLY Can. 2"
    override val commonsDFlyCh3 = "D-FLY Can. 3"
    override val commonsDFlyCh4 = "D-FLY Can. 4"
    override val commonsDFly = "D-FLY"
    override val commonsFunction = "Funzione"
    override val customizeGearShifting = "Cambio rapporti"
    override val customizeManualSelect = "Selezionare l'nterruttore manualmente"
    override val commonsUnit = "Unità"
    override val customizeScSetting = "Ciclocomputer"
    override val customizeSusSusTypeTitle = "Interruttore di controllo sospensioni"
    override val customizeSusSusPositionInform =
        "Su quale manopola è installato lʼinterruttore di controllo?"
    override val customizeSusSusPositionLeft = "A sinistra"
    override val customizeSusSusPositionRight = "A destra"
    override val customizeSusPosition = "Posizione"
    override val customizeSusFront = "Anteriore"
    override val customizeSusRear = "Posteriore"
    override val customizeFooterMsg1 = "Ripr. valori predef"
    override val customizeFooterMsg2 = "Ripristina le impostazioni predefinite {0}"
    override val customizeSusConsistencyCheckMsg1 = "Verifica il messaggio riportato di seguito."
    override val customizeSusConsistencyCheckMsg6 = "OK per continuare con la programmazione?"
    override val customizeSusConsistencyCheckMsg3 =
        "- Le medesime impostazioni sono state selezionate per due o più posizioni."
    override val customizeSusConsistencyCheckMsg2 = "- È stato impostato solo {1} per {0}."
    override val customizeSusConsistencyCheckMsg4 =
        "- La stessa impostazione è stata applicata a due o più posizioni in CTD."
    override val customizeSusClimb = "CLIMB(FIRM)"
    override val customizeSusDescend = "DESCEND(OPEN)"
    override val customizeSusTrail = "TRAIL(MEDIUM)"
    override val customizeSusSwVerCheck =
        "L'interruttore non può essere impostato come interruttore sospensioni in quanto la versione firmware di {0} è precedente a {1}.\nPer impostare l'interruttore come interruttore sospensioni, collegarsi a una rete ed eseguire l'aggiornamento alla versione più recente ({1} o successiva)."
    override val customizeSusSusConnectMsg1 =
        "Per impostare l'interruttore come interruttore delle sospensioni, è richiesta una delle seguenti unità."
    override val customizeSusSusConnectMsg2 = "Ricollegare l'unità richiesta e riprovare."
    override val customizeSusSusConnectMsg3 =
        "È inoltre possibile passare all'impostazione del cambio."
    override val customizeSusSusConnectMsg4 =
        "Per passare all'impostazione del cambio, premere [Sì]. Per collegare le unità richieste e ricominciare da capo senza modificare l'impostazione, premere [No]."
    override val customizeSusSusModeSelect =
        "L'impostazione \"→(T)\" non funziona con le sospensioni in uso. \nÈ possibile nascondere l'impostazione \"→(T)\" dalla schermata di configurazione. \nNascondere \"→(T)\"?"
    override val customizeSusSwitchShift = "Passa alla impostazione di cambiata"
    override val customizeSusTransitionShift =
        "Le informazioni correntemente selezionate verranno perse. \nPassare all'impostazione di cambiata?"
    override val customizeMuajstTitle = "Regolazione del deragliatore"
    override val customizeMuajstAdjustment = "Regolazione"
    override val customizeMuajstGearPosition = "Modifica posizione marcia"
    override val customizeMuajstInfo1 =
        "Gli interruttori installati sulla bicicletta non funzionano."
    override val customizeMuajstInfo2 =
        "Vedere qui per ulteriori informazioni sul metodo di regolazione."
    override val customizeMultiShiftModalTitle = "Impostazione velocità multirapporto"
    override val customizeMultiShiftModalDescription =
        "Le impostazioni iniziali usano valori predefiniti standard. \nDopo essersi accertati di aver compreso le caratteristiche della modalità multirapporto, selezionare le impostazioni multirapporto che meglio corrispondono alle condizioni in cui la bici verrà usata (terreno, stile di guida, ecc.)."
    override val customizeMultiShiftModalVeryFast = "Estremamente veloce"
    override val customizeMultiShiftModalFast = "Veloce"
    override val customizeMultiShiftModalNormal = "Normale"
    override val customizeMultiShiftModalSlow = "Lento"
    override val customizeMultiShiftModalVerySlow = "Estremamente lento"
    override val customizeMultiShiftModalFeatures = "Caratteristiche"
    override val customizeMultiShiftModalFeature1 =
        "La modalità multirapporto veloce è ora abilitata. \n\n•Questa funzione consente di regolare rapidamente il valore RPM della pedivella in risposta alle condizioni di guida che variano. \n•Consente di regolare rapidamente la velocità."
    override val customizeMultiShiftModalFeature2 = "Abilita funzionamento multirapporto affidabile"
    override val customizeMultiShiftModalNoteForUse = "Precauzioni d'uso"
    override val customizeMultiShiftModalUse1 =
        "1. È facile effettuare l'overshift. \n\n2. Se il valore RPM della pedivella è basso, la catena non potrà seguire il movimento del deragliatore posteriore. \nDi conseguenza, la catena potrebbe staccarsi dai denti della corona anziché innestarsi sulla cassetta pignoni."
    override val customizeMultiShiftModalUse2 =
        "Il funzionamento della modalità multirapporto richiede del tempo"
    override val customizeMultiShiftModalCrank =
        "RPM della pedivella richiesto per l'uso della modalità multirapporto"
    override val customizeMultiShiftModalCrank1 = "Con RPM pedivella alto"
    override val customizeMultiShiftModeViewControllerMsg3 =
        "Intervallo delle rotazioni dell'azionamento"
    override val customizeMultiShiftModeViewControllerMsg4 = "Numero massimo di azionamenti"
    override val commonsLumpShiftUnlimited = "nessun limite"
    override val commonsLumpShiftLimit2 = "2 azionamenti"
    override val commonsLumpShiftLimit3 = "3 azionamenti"
    override val customizeMultiShiftModeViewControllerMsg5 = "Altro interruttore del cambio"
    override val customizeShiftModeRootMsg1 =
        "Per impostare la modalità di cambiata, è richiesta un'unità ciascuno per (1) e (2) riportati di seguito."
    override val customizeShiftModeRootMsg3 = "Ricollegare l'unità richiesta e riprovare."
    override val customizeShiftModeRootMsg4 =
        "Per impostare la modalità di cambiata è richiesta una delle seguenti unità."
    override val customizeShiftModeRootMsg5 =
        "Anche se le impostazioni della modalità di cambiata sono state eseguite, saranno effettive solo se la versione firmware di {0} è {1} o superiore."
    override val customizeShiftModeRootMsg6 = "La versione firmware è {0} o superiore"
    override val customizeShiftModeSettingMsg1 = "Eliminare?"
    override val customizeShiftModeSettingMsg2 =
        "Le impostazioni della bici correnti verranno perse. Continuare?"
    override val customizeShiftModeSettingMsg3 =
        "Impossibile configurare le impostazioni perché l'unità non è collegata correttamente."
    override val customizeShiftModeSettingMsg4 =
        "La bici ha una struttura di rapporti che non può essere configurata. Controllare le impostazioni."
    override val customizeShiftModeSettingMsg5 =
        "Impossibile leggere il valore di impostazione dal file stock {0}. \n{0} verrà eliminato."
    override val customizeShiftModeSettingMsg7 = "Quale funzione si desidera assegnare?"
    override val customizeShiftModeSettingMsg8 = "Il nome del file delle impostazioni è già in uso."
    override val customizeShiftModeTeethMsg1 =
        "Selezionare la dentatura della corona per la bici corrente."
    override val customizeShiftModeGuideMsg1 =
        "Per usare il file delle impostazioni sul dispositivo e sovrascrivere le impostazioni della bici, trascinare il file sullo slot (S1 o S2) che si desidera sovrascrivere."
    override val customizeShiftModeGuideMsg2 =
        "L'icona mostrata sopra le impostazioni indica la modalità di cambiata."
    override val customizeShiftModeGuideMsg3 =
        "Synchronized shift è una funzione che consente di cambiare automaticamente il rapporto del deragliatore anteriore sincronizzandolo al deragliatore posteriore. È possibile configurare il funzionamento del Synchronized shift selezionando i punti sulla mappa."
    override val customizeShiftModeGuideMsg4 =
        "Spostare il cursore verde per impostare SU (da dentro a fuori) e il cursore blu per impostare GIÙ (da fuori a dentro)."
    override val customizeShiftModeGuideMsg5 =
        "Punti di sincronizzazione \nI punti in cui il deragliatore anteriore cambia rapporto in sincronia con il deragliatore posteriore. \nI requisiti per le frecce che indicano i punti di sincronizzazione sono: \nSU: Rivolta in alto o di lato\nGIÙ: Rivolta in basso o di lato"
    override val customizeShiftModeGuideMsg6 =
        "Rapporti di trasmissione disponibili \nSU: È possibile selezionare qualsiasi rapporto di trasmissione fino a quello inferiore al rapporto di trasmissione del punto di sincronizzazione. \nGIÙ: È possibile selezionare qualsiasi rapporto di trasmissione fino a quello superiore al rapporto di trasmissione del punto di sincronizzazione."
    override val customizeShiftModeTeethMsg6 =
        "Se la corona (FC) viene modificata da {0} a {1}, tutti i punti di cambiata impostati verranno reimpostati sui valori predefiniti.\nEffettuare la modifica?"
    override val customizeShiftModeTeethDouble = "doppia"
    override val customizeShiftModeTeethTriple = "tripla"
    override val customizeShiftModeTeethMsg2 = "Intervallo synchronized shift"
    override val customizeShiftModeTeethSettingTitle = "Selezionare il numero di denti"
    override val customizeShiftModeGuideTitle1 = "Synchronized shift"
    override val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    override val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    override val customizeShiftModeSynchroMsg1 =
        "Il punto di cambio rientra nell'intervallo non impostabile e non può funzionare normalmente."
    override val customizeShiftModeTeethMsg5 = "Nome del file delle impostazioni"
    override val customizeShiftModeBtn1 = "Su"
    override val customizeShiftModeBtn2 = "Giù"
    override val customizeShiftModeTeethMsg8 = "Controllo rapporto selezionato"
    override val customizeShiftModeSettingMsg9 =
        "Le informazioni riportate di seguito sono inoltre aggiornate per S1 e S2."
    override val customizeShiftModeTeethInward1 = "Posteriore sollevato su anteriore abbassato"
    override val customizeShiftModeTeethOutward1 = "Posteriore abbassato su anteriore sollevato"
    override val customizeShiftModeTeethInward2 = "Cambio superiore\nal cambio corona inferiore"
    override val customizeShiftModeTeethOutward2 = "Cambio inferiore\nal cambio corona superiore"
    override val customizeShiftModeTypeSelectTitle = "Selezione delle funzioni da assegnare"
    override val customizeShiftModeSettingMsg10 = "Continuare l'elaborazione?"
    override val customizeShiftModeTeethMsg7 =
        "Selezionare il numero di denti della corona per la bici corrente. \n*Fisso come {0} per il collegamento."
    override val commonsErrorCheck = "Verifica degli errori"
    override val commonsAll = "Tutti"
    override val errorCheckResultError = "{0} può essere difettoso."
    override val errorCheckContactDealer = "Rivolgersi al distributore o al rivenditore."
    override val commonsStart = "Avvio"
    override val commonsUpdate = "Aggiornamento firmware"
    override val firmwareUpdateUpdateAll = "Aggiorna tutto"
    override val firmwareUpdateCancelAll = "Annulla tutto"
    override val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Collegarsi a Internet e controllare se sono disponibili versioni di E-TUBE PROJECT o di prodotto aggiornate. \nL'aggiornamento alla versione più recente consentirà di usare nuovi prodotti e funzioni."
    override val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "L'applicazione o il firmware potrebbe essere non aggiornato. \nCollegarsi a Internet e cercare la versione più recente."
    override val firmwareUpdateConnectionLevelIsLow =
        "Impossibile effettuare l'aggiornamento a causa di un collegamento wireless scadente. \nAggiornare il firmware dopo aver stabilito un collegamento valido."
    override val firmwareUpdateIsUpdatingFw = "L'aggiornamento del firmware per {0} in corso."
    override val firmwareUpdateNotEnoughBattery =
        "L'aggiornamento del firmware richiede alcuni minuti. \nSe la carica della batteria del dispositivo è bassa, eseguire l'aggiornamento dopo averla ricaricata oppure collegarla a un caricabatteria. \nAvviare l'aggiornamento?"
    override val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "L'applicazione non è riuscita ad aggiornare il firmware. \nEseguire nuovamente il processo di aggiornamento del firmware. \nSe l'aggiornamento non riesce dopo diversi tentativi, l'unità {0} potrebbe essere guasta."
    override val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "È collegata un'unità che non funziona normalmente.\n\nSe il firmware di {0} è aggiornato a questa condizione, il nuovo firmware per {0} può sovrascrivere il firmware delle altre unità e può causare un guasto.\nScollegare tutte le unità diverse da {0}."
    override val firmwareUpdateLatest = "Recente"
    override val firmwareUpdateWaitingForUpdate = "Attesa aggiornamento"
    override val firmwareUpdateCompleted = "Fine"
    override val firmwareUpdateProtoType = "prototipo"
    override val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "Aggiornamento del firmware di {0} non riuscito. \nContinuare con il processo di ripristino."
    override val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "Scaricare la versione più recente del firmware?"
    override val firmwareUpdateInvalidFirmwareFileSingle = "Il file del firmware {0} non è valido."
    override val firmwareUpdateStopFwUpdate = "Annullare l'aggiornamento del software."
    override val firmwareUpdateStopFwRestoring = "Annullare il ripristino del firmware."
    override val firmwareUpdateRestoreComplete =
        "Quando si usa {0}, è possibile modificare l'impostazione dello stato delle sospensioni mostrato in SC dal Impostazione interruttore nel menu Personalizza."
    override val firmwareUpdateRestoreCompleteSc = "SC"
    override val firmwareUpdateSystemUpdateFailed = "Impossibile aggiornare il sistema."
    override val firmwareUpdateAfterUnitRecognitionFailed =
        "Impossibile connettersi dopo l'aggiornamento del firmware."
    override val firmwareUpdateUpdateBleUnitFirst = "Innanzitutto, aggiornare il firmware per {0}."
    override val firmwareUpdateSystemUpdateFailure = "Impossibile aggiornare il sistema."
    override val firmwareUpdateSystemUpdateFinishedWithoutUpdate = "Il sistema non era aggiornato."
    override val firmwareUpdateBleUpdateErrorOccuredConnectAgain =
        "Effettuare nuovamente il collegamento."
    override val firmwareUpdateFwRestoring =
        "Sovrascrittura firmware in corso. \nNon scollegare l'unità fino al completamento della sovrascrittura."
    override val firmwareUpdateFwRestorationUnitIsNormal =
        "Il firmware di {0} funziona normalmente."
    override val firmwareUpdateNoNeedForFwRestoration = "Non necessita di essere ripristinato."
    override val firmwareUpdateFwRestorationError = "Ripristino firmware di {0} non riuscito."
    override val firmwareUpdateFinishedFirmwareUpdate =
        "Il firmware è stato aggiornato alla versione più recente."
    override val firmwareUpdateFinishedFirmwareRestoration =
        "Il firmware di {0} è stato ripristinato."
    override val firmwareUpdateRetryFirmwareRestoration =
        "Il ripristino del firmware per {0} viene rieseguito."
    override val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "È necessario accettare i termini d'uso del software per aggiornare il firmware sulle seguenti unità."
    override val functionThresholdAdditionalSoftwareUsageAgreement =
        "Contratto di licenza software supplementare"
    override val functionThresholdMustRead = "(Leggere scrupolosamente)"
    override val functionThresholdContentUpdateDetail = "Modifiche"
    override val functionThresholdBtnTitle = "Accetta e aggiorna"
    override val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "È necessario accettare i termini d'uso del software per ripristinare il firmware."
    override val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "È necessario accettare i termini d'uso del software per ripristinare il firmware dell'unità."
    override val commonsPreset = "Preimpostazione"
    override val presetTopMenuLoadFileBtn = "Caricamento di un file delle impostazioni"
    override val presetTopMenuLoadFromBikeBtn = "Caricamento delle impostazioni dalla bici"
    override val presetDflyCantSet =
        "La configurazione dell'unità collegata non supporta le impostazioni D-FLY.\nImpossibile scrivere le impostazioni D-FLY."
    override val presetSkipUnitBelow = "La seguente unità verrà tralasciata."
    override val presetNoSupportedFileVersion =
        "Un file della preimpostazione creato in una versione precedente a {0} non può essere utilizzato con l'attuale tipo di bicicletta."
    override val presetPresetTitleShiftMode = "Modalità cambio"
    override val presetConnectedUnit = "Collegare un'unità."
    override val presetEndConnected = "Collegato"
    override val presetErrorWhileReading =
        "Si è verificato un errore durante la lettura delle impostazioni."
    override val presetOverConnect =
        "{1} unità {0} sono collegate.\nCollegare solo il numero selezionato di unità {0}."
    override val presetChangeSwitchType = "{0} è stato riconosciuto. Passare a {1}?"
    override val presetLackOfTargetUnits =
        "Manca un'unità richiesta nella configurazione corrente delle unità.\nRiesaminare la configurazione dell'unità e modificare nuovamente le impostazioni."
    override val presetLoadFileTopMessage =
        "Selezionare il file delle preimpostazioni da caricare o eliminare dall'elenco seguente."
    override val presetLoadFileModifiedOn = "Data dell'aggiornamento"
    override val presetLoadFileConfimDeleteFile =
        "Il file selezionato {0} verrà eliminato. \nContinuare?"
    override val presetLoadFileErrorDeleteFile =
        "L'applicazione non è riuscita a eliminare il file delle preimpostazioni."
    override val presetLoadFileErrorLoadFile =
        "Lettura del file della preimpostazione completato non riuscito."
    override val presetLoadFileErrorNoSetting =
        "Le impostazioni di un'unità supportata dall'attuale tipo di bicicletta non sono incluse."
    override val presetLoadFileErrorIncompatibleSetting =
        "Le impostazioni di un'unità non supportata dall'attuale tipo di bicicletta sono incluse.\nIgnorare le impostazioni delle unità non supportate?"
    override val presetLoadFileErrorShortageSetting =
        "Voci di impostazione aggiunte/eliminate.\nVerificare nuovamente le impostazioni del file preimpostazione, salvare il file e tentare nuovamente la scrittura."
    override val presetLoadFileErrorUnanticipatedSetting =
        "Un valore imprevisto è stato letto durante la lettura del valore impostato.\nVerificare nuovamente le impostazioni del file preimpostazione, salvare il file e tentare nuovamente la scrittura."
    override val presetWriteToBikeBan =
        "Impossibile scrivere le impostazioni Synchronized shift perché le impostazioni Synchronized shift e le impostazioni del deragliatore posteriore non corrispondono. \nContinuare?"
    override val presetWriteToBikePoint =
        "Impossibile scrivere le impostazioni Synchronized shift perché il punto di cambio rapporto non rientra nella gamma di impostazioni valida. \nContinuare?"
    override val presetWriteToBikeNoSettingToWrite =
        "Non sono presenti informazioni sull'impostazione da scrivere."
    override val presetQEndPreset = "Disattivare il collegamento e completare l'installazione?"
    override val commonsSynchromap = "Modalità cambio{0}"
    override val commonsSetting = "Impostazione"
    override val commonsDirection = "Direzioni"
    override val commonsChangePointFd = "Punto di cambio per FD"
    override val commonsChangePointRd = "Punto di cambio per RD"
    override val commonsAimPoint = "Posizione marcia di riferimento per RD"
    override val commonsDirectionUp = "Direzione SU"
    override val commonsDirectionDown = "Direzione GIÙ"
    override val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} è impostato nei pulsanti multipli di {0}."
    override val switchFunctionSettingDuplicationCheckMsg3 =
        "Assegnare un'impostazione diversa a ciascun pulsante."
    override val presetSaveFileOk =
        "Salvataggio del file della preimpostazione completato con successo."
    override val presetSaveFileError =
        "Salvataggio del file della preimpostazione completato non riuscito."
    override val presetFile = "File"
    override val presetSaveCheckErrorChangeSetting = "Modificare le impostazioni."
    override val presetSaveCheckErrorSemiSynchro =
        "Semi-Synchronized Shift è impostato su un valore non valido."
    override val presetConnectionNotPresetUnitConnected = "Impossibile riconoscere l'unità."
    override val presetConnectionSameUnitsConnectionError =
        "Sono state rilevate unità identiche. La funzione di preimpostazione non supporta unità identiche."
    override val presetNeedFirmwareUpdateToPreset =
        "Le impostazioni non possono essere scritte sull'/sulle unità seguente/i perché il firmware non è l'ultima versione."
    override val presetUpdateFirmwareToContinuePreset =
        "Per continuare a creare preimpostazioni, effettuare l'aggiornamento."
    override val presetOldFirmwareUnitsExist =
        "Il firmware delle unità seguenti non è aggiornato. \nAggiornare al firmware più recente ora?"
    override val presetOldFirmwareUnitsExistAndStopPreset =
        "Le impostazioni non possono essere scritte sull'/sulle unità seguente/i perché il firmware non è l'ultima versione.\nAggiornare il firmware dell'/delle unità su cui scrivere."
    override val presetDonotShowNextTimeCheckTitle =
        "Non mostrare questa schermata in futuro (selezionare solo se verranno riutilizzate le stesse impostazioni)."
    override val presetUpdateBtnTitle = "Aggiornamento"
    override val presetPresetOntyRecognizedUnit =
        "Scrivere le impostazioni solo sull'unità riconosciuta?"
    override val presetNewFirmwareUnitsExist =
        "Impossibile configurare le preimpostazioni perché il firmware dell'unità è più recente di quello dell'applicazione. \nAggiornare il firmware dell'applicazione alla nuova versione."
    override val presetNewFirmwareUnitsExistAndStopPreset =
        "Impossibile configurare normalmente le impostazioni perché il firmware dell'unità è più recente di quello dell'applicazione. \nIl processo è stato annullato."
    override val presetSynchronizedShiftSettings = "Impostazioni Synchronized shift "
    override val presetSemiSynchronizedShiftSettings = "Impostazioni Semi-Synchronized Shift "
    override val presetQContinue = "Continuare?"
    override val presetWriteToBikeNoUnit =
        "Impossibile scrivere {1} perché l'unità {0} non è collegata."
    override val presetWriteToBikeToothSettingNotMatch =
        "Impossibile scrivere {0} su S1 o S2 perché la struttura della dentatura nel file delle impostazioni differisce dalla struttura della dentatura dell'unità collegata."
    override val loginViewForgetPwdTitle = "Se si è dimenticata la password"
    override val loginViewForgetIdTitle = "ID dimenticato?"
    override val loginViewCreateUserContent =
        "*Se si preferisce che la registrazione dell'utente sia effettuata dal produttore di attrezzature originali, rivolgersi al distributore di zona o al personale Shimano."
    override val loginInternetConnection = "Impossibile collegarsi alla rete."
    override val loginAccountIsExist =
        "Questo account è bloccato.  Riprovare a eseguire l’accesso più tardi."
    override val loginCorrectIdOrPassword = "La password o l'ID utente non è corretto."
    override val loginPasswordErrorMessage = "La password non è corretta."
    override val loginPasswordExpired =
        "La password provvisoria è scaduta.\nRiavviare il processo di registrazione."
    override val loginPasswordExpiredChange =
        "La password non è stata cambiata da {0} giorni.\nModificare la password."
    override val loginChange = "Modifica"
    override val loginLater = "Più tardi"
    override val loginViewFinishLogin = "Apertura della sessione."
    override val loginErrorAll =
        "Problema nei contenuti immessi. Rivedere le voci immesse qui di seguito."
    override val loginViewUserId = "ID utente"
    override val loginViewEmail = "Indirizzo e-mail"
    override val loginViewPassword = "Password"
    override val loginViewCountry = "Paese"
    override val loginLoginMove = "Per accedere"
    override val loginMailSendingError =
        "Impossibile inviare l'e-mail. Attendere alcuni istanti e riprovare."
    override val loginAlphanumeric = "caratteri alfanumerici a mezza larghezza"
    override val loginTitleForgetPassword = "Ripristina password"
    override val loginViewForgetPasswordTitleFir =
        "Le informazioni necessarie per impostare una nuova password verranno inviate via e-mail."
    override val loginViewForgetMailSettingTitle =
        "Verificare le impostazioni in anticipo in modo da poter ricevere \"{0}\"."
    override val loginTitleForgetMailAdress = "Indirizzo e-mail registrato"
    override val loginUserIdAndEmailDataNotExist =
        "L'ID utente o l'indirizzo e-mail registrato non è corretto."
    override val loginViewForgotPassword = "E-mail per la reimpostazione della password inviata."
    override val loginTitleChangePassword = "Cambia password"
    override val loginViewCurrentPassword = "Password attuale"
    override val loginViewNewPassword = "La nuova password"
    override val loginErrorCurrentPasswordNonFormat =
        "La password attuale può contenere solo caratteri alfanumerici."
    override val loginErrorCurrentPasswordLength =
        "La password attuale è lunga da {0} a {1} caratteri."
    override val loginErrorNewPasswordNonFormat =
        "La nuova password può contenere solo caratteri alfanumerici."
    override val loginErrorNewPasswordLength =
        "La nuova password deve essere lunga da  {0} a {1} caratteri."
    override val loginErrorPasswordNoDifferent =
        "La nuova password non può essere uguale alla password attuale."
    override val loginErrorPasswordNoConsistent =
        "La nuova password non corrisponde alla password di conferma."
    override val loginViewFinishPasswordChange = "La password è stata modificata."
    override val loginTitleLogOut = "Logout"
    override val loginViewLogOut = "Chiusura della sessione."
    override val loginViewForgetIdTitleFir = "L'ID utente verrà inviato in una e-mail."
    override val loginTitleForgetId = "Notifica ID utente"
    override val loginViewForgetTitleSec =
        "*Se non è possibile usare l'indirizzo e-mail registrato, non sarà possibile rigenerare {0} in quanto non sarà possibile confermare l'identità dell'utente. Registrarsi come nuovo utente."
    override val loginEmailDataNotExist = "L'indirizzo e-mail non è stato registrato."
    override val loginViewForgotUserId = "L'ID utente è stato inviato in una e-mail."
    override val loginTitleUserInfo = "Query informazioni utente"
    override val loginViewAddress = "Indirizzo"
    override val loginUserDataNotExist = "Impossibile acquisire le informazioni sull’utente."
    override val commonsSet = "Impostare"
    override val commonsAverageVelocity = "Velocità media"
    override val commonsBackLightBrightness = "Luminosità"
    override val commonsBackLightBrightnessLevel = "Livello {0}"
    override val commonsCadence = "Cadenza"
    override val commonsDistanceUnit = "Unità"
    override val commonsDrivingTime = "Tempo trascorso"
    override val commonsForAssist = "per Servoassistenza"
    override val commonsForShift = "per Cambio"
    override val commonsInvalidate = "No"
    override val commonsKm = "Unità internazionali"
    override val commonsMaxGearUnit = "azionamenti"
    override val commonsMaximumVelocity = "Velocità massima"
    override val commonsMile = "Metodo iarda e libbra"
    override val commonsNotShow = "Non visualizzare"
    override val commonsShow = "Visualizzare"
    override val commonsSwitchDisplay = "Visualizzare passaggio"
    override val commonsTime = "Impostazione ora"
    override val commonsValidate = "Sì"
    override val commonsNowTime = "Ora esatta"
    override val commonsValidateStr = "Valida"
    override val commonsInvalidateStr = "Non valida"
    override val commonsAssistModeBoostRatio = "Modalità di servoassistenza BOOST"
    override val commonsAssistModeTrailRatio = "Modalità di servoassistenza TRAIL"
    override val commonsAssistModeEcoRatio = "Modalità di servoassistenza ECO"
    override val commonsStartMode = "Start mode (Modalità di partenza)"
    override val commonsAutoGearChangeModeLog = "Funzione cambio rapporti automatico"
    override val commonsAutoGearChangeAdjustLog = "Momento di cambiata"
    override val commonsBackLight = "Impostazione retroilluminazione"
    override val commonsFontColor = "Opzione sfondo carattere"
    override val commonsFontColorBlack = "Nero"
    override val commonsFontColorWhite = "Bianco"
    override val commonsInteriorTransmission = "Mozzo con sistema di cambio interno (catena)"
    override val commonsInteriorTransmissionBelt = "Mozzo con sistema di cambio interno (cinghia)"
    override val commonsRangeOverview = "Riepilogo percorso"
    override val commonsUnknown = "Non chiaro"
    override val commonsOthers = "Altro"
    override val commonsBackLightManual = "MANUALE"
    override val commonsStartApp = "Avvio"
    override val commonsSkipTutorial = "Salta l'esercitazione"
    override val powerMeterMonitorPower = "Alimentazione"
    override val powerMeterMonitorCadence = "Cadenza"
    override val powerMeterMonitorPowerUnit = "W"
    override val powerMeterMonitorCadenceUnit = "rpm"
    override val powerMeterMonitorCycleComputerConnected = "Ritorna al ciclocomputer"
    override val powerMeterMonitorGuideTitle = "Esecuzione della calibrazione con correzione zero"
    override val powerMeterMonitorGuideMsg =
        "1.Collocare la bicicletta su una superficie in piano.\n2.Posizionare la pedivella in modo che sia perpendicolare al suolo come mostrato nell'illustrazione.\n3.Premere il pulsante \"Calibrazione con correzione zero\".\n\nNon poggiare i piedi sui pedali, né esercitare pressione sulla pedivella."
    override val powerMeterLoadCheckTitle = "Carica la modalità di controllo"
    override val powerMeterLoadCheckUnit = "N"
    override val powerMeterFewCharged = "Livello batteria basso. Caricare la batteria."
    override val powerMeterZeroOffsetErrorTimeout =
        "Impossibile connettersi al misuratore di potenza a causa della connessione wireless scadente.\nSpostarsi in un ambiente con una migliore ricezione wireless."
    override val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Carica batteria insufficiente. Caricare la batteria e riprovare."
    override val powerMeterZeroOffsetErrorSensorValueOver =
        "È possibile che sia presente un carico sulla pedivella. Eliminare il carico dalla pedivella e riprovare."
    override val powerMeterZeroOffsetErrorCadenceSignalChange =
        "È possibile che la pedivella si sia mossa. Staccare le mani dalla pedivella e riprovare."
    override val powerMeterZeroOffsetErrorSwitchOperation =
        "L'interruttore potrebbe essere stato azionato. Staccare le mani dall'interruttore e riprovare."
    override val powerMeterZeroOffsetErrorCharging = "Scollegare il cavo di carica e riprovare."
    override val powerMeterZeroOffsetErrorCrankCommunication =
        "Il connettore della pedivella sinistra potrebbe essere staccato. Rimuovere il tappo esterno, controllare che il connettore sia collegato e riprovare."
    override val powerMeterZeroOffsetErrorInfo =
        "Per i dettagli, fare riferimento al manuale Shimano."
    override val drawerMenuLoadCheck = "Carica la modalità di controllo"
    override val commonsMonitor = "Modalità di monitoraggio"
    override val commonsZeroOffsetSetting = "calibrazione con correzione zero"
    override val commonsGroupPowermeter = "misuratore di potenza"
    override val commonsPresetError =
        "Impossibile preimpostare il misuratore di potenza.\nCollegare a un'altra unità."
    override val commonsWebPageTitleAdditionalFunction =
        "E-TUBE PROJECT|Informazioni sulle funzioni supplementari"
    override val commonsWebPageTitleFaq = "E-TUBE PROJECT|Domande frequenti"
    override val commonsWebPageTitleGuide = "E-TUBE PROJECT|Come usare E-TUBE PROJECT"
    override val connectionReconnectingBle =
        "Il collegamento Bluetooth® LE è stato interrotto.\nProvare a effettuare nuovamente il collegamento."
    override val connectionCompleteReconnect = "Ricollegamento completato."
    override val connectionFirmwareWillBeUpdated = "Il firmware verrà aggiornato."
    override val connectionFirmwareUpdatedFailed =
        "Ricollegare l'unità.\nSe si verifica lo stesso errore, l'unità potrebbe essere guasta.\nRivolgersi al distributore o al rivenditore."
    override val settingMsg7 = "Sincronizzare automaticamente l'impostazione ora nel ciclocomputer."
    override val commonsDestinationType = "Tipo {0}"
    override val customizeSwitchSettingViewControllerMsg15 = "Interruttore servoassistenza"
    override val presetNeedToChangeSettings =
        "È stata configurata una funzione che non può essere utilizzata. Modificare le funzioni che possono essere utilizzate in Personalizza, quindi riprovare."
    override val connectionErrorSelectNoDetectedMsg1 =
        "Impossibile ripristinare se non è presente nessun nome unità da selezionare."
    override val connectionTurnOnGps =
        "Per collegarsi a un'unità wireless, attivare le informazioni sulla posizione del dispositivo."
    override val drawerMenuAboutEtube = "Informazioni su E-TUBE PROJECT"
    override val commonsX2Y2Notice =
        "* Se l'impostazione per X2/Y2 è su \"Non utilizzare\" in {0}, non sarà possibile effettuare il riconoscimento se X2/Y2 è premuto."
    override val powerMeterZeroOffsetComplete = "Risultato zero offset"
    override val commonsMaxAssistSpeed = "Velocità massima assistita"
    override val commonsShiftingAdvice = "Cambiata suggerita"
    override val commonsSpeedAdjustment = "Velocità visualizzata"
    override val customizeSpeedAdjustGuideMsg =
        "Procedere con la regolazione se diversa da altri indicatori di velocità."
    override val commonsMaintenanceAlertDistance = "Avviso manutenzione: Distanza"
    override val commonsMaintenanceAlertDate = "Avviso manutenzione: Data"
    override val commonsAssistPatternComfort = "COMFORT"
    override val commonsAssistPatternSportive = "SPORTIVE"
    override val commonsEbike = "E-BIKE"
    override val connectionDialogMessage20 =
        "{0} dispone di un interruttore di impostazione per la servoassistenza. Modificare le impostazioni."
    override val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "Se {0} è impostato solo per la funzione servoassistenza\nè possibile impostare solo:\nInterruttore X: Servoassistenza rapporto superiore\nInterruttore Y: Servoassistenza rapporto inferiore"
    override val switchFunctionSettingDuplicationCheckMsg5 =
        "La funzione di sospensione non può essere impostata contemporaneamente a un’altra funzione."
    override val switchFunctionSettingDuplicationCheckMsg6 =
        "Per SW-E6000\nè possibile impostare solamente le seguenti combinazioni\nCambio superiore\nCambio inferiore\nDisplay\noppure\nServoassistenza rapporto superiore\nServoassistenza rapporto inferiore\nVisualizza/Luce"
    override val commonsGroupSw = "interruttore"
    override val customizeWarningMaintenanceAlert =
        "Questa impostazione dell’avviso di manutenzione comporta la visualizzazione di avvisi. Continuare?"
    override val customizeCommunicationModeSetting =
        "Impostazioni modalità di comunicazione wireless"
    override val customizeCommunicationModeCustomizeItemTitle =
        "Modalità comunicazione wireless (per ciclocomputer)"
    override val customizeCommunicationModeAntAndBle = "Modalità ANT/Bluetooth® LE"
    override val customizeCommunicationModeAnt = "Modalità ANT"
    override val customizeCommunicationModeBle = "Modalità Bluetooth® LE"
    override val customizeCommunicationModeOff = "Modalità OFF"
    override val customizeCommunicationModeGuideMsg =
        "Ridurre il consumo della batteria allineando le impostazioni con il sistema di comunicazione dei ciclocomputer. Se si effettua il collegamento a un ciclocomputer con un sistema di comunicazione diverso, ripristinare E-TUBE PROJECT.\nÈ possibile usare E-TUBE PROJECT per tablet e smartphone con qualsiasi sistema di comunicazione configurato"
    override val presetCantSet = "Non è possibile impostare quanto segue per {0}."
    override val presetWriteToBikeInvalidSettings = "{0} è collegato."
    override val presetDuMuSettingNotice =
        "Le impostazioni dellʼunità trasmissione non corrispondono a quelle dellʼunità motore. "
    override val presetDuSettingDifferent = "L'unità di trasmissione non è impostata per {0}."
    override val commonsAssistLockChainTension =
        "È stata eseguita una regolazione della tensione catena?"
    override val commonsAssistLockInternalGear =
        "Quando si utilizza un mozzo con sistema di cambio interno, è necessario regolare la tensione catena."
    override val commonsAssistLockInternalGear2 =
        "Regolare la tensione della catena, quindi premere il pulsante Esegui."
    override val commonsAssistLockCrank =
        "È stato eseguito un controllo dellʼangolo della pedivella?"
    override val commonsAssistLockCrankManual =
        "La pedivella sinistra deve essere installata nell’asse con l’angolazione appropriata. Verificare l’angolazione della pedivella installata, quindi premere il pulsante Esegui."
    override val switchFunctionSettingDuplicationCheckMsg7 =
        "La stessa funzione non può essere assegnata a più di un pulsante."
    override val commonsMaintenanceAlertUnitKm = "km"
    override val commonsMaintenanceAlertUnitMile = "mile"
    override val commonsMaxAssistSpeedUnitKm = "km/h"
    override val commonsMaxAssistSpeedUnitMph = "mph"
    override val firmwareUpdateBlefwUpdateFailed = "Impossibile aggiornare il firmware di {0}."
    override val firmwareUpdateShowFaq = "Visualizza informazioni dettagliate"
    override val commonsMaintenanceAlert = "Avviso manutenzione"
    override val commonsOutOfSettableRange =
        "{0} è al di fuori dell’intervallo di impostazione consentito."
    override val commonsCantWriteToUnit = "Impossibile scrivere {0} con le impostazioni correnti."
    override val connectionScanModalText =
        "Impossibile stabilire una connessione durante l’utilizzo di E-TUBE RIDE. Scollegare E-TUBE RIDE e l’unità wireless nell’applicazione, quindi riprovare a connettersi.\n Altrimenti, impostare l'unità wireless sulla bici sulla modalità di collegamento Bluetooth® LE."
    override val connectionUsingEtubeRide = "Quando si utilizza E-TUBE RIDE."
    override val connectionScanModalTitle = "Quando si utilizza E-TUBE RIDE."
    override val firmwareUpdateCommunicationIsUnstable =
        "L’ambiente di ricezione wireless è instabile.\n Avvicinare l’unità wireless al dispositivo per migliorare l’ambiente di ricezione wireless."
    override val connectionQBleFirmwareRestore =
        "È stata trovata un’unità wireless guasta.\n Ripristinare il firmware?"
    override val commonsAutoGearChange = "Funzione cambio rapporti"
    override val firmwareUpdateBlefwUpdateRestoration =
        "Riconnettersi tramite Bluetooth® LE, quindi avviare il processo di ripristino."
    override val firmwareUpdateBlefwUpdateReconnectFailed =
        "Impossibile riconnettere Bluetooth® LE automaticamente dopo aver aggiornato {0}."
    override val firmwareUpdateBlefwResotreReconnectFailed =
        "Impossibile riconnettere Bluetooth® LE automaticamente dopo aver ripristinato {0}."
    override val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Aggiornamento completato. Per continuare l’utilizzo, riconnettere prima Bluetooth® LE."
    override val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Ripristino completato. Per continuare l’utilizzo, riconnettere prima Bluetooth® LE."
    override val connectionConnectionToOsConnectingUnitSucceeded =
        "E-TUBE PROJECT connesso a {0} non visualizzato sullo schermo."
    override val connectionSelectYesToContinue =
        "Per continuare l’elaborazione della connessione, selezionare \"Conferma\"."
    override val connectionSelectNoToConnectOtherUnit =
        "Per disconnettere la connessione corrente e connettersi a un’altra unità, selezionare \"Annulla\"."
    override val commonsCommunicationIsNotStable =
        "L'ambiente di ricezione wireless è instabile.\n Portare {0} vicino al dispositivo per migliorare l'ambiente di ricezione wireless, quindi modificare nuovamente le impostazioni."

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    override val commonOn = "ACCESO"
    override val commonOff = "SPENTO"
    override val commonManual = "Manuale"
    override val commonInvalid = "Non valida"
    override val commonValid = "Valida"
    override val commonRecover = "Ripristina"
    override val commonRecoverEnter = "Ripristino (Invio)"
    override val commonNotRecover = "Non ripristinare"
    override val commonOKEnter = "OK (INVIO)"
    override val commonCancel = "Annulla"
    override val commonCancelEnter = "Annulla (INVIO)"
    override val commonUpdate = "Aggiorna (Invio)"
    override val commonNotUpdate = "Non aggiornare"
    override val commonDisconnect = "SCOLLEGA"
    override val commonSelect = "Seleziona"
    override val commonYes = "Sì"
    override val commonYesEnter = "Sì (INVIO)"
    override val commonNo = "No"
    override val commonNoEnter = "No (INVIO)"
    override val commonBack = "Indietro"
    override val commonRestore = "Indietro"
    override val commonSave = "Salva (Invio)"
    override val commonCancelEnterUppercase = "ANNULLA (INVIO)"
    override val commonAbort = "Annulla"
    override val commonApplyEnter = "Rifletti (Invio)"
    override val commonNext = "Avanti (Invio)"
    override val commonStartEnter = "Avvia (Invio)"
    override val commonClear = "Cancella"
    override val commonConnecting = "Collegamento in corso..."
    override val commonConnected = "Collegato"
    override val commonDiagnosing = "Diagnosi in corso..."
    override val commonProgramming = "Impostazione in corso..."
    override val commonRetrievingUnitData = "Rilevamento dati unità..."
    override val commonGetRetrieving = "Caricamento dati in corso..."
    override val commonRemainingSeconds = "Tempo rimasto:{0} secondi"
    override val commonTimeLimit = "Limite di tempo: {0} sec."
    override val commonCharging = "Caricamento in corso…"
    override val commonBeingPerformed = "Mentre vengono effettuate le attività"
    override val commonNotRun = "Non eseguito"
    override val commonNormal = "Normale"
    override val commonAbnormal = "Errore"
    override val commonCompletedNormally = "Completato correttamente"
    override val commonCompletedWithError = "Completato con errori"
    override val commonEBikeReport = "REPORT E-BIKE"
    override val commonBicycleInformation = "Informazioni sulla bicicletta"
    override val commonSkipUppercase = "SALTA"
    override val commonSkipEnter = "SALTA (ENTER)"
    override val commonWaitingForJudgment = "Attesa valutazione"
    override val commonStarting = "Avvio in corso {0}"
    override val commonCompleteSettingMessage = "L'impostazione è stata completata normalmente."
    override val commonProgrammingErrorMessage =
        "Si è verificato un errore durante l'elaborazione dell'impostazione."
    override val commonSettingValueGetErrorMessage =
        "Si è verificato un errore durante il recupero dei valori impostazioni."
    override val commonSettingUpMessage =
        "Impostazione in corso. Non scollegare l'unità fino al completamento dell'impostazione."
    override val commonCheckElectricWireMessage =
        "Controllare se il cavo elettrico è scollegato o meno."
    override val commonElectlicWireIsNotDisconnectedMessage =
        "Eseguire un controllo errori se il cavo elettrico non è scollegato."
    override val commonErrorCheckResults = "Risultati della verifica degli errori"
    override val commonErrorCheckCompleteMessage = "Verifica degli errori di {0} completata."
    override val commonCheckCompleteMessage = "Controllo di {0} completato."
    override val commonFaultCouldNotBeFound = "Non è stato possibile individuare un guasto."
    override val commonFaultMayExist = "Potrebbe esserci un guasto."
    override val commonMayBeFaulty = "{0} potrebbe essere guasto."
    override val commonItemOfMayBeFaulty = "La voce {0}° di {1} potrebbe essere guasta."
    override val commonSkipped = "Tralasciato"
    override val commonAllUnitErrorCheckCompleteMessage =
        "Verifica degli errori completata per tutte le unità."
    override val commonConfirmAbortWork = "Arrestare il processo?"
    override val commonReturnValue = "Verranno ripristinati i valori precedenti."
    override val commonManualFileIsNotFound = "File del manuale non trovato."
    override val commonReinstallMessage = "Disinstallare {0} e reinstallarla."
    override val commonWaiting = "Durante la modalità di attesa - PASSAGGIO {0}"
    override val commonDiagnosisInProgress = "Test diagnostico in corso - STEP {0}"
    override val commonNotPerformed = "Non eseguito"
    override val commonDefault = "Predefinito"
    override val commonDo = "Sì"
    override val commonNotDo = "No"
    override val commonOEMSetting = "Impostazioni OEM"
    override val commonTotal = "Totale"
    override val commonNow = "Attuale"
    override val commonReset = "Ripristina"
    override val commonEnd = "Fine (Invio)"
    override val commonComplete = "Fine"
    override val commonNextSwitch = "Al prossimo interruttore"
    override val commonStart = "Avvio"
    override val commonCountdown = "Conto alla rovescia"
    override val commonAdjustmentMethod = "Metodo di regolazione"
    override val commonUnknown = "Non chiaro"
    override val commonUnitRecognition = "Controllo collegamento"

    // 単位
    override val commonUnitStep = "{0} azionamenti"

    // 公式HPの言語設定
    override val commonHPLanguage = "it-IT"

    // アシスト
    override val commonAssistAssistOff = "Servoassistenza disattivata"

    // その他
    override val commonDelete = "Elimina"
    override val commonApply = "APPLICA"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    override val unitCommonFirmwareVersion = "Versione firmware"
    override val unitCommonSerialNo = "N. di serie"
    override val unitCommonFirmwareUpdate = "Aggiornamento firmware"
    override val unitCommonUpdateToTheLatestVersion = "Aggiornamento alla versione più recente."
    override val unitCommonFirmwareUpdateNecessary =
        "Se necessario, effettuare l'aggiornamento alla versione più recente."
    //endregion

    //region 一覧表示
    override val unitUnitBattery = "Batteria"
    override val unitUnitDI2Adapter = "Adattatore DI2"
    override val unitUnitInformationDisplay = "Display informazioni"
    override val unitUnitJunctionA = "Giunzione A"
    override val unitUnitRearSuspension = "Sospensione posteriore"
    override val unitUnitFrontSuspension = "Sospensione anteriore"
    override val unitUnitDualControlLever = "leva a doppio controllo"
    override val unitUnitShiftingLever = "leva cambio"
    override val unitUnitSwitch = "Interruttore"
    override val unitUnitBatterySwitch = "Interruttore batteria"
    override val unitUnitPowerMeter = "Misuratore di potenza"
    //endregion

    //region DU
    override val unitDUYes = "Sì"
    override val unitDUNO = "No"
    override val unitDUDestination = "Destinazione"
    override val unitDURemainingLightCapacity = "Autonomia luce residua"
    override val unitDUType = "Tipo {0}"
    override val unitDUPowerTerminalOutputSetting =
        "Impostazione di uscita terminale alimentazione fanale/accessori"
    override val unitDULightONOff = "Luce ON/OFF"
    override val unitDULightConnection = "Collegamento della luce"
    override val unitDUButtonOperation = "Operazione con il pulsante"
    override val unitDUAlwaysOff = "Sempre SPENTO"
    override val unitDUButtonOperations = "Operazione con il pulsante"
    override val unitDUAlwaysOn = "Sempre ACCESO"
    override val unitDUSetValue = "Impostare il valore"
    override val unitDUOutputVoltage = "Tensione di uscita"
    override val unitDULightOutput = "Emissione di luce"
    override val unitDUBatteryCapacity = "Capacità della batteria per la luce restante"
    override val unitDUWalkAssist = "Servoassistenza walk"
    override val unitDUTireCircumference = "Circonferenza pneumatico"
    override val unitDUGearShiftingType = "Tipologia di cambio"
    override val unitDUShiftingMethod = "Modifica metodo"
    override val unitDUFrontChainRing = "Numero di denti dell’anello della catena anteriore"
    override val unitDURearSprocket = "Numero di denti della ruota dentata posteriore"
    override val unitDUToothSelection = "Selezione dentatura"
    override val unitDUInstallationAngle =
        "Angolo di installazione dell'unità di trasmissione"
    override val unitDUAssistCustomize = "Personalizzazione servoassistenza"
    override val unitDUProfile1 = "Profilo 1"
    override val unitDUProfile2 = "Profilo 2"
    override val unitDUCostomize = "Personalizza"
    override val unitDUAssistCharacter = "Carattere servoassistenza"
    override val unitDUPowerful = "POWERFUL"
    override val unitDUMaxTorque = "Coppia di serraggio max"
    override val unitDUAssistStart = "Avvio servoassistenza"
    override val untDUMild = "MILD"
    override val unitDUQuick = "QUICK"
    override val unitDUAssistLv = "Lv.{0:D}"
    override val unitDUAssitLvForMobile = "Lv.{0}"
    override val unitDUAssistLvParen = "Lv.{0} ({1})"
    override val unitDUOther = "Altro"
    override val unitDU1stGear = "1° rapporto"
    override val unitDU2ndGear = "2° rapporto"
    override val unitDU3rdGear = "3° rapporto"
    override val unitDU4thGear = "4° rapporto"
    override val unitDU5thGear = "5° rapporto"
    override val unitDU6thGear = "6° rapporto"
    override val unitDU7thGear = "7° rapporto"
    override val unitDU8thGear = "8° rapporto"
    override val unitDU9thGear = "9° rapporto"
    override val unitDU10thGear = "10° rapporto"
    override val unitDU11thGear = "11° rapporto"
    override val unitDU12thGear = "12° rapporto"
    override val unitDU13thGear = "13° rapporto"
    override val unitDUShiftModeAfterDisconnect =
        "Modalità dopo lo scollegamento dell'app"
    override val unitDUAutoGearShiftAdjustment = "Regolazione cambio rapporti automatico"
    override val unitDUShiftingAdvice = "Cambiata suggerita"
    override val unitDUTravelingDistance = "Distanza percorso"
    override val unitDUTravelingDistanceMaintenanceAlert = "Distanza percorso (Avviso manutenzione)"
    override val unitDUDate = "Data"
    override val unitDUDateYear = "Data: Anno (Avviso manutenzione)"
    override val unitDUDateMonth = "Data: Mese (Avviso manutenzione)"
    override val unitDUDateDay = "Data: Giorno (Avviso manutenzione)"
    override val unitDUTotalDistance = "Distanza totale"
    override val unitDUTotalTime = "Tempo totale"
    override val unitDURemedy = "Rimedio"
    //endregion

    //region BT
    override val unitBTCycleCount = "Conteggio cicli"
    override val unitBTTimes = "Volte"
    override val unitBTRemainingCapacity = "Carica rimanente"
    override val unitBTFullChargeCapacity = "Capacità residua della batteria"

    override val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //endregion

    //region SC, EW
    override val unitSCEWMode = "modalità"
    override val unitSCEWModes = "modalità"
    override val unitSCEWDisplayTime = "Tempo di visualizzazione : {0}"
    override val unitSCEWBeep = "Bip : {0}"
    override val unitSCEWBeepSetting = "Impostazione bip"
    override val unitSCEWWirelessCommunication = "Comunicazione wireless"
    override val unitSCEWCommunicationModeOff = "Modalità OFF"
    override val unitSCEWBleName = "Nome Bluetooth® LE\n"
    override val unitSCEWCharacterLimit = "Da 1 a 8 caratteri alfanumerici ridotti"
    override val unitSCEWPasskeyDescription =
        "Numero di 6 cifre che inizia con un numero diverso da 0"
    override val unitSCEWConfirmation = "Conferma"
    override val unitSCEWPassKeyInitialization = "Inizializzazione PassKey"
    override val unitSCEWEnterPasskey =
        "Inserisci un numero di 6 cifre che inizi con un numero diverso da 0."
    override val unitSCEWNotMatchPassKey = "Passkey errata."
    override val unitSCEWDisplayUnits = "Visualizzare unità"
    override val unitSCEWInternationalUnits = "Unità internazionali"
    override val unitSCEWDisplaySwitchover = "Cambio"
    override val unitSCEWRangeOverview = "Gamma"
    override val unitSCEWAutoTimeSetting = "Ora (auto)"
    override val unitSCEWManualTimeSetting = "Ora (manuale)"
    override val unitSCEWUsePCTime = "Utilizzare ora del PC"
    override val unitSCEWTimeSetting = "Impostazione ora"
    override val unitSCEWDoNotSet = "Non impostare"
    override val unitSCEWBacklight = "Retroilluminazione"
    override val unitSCEWBacklightSetting = "Impostazione retroilluminazione"
    override val unitSCEWManual = "Manuale"
    override val unitSCEWBrightness = "Luminosità"
    override val unitSCEWFont = "Carattere"
    override val unitSCEWSet = "Impostare"
    //endregion

    //region MU
    override val unitMUGearPosition = "Posizione marcia"
    override val unitMUAdjustmentSetting = "Impostazione di regolazione"

    override val unitMU5thGear = "5° rapporto"
    override val unitMU8thGear = "8° rapporto"
    override val unitMU11thGear = "11° rapporto"
    //endregion
    //endregion

    //region SUS
    override val unitSusCD = "CD"
    override val unitSusCtd = "CTD"
    override val unitSusCtdBV = "CTD (BV)"
    override val unitSusCtdOrDps = "CTD (DISH) o DPS"
    override val unitSusPositionFront = "Posizione{0} anteriore"
    override val unitSusPositionRear = "Posizione{0} posteriore"
    override val unitSusPositionDisplayedOnSC = "Posizione{0} visualizzata su SC"
    override val unitSusC = "C"
    override val unitSusT = "T"
    override val unitSusD = "D"
    override val unitSusDUManufacturingSerial = "Numero di serie DU"
    override val unitSusBackupDateAndTime = "Data e ora di backup"
    //endregion

    //region ST,SW
    override val unitSTSWUse2ndGgear = "Usa il 2° rapporto"
    override val unitSTSWSwitchMode = "Modalità dell'interruttore"
    override val unitSTSWForAssist = "per Servoassistenza"
    override val unitSTSWForShift = "per Cambio"
    override val unitSTSWUse = "Utilizzare"
    override val unitSTSWDoNotUse = "Non utilizzare"
    //endregion

    //region Shift
    override val unitShiftCopy = "Copier {0}"
    override val unitShiftShiftUp = "Cambio superiore"
    override val unitShiftShiftDown = "Cambio inferiore"
    override val unitShiftGearNumberLimit = "Limite numero rapporto"
    override val unitShiftMultiShiftGearNumberLimit = "Limite numero rapporto (Cambiata multipla)"
    override val unitShiftInterval = "Intervallo delle rotazioni dell'azionamento"
    override val unitShiftMultiShiftGearShiftingInterval =
        "Intervallo di cambiata (Cambiata multipla)"
    override val unitShiftAutomaticGearShifting = "Cambio automatico"
    override val unitShiftMultiShiftFirstGearShiftingPosition =
        "Intervallo di cambiata 1° rapporto (Cambiata multipla)"
    override val unitShiftMultiShiftSecondGearShiftingPosition =
        "Intervallo di cambiata 2° rapporto (Cambiata multipla)"
    override val unitShiftSingleShiftingPosition = "Posizione cambio singolo"
    override val unitShiftDoubleShiftingPosition = "Posizione cambio doppio"
    override val unitShiftShiftInterval = "Intervallo cambio"
    override val unitShiftRemainingBatteryCapacity = "Autonomia residua della batteria"
    override val unitShiftBatteryMountingForm = "Configurazione montaggio batteria"
    override val unitShiftPowerSupply = "Con alimentazione esterna"
    override val unitShift20PercentOrLess = "20% o meno"
    override val unitShift40PercentOrLess = "40% o meno"
    override val unitShift60PercentOrLess = "60% o meno"
    override val unitShift80PercentOrLess = "80% o meno"
    override val unitShift100PercentOrLess = "100% o meno"
    override val unitShiftRearShiftingUnit = "Unità cambio posteriore"
    override val unitShiftAssistShiftSwitch = "Interruttore \nServoassistenza/cambio"
    override val unitShiftInner = "Interno"
    override val unitShiftMiddle = "Centrale"
    override val unitShiftOuter = "Esterno"
    override val unitShiftUp = "Su"
    override val unitShiftDown = "Giù"
    override val unitShiftGearShiftingInterval = "Intervallo delle rotazioni dell'azionamento"
    //endregion

    //region PC接続機器
    override val unitPCErrorCheck = "Verifica degli errori"
    override val unitPCBatteryConsumptionCheck = "Verifica consumo batteria"
    override val unitPCBatteryConsumption = "Consumo batteria"
    //endregion

    //region エラーチェック
    override val unitErrorCheckMalfunctionInsideProsduct = "Sono presenti guasti interni?"
    override val unitErrorCheckBatteryConnectedProperly = "La batteria è collegata correttamente?"
    override val unitErrorCheckCanDetectTorque = "È possibile rilevare la coppia?"
    override val unitErrorCheckCanDetectVehicleSpeed =
        "È possibile rilevare la velocità del veicolo?"
    override val unitErrorCheckCanDetectCadence = "È possibile rilevare la cadenza?"
    override val unitErrorCheckLightOperateNormally = "Il fanale funziona correttamente?"
    override val unitErrorCheckOperateNormally = "Funziona correttamente?"
    override val unitErrorCheckDisplayOperateNormally =
        "L'area di visualizzazione funziona correttamente?"
    override val unitErrorCheckBackLightOperateNormally =
        "La retroilluminazione funziona correttamente?"
    override val unitErrorCheckBuzzerOperateNormally = "Il cicalino funziona correttamente?"
    override val unitErrorCheckSwitchOperateNormally = "Gli interruttori funzionano correttamente?"
    override val unitErrorCheckwirelessFunctionNormally = "Il wireless funziona correttamente?"
    override val unitErrorCheckBatteryHasEnoughPower =
        "Il livello della batteria incorporata è sufficientemente alto?"
    override val unitErrorCheckNormalShiftToEachGear =
        "È possibile cambiare rapporto correttamente?"
    override val unitErrorCheckEnoughBatteryForShifting =
        "Il livello della batteria è sufficiente per il cambio di rapporto?"
    override val unitErrorCheckCommunicateNormallyWithBattery =
        "È possibile stabilire correttamente le comunicazioni con la batteria?"
    override val unitErrorCheckLedOperateNormally = "I LED funzionano correttamente?"
    override val unitErrorCheckSwitchOperation = "Funzionamento dell'interruttore"
    override val unitErrorCheckCrankArmPperation = "Funzionamento del braccio della pedivella"
    override val unitErrorCheckLcdCheck = "Verifica LCD"
    override val unitErrorCheckLedCheck = "Verifica LED"
    override val unitErrorCheckAudioCheck = "Verifica audio"
    override val unitErrorCheckWirelessCommunicationCheck = "Verifica comunicazione wireless"
    override val unitErrorCheckPleaseWait = "Attendere."
    override val unitErrorCheckCheckLight = "Verifica del fanale"
    override val unitErrorCheckSprinterSwitch = "Interruttore velocista"
    override val unitErrorCheckSwitch = "Interruttore"
    //endregion
    //endregion

    //region M0 基本構成
    override val m0RecommendationOfAccountRegistration =
        "Raccomandazione della registrazione account"
    override val m0AccountRegistrationMsg =
        "Se si crea un account, è possibile utilizzare E-TUBE PROJECT in modo più conveniente.\n・Consente di registrare la propria bicicletta.\n・Si ricorda del dispositivo wireless, facilitando la connessione della bicicletta."
    override val m0SignUp = "REGISTRATI"
    override val m0Login = "ACCEDI"
    override val m0SuspensionSwitch = "Interruttore sospensione"
    override val m0MayBeFaulty = "{0} potrebbe essere guasto."
    override val m0ReplaceOrRemoveMessage =
        "Sostituire o rimuovere l'unità seguente ed effettuare nuovamente il collegamento.\n{0}"
    override val m0UnrecognizableMessage =
        "Comando cambio o interruttore non riconosciuti.\nVerificare che il cavo elettrico non sia disconnesso."
    override val m0UnknownUnit = "Unità sconosciuta {0}"
    override val m0UnitNotDetectedMessage =
        "Verifica che il cavo elettrico non sia stato rimosso.\nSe è stato rimosso, scollegare l'unità e quindi ricollegarla."
    override val m0ConnectErrorMessage =
        "Si è verificato un errore di comunicazione. Provare a riconnettersi."
    //endregion

    //region M1 起動画面
    override val m1CountryOfUse = "Paese d'uso"
    override val m1Continent = "Continenti"
    override val m1Country = "Paesi/regioni"
    override val m1OK = "OK"
    override val m1PersonalSettingsMsg = "Puoi impostare la bici a seconda del tuo stile di guida."
    override val m1NewRegistration = "NUOVA REGISTRAZIONE"
    override val m1Login = "ACCEDI"
    override val m1ForCorporateUsers = "Per utenti aziendali"
    override val m1Skip = "Salta"
    override val m1TermsOfService = "Termini di utilizzo"
    override val m1ComfirmLinkContentsAndAgree =
        "Fai clic sul link qui sotto per accettare il contenuto."
    override val m1TermsOfServicePolicy = "Termini di utilizzo"
    override val m1AgreeToTheTermsOfService = "Accetto i termini di utilizzo"
    override val m1AgreeToTheAll = "Accetto tutto"
    override val m1AppOperationLogAgreement = "Accordo registro operazioni app"
    override val m1AgreeToTheAppOperationLogAgreement = "Accetto l'Accordo registro operazioni app."
    override val m1DataProtecitonNotice = "Avviso protezione dati"
    override val m1AgreeUppercase = "ACCETTO"
    override val m1CorporateLogin = "Accesso aziendale"
    override val m1Email = "E-mail"
    override val m1Password = "Password"
    override val m1DownloadingLatestFirmware = "Il firmware più recente è stato scaricato."
    override val m1Hi = "Ciao, {0}"
    override val m1GetStarted = "Inizia"
    override val m1RegisterBikeOrPowerMeter = "Registra bici o misuratore di potenza"
    override val m1ConnectBikeOrPowerMeter = "Collega bici o misuratore di potenza"
    override val m1BikeList = "La mia bici"
    override val m1LastConnection = "Ultima connessione"
    override val m1Searching = "Ricerca in corso..."
    override val m1Download = "DOWNLOAD"
    override val m1SkipUppercase = "SALTA"
    override val m1Copyright = "©SHIMANO INC. TUTTI I DIRITTI RISERVATI"
    override val m1CountryArea = "Paesi/regioni"
    override val m1ConfirmContentMessage =
        "Se accetti i termini e le condizioni, inserisci un segno di spunta e premi il pulsante \"Avanti\" per proseguire."
    override val m1Next = "Avanti"
    override val m1Agree = "Accetto"
    override val m1ImageRegistrationFailed = "Impossibile registrare l'immagine."
    override val m1InternetConnectionUnavailable =
        "Nessun collegamento a Internet. Collegarsi a Internet e riprovare."
    override val m1ResumeBikeRegistrationMessage =
        "Nessuna bicicletta pertinente trovata. Collegarsi a Internet e registrare nuovamente la bicicletta."
    override val m1ResumeImageRegistrationMessage =
        "Si è verificato un errore imprevisto. Registrare nuovamente l'immagine."
    override val m1NoStorageSpaceMessage =
        "Nello smartphone non è presente spazio libero sufficiente."
    override val m1Powermeter = "Misuratore di potenza | {0}"
    //endregion

    //region M2 ペアリング
    override val m2SearchingUnits = "Ricerca unità in corso..."
    override val m2HowToConnectUnits = "Come collegare le unità"
    override val m2Register = "Registrazione"
    override val m2UnitID = "ID:{0}"
    override val m2Passkey = "Passkey"
    override val m2EnterPasskeyToRegisterUnit =
        "Inserisci la passkey per registrare l'unità."
    override val m2OK = "OK"
    override val m2CancelUppercase = "ANNULLA"
    override val m2ChangeThePasskey = "Modificare la passkey?"
    override val m2NotNow = "Non ora"
    override val m2Change = "Modifica"
    override val m2Later = "Più tardi"
    override val m2NewPasskey = "Nuova passkey"
    override val m2PasskeyDescription =
        "Inserisci un numero di 6 cifre iniziando con un numero diverso da 0"
    override val m2Cancel = "Annulla"
    override val m2Nickname = "NICKNAME"
    override val m2ConfirmUnits = "CONFERMA UNITÀ"
    override val m2SprinterSwitchHasConnected = "Interruttore sprinter collegato"
    override val m2SkipRegister = "Salta registrazione"
    override val m2RegisterAsNewBike = "REGISTRA COME NUOVA BICI"
    override val m2Left = "Sinistra"
    override val m2Right = "Destra"
    override val m2ID = "ID"
    override val m2Update = "AGGIORNA"
    override val m2Customize = "PERSONALIZZA"
    override val m2Maintenance = "MANUTENZIONE"
    override val m2Connected = "Collegato"
    override val m2Connecting = "Collegamento in corso..."
    override val m2AddOnRegisteredBike = "Aggiungi una bici registrata"
    override val m2AddPowermeter = "Aggiungi misuratore di potenza"
    override val m2Monitor = "MONITOR"
    override val m2ConnectTheBikeAndClickNext =
        "COLLEGA LA BICI E FAI CLIC SU \"AVANTI\"."
    override val m2Next = "AVANTI"
    override val m2ChangePasskey = "Cambia passkey"
    override val m2PowerMeter = "Indicatore di potenza"
    override val m2CrankArmSet = "impostazione braccio della pedivella"
    override val m2NewBicycle = "Nuova bicicletta"
    override val m2ChangeWirelessUnit = "Modifica dispositivo wireless"
    override val m2DeleteConnectionInformationMessage =
        "Dopo aver apportato le modifiche, eliminare le informazioni di connessione da [Impostazioni] > [Bluetooth] sul dispositivo in uso.\nSe le informazioni di connessione non vengono eliminate, non verranno sostituite, pertanto, sarà impossibile stabilire una connessione."
    override val m2Help = "Guida"
    override val m2PairingCompleted = "Abbinamento completato"
    override val m2AddSwitch = "Aggiungi interruttore"
    override val m2PairDerailleurAndSwitch =
        "Abbina un interruttore al deragliatore. *Il deragliatore può essere avviato con l'interruttore una volta disconnessa la bicicletta da E-TUBE PROJECT."
    override val m2HowToPairing = "Metodo di abbinamento"
    override val m2IdInputAreaHint = "Aggiungi interruttore"
    override val m2InvalidQRCodeErrorMsg =
        "Scansionare il QR code sull’interruttore. Potrebbe essere stato scansionato un QR code diverso."
    override val m2QRScanMsg =
        "Toccare sullo schermo per focalizzare. Se non è possibile leggere il QR code, immettere l’ID a 11 cifre."
    override val m2QRConfirm = "QR code/posizione ID"
    override val m2SerialManualInput = "Inserimento ID manuale"
    override val m2CameraAccessGuideMsgForAndroid =
        "Accesso alla fotocamera non consentito. Nelle impostazioni dell’app, consentire l’accesso per l’abbinamento interruttori."
    override val m2CameraAccessGuideMsgForIos =
        "Accesso alla fotocamera non consentito. Consentire l’accesso all’impostazione dell’app. Quando l’impostazione viene modificata per consentire l’accesso, l’app viene riavviata."
    override val m2AddWirelessSwitches = "Aggiungi interruttore wireless"
    //endregion

    //region M3 接続と切断
    override val m3Searching = "Ricerca in corso..."
    override val m3Connecting = "Collegamento in corso..."
    override val m3Connected = "Collegato"
    override val m3Detected = "Rilevato"
    override val m3SearchOff = "SPENTO"
    override val m3Disconnect = "SCOLLEGA"
    override val m3Disconnected = "Scollegato"
    override val m3RecognizedUnits = "Unità riconosciute"
    override val m3Continue = "Continua"

    //region 互換性確認
    override val m3CompatibilityTable = "Tabella di compatibilità"
    override val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "Per le unità di seguito, è possibile connettere solo una unità di ciascun tipo."
    override val m3CompatibilityErrorUnitSpecMessage1 =
        "La combinazione di unità riconosciuta non è compatibile. Connettere le unità secondo la tabella di compatibilità."
    override val m3CompatibilityErrorUnitSpecMessage2 =
        "In alternativa è possibile soddisfare i requisiti di compatibilità eliminando le unità rosse indicate di seguito."
    override val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "Il firmware delle unità seguenti non è aggiornato."
    override val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "Il problema potrebbe essere risolto aggiornando il firmware per una compatibilità estesa.\nProvare ad aggiornare il firmware."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    override val m3CompatibilityErrorDUAndBT =
        "La combinazione di unità riconosciuta non è compatibile. Connettere le unità secondo la tabella di compatibilità."
    //endregion
    //endregion

    //region M4 アップデート
    override val m4Latest = "Recente"
    override val m4UpdateAvailable = "AGGIORNA"
    override val m4UpdateAllUnits = "AGGIORNA TUTTO"
    override val m4UpdateAnyUnits = "AGGIORNAMENTO | TEMPO STIMATO - {0}"
    override val m4History = "CRONOLOGIA"
    override val m4AllUpdateConfirmTitle = "Aggiorna tutto?"
    override val m4AllUpdateConfirmMessage = "Tempo stimato: {0}"
    override val m4SelectAll = "SELEZIONA TUTTI (INVIO)"
    override val m4EstimatedTime = "Tempo stimato: "
    override val m4Cancel = "ANNULLA"
    override val m4BleVersion = "Bluetooth® LE ver.{0}"
    override val m4OK = "OK (INVIO)"
    override val m4Update = "Aggiornamento"
    override val m4UpdateUppercase = "AGGIORNAMENTO"

    // TODO:TextTableには表記がないので要確認
    override val m4Giant = "GIANT"
    override val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    override val m5EBike = "E-BIKE"
    override val m5Assist = "Servoassistenza"
    override val m5MaximumAssistSpeed = "velocità massima assistita : {0}"
    override val m5AssistPattern = "modello servoassistenza : {0}"
    override val m5RidingCharacteristic = "caratteristiche : {0}"
    override val m5CategoryShift = "Cambio"
    override val m5Synchronized = "Cambio"
    override val m5AutoShift = "Cambio automatico"
    override val m5Advice = "Avviso"
    override val m5Switch = "Unità interruttore"
    override val m5ForAssist = "Per servoassistenza"
    override val m5Suspension = "Sospensione"
    override val m5ControlSwitch = "Interruttore di comando"
    override val m5CategoryDisplay = "Display"
    override val m5SystemInformation = "Informazioni sul sistema"
    override val m5Information = "Display"
    override val m5DisplayTime = "Tempo di visualizzazione: {0}"
    override val m5Mode = "Modalità {0}"
    override val m5Other = "Altro"
    override val m5WirelessSetting = "Impostazioni wireless"
    override val m5Fox = "FOX"
    override val m5ConnectBikeToApplyChangedSettings =
        "Collega la bici per applicare le impostazioni modificate."
    override val m5CannotBeSetMessage = "Sono presenti funzioni che non possono essere impostate."
    override val m5GearShiftingInterval = "Intervallo delle rotazioni dell'azionamento : {0}"
    override val m5GearNumberLimit = "Limite numero rapporto : {0}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    override val m5Shift = "sincronizzato"
    override val m5Display = "informazioni"
    //endregion
    //endregion

    //region M6 バイク設定
    override val m6Bike = "Bici"
    override val m6Nickname = "Nickname"
    override val m6ChangeWirelessUnit = "Modifica dispositivo wireless"
    override val m6DeleteUnits = "Elimina unità"
    override val m6DeleteBike = "Elimina bici"
    override val m6Preset = "Preimpostazione"
    override val m6SaveCurrentSettings = "Salva le impostazioni attuali"
    override val m6WriteSettings = "Scrivi impostazioni"
    override val m6SettingsCouldNotBeApplied = "Impossibile applicare le impostazioni."
    override val m6DeleteUppercase = "ELIMINA"
    override val m6Save = "SALVA"
    override val m6ReferenceBike = "Bici di riferimento"
    override val m6PreviousData = "Dati precedenti"
    override val m6SavedSettings = "Impostazioni salvate"
    override val m6LatestSetting = "Ultima impostazione"
    override val m6Delete = "Elimina"
    override val m6BikeSettings = "Impostazioni della bici"
    override val m6WritingSettingIsCompleted = "Impostazione scritta."
    override val m6Other = "Altro"

    //region M7 カスタマイズ詳細
    override val m7TwoStepShiftDescription =
        "Posizione in cui è possibile abilitare la modalità multirapporto"
    override val m7FirstGear = "1° rapporto"
    override val m7SecondGear = "2° rapporto"
    override val m7OtherSwitchMessage =
        "Quando si esegue una cambiata multipla con un interruttore collegato a un componente diverso da SW-M9050, attivare il 2° rapporto."
    override val m7NameDescription = "Max 8 caratteri"
    override val m7WirelessCommunication = "Comunicazione wireless"
    override val m7Bike = "Bici"
    override val m7Nickname = "Nickname"
    override val m7ChangeWirelessUnit = "Modifica dispositivo wireless"
    override val m7DeleteUnit = "Elimina unità"
    override val m7DeleteBikeRegistration = "Elimina registrazione bici"
    override val m7Setting = "Impostazione"
    override val m7Mode = "Modalità"
    override val m7AlwaysDisplay = "Mostra sempre"
    override val m7Auto = "Auto"
    override val m7Manual = "Manuale"
    override val m7MaximumAssistSpeed = "Velocità massima assistita"
    override val m7ShiftingAdvice = "Cambiata suggerita"
    override val m7ShiftingTiming = "Momento di cambiata"
    override val m7StartMode = "Modalità di partenza"
    override val m7ConfirmConnectSprinterSwitch = "L'interruttore sprinter è collegato?"
    override val m7Reflect = "Rifletti (Invio)"
    override val m7Left = "Sinistra"
    override val m7Right = "Destra"
    override val m7SettingsCouldNotBeApplied = "Impossibile applicare le impostazioni."
    override val m7Preset = "PREIMPOSTATO"
    override val m7SaveCurrentSettings = "Salva impostazioni attuali"
    override val m7WriteSettings = "Scrivi impostazioni"
    override val m7EditSettings = "Modifica impostazioni"
    override val m7ExportSavedSettings = "Esporta impostazioni salvate"
    override val m7Name = "Nome"
    override val m7SettingWereNotApplied = "Le impostazioni non sono state applicate."
    override val m7DragAndDropFile = "Trascina e rilascia qui il file delle impostazioni."
    override val m7CharactersLimit = "Da 1 a 8 caratteri alfanumerici ridotti"
    override val m7PasskeyDescriptionMessage =
        "Immettere 6 caratteri numerici.\n0 non può essere impostato come primo carattere della PassKey eccetto durate l'inizializzazione PassKey."
    override val m7Display = "Visualizzare"
    override val m7CompleteSettingMessage = "L'impostazione è stata completata normalmente."

    override val m7ConfirmCancelSettingsTitle = "Interrompi configurazione"
    override val m7ConfirmDefaultTitle = "Ripristina le impostazioni predefinite"
    override val m7PasskeyDescription =
        "Inserisci un numero di 6 cifre iniziando con un numero diverso da 0"
    override val m7AssistCharacterWithValue = "Carattere servoassistenza : Lv.{0}"
    override val m7MaxTorqueWithValue = "Coppia di serraggio max : {0} Nm"
    override val m7AssistStartWithValue = "Avvio servoassistenza : Lv.{0}}"
    override val m7CustomizeDisplayName = "Personalizzazione nome display"
    override val m7DisplayNameDescription = "Fino a 10 caratteri"
    override val m7AssistModeDisplay = "Display Modalità di servoassistenza"
    //endregion

    //region M8 カスタマイズ-スイッチ
    override val m8OptionalSwitchTitle = "Interruttore opzionale"
    override val m8SettingColudNotBeApplied = "Impossibile applicare l'impostazione."
    override val m8MixedAssistShiftCheckMessage =
        "Servoassistenza rapporto superiore, servoassistenza rapporto inferiore, display/fanali e cambio superiore, cambio inferiore, display, non potrai impostare il Canale D-FLY in un interruttore singolo."
    override val m8EmptyConfigurableListMessage = "Non sono presenti elementi configurabili."
    override val m8NotApplySettingMessage = "Alcuni pulsanti non applicano funzioni."
    override val m8PressSwitchMessage =
        "Premere l'interruttore sull'unità che si desidera selezionare."
    override val m8SetToGearShifting = "Imposta sul cambio rapporti"
    override val m8UseX2Y2 = "Usa il 2° rapporto"
    override val m8Model = "Modello"
    override val m8CustomizeSusSwitchPosition = "Imposta su impostazione posizione"
    override val m8SetReverseOperation = "Imposta funzionamento inverso su interruttore Y1"
    override val m8PortA = "Porta A"
    override val m8PortB = "Porta B"
    override val m8NoUseMultiShift = "Non utilizzare il cambio di marcia di secondo livello"
    override val m8SetReverce = "Assegnare la funzione invertita a {0}"
    override val m8ConfirmReadDcasWSwitchStatusText = "Caricare l’impostazione corrente. Premere un pulsante qualsiasi su {0} ({1}).	"
    override val m8ConfirmWriteDcasWSwitchStatusText = "Scrivere le impostazioni. Premere un pulsante qualsiasi su {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    override val m9SynchronizedShiftTitle = "Synchronized shift"
    override val m9TeethPatternSelectorHeaderTitle = "Numero di denti"
    override val m9Cancel = "Annulla"
    override val m9Casette = "Cassetta"
    override val m9Edit = "Modifica"
    override val m9FC = "Pedivella"
    override val m9CS = "Cassetta"
    override val m9NoSetting = "Nessuna impostazione"
    override val m9Table = "Tabella"
    override val m9Animation = "Animazione"
    override val m9DisplayConditionConfirmationMessage =
        "Per impostare la modalità di cambiata, è necessaria una connessione FD/RD compatibile."
    override val m9NotApplicableMessage =
        "Verificare quanto segue prima di applicare le impostazioni."
    override val m9NotApplicableUnselectedMessage = "S1 o S2 non impostato."
    override val m9NotApplicableUnsettableValueMessage =
        "Il punto di cambiata dell'impostazione cambio sincronizzato è nella gamma in cui non può essere impostato."
    override val m9SettingFileUpperLimitMessage =
        "Il limite di archiviazione del file di impostazione è stato superato."
    override val m9FrontUp = "Anteriore sollevato"
    override val m9FrontDown = "Anteriore abbassato"
    override val m9RearUp = "Ingranaggio {0} posteriore sollevato"
    override val m9RearDown = "Ingranaggio {0} posteriore abbassato"
    override val m9Crank = "la pedivella"
    override val m9SynchronizedShift = "SYNCHRONIZED SHIFT"
    override val m9SemiSynchronizedShift = "CAMBIO SYNCHRONIZED SHIFT"
    override val m9Option = "Opzione"
    override val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "Se i valori impostati per la dentatura e la verifica della posizione di cambio sono diversi tra S1 e S2, le impostazioni non possono essere applicate."

    // TODO: TextTableには表記がないので要確認
    override val m9Name = "Nome"

    //region M10 メンテナンス
    override val m10Battery = "Batteria"
    override val m10DerailleurAdjustment = "Regolazione del deragliatore"
    override val m10Front = "Anteriore"
    override val m10Rear = "Posteriore"
    override val m10NotesOnDerailleurAdjustmentMsg =
        "La regolazione del deragliatore richiede la rotazione manuale della pedivella. Creare un ambiente per montare un supporto per la manutenzione della bici. Inoltre fare attenzione affinché la mano non resti intrappolata nel cambio."
    override val m10NotAskAgain = "Non mostrare di nuovo la prossima volta"
    override val m10Step = "Passaggio {0}/{1}"
    override val m10BeforePerformingElectricAdjustmentMsg =
        "Prima di eseguire le regolazioni elettriche usando l'app, è necessario eseguire una regolazione usando il perno di regolazione sul deragliatore. Vedere la guida per i dettagli."
    override val m10MovementOfPositionOfDerailleurMsg =
        "Spostare il deragliatore nella posizione mostrata nella figura."
    override val m10ShiftStartMesssage = "Il cambio è avviato. Ruotare la pedivella."
    override val m10SettingChainAndFrontDerailleurMsg =
        "Regolare il divario tra la catena e il deragliatore su 0 - 0,5 mm."
    override val m10Adjust = "Regolazione : {0}"
    override val m10Gear = "Posizione marcia"
    override val m10ErrorLog = "Registro errori"
    override val m10AdjustTitle = "Regolazione"

    override val m10Restrictions = "Restrizioni"
    override val m10Remedy = "Rimedio"

    override val m10Status = "Stato"
    override val m10DcasWBatteryLowVoltage = "Basso livello della batteria. Sostituire la batteria."
    override val m10DcasWBatteryUpdateInfo =
        "Controllare il livello della batteria. Premere un pulsante qualsiasi su {0} ({1})."
    override val m10LeverLeft = "Leva sinistra"
    override val m10LeverRight = "Leva destra"

    // Error Message
    override val m10ErrorMessageSensorAbnormality =
        "Rilevata anomalia sensore nell'unità di trasmissione."
    override val m10ErrorMessageSensorFailure =
        "Rilevato guasto sensore nell'unità di trasmissione."
    override val m10ErrorMessageMotorFailure = "Rilevato guasto motore nell'unità di trasmissione."
    override val m10ErrorMessageAbnormality = "Rilevata anomalia nell'unità di trasmissione."
    override val m10ErrorMessageNotDetectedSpeedSignal =
        "Nessun segnale di velocità del veicolo rilevato dal sensore di velocità."
    override val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "Rilevata anomalia segnale di velocità del veicolo dal sensore di velocità."
    override val m10ErrorMessageBadCommunicationBTAndDU =
        "Rilevato errore di comunicazione tra la batteria e l'unità di trasmissione."
    override val m10ErrorMessageDifferentShiftingUnit =
        "È montata un'unità di cambio diversa dalla configurazione di sistema."
    override val m10ErrorMessageDUFirmwareAbnormality =
        "Rilevata anomalia nel firmware dell'unità di trasmissione."
    override val m10ErrorMessageVehicleSettingsAbnormality =
        "Rilevata anomalia nelle impostazioni del veicolo."
    override val m10ErrorMessageSystemConfiguration =
        "Errore causato dalla configurazione del sistema."
    override val m10ErrorMessageAbnormalHighTemperature =
        "Rilevata anomalia alta temperatura nell'unità di trasmissione."
    override val m10ErrorMessageNotCompletedSensorInitialization =
        "Non è stato possibile completare normalmente l'inizializzazione del sensore."
    override val m10ErrorMessageUnexpectBTDisconnected =
        "Rilevato scollegamento imprevisto dell'alimentazione."
    override val m10ErrorMessagePowerOffDueToOverTemperature =
        "L'alimentazione è stata disattivata perché la temperatura supera i valori di esercizio garantiti."
    override val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "L'alimentazione è stata disattivata a causa di una dispersione di corrente rilevata nel sistema."

    // Error Restrictions
    override val m10ErrorRestrictionsUnableUseAssistFunction =
        "Impossibile utilizzare la funzione di servoassistenza."
    override val m10ErrorRestrictionsNotStartAllFunction =
        "Nessuna delle funzioni del sistema si avvia."
    override val m10ErrorRestrictionLowerAssistPower =
        "La potenza della servoassistenza si riduce più del solito."
    override val m10ErrorRestrictionLowerMaxAssistSpeed =
        "La velocità massima della servoassistenza si riduce più del solito."
    override val m10ErrorRestrictionNoRestrictedAssistFunction =
        "Non ci sono funzioni di servoassistenza limitate durante la visualizzazione."
    override val m10ErrorRestrictionUnableToShiftGears = "Impossibile cambiare rapporto."

    // Error Remedy
    override val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Rivolgersi presso il proprio punto vendita o il rivenditore per ricevere assistenza."
    override val m10ErrorRemedyTurnPowerOffAndThenOn =
        "Disattivare e riattivare l'alimentazione. Se l'errore persiste, sospendere l'utilizzo e rivolgersi al punto vendita o al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Installare il sensore di velocità e il magnete nelle rispettive posizioni. \n• Ripristinare le impostazioni predefinite qualora siano state apportate modifiche alla bicicletta. \nSeguire le istruzioni riportate sopra e guidare la bicicletta per qualche minuto per eliminare l'errore. Se l'errore persiste, o se le informazioni sopra riportate non sono rilevanti, contattare il distributore per ricevere assistenza."
    override val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n･ Controllare che il cavo tra l'unità di trasmissione e la batteria sia collegato correttamente."
    override val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Connettersi a E-TUBE PROJECT e controllare le impostazioni. Se le impostazioni e lo stato del veicolo sono diversi, rivedere lo stato del veicolo. \nSe l'errore persiste, rivolgersi al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Connettersi a E-TUBE PROJECT e ripristinare il firmware. \nSe l'errore persiste, rivolgersi al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyContactBicycleManufacturer =
        "Contattare il produttore della bicicletta."
    override val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "Non guidare la bicicletta con la modalità servoassistenza attivata fino a quando non è scesa la temperatura dell'unità di trasmissione. \nSe l'errore persiste, rivolgersi al punto vendita o al distributore per ricevere assistenza."
    override val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Installare il sensore di velocità nella rispettiva posizione. \n• Installare il magnete nella rispettiva posizione. (Per istruzioni su come montare il magnete rimosso, consultare la sezione \"Freno a disco\" in \"Operazioni generali\" o la sequenza di passi riportata nel manuale del fornitore). \nSe l'errore persiste, o se le informazioni sopra riportate non sono rilevanti, contattare il distributore per ricevere assistenza."
    override val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Premere il pulsante di accensione batteria per disattivare l'alimentazione, quindi riattivarla senza poggiare i piedi sui pedali.\nSe l'errore persiste, rivolgersi al punto vendita o al distributore per ricevere assistenza."
    override val m10ErrorRemedyTorqueSensorOffset =
        "• Se l'unità di trasmissione è il modello DU-EP800 (il ciclocomputer visualizza W103): ruotare le pedivelle in senso inverso due o tre volte. \n• Se l'unità di trasmissione è un modello diverso (il ciclocomputer visualizza W013): premere il pulsante di accensione batteria per disattivare l'alimentazione, quindi riattivarla senza poggiare i piedi sui pedali. \nSe l'errore persiste dopo aver eseguito le operazioni precedenti, rivolgersi al punto vendita o al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyBTConnecting =
        "Disattivare e riattivare l'alimentazione. \nSe W105 viene visualizzato spesso, chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Eliminare il tintinnio dal supporto della batteria e assicurarsi che la batteria sia correttamente fissata in posizione. \n• Ispezionare il cavo di alimentazione per rilevare eventuali danni. Sostituire il cavo, se danneggiato. \nSe l'errore persiste, rivolgersi al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyOverTemperature =
        "Se la temperatura è superiore alla temperatura a cui è possibile la scarica, lasciare la batteria in un luogo fresco e non esposta alla luce solare diretta fino a quando la temperatura interna della batteria si sarà ridotta a sufficienza. Se la temperatura è inferiore alla temperatura a cui è possibile la scarica, lasciare la batteria al chiuso fino a quando raggiunge una temperatura adeguata. \nSe l'errore persiste, rivolgersi al punto vendita o al distributore per ricevere assistenza."
    override val m10ErrorRemedyReplaceShiftingUnit =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \n• Verificare lo stato attuale del sistema su E-TUBE PROJECT e sostituire con l'unità del cambio appropriata. \nSe l'errore persiste, rivolgersi al rivenditore per ricevere assistenza."
    override val m10ErrorRemedyCurrentLeakage =
        "Chiedere al punto vendita di effettuare le operazioni indicate di seguito. \nRimuovere i componenti diversi dall'unità di trasmissione uno per volta, quindi riattivare l'alimentazione. \n• Se il componente che causa il problema non è stato rimosso, l'alimentazione si disattiva durante l'avviamento. \n• Se il componente con un problema è stato rimosso, l'alimentazione si attiva e resta visualizzato W104. \n• Una volta identificato il componente che causa il problema, sostituire quel componente."
    override val m10ErrorTorqueSensorOffsetAdjust =
        "Premere il pulsante di accensione batteria per disattivare l'alimentazione, quindi riattivarla senza poggiare i piedi sui pedali. Se l'avvertenza persiste dopo il primo tentativo, ripetere la stessa operazione fino a quando non scompare. \n*Il sensore si attiva anche quando la bicicletta è in movimento. \n\nSe si continua a usare la bici la situazione potrebbe migliorare. Se la situazione non migliora, richiedere quanto segue presso il punto vendita o un rivenditore. \n• Regolare la tensione catena. \n• Con la ruota posteriore sollevata da terra, girare i pedali per far girare liberamente la ruota posteriore. Cambiare la posizione di arresto dei pedali mentre la ruota sta girando fino a quando l'avvertenza scompare."

    // TODO: イタリア語訳の確認
    override val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    override val m11Name = "Nome"
    override val m11Change = "Modifica"
    override val m11Power = "POTENZA"
    override val m11Cadence = "CADENZA"
    override val m11Watt = "watt"
    override val m11Rpm = "rpm"
    override val m11Waiting = "Attendere..."
    override val m11CaribrationIsCompleted = "Calibrazione completata."
    override val m11CaribrationIsFailed = "Calibrazione non riuscita."
    override val m11N = "N"
    override val m11PowerMeter = "Misuratore di potenza"
    override val m11ZeroOffsetCaribration = "Calibrazione con correzione zero"
    override val m11ChangeBike = "Cambia bici"
    override val m11CancelPairing = "Annulla abbinamento"
    override val m11Set = "Imposta"
    override val m11RegisteredBike = "Bicicletta registrata"
    override val m11RedLighting = "Livello batteria misuratore di potenza: \n1 - 5%"
    override val m11RedBlinking = "Livello batteria misuratore di potenza: \nInferiore a 1％"
    //endregion

    //region M12 アプリケーション設定
    override val m12Acconut = "Account"
    override val m12ShimanoIDPortal = "PORTALE ID SHIMANO"
    override val m12Setting = "Impostazione"
    override val m12ApplicationSettings = "Impostazioni applicazione"
    override val m12AutoBikeConnection = "Collegamento automatico bici"
    override val m12Language = "Lingua"
    override val m12Link = "Collegamento"
    override val m12ETubeProjectWebsite = "Sito Web E-TUBE PROJECT"
    override val m12ETubeRide = "E-TUBE RIDE"
    override val m12Other = "Altro"
    override val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    override val m12PoweredByShimano = "Powered by SHIMANO"
    override val m12SignUpLogIn = "Registrati/Accedi"
    override val m12CorporateClient = "Client aziendale"
    override val m12English = "Inglese"
    override val m12French = "Francese"
    override val m12German = "Tedesco"
    override val m12Dutch = "Olandese"
    override val m12Spanish = "Spagnolo"
    override val m12Italian = "Italiano"
    override val m12Chinese = "Cinese"
    override val m12Japanese = "Giapponese"
    override val m12Change = "Modifica"
    override val m12AppOperationLogAgreement = "Accordo registro operazioni app"
    override val m12AgreeAppOperationLogAgreemenMsg = "Accetto l'Accordo registro operazioni app."
    override val m12DataProtecitonNotice = "Avviso protezione dati"
    override val m12Apache = "Apache"

    // TODO: 仮
    override val m12AppOperationLog = "アプリ操作ログ取得"

    override val m12ConfirmLogout =
        "Disconnettersi? Inoltre, eventuali unità attualmente connesse verranno disconnesse."
    //endregion

    //region M13 ヘルプ
    override val m13Connection = "Collegamento"
    override val m13Title = "Guida"
    override val m13Operation = "Operazione"
    override val m13HowToConnectBike = "Come collegare la bici"
    override val m13SectionCustomize = "Personalizza"
    override val m13SynchronizedShiftTitle = "Synchronized shift"
    override val m13Maintenance = "Manutenzione"
    override val m13FrontDerailleurAdjustment = "Regolazione del deragliatore"
    override val m13RearDerailleurAdjustment = "Regolazione del cambio"
    override val m13ZeroOffsetCalibration = "Calibrazione con correzione zero"
    override val m13SystemInformationDisplayTitle = "Per display informazioni"
    override val m13JunctionATitle = "Per giunzione A"
    override val m13SCE8000Title = "Per SC-E8000"
    override val m13CycleComputerTitle =
        "Per ciclocomputer/EW-EN100\n(ad eccezione di SC-E8000)"
    override val m13PowerMeterTitle = "Per FC-R9100-P"
    override val m13SystemInformationDisplayDescription =
        "Tenere premuto il selettore modalità sulla bicicletta fino a quando viene visualizzato \"C\" sul display."
    override val m13ModeSwitchAnnotationRetention =
        "* Quando la bicicletta è pronta per il collegamento, rilasciare il selettore o il pulsante modalità. Se si continua a tenere premuto il selettore o il pulsante modalità, il sistema entra in una modalità diversa."
    override val m13ShiftMode = "Modalità cambio"
    override val m13LongPress = "Tenere premuto"
    override val m13Seconds = "{0} sec. o più"
    override val m13ConnectionMode = "Modalità collegamento Bluetooth LE"
    override val m13AdjustMode = "Modalità regolazione"
    override val m13RDProtectionResetMode = "Modalità azzeramento protezione RD"
    override val m13ExitModeDescription =
        "Tenere premuto per 0,5 sec. o più per uscire dalla modalità regolazione o azzeramento protezione RD."
    override val m13JunctionADescription =
        "Tenere premuto il pulsante giunzione A fino a quando il LED verde e il LED rosso iniziano ad accendersi alternativamente."
    override val m13HowToConnectWirelessUnit =
        "Quando si tiene premuto A mentre la bici è ferma, la schermata Elenco menu viene visualizzata sul ciclocomputer. Premere X1 o Y1 per selezionare Bluetooth LE, quindi premere A per confermare. \nSulla schermata LE Bluetooth, selezionare Start quindi premere A per confermare."
    override val m13CommunicationConditions =
        "Le comunicazioni sono consentite solo alle condizioni di seguito. \nEffettuare una delle operazioni."
    override val m13TurningOnStepsMainPower = "・ Per 15 secondi dopo l'accensione di SHIMANO STEPS."
    override val m13AnyButtonOperation =
        "・ Per 15 secondi dopo l'azionamento di uno dei pulsanti (ad eccezione del pulsante di accensione di SHIMANO STEPS)."
    override val m13PowerMeterDescription = "Premere il pulsante sulla centralina."
    override val m13SynchronizedShift = "Synchronized Shift"
    override val m13SynchronizedShiftDetail =
        "Questa funzione cambia automaticamente il rapporto del deragliatore in sincronizzazione con il cambio superiore e il cambio inferiore. "
    override val m13SemiSynchronizedShift = "Semi-Synchronized Shift"
    override val m13SemiSynchronizedShiftDetail =
        "Questa funzione consente di cambiare automaticamente il rapporto del cambio quando si cambia il rapporto del deragliatore allo scopo di ottenere una transizione ottimale da un rapporto all'altro."
    override val m13Characteristics = "Caratteristiche"
    override val m13CharacteristicsFeature1 =
        "La modalità multirapporto veloce è ora abilitata.\n\n•Questa funzione consente di regolare rapidamente il valore RPM della pedivella in risposta alle condizioni di guida che variano.\n•Consente di regolare rapidamente la velocità."
    override val m13CharacteristicsFeature2 = "Abilita funzionamento multirapporto affidabile"
    override val m13Note = "Nota"
    override val m13OperatingPrecautionsUse1 =
        "1. È facile effettuare l'overshift.\n\n2. Se il valore RPM della pedivella è basso, la catena non potrà seguire il movimento del deragliatore posteriore.\nDi conseguenza, la catena potrebbe staccarsi dai denti della corona anziché innestarsi sulla cassetta pignoni."
    override val m13OperatingPrecautionsUse2 =
        "Il funzionamento della modalità multirapporto richiede del tempo"
    override val m13Clank = "Velocità pedivella da utilizzare"
    override val m13ClankFeature = "Con RPM pedivella alto"
    override val m13AutoShiftOverview =
        "SHIMANO STEPS con un mozzo con sistema di cambio interno DI2 può cambiare automaticamente l'unità di cambio a seconda delle condizioni del percorso."
    override val m13ShiftTiming = "Momento di cambiata: "
    override val m13ShiftTimingOverview =
        "Modifica lo standard di cadenza per il cambio rapporti automatico. Aumenta il valore impostato per pedalata veloce con un carico leggero. Riduce il valore impostato per pedalata lenta con un carico moderato. "
    override val m13StartMode = "Modalità di partenza: "
    override val m13StartModeOverview =
        "Questa funzione abbassa automaticamente il rapporto dopo la sosta davanti a un semaforo, ecc., permettendo di partire da un rapporto preimpostato più basso  Questa funzione può essere usata per il cambio rapporti automatico o manuale."
    override val m13AssistOverview = "È possibile scegliere tra 2 modelli di servoassistenza."
    override val m13Comfort = "COMFORT :"
    override val m13ComfortOverview =
        "consente una guida più morbida e una sensazione più simile a una bici normale con la coppia di serraggio massima di 50 Nm."
    override val m13Sportive = "SPORTIVE :"
    override val m13SportiveOverview =
        "fornisce una servoassistenza che consente di affrontare con facilità salite ripide con la coppia di serraggio massima di 60 Nm. (A seconda del modello dell'unità di cambio interna, la coppia di serraggio massima può essere controllata a 50 Nm)."
    override val m13RidingCharacteristicOverview =
        "È possibile scegliere tra 3 caratteristiche di guida. "
    override val m13Dynamic = "(1) DYNAMIC :"
    override val m13DynamicOverview =
        "Un interruttore consente di selezionare 3 livelli di modalità di servoassistenza (ECO/TRAIL/BOOST). DYNAMIC è un'impostazione in cui la differenza è maggiore tra questi 3 livelli di modalità di servoassistenza. Offre supporto quando si guida su una E-MTB con \"ECO\" che offre maggiore servoassistenza rispetto alla modalità ECO nell'impostazione EXPLORER, \"TRIAL\" per un controllo superiore e \"BOOST\" per un'accelerazione potente."
    override val m13Explorer = "(2) EXPLORER :"
    override val m13ExplorerOverview =
        "EXPLORER offre sia il controllo della servoassistenza che la riduzione del consumo per i 3 livelli di modalità di servoassistenza. È adatta per la guida su single track."
    override val m13Customize = "(3) CUSTOMIZE :"
    override val m13CustomizeOverview =
        "Il livello di servoassistenza desiderato può essere selezionato tra LOW/MEDIUM/HIGH per ognuno dei 3 livelli di modalità di servoassistenza."
    override val m13AssistProfileOverview =
        "È possibile creare 2 tipi di profili di servoassistenza tra cui scegliere. È possibile spostarsi da un profilo all'altro anche con SC. Un profilo regola 3 parametri per ognuno dei 3 livelli di modalità di servoassistenza (ECO/TRAIL/BOOST) che possono essere modificati con un interruttore."
    override val m13AssistCharacter = "(1) Carattere servoassistenza :"
    override val m13AssistCharacterOverview =
        "Con SHIMANO STEPS, la coppia di serraggio della servoassistenza viene applicata a seconda della pressione esercitata sul pedale. Quando l'impostazione viene spostata su POWERFUL, l'assistenza viene fornita anche con una pressione esercitata sul pedale ridotta. Quando l'impostazione viene spostata su ECO, è possibile ottimizzare l'equilibrio tra il livello di assistenza e il consumo batteria ridotto."
    override val m13MaxTorque = "(2) Coppia di serraggio max :"
    override val m13MaxTorqueOverview =
        "L'uscita coppia di serraggio massima dell'unità di trasmissione può essere modificata."
    override val m13AssistStart = "(3) Avvio servoassistenza :"
    override val m13AssistStartOverview =
        "Il momento in cui viene fornita la servoassistenza può essere modificato. Quando l'impostazione è su QUICK, l'assistenza viene fornita subito dopo che la pedivella inizia a ruotare. Quando l'impostazione è su MILD, l'assistenza viene fornita lentamente."
    override val m13DisplaySpeedAdjustment = "Regolazione velocità display"
    override val m13DisplaySpeedAdjustmentOverview =
        "Se è presente una differenza tra la velocità visualizzata sul ciclocomputer e la velocità visualizzata sul proprio dispositivo, regolare la velocità visualizzata.\nSe si aumenta il valore premendo Assist-X, il valore della velocità visualizzata aumenta.\nSe si riduce il valore premendo Assist-Y, il valore della velocità visualizzata diminuisce."
    override val m13SettingIsNotAvailableMessage =
        "* Questa impostazione non è disponibile per alcuni modelli di unità di trasmissione."
    override val m13TopSideOfTheAdjustmentMethod = "Esecuzione di una regolazione superiore"
    override val m13SmallestSprocket = "Pignone più piccolo"
    override val m13LargestGear = "Ingranaggio più grande"
    override val m13Chain = "Catena"
    override val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nRuotare il perno di regolazione del lato superiore con una chiave esagonale di 2 mm. Lo spazio tra la catena e la piastrina esterna del guidacatena, e quindi regolarlo a 0, 5 - 1 mm."
    override val m13TopSideOfAdjustmentMethod =
        "Prima di eseguire le regolazioni elettriche usando l'app, è necessario eseguire una regolazione usando il perno di regolazione sul deragliatore. Seguire i passaggi qui sotto per eseguire la regolazione."
    override val m13BoltLocationCheck = "Controllo posizione del perno"
    override val m13BoltLocationCheckDescription =
        "Il perno di regolazione inferiore, il perno di regolazione superiore e il perno di supporto sono posizionati uno vicino all'altro.\nAssicurarsi di regolare il perno corretto."
    override val m13Bolts =
        "(A) Perno di regolazione inferiore\n(B) Perno di regolazione superiore\n(C) Perno di supporto"
    override val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nPosizionare la catena sulla corona più grande e sul pignone più piccolo."
    override val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nAllentare il bullone di fissaggio dell’escursione con una chiave Allen da 2 mm.\n (A) Perno di regolazione dell’escursione\n (B) Perno di regolazione superiore"
    override val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nRuotare il perno di regolazione superiore con una chiave Allen da 2 mm per regolare la distanza. Regolare il divario tra la catena e la piastra interna del guidacatena tra 0 e 0, 5 mm."
    override val m13AfterAdujustmentMessge =
        "4.\nDopo la regolazione, serrare a fondo il perno di regolazione dell’escursione."
    override val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nSpostare la catena sul 5° pignone. Spostare la puleggia guida verso l'interno finché la catena tocca il 4° pignone producendo un leggero rumore."
    override val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nSpostare la puleggia di guida verso l'esterno di 4 passaggi (5 passaggi per MTB) fino alla posizione di destinazione."
    override val m13PerformingZeroOffsetTitle = "Esecuzione della calibrazione con correzione zero"
    override val m13PerformingZeroOffsetMsg =
        "1.\nCollocare la bicicletta su una superficie in piano.\n\n2.\nPosizionare la pedivella in modo che sia perpendicolare al suolo come mostrato nell'illustrazione.\n\n3.\nPremere il pulsante \"Calibrazione con correzione zero\".\n\nNon poggiare i piedi sui pedali, né esercitare pressione sulla pedivella."
    override val m13ShiftingAdvice = "Cambiata suggerita:"
    override val m13ShiftingAdviceOverview =
        "Questa funzione segnala il momento di cambiata appropriato tramite il ciclocomputer nella cambiata manuale. Il momento in cui la notifica viene visualizzata varia a seconda del valore impostato per il momento di cambiata."
    override val m13PerformingLowAdjustment =
        "Come eseguire una regolazione bassa (solo FD-6870/FD-9070)"
    override val m13PerformingLowAdjustmentDescription1 =
        "1.\nPosizionare la catena sulla corona più piccola e sul pignone più grande."
    override val m13PerformingLowAdjustmentDescription2 =
        "2.\nRuotare il bullone di regolazione inferiore con una chiave esagonale di 2 mm. Regolare lo spazio tra la catena e la piastrina esterna del guidacatena da 0 mm a 0,5 mm."
    override val m13FrontDerailleurMethod2 =
        "2.\nAllentare il bullone di fissaggio dell’escursione con una brugola di 2 mm.\n(A) Perno di regolazione dell’escursione\n(B) Perno di regolazione superiore"
    override val m13FrontDerailleurMethod3 =
        "3.\nRuotare il perno di regolazione superiore con una brugola da 2 mm per regolare la distanza. Regolare il gioco tra la catena e la piastrina interna del guidacatena da 0 mm a 0,5 mm."
    override val m13VerySlow = "estremamente\nlento"
    override val m13Slow = "lento"
    override val m13Normal = "normale"
    override val m13Fast = "veloce"
    override val m13VeryFast = "estremamente\nveloce"
    override val m13MtbTopSideOfAdjustmentMethod1 =
        "1.\nPosizionare la catena sulla corona più grande e sul pignone più grande."
    override val m13AdjustmentMethod = "調整方法"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    override val dialog17SwipeToSwitchTitle = "Scorri per passare"
    override val dialog28BluetoothOffTitle = "Bluetooth disattivato"
    override val dialog29LocationInformationOffTitle = "Informazioni sulla posizione disattivate"
    override val dialog2_3AddBikeTitle = "Aggiungi bici"
    override val dialog25PasskeyErrorTitle = "Errore passkey"
    override val dialog27RegistersBikeTitle = "Unità registrata"
    override val dialog2_8RegisterBikeNameTitle = "Registra nome bici"
    override val dialog2_9PasskeyTitle = "Passkey"
    override val dialog2_11ConfirmBikeTitle = "Controlla bici"
    override val dialog69SwitchBikeConnectionTitle = "Scollega bici attuale"
    override val dialog3_4UnitNotDetectdTitle = "Unità non rilevata"
    override val dialog3_5UnitRegisteredAgainTitle = "Registra di nuovo l'unità"
    override val dialog4_1UpdateDetailTitle = "{0} ver.{1}"
    override val dialog4_2AllUpdateConfirmTitle = "Vuoi\naggiornare tutte le unità?"
    override val dialog4_4CancelUpdateTitle = "Annulla aggiornamento"
    override val dialog6_1DeleteBikeTitle = "Elimina bici"
    override val dialog6_2ConfirmTitle = "Conferma"
    override val dialog6_3DeleteTitle = "Elimina"
    override val dialog7_1ConfirmCancelSettingTitle = "Interrompi configurazione"
    override val dialog7_2ConfirmDefaultTitle = "Ripristina le impostazioni predefinite"
    override val dialog10_2PauseSetupTitle = "Interrompi regolazione"
    override val dialog_jira258PushedIntoThe2ndGearTitle =
        "Operazione eseguita quando l'interruttore è premuto nel 2° rapporto"
    //endregion

    //reigon 文章
    override val dialog01ConfirmExecuteZeroOffsetMessage = "Eseguire spostamento zero?"
    override val dialog02ConnectedMessage = "Collegato."
    override val dialog02CaribrationIsCompleted = "Calibrazione completata."
    override val dialog02CaribrationIsFailed = "Calibrazione non riuscita."
    override val dialog03FailedToExecuteZeroOffsetMessage =
        "Esecuzione spostamento zero non riuscita."
    override val dialog03DoesNotrespondProperlyMessage =
        "Il misuratore di potenza non risponde correttamente."
    override val dialog04UpdatingFirmwareMessage = "Aggiornamento del firmware in corso..."
    override val dialog05CompleteSetupMessage = "Impostazione completata."
    override val dialog06ConfirmStartUpdateMessage =
        "Prima di avviare l'aggiornamento, accertarsi di leggere l’”AVVISO” riportato di seguito. Non eseguire altre operazioni durante l'aggiornamento."
    override val dialog06AllUpdateConfirmMessage = "Tempo richiesto approssimativo: {0}"
    override val dialog07CancelUpdateMessage =
        "Vuoi annullare l'aggiornamento del firmware?\n* Se scegli \"Annulla\", gli aggiornamenti successivi dell'unità saranno annullati. L'aggiornamento dell'unità attualmente in corso continua."
    override val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Riesaminare le seguenti modifiche a {0} che verranno applicate dopo l'operazione di ripristino del firmware."
    override val dialog13Changes = "Modifiche"
    override val dialog13MustAgreeToTermsOfUseMessage =
        "È necessario accettare i termini d'uso del software per ripristinare il firmware."
    override val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Contratto di licenza software supplementare\n (Leggere scrupolosamente)"
    override val dialog15FirmwareWasUpdateMessage =
        "Il firmware di {0} funziona normalmente.\nIl firmware è stato aggiornato alla versione più recente."
    override val dialog15FirmwareRestoreMessage = "Il firmware di {0} è stato ripristinato."
    override val dialog15CanChangeTheSwitchSettingMessage =
        "Quando si usa {0}, è possibile modificare l'impostazione dello stato delle sospensioni mostrato in SC dal Impostazione interruttore nel menu Personalizza."
    override val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "Il firmware di {0} funziona normalmente.\nNon necessita di essere ripristinato."
    override val dialog15FirmwareRestorationFailureMessage =
        "Ripristino firmware di {0} non riuscito."
    override val dialog15MayBeFaultyMessage = "{0} potrebbe essere guasto."
    override val dialog15FirmwareRecoveryAgainMessage =
        "Il ripristino del firmware per {0} viene rieseguito."
    override val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "Il file del firmware {0} non è valido.\nScaricare la versione più recente del firmware?"
    override val dialog15FirstUpdateFirmwareMessage =
        "Innanzitutto, aggiornare il firmware per {0}."
    override val diaog15CannotUpdateMessage =
        "Impossibile aggiornare a causa della scarsa ricezione del segnale. Riprovare in un luogo con una ricezione del segnale migliore."
    override val dialog15ConfirmStartUpdateMessage =
        "L'aggiornamento del firmware richiede alcuni minuti. \nSe la carica della batteria del dispositivo è bassa, eseguire l'aggiornamento dopo averla ricaricata oppure collegarla a un caricabatteria. \nAvviare l'aggiornamento?"
    override val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Impossibile connettersi dopo l'aggiornamento del firmware.\nEffettuare nuovamente il collegamento."
    override val dialog15ConfirmConnectedUnitMessage = "{0} riconosciuto.\nL'unità collegata è {0}?"
    override val dialog17RegistrationIsCompletedMessage = "Registrazione completata."
    override val dialog17ConnectionIsCompletedMessage = "Collegamento completato."
    override val dialog19TryAgainWirelessCommunicationMessage =
        "Ambiente wireless instabile. Riprova in un luogo con una comunicazione stabile."
    override val dialog20BetaVersionMsg =
        "Questo software è una versione beta.\n La valutazione non è ancora completata, ed è possibile che possano verificarsi vari problemi.\n Dopo aver accettato queste cose, userai questo software?"
    override val dialog21BetaVersionMsg =
        "Questo software è una versione beta.\n La valutazione non è ancora completata, ed è possibile che possano verificarsi vari problemi.\n Dopo aver accettato queste cose, userai questo software?\n \n Data di scadenza del software : {0}"
    override val dialog22BetaVersionMsg =
        "Questo software è una versione beta.\n Questo software ha superato la data di scadenza.\n Reinstallare il software originale."
    override val dialog24PasskeyMessage = "Inserire la passkey."
    override val dialog25PasskeyErrorMessage =
        "Impossibile autenticare. PassKey inserita in modo errato, oppure la PassKey per l’unità wireless è stata modificata. Riconnettersi e provare a reimmettere la PassKey impostata nell’unità wireless."
    override val dialog26ChangePasskeyMessage = "Vuoi modificare la passkey?"
    override val dialog27RegisterdBikeMessage =
        "{0} è già registrato come dispositivo wireless per {1}. Vuoi usarlo come dispositivo wireless per la nuova bici?* La registrazione di {1} verrà annullata."
    override val dialog28BluetoothOffMesssage =
        "Attiva l'impostazione Bluetooth sul dispositivo da collegare."
    override val dialog29LocationInformationOffMessage =
        "Abilita il collegamento del servizio informazioni sulla posizione."
    override val dialog30DeleteBikeMessage =
        "Quando una bici viene eliminata, anche i suoi dati vengono eliminati."
    override val dialog31ConfirmDeleteBikeMessage = "Eliminare la bici?"
    override val dialog32UpdatedProperlyMessage =
        "Le impostazioni sono state aggiornate correttamente."
    override val dialog32UpdateCompletedMessage = "Aggiornamento completato."
    override val dialog36PressSwitchMessage =
        "Premere l'interruttore sull'unità che si desidera selezionare."
    override val dialog37SelectTheDerectionMessage =
        "Su quale manopola è installato lʼinterruttore di controllo?"
    override val dialog38SameMarksCannotBeAssignedMessage =
        "- Se il modello di combinazione è diverso tra le sospensioni anteriore e posteriore, non è possibile assegnare la stessa indicazione (CTD)."
    override val dialog38DifferentMarksCannotBeAssignedMessage =
        "- Se viene utilizzato lo stesso modello di combinazione per le sospensioni anteriore e quella posteriore, non è possibile assegnare indicazioni diverse (CTD)."
    override val dialog39CheckTheFollowingInformationMessage =
        "Verifica il messaggio riportato di seguito."
    override val dialog39ConfirmContinueWithProgrammingMessage =
        "OK per continuare con la programmazione?"
    override val dialog39SameSettingMessage =
        "- Le medesime impostazioni sono state selezionate per due o più posizioni."
    override val dialog39OnlyHasBeenSetMessage = "- È stato impostato solo {1} per {0}."
    override val dialog40CannotSelectSwitchMessage =
        "Se non è possibile effettuare la selezione con gli interruttori, controllare se alcuni cavi elettrici sono scollegati. \nSe si individuano cavi elettrici scollegati, ricollegarli. \nSe non vi sono cavi elettrici scollegati, un interruttore potrebbe non funzionare correttamente."
    override val dialog41ConfirmDefaultMessage =
        "Ripristinare tutte le impostazioni alle impostazioni predefinite?"
    override val dialog42ConfirmDeleteSettingMessage = "Eliminare le impostazioni?"
    override val dialog43ComfirmUnpairPowerMeterMessage = "Scollegare il misuratore di potenza?"
    override val dialog44ConfirmCancelSettingMessage =
        "Ignorare le informazioni inserite?"
    override val dialog45DisplayConditionConfirmationMessage =
        "Impossibile impostare il cambio sincronizzato. Consultare il sito Web E-TUBE PROJECT"
    override val dialog46DisplayConditionConfirmationMessage =
        "Impossibile impostare la modalità multirapporto. Consultare il sito Web E-TUBE PROJECT"
    override val dialog47DuplicateSwitchesMessage =
        "Sono collegati più interruttori identici. Impossibile salvare le impostazioni."
    override val dialog48FailedToUpdateSettingsMessage = "Impossibile aggiornare le impostazioni."
    override val dialog48FailedToConnectBicycleMessage =
        "Connessione alla bicicletta non riuscita."
    override val dialog49And50CannotOperateOormallyMessage =
        "Il punto di cambio rientra nell'intervallo non impostabile e non può funzionare normalmente."
    override val dialog51ConfirmContinueSettingMessage =
        "Le seguenti funzioni non sono incluse. Continuare?\n{0}"
    override val dialog55NoInformationMessage = "Nessuna informazione trovata su questa bici."
    override val dialog56UnpairConfirmationDialogMessage =
        "Eliminare il misuratore di potenza?"
    override val dialog57DisconnectBluetoothDialogMessage =
        "Il misuratore di potenza è stato eliminato. L'unità wireless deve essere eliminata anche nell'impostazione Bluetooth per il sistema operativo."
    override val dialog58InsufficientStorageAvailableMessage =
        "Spazio di archiviazione insufficiente. Impossibile salvare il file delle preimpostazioni."
    override val dialog59ConfirmLogoutMessage = "Disconnettersi?"
    override val dialog59LoggedOutMessage = "Chiusura della sessione."
    override val dialog60BeforeApplyingSettingMessage =
        "Verificare quanto segue prima di applicare le impostazioni."
    override val dialog60S1OrS2IsNotSetMessage = "S1 o S2 non impostato."
    override val dialog60AlreadyAppliedSettingMessage = "Questa impostazione è già stata applicata."
    override val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "Se i valori impostati per la dentatura e la verifica della posizione di cambio sono diversi tra S1 e S2, le impostazioni non possono essere applicate."
    override val dialog60NotApplicableDifferentSettingsMessage =
        "Se i valori impostati per l'intervallo di cambiata sono diversi tra S1 e S2, le impostazioni non possono essere applicate."
    override val dialog60ShiftPointCannotAppliedMessage =
        "Il punto di cambiata dell'impostazione cambio sincronizzato è nella gamma in cui non può essere impostato."
    override val dialog61CreateNewSettingMessage =
        "Per creare una nuova impostazione, eliminare una delle impostazioni esistenti."
    override val dialog62AdjustDerailleurMessage =
        "Per regolare il deragliatore, la pedivella deve essere ruotata manualmente. Prepararsi posizionando la bici sul supporto per la manutenzione o su un altro dispositivo. Inoltre, fare attenzione affinché la mano non resti intrappolata negli ingranaggi."
    override val dialog63ConfirmCancellAdjustmentMessage = "Uscire?"
    override val dialog64RecoveryFirmwareMessage =
        "Il firmware su {0} potrebbe non funzionare correttamente. Ripristino del firmware eseguito. Tempo richiesto approssimativo: {1}"
    override val dialog64ConfirmRestoreTheFirmwareMessage =
        "È stata trovata un’unità wireless guasta.\n Ripristinare il firmware?"
    override val dialog64UpdateFirmwareMessage =
        "L'unità wireless potrebbe essere guasta.\nIl firmware verrà aggiornato."
    override val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "L'applicazione o il firmware possono essere obsoleti. Collegarsi a Internet e verificare la presenza della versione più recente."
    override val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "Collegare l'unità collegata in precedenza?"
    override val dialog67BluetoothOffMessage =
        "Bluetooth sul dispositivo non abilitato. Attiva l'impostazione Bluetooth sul dispositivo da collegare."
    override val dialog68LocationInformationOffMessage =
        "Informazioni sulla posizione del dispositivo non disponibili. Abilita il collegamento del servizio informazioni sulla posizione."
    override val dialog69SwitchBikeConnectionMessage = "Scollegare la bici attualmente collegata?"
    override val dialog71LanguageChangeCompleteMessage =
        "L'impostazione della lingua è stata modificata. \nLa lingua cambierà dopo aver chiuso e riavviato l'applicazione."
    override val dialog72ConfirmBikeMessage =
        "Sono utilizzate {0} unità. Registrare come una bici esistente? Registrare come una nuova bici?"
    override val dialog73CannnotConnectToNetworkMessage = "Impossibile collegarsi alla rete."
    override val dialog74AccountIsLockedMessage =
        "Questo account è bloccato. Riprovare a eseguire l’accesso più tardi."
    override val dialog75IncorrectIdOrPasswordMessage = "La password o l'ID utente non è corretto."
    override val dialog76PasswordHasExpiredMessage =
        "La password provvisoria è scaduta.\nRiavviare il processo di registrazione."
    override val dialog77UnexpectedErrorMessage = "Si è verificato un errore imprevisto."
    override val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} per cambio in modalità assistita collegato. \nIl tipo di bici corrente supporta solo l'impostazione per il cambio rapporti. \nImpostare tutte le unità {0} sul cambio rapporti?"
    override val dialog78ShiftModeSwitchingIsConnectedMessage =
        "{0} per il cambio marcia è connessa.\nIl tipo di bicicletta corrente supporta soltanto l’impostazione per la selezione modalità per servoassistenza.\nImpostare {0} per la selezione modalità per servoassistenza?"
    override val dialog79WirelessConnectionIsPoorMessage =
        "Il collegamento wireless è scadente. \nIl collegamento Bluetooth® LE potrebbe essere interrotto."
    override val dialog79ConfirmAjustedChainTensionMessage =
        "È stata eseguita una regolazione della tensione catena?"
    override val dialog79ConfirmAjustedChainTensionDetailMessage =
        "Quando si utilizza un mozzo con sistema di cambio interno, è necessario regolare la tensione catena.\nRegolare la tensione della catena, quindi premere il pulsante Esegui."
    override val dialog79ConfirmCrankAngleMessage =
        "È stato eseguito un controllo dellʼangolo della pedivella?"
    override val dialog79ConfirmCrankAngleDetailMessage =
        "La pedivella sinistra deve essere installata nell’asse con l’angolazione appropriata. Verificare l’angolazione della pedivella installata, quindi premere il pulsante Esegui."
    override val dialog79RecomendUpdateOSVersionMessage =
        "L'unità indicata di seguito non supporta il sistema operativo dello smartphone o del tablet.\n{0}\nSi consiglia di aggiornare il sistema operativo dello smartphone o tablet alla versione più recente per poterlo usare con l'applicazione."
    override val dialog79CannotUseUnitMessage = "Impossibile usare l'unità seguente."
    override val dialog79RemoveUnitMessage = "Rimuovere l'unità."
    override val dialog79UpdateApplicationMessage =
        "Aggiornare l'applicazione alla versione più recente e riprovare."
    override val dialog79ConnectToTheInternetMessage =
        "Impossibile verificare un collegamento al server. \nCollegarsi a Internet e riprovare."
    override val dialog79NewFirmwareVersionWasFoundMessage =
        "È stata trovata una nuova versione firmware. \nDownload della nuova versione…"
    override val dialog79DownloadedFileIsCorrupMessage =
        "Il file scaricato è corrotto.\nScaricare nuovamente il file.\nSe il download fallisce ripetutamente, ci potrebbe essere un problema con la connessione Internet o con il web server di Shimano.\nRiprovare più tardi."
    override val dialog79FileDownloadFailedMessage = "Impossibile scaricare il file."
    override val dialog79ConfirmConnectInternetMessage =
        "Il file sarà aggiornato. \nIl dispositivo è collegato a Internet?"
    override val dialog79UpdateFailedMessage = "Aggiornamento non riuscito."
    override val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nTogliere la mano dall'interruttore.\nSe la finestra di dialogo non si chiude dopo averlo rilasciato, potrebbe essersi verificato un errore nell'interruttore. In tal caso, collegare solo l'interruttore ed effettuare un controllo degli errori."
    override val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Collegarsi a Internet e controllare se sono disponibili versioni di E-TUBE PROJECT o di prodotto aggiornate. \nL'aggiornamento alla versione più recente consentirà di usare nuovi prodotti e funzioni."
    override val dialog82FailedToRegisterTheImageMessage = "Impossibile registrare l'immagine."
    override val dialog83NotConnectedToTheInternetMessage =
        "Nessun collegamento a Internet. Collegarsi a Internet e riprovare."
    override val dialog84RegisterTheBicycleAgainMessage =
        "Nessuna bicicletta pertinente trovata. Collegarsi a Internet e registrare nuovamente la bicicletta."
    override val dialog85RegisterTheImageAgainMessage =
        "Si è verificato un errore imprevisto. Registrare nuovamente l'immagine."
    override val dialog87ProgrammingErrorMessage =
        "Si è verificato un errore durante l'elaborazione dell'impostazione."
    override val dialog88MoveNextStepMessage =
        "Il deragliatore è già nella posizione cambio specificata. Procedere al passo successivo."
    override val dialog89ConfirmWhenGoingToETubeRide =
        "Avvio di E-TUBE RIDE. Eventuali unità attualmente connesse verranno disconnesse. Proseguire?"
    override val dialog92Reading = "Lettura in corso."
    override val dialog103BleAutoConnectCancelMessage = "Annullare la connessione?"
    override val dialog107_PairingCompleteMessage =
        "ID:{0}({1})\n\nper completare l’abbinamento, premere un pulsante qualsiasi sull’interruttore di cui è stato immesso l’ID."
    override val dialog108_DuplicateMessage =
        "ID:{0}({1})\n\n{2} è già abbinato. Scollegare gli elementi abbinati in precedenza {2} e procedere al passaggio successivo?"
    override val dialog108_LeftLever = "Leva sinistra"
    override val dialog108_RightLever = "Leva destra"
    override val dialog109_UnrecognizableMessage =
        "ID:{0}\n\nID non riconosciuto. Verificare se l’ID inserito è corretto."
    override val dialog110_WriteFailureMessage = "ID:{0}({1})\n\nimpossibile scrivere l’ID."
    override val dialog112_PairingDeleteMessage = "Scollegare l’interruttore?"
    override val dialog113GetSwitchFWVersionMessage =
        "Verificare la versione firmware dell’interruttore. Premere un pulsante qualsiasi su {0} ({1})."
    override val dialog116HowToUpdateWirelessSwitchMessage =
        "Vedere “Dettagli” per la procedura di aggiornamento."
    override val dialog122ConfirmFirmwareLatestUpdateMessage =
        "Verificare la disponibilità della versione più recente del file del firmware?"
    override val dialog123AllFirmwareIsUpToDateMessage =
        "Il firmware è aggiornato su tutte le unità della bicicletta connessa."
    override val dialog123FoundNewFirmwareVersionMessage =
        "È stata trovata una nuova versione del firmware per l’unità sulla bicicletta connessa."
    override val dialog3_5UnitRegisteredAgainMessage =
        "Informazioni sulla bici non sincronizzate. L'unità deve essere registrata di nuovo."
    override val dialog4_1UpdatesMessage = "Aggiorna"
    override val dialog6_3DeleteMessage =
        "La bici è stata eliminata. L'unità wireless deve essere eliminata anche nell'impostazione Bluetooth per il sistema operativo."
    override val dialog8_2ConfirmButtonFunction = "Alcuni pulsanti non hanno funzioni assegnate."
    override val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "Abilitare il cambio rapporti a due livelli?"
    override val dialog_jira258ConfirmSameOperationMessage =
        "Specificare di eseguire la stessa operazione come l'operazione eseguita quando l'interruttore viene premuto due volte?"
    override val dialog2_2NeedFirmwareUpdateMessage =
        "Per usare E-TUBE PROJECT, il firmware su {0} deve essere aggiornato. Tempo richiesto approssimativo: {1}"
    override val dialog3_1CompleteFirmwareUpdateMessage = "Il firmware è stato aggiornato."
    override val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "Annullare l'aggiornamento del firmware? \n*Gli aggiornamenti successivi dell'unità sono annullati. \nL'aggiornamento dell'unità attualmente in corso continua."
    override val dialog3_3ConfirmRewriteFirmwareMessage =
        "Il firmware su {0} è la versione più recente. Riscrivere?"
    override val dialog3_4UpdateForNewFirmwareMessage =
        "Esiste un nuovo firmware per {0}, ma non può essere aggiornato. La versione Bluetooth® LE non è supportata. Aggiorna con la versione mobile."
    override val dialog4_1ConfirmDisconnectMessage =
        "Vuoi scollegarti?\nEsistono impostazioni personalizzate. Se ti scolleghi, le impostazioni non saranno applicate."
    override val dialog4_2ConfirmResetAllChangedSettingMessage =
        "Ripristinare tutte le impostazioni modificate?"
    override val dialog4_3ConfirmDefaultMessage =
        "Ripristinare tutte le impostazioni selezionate alle impostazioni predefinite?"
    override val dialog12_2CanNotErrorCheckMessage =
        "Impossibile eseguire la verifica degli errori con SM-BCR2."
    override val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "Interrompere il caricamento e scollegare?"
    override val dialog13_1ConfirmDiscaresAdjustmentsMessage = "Ignorare le regolazioni effettuate?"
    override val dialog_jira258BrightnessSettingMessage =
        "L'impostazione della luminosità display avrà effetto dopo la disconnessione."
    override val dialogUsingSM_Pce02Message =
        "SM-PCE02 rende E-TUBE PROJECT più utile.\n\n・Controllo del consumo batteria che può controllare perdite di corrente in qualsiasi parte dell'unità collegata.\n・Stabilità delle comunicazioni migliorata\n・Maggiore velocità di aggiornamento"
    override val dialogLocationServiceIsNotPermittedMessage =
        "Se l'accesso al servizio di posizionamento non è consentito, non è possibile stabilire il collegamento Bluetooth® LE a causa delle limitazioni di utilizzo di Android OS. Attivare il servizio di localizzazione per questa applicazione, quindi riavviare l'applicazione."
    override val dialogStorageServiceIsNotPermittedMessage =
        "Se l'accesso all’archiviazione non è consentito, non è possibile registrare le immagini a causa delle limitazioni di utilizzo di Android OS. Attivare l’archiviazione per questa applicazione, quindi riavviare l'applicazione."
    override val dialog114AcquisitionFailure = "Impossibile recuperare."
    override val dialog114WriteFailure = "Impossibile scrivere."
    override val dialog129_DuplicateMessage = "{0}\n\nQuesto interruttore è già abbinato."
    override val dialogUnsupportedFetchErrorLogMessage =
        "I registri errori non possono essere visualizzati con la versione firmware della propria unità."
    override val dialogAcquisitionFailedMessage = "Impossibile recuperare."
    //endregion

    //region 選択肢
    override val dialog06TopButtonTitle = "AVVISO"
    override val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Accetta e aggiorna"
    override val dialog20And21Yes = "SÌ"
    override val dialog20And21No = "NO"
    override val dialog22OK = "OK"
    override val dialog25PasskeyErrorOption1 = "Inserisci di nuovo"
    override val dialog26ChangePasskeyOption1 = "Più tardi"
    override val dialog26ChangePasskeyOption2 = "Modifica"
    override val dialog27RegisterBikeOption2 = "Usa"
    override val dialog28BluetoothOffOption1 = "Imposta"
    override val dialog39ConfirmContinueOption2 = "Continua"
    override val dialog41ConfirmDefaultOption2 = "Indietro"
    override val dialog45SecondButtonTitle = "Altro"
    override val dialog55NoInformationOption1 = "Elimina"
    override val dialog55NoInformationOption2 = "Registra unità"
    override val dialog69SwitchBikeConnectionOption1 = "Scollega"
    override val dialog72ConfirmBikeOption1 = "Registra come\nuna nuova bici"
    override val dialog72ConfirmBikeOption2 = "Registra come\nuna bici esistente"
    override val dialog88MoveNextStepOption = "Avanti"
    override val dialog3_5UnitRegisteredAgainOption1 = "Registra"
    override val dialog4_1UpdateDetailOption2 = "Aggiorna storia"
    override val dialog6_3DeleteOption1 = "Imposta"
    override val dialog2_1RecoveryFirmwareOption1 = "Ripristino (Invio)"
    override val dialog2_1RecoveryFirmwareOption2 = "Non ripristinare"
    override val dialog2_2NeedFirmwareUpdateOption1 = "Aggiorna (Invio)"
    override val dialog2_2NeedFirmwareUpdateOption2 = "Non aggiornare"
    override val dialog3_1CompleteFirmwareUpdateOption = "OK (Invio)"
    override val dialog3_3ConfirmRewriteFirmwareOption2 = "Seleziona"
    override val dialogPhotoLibraryAuthorizationSet = "Imposta"
    override val dialogPhotoLibraryAuthorizationMessage =
        "Quest’app utilizza Raccolta foto per caricare le immagini."
    override val dialog109_Retry = "Inserisci di nuovo"
    //endregion
}
