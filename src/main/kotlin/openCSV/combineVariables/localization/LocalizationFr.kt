package openCSV.combineVariables.localization

import openCSV.combineVariables.localization.*

class LocalizationFR : Localization() {
    override val languageCode = LanguageCode.FR

    override val commonsConnectBle = "Connexion Bluetooth® LE"
    override val commonsOperation = "Comptoir d'opérations"
    override val commonsBleSettings = "Paramètres Bluetooth® LE"
    override val commonsYes = "Oui"
    override val commonsNo = "Non"
    override val commonsBikeType = "Type de vélo"
    override val commonsRoad10 = "ROUTE10"
    override val commonsRoad11 = "ROUTE11"
    override val commonsMtb = "VTT"
    override val commonsUrbancity = "URBAIN/VILLE"
    override val commonsUnitName = "Nom d'unité"
    override val commonsOnStr = "MARCHE"
    override val commonsOffStr = "ARRÊT"
    override val commonsLumpShiftTimeSuperFast = "Ultra Rapide"
    override val commonsLumpShiftTimeVeryFast = "Très rapide"
    override val commonsLumpShiftTimeFast = "Rapide"
    override val commonsLumpShiftTimeNormal = "Normal"
    override val commonsLumpShiftTimeSlow = "Lent"
    override val commonsLumpShiftTimeVerySlow = "Très lent"
    override val commonsRetry = "Réessayer"
    override val commonsUse = "Utilisation"
    override val commonsNoUse = "Rechange"
    override val commonsDone = "Terminer"
    override val commonsDecide = "Confirmer"
    override val commonsCancel = "Annuler"
    override val commonsClose = "Fermer"
    override val commonsCategoryBmr = "Support de la batterie"
    override val commonsCategoryBtr2 = "Batterie intégrée"
    override val commonsCategorySt = "Levier de vitesse"
    override val commonsCategorySw = "Interrupteur"
    override val commonsCategoryEw = "Jonction"
    override val commonsCategoryFd = "Dérailleur avant"
    override val commonsCategoryRd = "Dérailleur arrière"
    override val commonsCategoryMu = "Bloc moteur"
    override val commonsCategoryDu = "Unité motrice"
    override val commonsCategoryBt = "batterie"
    override val commonsCategoryCj = "Raccord de cassette"
    override val commonsGroupMaster = "Unité maître"
    override val commonsGroupJunction = "raccord (A)"
    override val commonsGroupFshift = "Unité de changement de vitesse avant"
    override val commonsGroupRshift = "Unité de changement de vitesse arrière"
    override val commonsGroupSus = "Interrupteur de commande de suspension"
    override val commonsGroupStsw = "Manette de changement de vitesse/Contacteur"
    override val commonsGroupEww = "Unité sans fil"
    override val commonsGroupBattery = "Batterie"
    override val commonsGroupPowerswitch = "Contacteur d’alimentation"
    override val commonsGroupUnknown = "Unité inconnue"
    override val commonsSusControlSw = "Interrupteur de commande de suspension"
    override val commonsBleUnit = "Unité sans fil"
    override val commonsPresetName = "Nom du fichier de préréglages"
    override val commonsComment = "Commenter"
    override val commonsOptional = "(option)"
    override val commonsTargetUnitMayBeBroken = "{0} est peut-être défectueux."
    override val commonsElectricWires = "Vérifiez que le câble électrique n'est pas déconnecté."
    override val commonsAlreadyRecognizedUnit = "Cette unité a déjà été reconnue."
    override val commonsNumberOfTeethFront = "Nombre de dents du plateau avant"
    override val commonsNumberOfTeethRear = "Nombre de dents du plateau arrière"
    override val commonsMaxGear = "Nombre de vitesses maximum"
    override val commonsLamp = "Connexion de l'éclairage"
    override val commonsAgree = "Accepter"
    override val commonsFirmwareUpdateTerminated =
        "La mise à jour du firmware de {0} a été interrompue."
    override val commonsBeginFwRestoration = "Le firmware de {0} est restauré."
    override val commonsPleaseConnectBle = "Établir la connexion Bluetooth® LE."
    override val commonsDownLoadFailed = "Échec de téléchargement du fichier."
    override val commonsQDeleteSetting = "Les réglages seront perdus. Continuer ?"
    override val commonsDisplay = "Affichage"
    override val commonsSwitch = "Interrupteur"
    override val commonsSwitchA = "Interrupteur A"
    override val commonsSwitchX = "Interrupteur X"
    override val commonsSwitchY = "Interrupteur Y"
    override val commonsSwitchZ = "Interrupteur Z"
    override val commonsSwitchX1 = "Interrupteur X1"
    override val commonsSwitchX2 = "Interrupteur X2"
    override val commonsSwitchY1 = "Interrupteur Y1"
    override val commonsSwitchY2 = "Interrupteur Y2"
    override val commonsFirmer = "Firmer"
    override val commonsSofter = "Softer"
    override val commonsPosition = "Position {0}"
    override val commonsShiftCountPlural = "{0} vitesses"
    override val commonsLocked = "CLIMB(FIRM)"
    override val commonsUnlocked = "DESCEND(OPEN)"
    override val commonsFirm = "TRAIL(MEDIUM)"
    override val commonsSuspensionType = "Type de suspension"
    override val commonsSprinterSwitch = "Levier satellite"
    override val commonsMultiShiftMode = "Mode Multi-vitesses"
    override val commonsShiftMode = "Mode de changement de vitesse"
    override val commonsLogin = "Connexion"
    override val commonsSettingUsername = "Identifiant d'utilisateur"
    override val commonsSettingPassword = "Mot de passe"
    override val commonsUnexpectedError = "Une erreur inattendue s'est produite."
    override val commonsAccessKeyAuthenticationError =
        "La session de connexion a expiré. Veuillez vous reconnecter."
    override val commonsSoftwareLicenseAgreement = "Conditions d'utilisation"
    override val commonsReInput = "Veuillez-la saisir à nouveau."
    override val commonsPerson = "Personne responsable"
    override val commonsCompany = "Nom de la société"
    override val commonsPhoneNumber = "Numéro de téléphone"
    override val commonsErrorNotice =
        "Problème avec le contenu saisi. Veuillez vérifier les éléments ci-dessous."
    override val commonsFirmwareVersion = "Version du firware"
    override val commonsNotConnected = "Déconnecté"
    override val commonsBeep = "Bip"
    override val commonsDisplayTime = "Heure d'affichage"
    override val commonsTheSameMarkCtdCannotBeAssigned =
        "- Si le modèle de combinaison est différent pour les suspensions avant et arrière, il est impossible d'affecter la même marque (CTD)."
    override val commonsDifferentMarksCtdCannotBeAssigned =
        "- Si le même modèle de combinaison est utilisé pour les suspensions avant et arrière, il est impossible d'affecter des marques (CTD) différentes."
    override val commonsInternetConnectionUnavailable =
        "Non connecté à Internet.\nConnectez-vous à Internet et réessayez."
    override val commonsNotAvailableApp =
        "Voici l’édition {0} de l’application. \nElle ne peut pas être utilisée sur votre {1}."
    override val commonsNotAvailableAppLink = "Téléchargez l’édition {0} de l’application"
    override val commonsSmartPhone = "smartphone"
    override val commonsTablet = "tablette"
    override val commonsCycleComputerRight = "Compteur droit"
    override val commonsCycleComputerLeft = "Compteur gauche"
    override val commonsError = "Erreur"
    override val commonsPleaseConnectAgain =
        "Si l'unité a été déconnectée, vérifiez de nouveau la connexion."
    override val commonsPleasePerformErrorCheckIfNotDisconnected =
        "S'il n'est pas déconnecté, consultez un distributeur."
    override val commonsPleasePerformErrorCheck = "Contactez un distributeur."
    override val commonsErrorC =
        "Il n'y a pas de batterie, ou la batterie n'est pas suffisamment chargée. \nUtilisez une batterie suffisamment chargée, ou chargez puis reconnectez la batterie."
    override val commonsErrorIfNoSwitchResponse =
        "Si les contacteurs ne produisent aucun effet, vérifiez si un des fils électriques est déconnecté."
    override val commonsNetworkError = "Erreur de réseau"
    override val commonsNext = "Suivant"
    override val commonsErrorOccurdDuringSetting =
        "Une erreur s'est produite pendant l'exécution des paramètres."
    override val commonsFrontShiftUp = "Dérailleur avant haut"
    override val commonsFrontShiftDown = "Dérailleur avant bas"
    override val commonsRearShiftUp = "Dérailleur arrière haut"
    override val commonsRearShiftDown = "Dérailleur arrière bas"
    override val commonsGroupFsus = "Fourche avant suspension"
    override val commonsErrorBattery =
        "La capacité restante de la batterie n'a pas pu être vérifiée.\nVérifiez que l'unité maître et la batterie sont correctement connectées."
    override val commonsUpdateCheckUpdateSetting =
        "Recherche des mises à jour d'E-TUBE PROJECT."
    override val commonsApply = "Appliquer"
    override val commonsFinish = "Finition"
    override val commonsDoNotUseProhibitionCharacter =
        "N'utilisez pas de caractères interdits ({1}) dans {0}."
    override val commonsInputError = "Saisissez {0}."
    override val commonsInputNumberError = "Saisissez {0} en caractères numériques de demi-largeur."
    override val commonsDi2 = "DI2"
    override val commonsSteps = "STePS"
    override val commonsSwitchForAssist = "Le contacteur pour le changement du mode d'assistance "
    override val commonsUsinBleUnitError =
        "Une erreur s'est produite lors de l'utilisation de l'unité sans fil."
    override val commonsCommunicationMode = "Mode de communication sans fil"
    override val errorBleDisconnect =
        "La connexion Bluetooth® LE a été interrompue.\nEssayez de vous connecter de nouveau."
    override val errorOccurredAbnormalCommunication =
        "Une erreur de communication s'est produite. Essayez de vous reconnecter."
    override val commonsDrawerMenu = "Menu déroulant"
    override val drawerMenuUnitList = "Liste des unités"
    override val drawerMenuDisconnectBle = "Déconnecter le Bluetooth® LE"
    override val drawerMenuTutorial = "Tutorial"
    override val drawerMenuApplicationSettings = "Paramètres de l'application"
    override val drawerMenuLanguageSettings = "Choix de la langue"
    override val drawerMenuVersionInfo = "Informations relatives à la version"
    override val drawerMenuLogin = "Connexion"
    override val drawerMenuLogout = "Déconnexion"
    override val drawerMenuChangePassword = "Changez de mot de passe"
    override val drawerMenuRegistered = "Demande d'informations utilisateur"
    override val commonsBetaMessageMsg1 =
        "Ce logiciel est en version bêta.\nIl n'a pas été entièrement validé et peut donc générer plusieurs problèmes.\nOK?"
    override val commonsBetaMessageMsg2 = "Date d'expiration du logiciel"
    override val commonsBetaMessageMsg3 = "Ce logiciel a dépassé sa date d'expiration."
    override val launchLicenseMsg1 =
        "Passez en revue les conditions d'utilisation avant d'utiliser l'application."
    override val launchLicenseMsg2 =
        "Cliquez sur le bouton \"Accepter\" après avoir lu les clauses de l'accord pour poursuivre le processus. Vous devez accepter les termes de l'accord pour pouvoir utiliser cette application."
    override val launchFreeSize =
        "L'espace disque est insuffisant ({1} Mo) pour pouvoir exécuter E-TUBE PROJECT. \nLes fichiers requis seront téléchargés la prochaine fois que l'application sera exécutée. \nVérifiez qu'il y a au moins {0} Mo d'espace disque disponible, la prochaine fois, avant d'exécuter l'application."
    override val connectionErrorRepairMsg2 = "{0} est peut-être défectueux."
    override val connectionErrorRepairMsg3 = "Sélectionnez un groupe d'unité non détecté."
    override val connectionErrorRepairMsg4 = "Une unité peut être défectueuse."
    override val connectionErrorRepairMsg6 = "Unité de {0}"
    override val connectionErrorRepairMsg7 =
        "Le micrologiciel de cette unité est récupéré."
    override val connectionErrorRepairMsg8 =
        "Les unités suivantes sont connectées correctement."
    override val connectionErrorUnknownUnit =
        "Une ou plusieurs unités non prises en charge par la version actuelle de E-TUBE PROJECT sont connectées.\nInstallez la dernière application à partir de l'{0}."
    override val connectionErrorOverMsg1 =
        "Jusqu'à uniquement {1} unités de {0} peuvent être connectées."
    override val connectionErrorOverMsg2 = "Unité branchée"
    override val connectionErrorNoCompatibleUnitsMsg1 =
        "Une combinaison incompatible a été trouvée. Vérifiez qu'une unité compatible avec votre type de vélo est connectée et recommencez depuis le début."
    override val connectionErrorNoCompatibleUnitsMsg2 =
        "Ce problème peut parfois être résolu par une mise à jour du micrologiciel permettant une compatibilité étendue. \nEssayez de mettre à jour le micrologiciel."
    override val connectionErrorNoCompatibleUnitsLink =
        "Afficher un tableau de compatibilité plus détaillé"
    override val connectionErrorFirmwareBroken =
        "Le micrologiciel de {0} risque de ne pas fonctionner correctement.\nLe micrologiciel sera rétabli."
    override val connectionDialogMessage1 =
        "Le {0} pour le mode d'assistance n'est pas connecté.\nLorsque vous activez le changement de mode, le réglage de {1} peut être ajusté depuis le changement de vitesses."
    override val connectionDialogMessage3 = "{0} est réglé pour suspension.\nModifiez le réglage."
    override val connectionDialogMessage4 =
        "Le réglage \"{1}\" de {0} ne fonctionne pas avec le vélo utilisé.\nVeuillez modifier le réglage."
    override val connectionDialogMessage5 =
        "Le système ne fonctionnera pas normalement avec la combinaisons d'unités reconnue.\nPour activer le système pour un fonctionnement correct, connectez les unités et vérifiez de nouveau les connexions. Continuer ?"
    override val connectionDialogMessage6 = "Unité requise (une des suivantes)"
    override val connectionDialogMessage7 =
        "Le système ne fonctionnera pas normalement avec la combinaisons d’unités reconnue.\nPour activer le système pour un fonctionnement correct, connectez les unités et vérifiez de nouveau les connexions."
    override val connectionDescription1 =
        "Définissez l'unité du vélo sur le mode de connexion Bluetooth® LE."
    override val connectionDescription2 =
        "Sélectionnez l'unité sans fil  que vous souhaitez connecter."
    override val connectionWhatIsPairing = "Activation du mode de connexion sur l'unité sans fil."
    override val connectionDisconnectDescription =
        "Pour vous déconnecter, sélectionnez \"Déconnecter le Bluetooth® LE\" à partir du menu en haut à droite."
    override val connectionBluetoothInitialPasskey =
        "Cette application utilise Bluetooth® LE pour les communications sans fil.\nLa clé initiale \"PassKey\" pour Bluetooth® LE est \"000000\"."
    override val connectionPasskeyMessage =
        "Modifiez le PassKey initial.\nUne tierce partie risque de se connecter."
    override val connectionPasskey = "PassKey"
    override val connectionPasswordConfirm = "PassKey (pour confirmation)"
    override val connectionPleaseInputPassword = "Entrez votre PassKey."
    override val connectionDisablePassword =
        "Authentification impossible. Le PassKey n'a pas été correctement saisi ou le PassKey de l'unité sans fil a été modifié. Reconnectez-vous, puis réessayez de saisir le PassKey défini dans l'unité sans fil."
    override val connectionValidityPassword =
        "0 ne peut pas être défini comme premier caractère dans le PassKey."
    override val connectionBleFirmwareRestore =
        "La récupération du micrologiciel sera effectuée sur l'unité sans fil."
    override val connectionBleFirmwareRestoreFailed =
        "Reconnectez l'unité.\nSi l'erreur persiste, utilisez la version PC de E-TUBE PROJECT."
    override val connectionBleWeakWaves =
        "La connexion sans fil est pauvre. \nLa connexion Bluetooth® LE peut être interrompue."
    override val connectionBleNotRequiredRestore =
        "L'unité sans fil fonctionne correctement.\nLa récupération du micrologiciel n'est pas nécessaire."
    override val connectionFoundNewBleFirmware =
        "Un nouveau micrologiciel pour  l'unité sans fil a été détecté.\nLe mise à jour va commencer."
    override val connectionScanReload = "Recharger"
    override val connectionUnmatchPassword = "La clé PassKey ne correspond pas."
    override val connectionBikeTypeDi2Type1 = "ROUTE"
    override val connectionBikeTypeDi2Type2 = "VTT"
    override val connectionBikeTypeDi2Type3 = "URBAIN/VILLE"
    override val connectionBikeTypeEbikeType1 = "VTT"
    override val connectionNeedPairingBySetting =
        "Réglez l'unité sans fil pour la connexion et procédez à l'appariement à partir de [Paramètres] > [Bluetooth] sur le dispositif."
    override val connectionSwE6000Recognize =
        "Maintenez appuyé au mois l'un des boutons de {1} et réglez {0}"
    override val commonsGroupDU = "Unité motrice"
    override val commonsGroupSC = "Compteur"
    override val commonsGroupRSus = "Suspension arrière"
    override val commonsGroupForAssist = "Manette de changement de vitesse pour assistance"
    override val commonsGroupForShift = "Manette de changement de vitesse"
    override val connectionMultipleChangeShiftToAssist =
        "{0} {1} est connecté. \nLe type de vélo actuel ne prend en charge que {2}. \nSouhaitez-vous configurer toutes les {0} sur {2} ?"
    override val connectionNewFirmwareFileNotFound =
        "La version du fichier de micrologiciel n'est pas la plus récente."
    override val commonsGroupMasterCap = "Unité maître"
    override val commonsGroupJunctionCap = "Raccord (A)"
    override val commonsGroupFshiftCap = "Unité de changement de vitesse avant"
    override val commonsGroupRshiftCap = "Unité de changement de vitesse arrière"
    override val commonsGroupSusCap = "Interrupteur de commande de suspension"
    override val commonsGroupStswCap = "Manette de changement de vitesse/contacteur"
    override val commonsGroupFsusCap = "Fourche avant suspension"
    override val commonsGroupRsusCap = "Suspension arrière"
    override val commonsGroupDuCap = "Unité motrice"
    override val commonsGroupForassistforshiftCap =
        "Manette de changement de vitesse pour assistance/manette de changement de vitesse"
    override val commonsGroupForassistforshift =
        "manette de changement de vitesse pour assistance/manette de changement de vitesse"
    override val connectionErrorNoSupportedMaster =
        "{0} n'est pas pris en charge par cette application.\n{0} peut être utilisé avec la version PC de E-TUBE PROJECT."
    override val connectionErrorNoSupportedMaster2 = "Non compatible."
    override val connectionErrorUnknownMaster = "Une unité maître inconnue est connectée."
    override val connectionErrorPleaseConfirmMasterUnit = "Vérifiez l'unité maître."
    override val connectionErrorUnSupportedMsg1 =
        "Une unité incompatible avec une connexion Bluetooth® LE est connectée.\nDéconnectez toutes les unités ci-dessous et reconnectez-les."
    override val connectionErrorPcLinkageDevice = "Dispositif de liaison PC"
    override val connectionErrorUnSupportedMsg2 =
        "Plusieurs unités sans fil sont connectées.\nDéconnectez toutes les unités sauf une des unités ci-dessous et reconnectez-les."
    override val connectionErrorBroken =
        "L'application n'a pas pu se connecter à E-TUBE PROJECT car l'une des unité connectées est peut-être défectueuse. \nContactez votre distributeur ou revendeur."
    override val connectionChargeMsg1 = "Vérifiez l'état de la batterie."
    override val connectionChargeMsg2 =
        "Son utilisation n'est pas possible pendant la charge. Connectez de nouveau une fois la charge terminée."
    override val connectionChargeMsg3 =
        "Si la charge n'est pas en cours, la batterie est peut-être défectueuse. \nContactez un distributeur."
    override val connectionChargeRdMsg =
        "Charge arrêtée. Pour charger à nouveau, déconnectez-vous de l’application, puis rebranchez le câble de charge."
    override val connectionBikeTypePageTitle = "Sélection du type de vélo"
    override val connectionBikeTypeMsg =
        "L'application n'a pas réussi à détecter le type de vélo. Sélectionnez le type de vélo."
    override val connectionBikeTypeTitle1 = "Système DI2"
    override val connectionBikeTypeTitle2 = "syst`eme E-bike"
    override val connectionBikeTypeError =
        "Le type de vélo pour les réglages en cours d'enregistrement sont différents du type de vélo défini pour l'unité connectée. \nVérifiez les réglages ou l'unité connectée avant d'enregistrer les réglages."
    override val connectionDialogMessage9 =
        "{0} pour la commutation du mode d'assistance est connecté."
    override val connectionDialogMessage12 =
        "Le type de vélo actuel prend uniquement en charge le réglage de changement de vitesse."
    override val connectionDialogMessage13 = "Régler {0} pour le changement de vitesse?"
    override val connectionDialogMessage14 = "Régler tous les {0} pour le changement de vitesse?"
    override val connectionDialogMessage19 =
        "Le micrologiciel sur l'appareil utilisé n'est peut-être plus à jour.\nConnectez-vous à Internet et sélectionnez \"Tout télécharger\" à partir de \"Informations relatives à la version\" pour télécharger le dernier micrologiciel."
    override val connectionChangeShiftToAssist =
        "{0} {1} est connecté. \nLe type de vélo actuel ne prend en charge que {2}. \nSouhaitez-vous configurer {0} sur {2} ?"
    override val connectionDialogMessage11 = "L'unité suivante ne peut pas être utilisée."
    override val connectionDialogMessage16 =
        "Mettez à jour l'application vers la dernière version, puis réessayez."
    override val connectionDialogMessage15 = "Supprimer l'unité."
    override val connectionDialogMessage17 =
        "L'unité suivante ne prend pas en charge le système d'exploitation de votre smartphone ou de votre tablette."
    override val connectionDialogMessage18 =
        "Il est recommandé de mettre à jour le système d'exploitation de votre smartphone ou tablette vers la dernière version pour pouvoir l'utiliser avec cette application."
    override val connectionSprinterMsg1 = "Utilisez-vous l'interrupteur régulateur sur {0}?"
    override val connectionSprinterMsg2 =
        "Plusieurs {0} sont connectés.\nUtilisez toutes les manettes de changement de vitesses spécial \"Sprint\"?"
    override val commonsSelect = "Sélectionner"
    override val connectionSprinterContinue = "Continuer la sélection?"
    override val connectionAppVersion = "Ver.{0}"
    override val commonsVersionInfoEtubeVersion = "Version du E-TUBE PROJECT"
    override val commonsVersionInfoBleFirmwareVersion =
        "Version du micrologiciel du dispositif Bluetooth® LE"
    override val commonsVersionInfoMessage1 =
        "La fonction de vérification utilise l'unité sans fil et E-TUBE PROJECT  pour détecter les ruptures de fils et les erreurs du système. Elle ne détecte pas les problèmes dans toutes les unités, ni les oscillations de fonctionnement.\nContactez le magasin où vous avez effectué votre achat ou un revendeur."
    override val commonsVersionInfoMessage2 =
        "Une nouvelle version d'E-TUBE PROJECT est disponible, mais elle ne prend pas en charge votre système d'exploitation. Pour plus d'informations sur les systèmes d'exploitation pris en charge, consultez le site Internet SHIMANO."
    override val commonsVersionInfoUpdateCheck = "Tout télécharger"
    override val settingMsg3 = "Vérifiez que l'écran de modification du PassKey initial s'affihce."
    override val settingMsg5 =
        "Vérifiez les dernières applications destinées aux différents systèmes d'exploitations."
    override val settingMsg6 =
        "Vérifiez que le guide d'animation de mode de changement de vitesse est affiché."
    override val settingMsg4 =
        "Vérifiez que le guide d'animation sur le changement de vitesse multiple est affiché."
    override val settingMsg1 = "Vérifiez la mise à jour du micrologiciel pendant le préréglage."
    override val settingMsg2 = "Le serveur proxy exige des données d'authentification."
    override val settingServer = "Serveur"
    override val settingPort = "Port"
    override val settingUseProxy = "Utilisez le serveur proxy"
    override val settingUsername = "Identifiant d'utilisateur"
    override val settingPassword = "Mot de passe"
    override val settingFailed =
        "Une erreur est survenue lors de l'enregistrement des paramètres de l'application."
    override val commonsEnglish = "Anglais"
    override val commonsJapanese = "Japonais"
    override val commonsChinese = "Chinois"
    override val commonsFrench = "Français"
    override val commonsItalian = "Italien"
    override val commonsDutch = "Néerlandais"
    override val commonsSpanish = "Espagnol"
    override val commonsGerman = "Allemand"
    override val commonsLanguageChangeComplete =
        "Le réglage de la langue a été modifié. \nLa langue sera modifiée lorsque vous quitterez puis relancerez l'application."
    override val commonsDiagnosisResult3 =
        "Remplacez ou supprimez l'unité suivante et connectez de nouveau."
    override val connectionErrorSelectNoDetectedMsg = "Sélectionnez une unité non détectée."
    override val connectionErrorSelectNoDetectedAlert =
        "Si ne vous sélectionnez pas la bonne unité, un micrologiciel inapproprié risque d'être enregistré sur unité, la rendant inopérante. Vérifiez que vous avez choisi la bonne unité et passez à l'étape suivante."
    override val commonsCheckElectricWires2 = "{0} n'est pas reconnu."
    override val commonsDiagnosisCheckMsg = "{0} est reconnu.\nL'unité {0} est-elle connectée?"
    override val commonsUpdateCheckUpdateError =
        "Impossible de vérifier une connexion avec le serveur. \nConnectez-vous à Internet, puis réessayez."
    override val commonsUpdateCheckWebServerError =
        "Il peut y avoir un problème avec la connexion internet ou leserveur web de Shimano. \nAttendez un peu et recommencez."
    override val commonsUpdateCheckUpdateSettingIOs = "Lecture en cours."
    override val firmwareUpdateRecognizeUnitDialogMsg =
        "Continuez d'appuyer sur l'un des interrupteurs {0} pour l'unité pour laquelle vous souhaitez mettre à jour le firmware."
    override val customizeSwitchFunctionDialogMsg1 =
        "Continuez d'appuyer sur l'un des interrupteurs {0} pour l'unité que vous souhaitez configurer."
    override val connectionSprinterRecognize =
        "Continuez d'appuyer sur l'un des interrupteurs {0} pour l'unité que vous souhaitez sélectionner."
    override val commonsConfirmPressedSwitch = "Avez-vous appuyé sur l'interrupteur ?"
    override val connectionSprinterRelease =
        "D'accord. Relâchez le contacteur. \n\nSi cette boîte de dialogue ne se ferme pas une fois que vous avez relâché le contacteur, il se peut qu'un contacteur présent un dysfonctionnement. Dans ce cas, contactez un distributeur."
    override val commonsLeft = "Gauche"
    override val commonsRight = "Droite"
    override val commonsUpdateCheckIsInternetConnected =
        "Le fichier va être mis à jour. \nVotre appareil est-il connecté à Internet ?"
    override val commonsUpdateCheckUpdateFailed = "Échec de la mise à jour."
    override val commonsUpdateFailed =
        "L'application n'a pas trouvé un ou plusieurs fichiers permettant d'exécuter E-TUBE PROJECT. \nDésinstallez, puis réinstallez E-TUBE PROJECT."
    override val customizeBleBleName = "Nom de l'unité sans fil"
    override val customizeBlePassKeyRule = "(6 caractères alphanumériques de demi-largeur)"
    override val customizeBlePassKeyDisplay = "Afficher"
    override val customizeBlePassKeyPlaceholder = "Veuillez-la saisir à nouveau."
    override val customizeBleErrorBleName =
        "Saisissez 1 à 8 caractères alphanumériques de demi-largeur."
    override val customizeBleErrorPassKey = "Saisissez 6 caractères numériques de demi-largeur."
    override val customizeBleErrorPassKeyConsistency = "La clé PassKey ne correspond pas."
    override val customizeBleAlphanumeric = "demi-largeur des caractères alphanumériques"
    override val customizeBleHalfWidthNumerals = "Chiffres de demi-largeur"
    override val commonsCustomize = "Personnaliser"
    override val customizeFunctionDriveUnit = "Unité motrice"
    override val customizeFunctionNonUnit = "Aucune unité personnalisable n'est connectée."
    override val customizeCadence = "Cadence"
    override val customizeAssistSetting = "Assistance"
    override val commonsAssistPattern = "Modèle d'assistance"
    override val commonsElectricType = "Electrique"
    override val commonsExteriorTransmission = "Type de dérailleur (chaîne)"
    override val commonsMechanical = "Mécanique"
    override val commonsAssistPatternDynamic = "DYNAMIC"
    override val commonsAssistPatternExplorer = "EXPLORER"
    override val commonsAssistModeHigh = "HIGH"
    override val commonsAssistModeMedium = "MEDIUM"
    override val commonsAssistModeLow = "LOW"
    override val commonsRidingCharacteristic = "Caractéristique de conduite"
    override val commonsFunctionRidingCharacteristic = "Caractéristique de conduite"
    override val commonsAssistPatternCustomize = "CUSTOMIZE"
    override val commonsAssistModeBoost = "BOOST"
    override val commonsAssistModeTrail = "TRAIL"
    override val commonsAssistModeEco = "ECO"
    override val customizeSceKm = "m\nkg"
    override val customizeSceMile = "y\nlb"
    override val commonsLanguage = "Langue"
    override val customizeDisplayNowTime = "Heure"
    override val customizeDisplaySet = "Régler"
    override val customizeDisplayDontSet = "Ne pas régler"
    override val commonsDisplayLight = "Affichage/Lumière"
    override val customizeSwitchSettingViewControllerMsg1 = "Manette de changement de vitesse"
    override val customizeSwitchSettingViewControllerMsg8 = "Utilisez {0}"
    override val customizeSwitchSettingViewControllerMsg2 = "Passer au réglage de suspension"
    override val customizeSwitchSettingViewControllerMsg3 =
        "À propos du changement de vitesses synchronisé."
    override val customizeSwitchSettingViewControllerMsg11 =
        "L'état de connexion de l'unité a changé. Ne déconnectez ou ne modifiez pas les unités pendant la configuration des réglages. \nRépétez le processus depuis le début."
    override val customizeSwitchSettingViewControllerMsg4 =
        "Les informations actuellement sélectionnées seront perdues. \nModifier sur le réglage de suspension ?"
    override val customizeSwitchSettingViewControllerMsg9 = "Que signifie Synchronized shift ?"
    override val customizeSwitchSettingViewControllerMsg5 =
        "Le Synchronized shift passe le rapport FD en synchronisation avec Dérailleur arrière haut et Dérailleur arrière bas."
    override val customizeSwitchSettingViewControllerMsg6 =
        "Les fonctions suivantes ne sont pas incluses. Continuer le traitement ?"
    override val customizeSwitchSettingViewControllerMsg7 = "Interrupteur {0}"
    override val customizeSwitchSettingViewControllerMsg10 =
        "Réglez le contacteur {0} pour un fonctionnement en sens inverse"
    override val customizeSwitchSettingViewControllerMsg12 =
        "Semi-Synchronized Shift  est une fonction qui actionne automatiquement le dérailleur arrière lorsque le dérailleur avant est commandé afin d'obtenir un passage optimal des vitesses."
    override val customizeSwitchSettingViewControllerMsg13 =
        "Les points de passage peuvent être sélectionnés."
    override val customizeSwitchSettingViewControllerMsg14 =
        "A ce stade, la position du dérailleur arrière peut être changée de 0 à 4. (En fonction de la combinaison, il peut y avoir une position de changement non sélectionnable.)"
    override val commonsSwitchType = "Mode interrupteur"
    override val commonsAssistUp = "Augmenter assistance"
    override val commonsAssistDown = "Diminuer assistance"
    override val customizeSwitchModeChangeAssist =
        "changer la position de l'interrupteur　[pour Assistance].\nContinuer ?"
    override val customizeSwitchModeChangeShift =
        "changer la position de l'interrupteur　[pour Changement].\nContinuer ?"
    override val customizeSwitchModeLostAssist =
        "Lorsque le mode interrupteur est placé sur [pour Changement], l'interrupteur pour la commutation du mode d'assistance disparaitra.\nContinuer pour modifier ?"
    override val commonsDFlyCh1 = "D-FLY Ch. 1"
    override val commonsDFlyCh2 = "D-FLY Ch. 2"
    override val commonsDFlyCh3 = "D-FLY Ch. 3"
    override val commonsDFlyCh4 = "D-FLY Ch. 4"
    override val commonsDFly = "D-FLY"
    override val commonsFunction = "Fonction"
    override val customizeGearShifting = "Changement de vitesse"
    override val customizeManualSelect = "Sélectionnez le contacteur manuellement"
    override val commonsUnit = "Unité"
    override val customizeScSetting = "Compteur"
    override val customizeSusSusTypeTitle = "Interrupteur de commande de suspension"
    override val customizeSusSusPositionInform =
        "Sur quelle poignée le contacteur de commande est-il installé ?"
    override val customizeSusSusPositionLeft = "Côté gauche"
    override val customizeSusSusPositionRight = "Côté droit"
    override val customizeSusPosition = "Position"
    override val customizeSusFront = "Avant"
    override val customizeSusRear = "Arrière"
    override val customizeFooterMsg1 = "Restaurer val. déf"
    override val customizeFooterMsg2 = "Rétablit les réglages par défaut {0}"
    override val customizeSusConsistencyCheckMsg1 = "Vérifiez le message ci-dessous."
    override val customizeSusConsistencyCheckMsg6 = "D'accord pour continuer la programmation ?"
    override val customizeSusConsistencyCheckMsg3 =
        "- Le même réglage a été choisi pour deux ou plus de positions."
    override val customizeSusConsistencyCheckMsg2 = "- Seul {1} a été réglé pour {0}."
    override val customizeSusConsistencyCheckMsg4 =
        "- Le même réglage a été appliqué à deux positions ou plus dans CTD."
    override val customizeSusClimb = "CLIMB(FIRM)"
    override val customizeSusDescend = "DESCEND(OPEN)"
    override val customizeSusTrail = "TRAIL(MEDIUM)"
    override val customizeSusSwVerCheck =
        "L'interrupteur ne peut pas être réglé sur suspension car la version du micrologiciel de {0} est inférieure à {1}.\nPour régler l'interrupteur sur suspension, connectez-vous à un réseau et effectuez une mise à jour vers la dernière version ({1} ou ultérieure)."
    override val customizeSusSusConnectMsg1 =
        "L'une des unités suivantes est requise pour régler l'interrupteur sur suspension."
    override val customizeSusSusConnectMsg2 = "Reconnectez l'unité requise et réessayez."
    override val customizeSusSusConnectMsg3 =
        "Il est également possible de le faire passer au réglage de changement de vitesse."
    override val customizeSusSusConnectMsg4 =
        "Pour le faire passer au réglage de changement de vitesse, appuyez sur [Oui]. Pour connecter les unités requises et recommencer sans changer le réglage, appuyez sur [Non]."
    override val customizeSusSusModeSelect =
        "Le réglage \"→(T)\" ne fonctionne pas avec la suspension utilisée. \nVous pouvez masque le réglage \"→(T)\" via l'écran de configuration. \nMasquer \"→(T)\" ?"
    override val customizeSusSwitchShift = "Passer au réglage du changement de vitesse"
    override val customizeSusTransitionShift =
        "Les informations actuellement sélectionnées seront perdues. \nModifier sur le réglage de changement ?"
    override val customizeMuajstTitle = "Réglage du dérailleur"
    override val customizeMuajstAdjustment = "Réglage"
    override val customizeMuajstGearPosition = "Changement de position de la vitesse"
    override val customizeMuajstInfo1 =
        "Les interrupteurs installés sur la bicyclette ne fonctionnent pas."
    override val customizeMuajstInfo2 =
        "Voir ici pour plus d'informations sur la méthode de réglage."
    override val customizeMultiShiftModalTitle = "Réglage de la vitesse du mode Multi-vitesses"
    override val customizeMultiShiftModalDescription =
        "Les réglages initiaux utilisent les valeurs par défaut standard. \nAprès vous être assuré de bien comprendre les caractéristiques du changement de vitesse multiple, choisissez le réglage de la vitesse de changement de vitesse multiple en fonction des conditions dans lesquelles le vélo sera utilisé (type de terrain, style de conduite, etc.)."
    override val customizeMultiShiftModalVeryFast = "Très rapide"
    override val customizeMultiShiftModalFast = "Rapide"
    override val customizeMultiShiftModalNormal = "Normal"
    override val customizeMultiShiftModalSlow = "Lent"
    override val customizeMultiShiftModalVerySlow = "Très lent"
    override val customizeMultiShiftModalFeatures = "Caractéristiques"
    override val customizeMultiShiftModalFeature1 =
        "Le changement de vitesse multiple rapide est à présent activé. \n\n•Cette fonction vous permet de régler rapidement la vitesse de rotation de la manivelle (RPM) en réponse aux changements des conditions de conduite. \n•Vous permet de régler rapidement votre vitesse."
    override val customizeMultiShiftModalFeature2 =
        "Permet le fonctionnement du changement de vitesse multiple fiable"
    override val customizeMultiShiftModalNoteForUse = "Précautions relatives à l'utilisation"
    override val customizeMultiShiftModalUse1 =
        "1. Il est facile de changer une vitesse de trop. \n\n2. Si la vitesse de rotation de la manivelle (RPM) est faible, la chaîne ne pourra pas suivre le mouvement du dérailleur arrière. \nPar conséquent, la chaîne peut glisser sur les dents de pignon au lieu de s'engager dans le pignon de cassette."
    override val customizeMultiShiftModalUse2 =
        "Le fonctionnement du changement de vitesse multiple dure un certain temps"
    override val customizeMultiShiftModalCrank =
        "Vitesse de rotation de la manivelle (RPM) requise pour l'utilisation du changement de vitesse multiple"
    override val customizeMultiShiftModalCrank1 =
        "À une vitesse de rotation de la manivelle élevée (RPM)"
    override val customizeMultiShiftModeViewControllerMsg3 = "Intervalle de changement de vitesse"
    override val customizeMultiShiftModeViewControllerMsg4 = "Limite du nombre de vitesses"
    override val commonsLumpShiftUnlimited = "pas de limite"
    override val commonsLumpShiftLimit2 = "2 vitesses"
    override val commonsLumpShiftLimit3 = "3 vitesses"
    override val customizeMultiShiftModeViewControllerMsg5 =
        "Autre interrupteur de changement de vitesses"
    override val customizeShiftModeRootMsg1 =
        "Une unité de chaque (1) et (2) ci-dessous est requise pour définir le mode de changement de vitesse."
    override val customizeShiftModeRootMsg3 = "Reconnectez l'unité requise et réessayez."
    override val customizeShiftModeRootMsg4 =
        "L'une des unités suivantes est requise pour définir le mode de changement de vitesse."
    override val customizeShiftModeRootMsg5 =
        "Même si le choix des paramètres du mode de changement de vitesse est fait, ils ne seront effectifs que si la version du micrologiciel de {0} est {1} ou supérieure."
    override val customizeShiftModeRootMsg6 =
        "La version du micrologiciel est {0} ou une version ultérieure"
    override val customizeShiftModeSettingMsg1 = "Supprimer ?"
    override val customizeShiftModeSettingMsg2 =
        "Les réglages du vélo actuel seront perdus. Continuer ?"
    override val customizeShiftModeSettingMsg3 =
        "Les réglages ne peuvent pas être configurés car l'unité n'est pas correctement connectée."
    override val customizeShiftModeSettingMsg4 =
        "Le vélo a une structure de rapports qui ne peut pas être configurée. Vérifiez les réglages."
    override val customizeShiftModeSettingMsg5 =
        "Impossible de lire la valeur de réglage du fichier {0} stocké. \n{0} sera supprimé."
    override val customizeShiftModeSettingMsg7 = "Quelle fonction voulez-vous attribuer ?"
    override val customizeShiftModeSettingMsg8 = "Le nom de fichier de réglages est déjà utilisé."
    override val customizeShiftModeTeethMsg1 =
        "Sélectionnez le nombre de dents de pignon pour votre vélo actuel."
    override val customizeShiftModeGuideMsg1 =
        "Pour utiliser le fichier des réglages sur votre appareil afin de remplacer les réglages de votre vélo, faites glisser le fichier sur l'emplacement (S1 ou S2) à remplacer."
    override val customizeShiftModeGuideMsg2 =
        "L'icône affichée au-dessus des réglages indique le mode de changement."
    override val customizeShiftModeGuideMsg3 =
        "Synchronized shift est une fonction qui permet d'actionner automatiquement le dérailleur avant de manière synchronisée avec le dérailleur arrière. Vous pouvez configurer le fonctionnement du Synchronized shift  en sélectionnant des points sur le schéma."
    override val customizeShiftModeGuideMsg4 =
        "Déplacez le curseur vert pour un réglage HAUT (de l'interieur vers l'extérieur) et le curseur bleu pour un réglage BAS (de l'extérieur vers l'intérieur)."
    override val customizeShiftModeGuideMsg5 =
        "Points de synchronisation \nPoints auquel le dérailleur avant  change de vitesse simultanément avec le dérailleur arrière. \nLes exigences concernant les flèches indiquant les points de synchronisation sont les suivantes : \nHAUT : Pointant vers le haut ou le côté \nBAS : Pointant vers le haut ou le côté"
    override val customizeShiftModeGuideMsg6 =
        "Rapports de vitesse disponibles \nHAUT : Vous pouvez sélectionner n'importe quel rapport jusqu'à celui juste en dessous du rapport du point de synchronisation . \nBAS : Vous pouvez sélectionner n'importe quel rapport de vitesse jusqu'à celui juste au-dessus du rapport du point de synchronisation."
    override val customizeShiftModeTeethMsg6 =
        "Si le pédalier (FC) est passé de  {0} à {1}, tous les points de changement de vitesse réglés seront réinitialises sur leurs valeurs par défaut.\nEffectuer les modifications ?"
    override val customizeShiftModeTeethDouble = "2 plateaux"
    override val customizeShiftModeTeethTriple = "3 plateaux"
    override val customizeShiftModeTeethMsg2 = "Intervalle synchronized shift"
    override val customizeShiftModeTeethSettingTitle = "Sélectionnez le nombre de dents"
    override val customizeShiftModeGuideTitle1 = "Synchronized shift"
    override val customizeShiftModeGuideTitle2 = "Semi-Synchronized Shift"
    override val customizeShiftModeGuideTitle3 = "Semi-Synchronized shift"
    override val customizeShiftModeSynchroMsg1 =
        "Le point de changement de vitesses se trouve dans la plage non réglable et ne peut pas fonctionner normalement."
    override val customizeShiftModeTeethMsg5 = "Nom du fichier de réglages"
    override val customizeShiftModeBtn1 = "Haut"
    override val customizeShiftModeBtn2 = "Bas"
    override val customizeShiftModeTeethMsg8 = "Commande de la position des pignons"
    override val customizeShiftModeSettingMsg9 =
        "Les informations ci-dessous sont aussi mises à jour pour S1 et pour S2."
    override val customizeShiftModeTeethInward1 =
        "Arrière haut sur avant bas"
    override val customizeShiftModeTeethOutward1 =
        "Arrière bas sur avant haut"
    override val customizeShiftModeTeethInward2 =
        "Dérailleur arrière haut \nlorsque dérailleur avant bas"
    override val customizeShiftModeTeethOutward2 =
        "Dérailleur arrière bas \nlorsque dérailleur avant haut"
    override val customizeShiftModeTypeSelectTitle = "Sélection des fonctions à attribuer"
    override val customizeShiftModeSettingMsg10 = "Continuer le traitement ?"
    override val customizeShiftModeTeethMsg7 =
        "Sélectionnez le nombre de dents de pignon pour le vélo en question. \n*Fixé comme {0} est connecté."
    override val commonsErrorCheck = "Contrôle d'erreurs"
    override val commonsAll = "Tout"
    override val errorCheckResultError = "{0} est peut-être défectueux."
    override val errorCheckContactDealer =
        "Veuillez vous renseigner auprès du détaillant ou du distributeur."
    override val commonsStart = "Démarrer"
    override val commonsUpdate = "Mise à jour le microprogramme"
    override val firmwareUpdateUpdateAll = "Tout mettre à jour"
    override val firmwareUpdateCancelAll = "Tout annuler"
    override val firmwareUpdateUnitFwVersionIsNewerThanLocalFwVersion =
        "Connectez-vous à Internet et vérifiez s'il y a des mises à jour de E-TUBE PROJECT ou de produit disponibles. \nLa mise à jour vers la dernière version vous permettra d'utiliser de nouveaux produits et de nouvelles fonctionnalités."
    override val firmwareUpdateNoNewFwVersionAndNoInternetConnection =
        "L'application ou le micrologiciel n'est peut être plus à jour. \nConnectez-vous à Interne et recherchez la dernière version."
    override val firmwareUpdateConnectionLevelIsLow =
        "La mise à jour n'est pas possible en raison d'une connexion sans fil faible. \nMettez à jour le micrologiciel après avoir établi une connexion satisfaisante."
    override val firmwareUpdateIsUpdatingFw =
        "La mise à jour du microprogramme pour {0} est en cours."
    override val firmwareUpdateNotEnoughBattery =
        "La mise à jour du micrologiciel prendra quelques minutes. \nSi la batterie de votre appareil est faible, effectuez la mise à jour après l'avoir chargée ou connectez-la à un chargeur. \nDémarrer la mise à jour ?"
    override val firmwareUpdateFwUpdateErrorOccuredAndReturnBeforeConnect =
        "L'application n'a pas réussi à mettre à jour micrologiciel. \nExécutez de nouveau le processus de mise à jour du micrologiciel. \nSi la mise à jour échoue après plusieurs tentatives, cela peut provenir d'une défaillance de {0}."
    override val firmwareUpdateOtherFwBrokenUnitIsConnected =
        "Une unité qui ne fonctionne pas normalement est connectée.\n\nSi le microprogramme de {0} est mis à jour dans ces conditions, le nouveau microprogramme de {0} peut écraser le microprogramme d'autres unités, ce qui risque de provoquer un dysfonctionnement.\nDébranchez toutes les unités autres que {0}."
    override val firmwareUpdateLatest = "Dernière"
    override val firmwareUpdateWaitingForUpdate = "En attente de mise à jour"
    override val firmwareUpdateCompleted = "Terminer"
    override val firmwareUpdateProtoType = "prototype"
    override val firmwareUpdateFwUpdateErrorOccuredAndGotoFwRestoration =
        "La mise à jour du micrologiciel de {0} a échoué. \nPassez au processus de rétablissement."
    override val firmwareUpdateInvalidFirmwareFileDownloadComfirm =
        "Télécharger la dernière version du micrologiciel ?"
    override val firmwareUpdateInvalidFirmwareFileSingle =
        "Le fichier du microprogramme de {0} est vide."
    override val firmwareUpdateStopFwUpdate = "Annuler la mise à jour du microprogramme."
    override val firmwareUpdateStopFwRestoring = "Annuler la restauration du microprogramme."
    override val firmwareUpdateRestoreComplete =
        "Lors de l'utilisation de {0}, vous pouvez modifier l'état du réglage de suspension affiché dans SC à partir du Réglage de l'interrupteur dans le menu Personnaliser."
    override val firmwareUpdateRestoreCompleteSc = "SC"
    override val firmwareUpdateSystemUpdateFailed = "Echec de mise à jour du système."
    override val firmwareUpdateAfterUnitRecognitionFailed =
        "Echec de connexion après la mise à jour du microprogramme."
    override val firmwareUpdateUpdateBleUnitFirst = "D'abord, mettez à jour le firmware pour {0}."
    override val firmwareUpdateSystemUpdateFailure = "Impossible de mettre à jour le système."
    override val firmwareUpdateSystemUpdateFinishedWithoutUpdate =
        "Le système n'a pas été mis à jour."
    override val firmwareUpdateBleUpdateErrorOccuredConnectAgain =
        "Veuillez essayer de vous connecter de nouveau."
    override val firmwareUpdateFwRestoring =
        "Le microprogramme est en cours du remplacement. \nNe pas déconnecter avant la fin du remplacement."
    override val firmwareUpdateFwRestorationUnitIsNormal =
        "Le firmware de {0} fonctionne normalement."
    override val firmwareUpdateNoNeedForFwRestoration = "Il n'a pas besoin d'être restauré."
    override val firmwareUpdateFwRestorationError = "Restauration du firmware de {0} échouée."
    override val firmwareUpdateFinishedFirmwareUpdate =
        "Le micrologiciel a été mis à jour vers la dernière version."
    override val firmwareUpdateFinishedFirmwareRestoration = "Le firmware de {0} a été réparé."
    override val firmwareUpdateRetryFirmwareRestoration =
        "Un nouvel essai de récupération du micrologiciel pour {0} est en cours."
    override val functionThresholdNeedSoftwareUsageAgreementMultiple =
        "Vous devez accepter les conditions d'utilisation du logiciel pour pouvoir mettre à jour le micrologiciel des unités suivantes."
    override val functionThresholdAdditionalSoftwareUsageAgreement =
        "Contrat de licence logiciel supplémentaire"
    override val functionThresholdMustRead = "(À lire impérativement)"
    override val functionThresholdContentUpdateDetail = "Changements"
    override val functionThresholdBtnTitle = "Accepter et mettre à jour"
    override val functionThresholdNeedSoftwareUsageAgreementForRestoration =
        "Vous devez accepter les conditions d'utilisation du logiciel pour pouvoir rétablir le micrologiciel."
    override val functionThresholdNeedSoftwareUsageAgreementForRestorationUnit =
        "Vous devez accepter les conditions d'utilisation du logiciel pour pouvoir rétablir le micrologiciel de l'unité."
    override val commonsPreset = "Pré-régler"
    override val presetTopMenuLoadFileBtn = "Chargement d'un fichier de réglages"
    override val presetTopMenuLoadFromBikeBtn = "Chargement des réglages à partir du vélo"
    override val presetDflyCantSet =
        "La configuration de l'unité connectée ne prend pas en charge les réglages D-FLY.\nLes réglages D-FLY ne peuvent pas être écrits."
    override val presetSkipUnitBelow = "L'unité suivante sera ignorée."
    override val presetNoSupportedFileVersion =
        "Un fichier de pré-réglage créé dans une version antérieure à {0} ne peut pas être utilisé avec le type de vélo actuel."
    override val presetPresetTitleShiftMode = "Mode de changement de vitesse"
    override val presetConnectedUnit = "Connectez une unité."
    override val presetEndConnected = "Connecté"
    override val presetErrorWhileReading =
        "Une erreur s'est produite lors de la lecture des réglages."
    override val presetOverConnect =
        "{1} unités de {0} sont connectés.\nConnectez simplement le nombre choisi de {0}."
    override val presetChangeSwitchType = "{0} a été détecté. Passer à {1} ?"
    override val presetLackOfTargetUnits =
        "Une unité nécessaire à la configuration actuelle d'unités manque.\nPassez en revue la configuration de l'unité et modifiez les réglages de nouveau."
    override val presetLoadFileTopMessage =
        "Sélectionnez le fichier de préréglages à charger ou supprimer à partir de la liste suivante de fichiers de préréglages."
    override val presetLoadFileModifiedOn = "Date de mise à jour"
    override val presetLoadFileConfimDeleteFile =
        "Le fichier {0} sélectionné sera supprimé. \nContinuer ?"
    override val presetLoadFileErrorDeleteFile =
        "L'application n'a pas réussi à supprimer le fichier de préréglages."
    override val presetLoadFileErrorLoadFile = "La lecture du fichier de pré-réglage a échoué."
    override val presetLoadFileErrorNoSetting =
        "Les réglages d'une unité compatible avec le type de vélo actuel ne sont pas inclus."
    override val presetLoadFileErrorIncompatibleSetting =
        "Les réglages d'une unité non compatible avec le type de vélo actuel sont inclus.\nVoulez-vous ignorer les réglages des unités non compatibles ?"
    override val presetLoadFileErrorShortageSetting =
        "Des éléments de réglage ont été ajoutés/supprimés.\nVérifiez à nouveau les valeurs du fichier de préréglage, enregistrez le fichier et essayez à nouveau d'enregistrer."
    override val presetLoadFileErrorUnanticipatedSetting =
        "Une valeur non prévue a été lue lors de la lecture de la valeur définie.\nVérifiez à nouveau les valeurs du fichier de préréglage, enregistrez le fichier et essayez à nouveau d'enregistrer."
    override val presetWriteToBikeBan =
        "Les réglages du Synchronized shift ne peuvent pas être enregistrés car les réglages du Synchronized shift et ceux du dérailleur arrière ne correspondent pas. \nContinuer ?"
    override val presetWriteToBikePoint =
        "Les réglages du Synchronized shift ne peuvent pas être enregistrés car le point de changement de vitesse est en dehors de la plage de réglage valide. \nContinuer ?"
    override val presetWriteToBikeNoSettingToWrite =
        "Il n'y a pas d'information de réglage à enregistrer."
    override val presetQEndPreset = "Désactiver la connexion et terminer la configuration ?"
    override val commonsSynchromap = "Mode de changement de vitesse{0}"
    override val commonsSetting = "Paramètre"
    override val commonsDirection = "Sens"
    override val commonsChangePointFd = "Point de changement de vitesses pour FD"
    override val commonsChangePointRd = "Point de changement de vitesses pour RD"
    override val commonsAimPoint = "Position de la vitesse cible pour RD"
    override val commonsDirectionUp = "Sens HAUT"
    override val commonsDirectionDown = "Sens BAS"
    override val switchFunctionSettingDuplicationCheckMsg1 =
        "{1} est réglé sur les multiples boutons de {0}."
    override val switchFunctionSettingDuplicationCheckMsg3 =
        "Attribuez un réglage différent à chaque bouton."
    override val presetSaveFileOk =
        "L'enregistrement du fichier de pré-réglage s'est achevé avec succès."
    override val presetSaveFileError = "L'enregistrement du fichier de pré-réglage a échoué."
    override val presetFile = "Fichier"
    override val presetSaveCheckErrorChangeSetting = "Modifiez les réglages."
    override val presetSaveCheckErrorSemiSynchro =
        "Le changement de vitesse Semi-Synchronized Shift est réglé sur une valeur incorrecte."
    override val presetConnectionNotPresetUnitConnected = "Impossible de reconnaître l'unité."
    override val presetConnectionSameUnitsConnectionError =
        "Des unités identiques ont été détectées. La fonction de pré-réglage n'accepte pas les unités identiques."
    override val presetNeedFirmwareUpdateToPreset =
        "Les réglages n'ont pas pu être enregistrés depuis la ou les unité(s) suivante(s), parce que le firmware n'est pas équipé de la toute dernière version."
    override val presetUpdateFirmwareToContinuePreset =
        "Pour continuer à faire des préréglages, veuillez effectuer une mise à jour."
    override val presetOldFirmwareUnitsExist =
        "Le micrologiciel des unités suivantes n'est plus à jour. \nMettre à jour le micrologiciel maintenant ?"
    override val presetOldFirmwareUnitsExistAndStopPreset =
        "Les réglages n'ont pas pu être enregistrés depuis la ou les unité(s) suivante(s), parce que le firmware n'est pas équipé de la toute dernière version.\nMettez à jour le firmware de la ou des unité(s) à enregistrer."
    override val presetDonotShowNextTimeCheckTitle =
        "Ne pas afficher cet écran ultérieurement (sélectionner uniquement quand les mêmes réglages seront réutilisés)."
    override val presetUpdateBtnTitle = "Mise à jour"
    override val presetPresetOntyRecognizedUnit =
        "Enregistrer uniquement les réglages de l'unité reconnue ?"
    override val presetNewFirmwareUnitsExist =
        "Le préréglage ne peut pas être configuré car le micrologiciel de l'unité est plus récent que l'application. \nMettez à jour le micrologiciel de l'application vers la nouvelle version."
    override val presetNewFirmwareUnitsExistAndStopPreset =
        "Il peut être impossible de configurer normalement les réglages car le micrologiciel de l'unité est plus récent que l'application. \nLe processus est en cours d'annulation."
    override val presetSynchronizedShiftSettings = "Réglages Synchronized shift "
    override val presetSemiSynchronizedShiftSettings = "Réglages Semi-Synchronized Shift "
    override val presetQContinue = "Continuer ?"
    override val presetWriteToBikeNoUnit =
        "{1} ne peuvent pas être enregistrés car {0} n'est pas connecté."
    override val presetWriteToBikeToothSettingNotMatch =
        "{0} ne peuvent pas être enregistrés vers S1 ou S2 car la denture dans le fichier de réglage diffère de la denture de l'unité connectée."
    override val loginViewForgetPwdTitle = "Si vous avez oublié votre mot de passe"
    override val loginViewForgetIdTitle = "Vous avez oublié votre identifiant ?"
    override val loginViewCreateUserContent =
        "*Si vous souhaitez que l'enregistrement de l'utilisateur soit effectué par le fabricant, contactez le distributeur local ou le personnel Shimano."
    override val loginInternetConnection = "Connexion au réseau impossible."
    override val loginAccountIsExist =
        "Ce compte est verrouillé.  Essayez à nouveau de vous connecter ultérieurement."
    override val loginCorrectIdOrPassword =
        "L'identifiant utilisateur ou le mot de passe entré est incorrect."
    override val loginPasswordErrorMessage = "Le mot de passe est incorrect."
    override val loginPasswordExpired =
        "Votre mot de passe provisoire a expiré.\nRecommencez le processus d'enregistrement."
    override val loginPasswordExpiredChange =
        "Votre mot de passe n'a pas été modifié depuis {0} jours.\nModifiez votre mot de passe."
    override val loginChange = "Modifier"
    override val loginLater = "Plus tard"
    override val loginViewFinishLogin = "Vous vous êtes connecté."
    override val loginErrorAll =
        "Problème avec le contenu saisi. Veuillez vérifier les éléments ci-dessous."
    override val loginViewUserId = "Identifiant d'utilisateur"
    override val loginViewEmail = "Adresse de courrier électronique"
    override val loginViewPassword = "Mot de passe"
    override val loginViewCountry = "Pays"
    override val loginLoginMove = "Pour vous connecter"
    override val loginMailSendingError =
        "Impossible d'envoyer le courrier électronique. Patientez quelques instants, puis réessayez."
    override val loginAlphanumeric = "demi-largeur des caractères alphanumériques"
    override val loginTitleForgetPassword = "Réinitialisez votre mot de passe"
    override val loginViewForgetPasswordTitleFir =
        "Vous recevrez par courrier électronique les informations nécessaires pour définir un nouveau mot de passe."
    override val loginViewForgetMailSettingTitle =
        "Vérifiez les réglages à l'avance, de manière à pouvoir recevoir \"{0}\"."
    override val loginTitleForgetMailAdress = "Adresse de courrier électronique enregistrée"
    override val loginUserIdAndEmailDataNotExist =
        "L'identifiant utilisateur ou l'adresse de courrier électronique entré est incorrect."
    override val loginViewForgotPassword =
        "Message électronique du mot de passe ré-initialisé envoyé."
    override val loginTitleChangePassword = "Changez de mot de passe"
    override val loginViewCurrentPassword = "Mot de passe actuel"
    override val loginViewNewPassword = "Le nouveau mot de passe"
    override val loginErrorCurrentPasswordNonFormat =
        "Le mot de passe actuel peut uniquement contenir des caractères alphanumériques."
    override val loginErrorCurrentPasswordLength =
        "Le mot de passe actuel comprend entre {0} et {1} caractères."
    override val loginErrorNewPasswordNonFormat =
        "Le nouveau mot de passe peut uniquement contenir des caractères alphanumériques."
    override val loginErrorNewPasswordLength =
        "Le nouveau mot de passe comprend entre  {0} et {1} caractères."
    override val loginErrorPasswordNoDifferent =
        "Votre nouveau mot de passe ne peut pas être le même que votre mot de passe actuel."
    override val loginErrorPasswordNoConsistent =
        "Le nouveau mot de passe ne correspond pas au mot de passe de confirmation."
    override val loginViewFinishPasswordChange = "Votre mot de passe a été modifié."
    override val loginTitleLogOut = "Déconnexion"
    override val loginViewLogOut = "Vous vous êtes déconnecté."
    override val loginViewForgetIdTitleFir =
        "Votre identifiant utilisateur vous sera transmis par courrier électronique."
    override val loginTitleForgetId = "Notification de l'identifiant utilisateur"
    override val loginViewForgetTitleSec =
        "*Si vous ne pouvez pas utiliser l'adresse de courrier électronique enregistrée, il ne sera pas possible de vous renvoyer votre {0} car votre identité ne pourra pas être vérifiée. Enregistrez-vous en tant que nouvel utilisateur."
    override val loginEmailDataNotExist =
        "L'adresse de courrier électronique n'a pas été enregistrée."
    override val loginViewForgotUserId =
        "Votre identifiant utilisateur vous a été transmis par courrier électronique."
    override val loginTitleUserInfo = "Demande d'informations utilisateur"
    override val loginViewAddress = "Adresse"
    override val loginUserDataNotExist = "Échec de l'obtention des informations de l'utilisateur."
    override val commonsSet = "Régler"
    override val commonsAverageVelocity = "Vitesse moyenne"
    override val commonsBackLightBrightness = "Luminosité"
    override val commonsBackLightBrightnessLevel = "Niveau {0}"
    override val commonsCadence = "Cadence"
    override val commonsDistanceUnit = "Unité"
    override val commonsDrivingTime = "Temps de parcours"
    override val commonsForAssist = "pour Assistance"
    override val commonsForShift = "pour Changement"
    override val commonsInvalidate = "Non"
    override val commonsKm = "Unités internationales"
    override val commonsMaxGearUnit = "vitesses"
    override val commonsMaximumVelocity = "Vitesse maxi"
    override val commonsMile = "Système de mesure anglo-saxon"
    override val commonsNotShow = "Ne pas afficher"
    override val commonsShow = "Afficher"
    override val commonsSwitchDisplay = "Commutation de l'affichage"
    override val commonsTime = "Réglage de l'heure"
    override val commonsValidate = "Oui"
    override val commonsNowTime = "Heure"
    override val commonsValidateStr = "Valide"
    override val commonsInvalidateStr = "Non valide"
    override val commonsAssistModeBoostRatio = "Mode d'assistance BOOST"
    override val commonsAssistModeTrailRatio = "Mode d'assistance TRAIL"
    override val commonsAssistModeEcoRatio = "Mode d'assistance ECO"
    override val commonsStartMode = "Start mode (Mode de demarrage)"
    override val commonsAutoGearChangeModeLog = "Fonction de changement de vitesse automatique"
    override val commonsAutoGearChangeAdjustLog = "Moment du changement"
    override val commonsBackLight = "Paramètres du rétro-éclairage"
    override val commonsFontColor = "Couleur police"
    override val commonsFontColorBlack = "Noir"
    override val commonsFontColorWhite = "Blanc"
    override val commonsInteriorTransmission = "Moyeu à vitesses intégrées (chaîne)"
    override val commonsInteriorTransmissionBelt = "Moyeu à vitesses intégrées (courroie)"
    override val commonsRangeOverview = "Apperçu des fonctions"
    override val commonsUnknown = "Imprécis"
    override val commonsOthers = "Autres"
    override val commonsBackLightManual = "Manuel"
    override val commonsStartApp = "Démarrer"
    override val commonsSkipTutorial = "Ignorer le tutoriel"
    override val powerMeterMonitorPower = "Alimentation"
    override val powerMeterMonitorCadence = "Cadence"
    override val powerMeterMonitorPowerUnit = "W"
    override val powerMeterMonitorCadenceUnit = "rpm"
    override val powerMeterMonitorCycleComputerConnected = "Revenez à computer"
    override val powerMeterMonitorGuideTitle = "Réalisation de l'étalonnage de décalage zéro"
    override val powerMeterMonitorGuideMsg =
        "1.Placez le vélo sur une surface plane.\n2.Positionnez la manivelle de sorte qu'elle soit perpendiculaire au sol comme dans l'illustration.\n3.Appuyez sur le bouton \"Étalonnage de décalage zéro\".\n\nNe placez pas les pieds sur les pédales ou n'appliquez aucune charge sur le pédalier."
    override val powerMeterLoadCheckTitle = "Mode de contrôle de charge"
    override val powerMeterLoadCheckUnit = "N"
    override val powerMeterFewCharged = "Niveau faible de la batterie. Chargez la batterie."
    override val powerMeterZeroOffsetErrorTimeout =
        "La connexion au compteur électrique a échoué à cause d'une mauvaise connexion sans fil.\nEffectuez la procédure dans un endroit permettant une meilleure réception sans fil."
    override val powerMeterZeroOffsetErrorBatteryPowerShort =
        "Charge insuffisante de la batterie. Chargez la batterie et réessayez."
    override val powerMeterZeroOffsetErrorSensorValueOver =
        "Le pédalier est peut être chargé. Retirez toute charge du pédalier et réessayez."
    override val powerMeterZeroOffsetErrorCadenceSignalChange =
        "Le pédalier s'est peut-être déplacé. Retirez les mains du pédalier et réessayez."
    override val powerMeterZeroOffsetErrorSwitchOperation =
        "L'interrupteur a peut-être été actionné. Retirez les mains de l'interrupteur et réessayez."
    override val powerMeterZeroOffsetErrorCharging = "Débranchez le câble de charge et réessayez."
    override val powerMeterZeroOffsetErrorCrankCommunication =
        "Il se peut que le connecteur du pédalier gauche soit détaché. Déposer le bouchon extérieur, contrôlez que le connecteur est connecté et réessayez."
    override val powerMeterZeroOffsetErrorInfo =
        "Reportez-vous au manuel Shimano pour plus de détails."
    override val drawerMenuLoadCheck = "Mode de contrôle de charge"
    override val commonsMonitor = "Mode de surveillance"
    override val commonsZeroOffsetSetting = "étalonnage de décalage zéro"
    override val commonsGroupPowermeter = "compteur électrique"
    override val commonsPresetError =
        "Le compteur électrique ne peut pas être préréglé.\nConnectez-le à une autre unité."
    override val commonsWebPageTitleAdditionalFunction = "E-TUBE PROJECT|Avis important"
    override val commonsWebPageTitleFaq = "E-TUBE PROJECT|FAQ"
    override val commonsWebPageTitleGuide = "E-TUBE PROJECT|Comment utiliser E-TUBE PROJECT"
    override val connectionReconnectingBle =
        "La connexion Bluetooth® LE a été interrompue.\nEssai de reconnexion en cours."
    override val connectionCompleteReconnect = "Reconnexion terminée."
    override val connectionFirmwareWillBeUpdated = "Le micrologiciel sera mis à niveau."
    override val connectionFirmwareUpdatedFailed =
        "Reconnectez l'unité.\nSi l'erreur se reproduit, il peut y avoir un défaut.\nContactez votre distributeur ou revendeur."
    override val settingMsg7 =
        "Synchroniser automatiquement le réglage du temps de l’ordinateur du vélo."
    override val commonsDestinationType = "Type {0}"
    override val customizeSwitchSettingViewControllerMsg15 = "Contacteur d'assistance"
    override val presetNeedToChangeSettings =
        "Une fonction qui ne peut être utilisée a été configurée. Modifiez les fonctions utilisables dans Personnaliser, puis réessayez."
    override val connectionErrorSelectNoDetectedMsg1 =
        "La restauration est impossible s'il n'existe aucun nom d'unité à sélectionner."
    override val connectionTurnOnGps =
        "Pour activer la connexion à une unité sans fil, ACTIVEZ les informations sur l'emplacement du dispositif."
    override val drawerMenuAboutEtube = "A propos de E-TUBE PROJECT"
    override val commonsX2Y2Notice =
        "* Si le réglage de X2/Y2 est défini sur « Ne pas utiliser » dans {0}, le dispositif ne sera pas reconnu, même si vous appuyez sur X2/Y2."
    override val powerMeterZeroOffsetComplete = "Résultat du zéro offset"
    override val commonsMaxAssistSpeed = "Vitesse maximale avec l'assistance"
    override val commonsShiftingAdvice = "Changement conseillé"
    override val commonsSpeedAdjustment = "Afficher vitesse"
    override val customizeSpeedAdjustGuideMsg =
        "En cas de différence avec d'autres indicateurs de vitesse, réglez-le."
    override val commonsMaintenanceAlertDistance =
        "Distance à parcourir avant l'alerte de maintenance"
    override val commonsMaintenanceAlertDate = "Date d'alerte de maintenance"
    override val commonsAssistPatternComfort = "COMFORT"
    override val commonsAssistPatternSportive = "SPORTIVE"
    override val commonsEbike = "E-BIKE"
    override val connectionDialogMessage20 =
        "{0} possède un commutateur de réglage pour l'assistance. Modifiez les réglages."
    override val commonsItIsNotPossibleToSetAnythingOtherThanTheSpecifiedValue =
        "Si {0} est défini sur la fonction d'assistance uniquement\nseuls les éléments suivants peuvent être définis :\nInterrupteur X : Augmenter Assistance\nInterrupteur Y : Diminuer Assistance"
    override val switchFunctionSettingDuplicationCheckMsg5 =
        "La fonction de suspension ne peut pas être définie en parallèle d'une autre fonction."
    override val switchFunctionSettingDuplicationCheckMsg6 =
        "Seules les combinaisons suivantes peuvent être définies\npour le modèle SW-E6000 :\nDérailleur arrière haut\nDérailleur arrière bas\nÉcran\nou\nAugmenter Assistance\nDiminuer Assistance\nAffichage/Lumière"
    override val commonsGroupSw = "contacteur"
    override val customizeWarningMaintenanceAlert =
        "Ce réglage d'alerte de maintenance permet d'afficher les alertes. Continuer ?"
    override val customizeCommunicationModeSetting = "Réglages du mode de communication sans fil"
    override val customizeCommunicationModeCustomizeItemTitle =
        "Mode de communication sans fil (pour les compteurs)"
    override val customizeCommunicationModeAntAndBle = "Mode ANT/Bluetooth® LE"
    override val customizeCommunicationModeAnt = "Mode ANT"
    override val customizeCommunicationModeBle = "Mode Bluetooth® LE"
    override val customizeCommunicationModeOff = "Mode Arrêt"
    override val customizeCommunicationModeGuideMsg =
        "Conservez le niveau de consommation de la batterie en alignant les réglages avec le système de communication du compteur. En cas de connexion à un compteur avec un système de communication différent, réinitialisez E-TUBE PROJECT.\nE-TUBE PROJECT pour tablettes et smartphones peut être utilisé avec n'importe quel système de communication configuré."
    override val presetCantSet = "Les éléments suivants ne peuvent pas être définis pour {0}."
    override val presetWriteToBikeInvalidSettings = "{0} est connectée."
    override val presetDuMuSettingNotice =
        "Les réglages de l’unité de commande ne correspondent pas à ceux de l'unité du moteur. "
    override val presetDuSettingDifferent = "L'unité de commande n'est pas réglée pour {0}."
    override val commonsAssistLockChainTension = "Avez-vous réglé la tension de chaîne?"
    override val commonsAssistLockInternalGear =
        "Lors de l'utilisation d'un moyeu à vitesses intégrées, il est nécessaire d'ajuster la tension de chaîne."
    override val commonsAssistLockInternalGear2 =
        "Réglez la tension de la chaîne, puis appuyez sur le bouton Exécuter."
    override val commonsAssistLockCrank = "Avez-vous vérifié l'angle du pédalier?"
    override val commonsAssistLockCrankManual =
        "La manivelle gauche doit être installée sur l'axe à l'angle approprié. Vérifiez l'angle de la manivelle installée, puis appuyez sur le bouton Exécuter."
    override val switchFunctionSettingDuplicationCheckMsg7 =
        "La même fonction ne peut être attribuée à plusieurs boutons."
    override val commonsMaintenanceAlertUnitKm = "km"
    override val commonsMaintenanceAlertUnitMile = "mile"
    override val commonsMaxAssistSpeedUnitKm = "km/h"
    override val commonsMaxAssistSpeedUnitMph = "mph"
    override val firmwareUpdateBlefwUpdateFailed =
        "La mise à jour du micrologiciel de {0} a échoué."
    override val firmwareUpdateShowFaq = "Afficher les informations détaillées"
    override val commonsMaintenanceAlert = "Alerte maintenance"
    override val commonsOutOfSettableRange = "{0} est en dehors de la plage autorisée."
    override val commonsCantWriteToUnit =
        "Il est impossible d'écrire dans {0} avec les réglages actuels."
    override val connectionScanModalText =
        "Il est impossible d'établir une connexion en utilisant E-TUBE RIDE. Déconnectez E-TUBE RIDE et l'unité sans fil dans l'application, puis réessayez de les connecter.\n Sinon, définissez l'unité sans fil du vélo sur le mode de connexion Bluetooth® LE."
    override val connectionUsingEtubeRide = "Lorsque E-TUBE RIDE est utilisé."
    override val connectionScanModalTitle = "Lorsque E-TUBE RIDE est utilisé."
    override val firmwareUpdateCommunicationIsUnstable =
        "L'environnement de réception sans fil est instable.\n Rapprochez l'unité sans fil de l'appareil et améliorez l'environnement de réception sans fil."
    override val connectionQBleFirmwareRestore =
        "Une unité sans fil défectueuse a été trouvée.\n Restaurer le firmware ?"
    override val commonsAutoGearChange = "Changement de vitesse automatique"
    override val firmwareUpdateBlefwUpdateRestoration =
        "Rétablissez la connexion via Bluetooth® LE, puis passez à la procédure de restauration."
    override val firmwareUpdateBlefwUpdateReconnectFailed =
        "Impossible de reconnecter Bluetooth® LE automatiquement après avoir mis à jour {0}."
    override val firmwareUpdateBlefwResotreReconnectFailed =
        "Impossible de reconnecter Bluetooth® LE automatiquement après avoir restauré {0}."
    override val firmwareUpdateBlefwUpdateTryConnectingAgain =
        "Mise à jour terminée. Pour continuer à utiliser, reconnectez d'abord Bluetooth® LE."
    override val firmwareUpdateBlefwResotreTryConnectingAgain =
        "Restauration terminée. Pour continuer à utiliser, reconnectez d'abord Bluetooth® LE."
    override val connectionConnectionToOsConnectingUnitSucceeded =
        "L'E-TUBE PROJECT connecté à {0} ne s'affiche pas à l'écran."
    override val connectionSelectYesToContinue =
        "Pour continuer la connexion, sélectionnez \"Confirmer\"."
    override val connectionSelectNoToConnectOtherUnit =
        "Pour déconnecter la connexion actuelle et connecter à une autre unité, sélectionnez \"Annuler\"."
    override val commonsCommunicationIsNotStable =
        "La réception sans fil n'est pas stable.\n Rapprochez {0} de l'appareil pour améliorer la réception, puis modifiez les paramètres à nouveau."

    //region 共通. common[カテゴリ][キーワード]
    // 一般
    override val commonOn = "MARCHE"
    override val commonOff = "ARRÊT"
    override val commonManual = "Manuel"
    override val commonInvalid = "Non valide"
    override val commonValid = "Valide"
    override val commonRecover = "Récupérer"
    override val commonRecoverEnter = "Récupérer (Entrée)"
    override val commonNotRecover = "Ne pas récupérer"
    override val commonOKEnter = "OK (ENTRÉE)"
    override val commonCancel = "Annuler"
    override val commonCancelEnter = "Annuler (ENTRÉE)"
    override val commonUpdate = "Mettre à jour (Entrée)"
    override val commonNotUpdate = "Ne pas mettre à jour"
    override val commonDisconnect = "DÉCONNECTER"
    override val commonSelect = "Sélectionner"
    override val commonYes = "Oui"
    override val commonYesEnter = "Oui (ENTRÉE)"
    override val commonNo = "Non"
    override val commonNoEnter = "Non (ENTRÉE)"
    override val commonBack = "Retour"
    override val commonRestore = "Retour"
    override val commonSave = "Enregistrer (Entrée)"
    override val commonCancelEnterUppercase = "ANNULER (ENTRÉE)"
    override val commonAbort = "Annuler"
    override val commonApplyEnter = "Refléter (Entrée)"
    override val commonNext = "Suivant (Entrée)"
    override val commonStartEnter = "Démarrer (Entrée)"
    override val commonClear = "Effacer"
    override val commonConnecting = "Connexion en cours..."
    override val commonConnected = "Connecté"
    override val commonDiagnosing = "Diagnostic en cours..."
    override val commonProgramming = "Paramètres en cours..."
    override val commonRetrievingUnitData = "Récupération des données de l'unité..."
    override val commonGetRetrieving = "Chargement des données..."
    override val commonRemainingSeconds = "{0} secondes restantes."
    override val commonTimeLimit = "Limite de temps : {0} sec."
    override val commonCharging = "Charge en cours…"
    override val commonBeingPerformed = "Pendant l'exécution des tâches"
    override val commonNotRun = "Non exécuté"
    override val commonAbnormal = "Erreur"
    override val commonCompletedNormally = "Exécution terminée normalement"
    override val commonCompletedWithError = "Exécution terminée avec des erreurs"
    override val commonEBikeReport = "RAPPORT E-BIKE"
    override val commonBicycleInformation = "Informations sur le vélo"
    override val commonSkipUppercase = "IGNORER"
    override val commonSkipEnter = "IGNORER (ENTER)"
    override val commonWaitingForJudgment = "En attente de jugement"
    override val commonStarting = "Démarrer {0}."
    override val commonCompleteSettingMessage = "Les paramètres se sont terminés normalement."
    override val commonProgrammingErrorMessage =
        "Une erreur s'est produite pendant l'exécution des paramètres."
    override val commonSettingValueGetErrorMessage =
        "Une erreur s'est produite pendant la récupération des valeurs de réglage."
    override val commonSettingUpMessage =
        "Paramètres en cours. Ne déconnectez pas avant que les paramètres ne soient terminés."
    override val commonCheckElectricWireMessage =
        "Vérifiez que le câble électrique est correctement branché."
    override val commonElectlicWireIsNotDisconnectedMessage =
        "Effectuer un contrôle d'erreur si le câble électrique n'est pas débranché."
    override val commonErrorCheckResults = "Résultats du contrôle d'erreurs"
    override val commonErrorCheckCompleteMessage = "Le contrôle d'erreurs du {0} a été effectué."
    override val commonCheckCompleteMessage = "Contrôle de {0} est terminé."
    override val commonFaultCouldNotBeFound = "Une défaillance n'a pas pu être trouvée."
    override val commonFaultMayExist = "Une défaillance peut exister."
    override val commonMayBeFaulty = "{0} est peut-être défectueux."
    override val commonItemOfMayBeFaulty = "Le {0}ième élément de {1} est peut-être défectueux."
    override val commonSkipped = "Ignoré"
    override val commonAllUnitErrorCheckCompleteMessage =
        "Le contrôle d'erreurs est terminé pour toutes les unités."
    override val commonConfirmAbortWork = "Voulez-vous arrêter le processus ?"
    override val commonReturnValue = "Les valeurs précédentes seront restaurées."
    override val commonManualFileIsNotFound = "Le fichier manuel est introuvable."
    override val commonReinstallMessage = "Désinstallez {0}, puis réinstallez-le."
    override val commonWaiting = "Pendant la veille - ÉTAPE {0}"
    override val commonDiagnosisInProgress = "Diagnostic en cours - STEP {0}"
    override val commonNotPerformed = "Non exécuté"
    override val commonDefault = "Par défaut"
    override val commonDo = "Oui"
    override val commonNotDo = "Non"
    override val commonOEMSetting = "Réglage OEM"
    override val commonTotal = "Total"
    override val commonNow = "Courant"
    override val commonReset = "Réinitialiser"
    override val commonEnd = "Terminer (Entrée)"
    override val commonComplete = "Terminer"
    override val commonNextSwitch = "Vers le contacteur suivant"
    override val commonStart = "Démarrer"
    override val commonCountdown = "Compte à rebours"
    override val commonAdjustmentMethod = "Méthode de réglage"
    override val commonUnknown = "Imprécis"
    override val commonUnitRecognition = "Vérification de la connexion"

    // 単位
    override val commonUnitStep = "{0} vitesses"
    override val commonUnitSecond = "sec."

    // 公式HPの言語設定
    override val commonHPLanguage = "fr-FR"

    // アシスト
    override val commonAssistAssistOff = "Assistance éteinte"

    // その他
    override val commonDelete = "Supprimer"
    override val commonApply = "APPLIQUER"
    //endregion

    //region ユニット. unit[カテゴリ][キーワード]
    //region 一般
    override val unitCommonFirmwareVersion = "Version du firware"
    override val unitCommonSerialNo = "N° de série"
    override val unitCommonFirmwareUpdate = "Mise à jour du micrologiciel"
    override val unitCommonUpdateToTheLatestVersion = "Mettez à jour à la dernière version."
    override val unitCommonFirmwareUpdateNecessary =
        "Au besoin, mettez à jour à la dernière version."
    //endregion

    //region 一覧表示
    override val unitUnitBattery = "Batterie"
    override val unitUnitDI2Adapter = "Adaptateur DI2"
    override val unitUnitInformationDisplay = "Affichage des informations"
    override val unitUnitJunctionA = "Raccord A"
    override val unitUnitRearSuspension = "Suspension arrière"
    override val unitUnitFrontSuspension = "Suspension avant"
    override val unitUnitDualControlLever = "Manette à commande double"
    override val unitUnitShiftingLever = "Levier de vitesse"
    override val unitUnitSwitch = "Interrupteur"
    override val unitUnitBatterySwitch = "Interrupteur de batterie"
    override val unitUnitPowerMeter = "Capteur de puissance"
    //endregion

    //region DU
    override val unitDUYes = "Oui"
    override val unitDUNO = "Non"
    override val unitDUDestination = "Destination"
    override val unitDURemainingLightCapacity = "Capacité d’éclairage restante"
    override val unitDUType = "Type {0}"
    override val unitDUPowerTerminalOutputSetting =
        "Paramètre de sortie des éléments lumineux/de la source d'alimentation accessoire"
    override val unitDULightONOff = "Éclairage ACTIVÉ/DÉSACTIVÉ"
    override val unitDULightConnection = "Connexion de l'éclairage"
    override val unitDUButtonOperation = "Fonctionnement des boutons"
    override val unitDUAlwaysOff = "Toujours DÉSACTIVÉ"
    override val unitDUButtonOperations = "Fonctionnement des boutons"
    override val unitDUAlwaysOn = "Toujours ACTIVÉ"
    override val unitDUSetValue = "Valeur définie"
    override val unitDUOutputVoltage = "Tension de sortie"
    override val unitDUWalkAssist = "Assistance à la marche"
    override val unitDULightOutput = "Intensité lumineuse"
    override val unitDUBatteryCapacity = "Capacité de la batterie pour le lumière restante"
    override val unitDUTireCircumference = "Circonférence du pneu"
    override val unitDUGearShiftingType = "Type de changement de vitesses"
    override val unitDUShiftingMethod = "Méthode de changement de vitesse"
    override val unitDUFrontChainRing = "Nombre de dents du plateau avant"
    override val unitDURearSprocket = "Nombre de dents du pignon arrière"
    override val unitDUToothSelection = "Sélection de dents"
    override val unitDUInstallationAngle = "Angle d'instalation de l'unité motrice"
    override val unitDUAssistCustomize = "Personnalisation de l'assistance"
    override val unitDUProfile1 = "Profil 1"
    override val unitDUProfile2 = "Profil 2"
    override val unitDUCostomize = "Personnaliser"
    override val unitDUAssistCharacter = "Type d'assistance"
    override val unitDUPowerful = "POWERFUL"
    override val unitDUMaxTorque = "Couple maximal"
    override val unitDUAssistStart = "Démarrage de l'assistance"
    override val untDUMild = "MILD"
    override val unitDUQuick = "QUICK"
    override val unitDUAssistLv = "Nv.{0:D}"
    override val unitDUAssitLvForMobile = "Nv.{0}"
    override val unitDUAssistLvParen = "Nv.{0} ({1})"
    override val unitDUOther = "Autre"
    override val unitDU1stGear = "1er rapport"
    override val unitDU2ndGear = "2e rapport"
    override val unitDU3rdGear = "3e rapport"
    override val unitDU4thGear = "4e rapport"
    override val unitDU5thGear = "5e rapport"
    override val unitDU6thGear = "6e rapport"
    override val unitDU7thGear = "7e rapport"
    override val unitDU8thGear = "8e rapport"
    override val unitDU9thGear = "9e rapport"
    override val unitDU10thGear = "10e rapport"
    override val unitDU11thGear = "11e rapport"
    override val unitDU12thGear = "12e rapport"
    override val unitDU13thGear = "13e rapport"
    override val unitDUShiftModeAfterDisconnect =
        "Mode après déconnexion de l'application"
    override val unitDUAutoGearShiftAdjustment = "Réglage de changement de vitesse automatique"
    override val unitDUShiftingAdvice = "Changement conseillé"
    override val unitDUTravelingDistance = "Distance de déplacement"
    override val unitDUTravelingDistanceMaintenanceAlert = "Distance parcourue (Alerte entretien)"
    override val unitDUDateYear = "Date : Année (Alerte entretien)"
    override val unitDUDateMonth = "Date : Mois (Alerte entretien)"
    override val unitDUDateDay = "Date : Jour (Alerte entretien)"
    override val unitDUTotalDistance = "Distance totale"
    override val unitDUTotalTime = "Temps total"
    override val unitDURemedy = "Solution"
    //endregion

    //region BT
    override val unitBTTimes = "Temps"
    override val unitBTRemainingCapacity = "État de charge"
    override val unitBTFullChargeCapacity = "Batterie complétement chargée"

    override val unitBTSupportedByShimano = "SUPPORTED BY SHIMANO"
    //endregion

    //region SC, EW
    override val unitSCEWMode = "mode"
    override val unitSCEWModes = "modes"
    override val unitSCEWDisplayTime = "Heure d'affichage : {0}"
    override val unitSCEWBeep = "Bip : {0}"
    override val unitSCEWBeepSetting = "Réglage du bip"
    override val unitSCEWWirelessCommunication = "Communication sans fil"
    override val unitSCEWCommunicationModeOff = "ARRÊT"
    override val unitSCEWBleName = "Bluetooth® LE nom\n"
    override val unitSCEWCharacterLimit = "1 à 8 caractères alphanumériques de demi-largeur"
    override val unitSCEWPasskeyDescription =
        "Nombre à 6 chiffres commençant par un autre chiffre que 0"
    override val unitSCEWConfirmation = "Confirmation"
    override val unitSCEWPassKeyInitialization = "Initialisation de la clé PassKey"
    override val unitSCEWEnterPasskey =
        "Entrez un nombre à 6 chiffres commençant par un autre chiffre que 0."
    override val unitSCEWNotMatchPassKey = "Le PassKey ne correspond pas."
    override val unitSCEWDisplayUnits = "Unités d'affichage"
    override val unitSCEWInternationalUnits = "Unités internationales"
    override val unitSCEWDisplaySwitchover = "Changement de vitesse"
    override val unitSCEWRangeOverview = "Mode"
    override val unitSCEWAutoTimeSetting = "Heure (auto)"
    override val unitSCEWManualTimeSetting = "Heure (manuel)"
    override val unitSCEWUsePCTime = "Utiliser l'heure du PC"
    override val unitSCEWTimeSetting = "Réglage de l'heure"
    override val unitSCEWDoNotSet = "Ne pas régler"
    override val unitSCEWBacklight = "Rétroéclairage"
    override val unitSCEWBacklightSetting = "Paramètres du rétro-éclairage"
    override val unitSCEWManual = "Manuel"
    override val unitSCEWBrightness = "Luminosité"
    override val unitSCEWFont = "Police"
    override val unitSCEWSet = "Régler"
    //endregion

    //region MU
    override val unitMUGearPosition = "Position de la vitesse"
    override val unitMUAdjustmentSetting = "Paramètres de réglage"

    override val unitMU5thGear = "5e rapport"
    override val unitMU8thGear = "8e rapport"
    override val unitMU11thGear = "11e rapport"
    //endregion
    //endregion

    //region SUS
    override val unitSusCD = "CD"
    override val unitSusCtd = "CTD"
    override val unitSusCtdBV = "CTD (BV)"
    override val unitSusCtdOrDps = "CTD (DISH) ou DPS"
    override val unitSusPositionFront = "Position{0} avant"
    override val unitSusPositionRear = "Position{0} arrière"
    override val unitSusPositionDisplayedOnSC = "Position{0} affichée sur le SC"
    override val unitSusC = "C"
    override val unitSusT = "T"
    override val unitSusD = "D"
    override val unitSusDUManufacturingSerial = "Numéro de série de fabrication du DU"
    override val unitSusBackupDateAndTime = "Date et heure de sauvegarde"
    //endregion

    //region ST,SW
    override val unitSTSWUse2ndGgear = "Utiliser la 2e vitesse"
    override val unitSTSWSwitchMode = "Mode interrupteur"
    override val unitSTSWForAssist = "pour Assistance"
    override val unitSTSWForShift = "pour Changement"
    override val unitSTSWUse = "Utilisation"
    override val unitSTSWDoNotUse = "Ne pas utiliser"
    //endregion

    //region Shift
    override val unitShiftCopy = "Copier {0}"
    override val unitShiftShiftUp = "Passage à la vitesse supérieure"
    override val unitShiftShiftDown = "Passage à la vitesse inférieure"
    override val unitShiftGearNumberLimit = "Limite du nombre de vitesses"
    override val unitShiftMultiShiftGearNumberLimit =
        "Limite du nombre de vitesses (Changement de vitesse multiple)"
    override val unitShiftInterval = "Intervalle de changement de vitesse"
    override val unitShiftMultiShiftGearShiftingInterval =
        "Intervalle de changement de vitesse (Changement de vitesse multiple)"
    override val unitShiftAutomaticGearShifting = "Vitesse auto"
    override val unitShiftMultiShiftFirstGearShiftingPosition =
        "Position de changement de 1ère vitesse (Changement de vitesse multiple)"
    override val unitShiftMultiShiftSecondGearShiftingPosition =
        "Position de changement de 2e vitesse (Changement de vitesse multiple)"
    override val unitShiftSingleShiftingPosition = "Position de changement de vitesse unique"
    override val unitShiftDoubleShiftingPosition = "Position de changement de vitesse double"
    override val unitShiftShiftInterval = "Intervalle de changement de vitesse"
    override val unitShiftRemainingBatteryCapacity = "Capacité de batterie restante"
    override val unitShiftBatteryMountingForm = "Configuration de la fixation de la batterie"
    override val unitShiftPowerSupply = "Avec alimentation externe"
    override val unitShift20PercentOrLess = "20 % ou moins"
    override val unitShift40PercentOrLess = "40 % ou moins"
    override val unitShift60PercentOrLess = "60 % ou moins"
    override val unitShift80PercentOrLess = "80 % ou moins"
    override val unitShift100PercentOrLess = "100 % ou moins"
    override val unitShiftRearShiftingUnit = "Unité de changement de vitesse arrière"
    override val unitShiftAssistShiftSwitch =
        "Contacteur\nd'assistance/Manette\nDe changement de vitesse"
    override val unitShiftInner = "Intérieur"
    override val unitShiftMiddle = "Central"
    override val unitShiftOuter = "Extérieur"
    override val unitShiftUp = "Haut"
    override val unitShiftDown = "Bas"
    override val unitShiftGearShiftingInterval = "Intervalle de changement de vitesse"
    //endregion

    //region PC接続機器
    override val unitPCErrorCheck = "Contrôle d'erreurs"
    override val unitPCBatteryConsumptionCheck = "Vérification de la consommation de batterie"
    override val unitPCBatteryConsumption = "Consommation de la batterie"
    //endregion

    //region エラーチェック
    override val unitErrorCheckMalfunctionInsideProsduct = "Y a-t-il des défaillances internes ?"
    override val unitErrorCheckBatteryConnectedProperly =
        "La batterie est-elle connectée correctement ?"
    override val unitErrorCheckCanDetectTorque = "Le couple peut-il être détecté ?"
    override val unitErrorCheckCanDetectVehicleSpeed =
        "La vitesse du véhicule peut-elle être détectée ?"
    override val unitErrorCheckCanDetectCadence = "La cadence peut-elle être détectée ?"
    override val unitErrorCheckLightOperateNormally = "L'éclairage fonctionne-t-il correctement ?"
    override val unitErrorCheckOperateNormally = "Fonctionne-t-il correctement ?"
    override val unitErrorCheckDisplayOperateNormally = "L'écran fonctionne-t-il correctement ?"
    override val unitErrorCheckBackLightOperateNormally =
        "Le rétroéclairage fonctionne-t-il correctement ?"
    override val unitErrorCheckBuzzerOperateNormally = "Le son fonctionne-t-il correctement ?"
    override val unitErrorCheckSwitchOperateNormally =
        "Les contacteurs fonctionnent-ils correctement ?"
    override val unitErrorCheckwirelessFunctionNormally =
        "La fonction sans fil fonctionne-t-elle correctement ?"
    override val unitErrorCheckBatteryHasEnoughPower =
        "La batterie intégrée est-elle suffisamment chargée ?"
    override val unitErrorCheckNormalShiftToEachGear =
        "Les changements de vitesse se font-ils correctement ?"
    override val unitErrorCheckEnoughBatteryForShifting =
        "Le niveau de la batterie est-il suffisamment élevé pour le changement de vitesse ?"
    override val unitErrorCheckCommunicateNormallyWithBattery =
        "Les communications avec la batterie peuvent-elles être établies correctement ?"
    override val unitErrorCheckLedOperateNormally = "Les DEL fonctionnent-elles correctement ?"
    override val unitErrorCheckSwitchOperation = "Fonctionnement du contacteur"
    override val unitErrorCheckCrankArmPperation = "Fonctionnement de la manivelle"
    override val unitErrorCheckLcdCheck = "Contrôle de l'écran LCD"
    override val unitErrorCheckLedCheck = "Contrôle des DEL"
    override val unitErrorCheckAudioCheck = "Contrôle audio"
    override val unitErrorCheckWirelessCommunicationCheck = "Contrôle de la communication sans fil"
    override val unitErrorCheckPleaseWait = "Veuillez patienter."
    override val unitErrorCheckCheckLight = "Vérifier l'éclairage"
    override val unitErrorCheckSprinterSwitch = "Interrupteur régulateur"
    override val unitErrorCheckSwitch = "Interrupteur"
    //endregion
    //endregion

    //region M0 基本構成
    override val m0RecommendationOfAccountRegistration = "Recommandation d'enregistrement du compte"
    override val m0AccountRegistrationMsg =
        "Si vous créez un compte, vous pouvez utiliser E-TUBE PROJECT de façon plus conviviale.\n・Vous permet d'enregistrer votre vélo.\n・N'oubliez pas que l'unité sans fil facilite la connexion de votre vélo."
    override val m0SignUp = "INSCRIPTION"
    override val m0Login = "CONNEXION"
    override val m0SuspensionSwitch = "Interrupteur de suspension"
    override val m0MayBeFaulty = "{0} est peut-être défectueux."
    override val m0ReplaceOrRemoveMessage =
        "Remplacez ou supprimez l'unité suivante et connectez de nouveau.\n{0}"
    override val m0UnrecognizableMessage =
        "La manette de changement de vitesse ou le contacteur n'est pas reconnu.\nVérifiez que le câble électrique n'est pas déconnecté."
    override val m0UnknownUnit = "Unité inconnue {0}"
    override val m0UnitNotDetectedMessage =
        "Vérifiez que le câble électrique n'est pas retiré.\nDans ce cas, déconnectez l'unité puis reconnectez-la."
    override val m0ConnectErrorMessage =
        "Une erreur de communication s'est produite. Essayez de vous reconnecter."
    //endregion

    //region M1 起動画面
    override val m1CountryOfUse = "Pays d'utilisation"
    override val m1Continent = "Continents"
    override val m1Country = "Pays/Régions"
    override val m1OK = "OK"
    override val m1PersonalSettingsMsg =
        "Vous pouvez facilement régler le vélo en fonction de votre type de conduite."
    override val m1NewRegistration = "NOUVEL ENREGISTREMENT"
    override val m1Login = "CONNEXION"
    override val m1ForCorporateUsers = "Pour les utilisateurs professionnels"
    override val m1Skip = "Ignorer"
    override val m1TermsOfService = "Conditions d'utilisation"
    override val m1ComfirmLinkContentsAndAgree =
        "Cliquez sur le lien ci-après et acceptez le contenu."
    override val m1TermsOfServicePolicy = "Conditions d'utilisation"
    override val m1AgreeToTheTermsOfService = "J'accepte les conditions d'utilisation"
    override val m1AgreeToTheAll = "J'accepte tout"
    override val m1AppOperationLogAgreement =
        "Accord concernant le journal des opérations de l'application"
    override val m1AgreeToTheAppOperationLogAgreement =
        "J'accepte l'accord concernant le journal des opérations de l'application."
    override val m1DataProtecitonNotice = "Avis concernant la protection des données"
    override val m1AgreeUppercase = "ACCEPTER"
    override val m1CorporateLogin = "Identifiant d'entreprise"
    override val m1Email = "Adresse e-mail"
    override val m1Password = "Mot de passe"
    override val m1DownloadingLatestFirmware =
        "La dernière version du micrologiciel est en cours de téléchargement."
    override val m1Hi = "Bonjour, {0}"
    override val m1GetStarted = "Commencer"
    override val m1RegisterBikeOrPowerMeter = "Enregistrer le vélo ou le capteur de puissance"
    override val m1ConnectBikeOrPowerMeter = "Se connecter au vélo ou au capteur de puissance"
    override val m1BikeList = "Mon vélo"
    override val m1LastConnection = "Dernière connexion"
    override val m1Searching = "Recherche..."
    override val m1Download = "TÉLÉCHARGER"
    override val m1SkipUppercase = "IGNORER"
    override val m1Copyright = "©SHIMANO INC. TOUS DROITS RÉSERVÉS"
    override val m1CountryArea = "Pays/Régions"
    override val m1ConfirmContentMessage =
        "Si vous acceptez ces conditions générales, cochez la case et appuyez sur le bouton « Suivant » pour continuer."
    override val m1Next = "Suivant"
    override val m1Agree = "Accepter"
    override val m1ImageRegistrationFailed = "Échec d'enregistrement de l'image."
    override val m1InternetConnectionUnavailable =
        "Vous n'êtes pas connecté à Internet. Connectez-vous à Internet et réessayez."
    override val m1ResumeBikeRegistrationMessage =
        "Aucun vélo applicable détecté. Connectez-vous à Internet et réenregistrez le vélo."
    override val m1ResumeImageRegistrationMessage =
        "Une erreur inattendue s'est produite. Réenregistrez l'image."
    override val m1NoStorageSpaceMessage =
        "L'espace disponible sur votre smartphone est insuffisant."
    override val m1Powermeter = "Capteur de puissance | {0}"
    //endregion

    //region M2 ペアリング
    override val m2SearchingUnits = "Recherche des unités..."
    override val m2HowToConnectUnits = "Comment connecter les unités"
    override val m2Register = "Enregistrement"
    override val m2UnitID = "ID:{0}"
    override val m2Passkey = "PassKey"
    override val m2EnterPasskeyToRegisterUnit =
        "Entrez le PassKey pour enregistrer votre unité."
    override val m2OK = "OK"
    override val m2CancelUppercase = "ANNULER"
    override val m2ChangeThePasskey = "Modifier le PassKey ?"
    override val m2NotNow = "Pas maintenant"
    override val m2Change = "Modifier"
    override val m2Later = "Plus tard"
    override val m2NewPasskey = "Nouveau PassKey"
    override val m2PasskeyDescription =
        "Entrez un nombre à 6 chiffres commençant par un autre chiffre que 0"
    override val m2Cancel = "Annuler"
    override val m2Nickname = "PSEUDO"
    override val m2ConfirmUnits = "CONFIRMER LES UNITÉS"
    override val m2SprinterSwitchHasConnected = "La commande Sprinter a été connectée"
    override val m2SkipRegister = "Ignorer l'enregistrement"
    override val m2RegisterAsNewBike = "ENREGISTRER EN TANT QUE NOUVEAU VÉLO"
    override val m2Left = "Gauche"
    override val m2Right = "Droite"
    override val m2ID = "ID"
    override val m2Update = "METTRE À JOUR"
    override val m2Customize = "PERSONNALISER"
    override val m2Maintenance = "ENTRETIEN"
    override val m2Connected = "Connecté"
    override val m2Connecting = "Connexion en cours..."
    override val m2AddOnRegisteredBike = "Ajouter un vélo enregistré"
    override val m2AddPowermeter = "Ajouter capteur de puissance"
    override val m2Monitor = "CONTRÔLER"
    override val m2ConnectTheBikeAndClickNext =
        "CONNECTEZ LE VÉLO ET CLIQUEZ SUR « SUIVANT »."
    override val m2Next = "SUIVANT"
    override val m2ChangePasskey = "Modifier le PassKey"
    override val m2PowerMeter = "Ensemble capteur de puissance"
    override val m2CrankArmSet = "manivelle"
    override val m2NewBicycle = "Nouveau vélo"
    override val m2ChangeWirelessUnit = "Modifier l'unité sans fil"
    override val m2DeleteConnectionInformationMessage =
        "Après avoir effectué des modifications, effacez les informations de connexion sous [Réglages] > [Bluetooth] sur votre appareil.\nSi les informations de connexion ne sont pas effacées, elles ne seront pas renouvelées et aucune connexion ne pourra alors être établie."
    override val m2Help = "Aide"
    override val m2PairingCompleted = "Appariement terminé"
    override val m2AddSwitch = "Ajouter un contacteur"
    override val m2PairDerailleurAndSwitch =
        "Appairez un contacteur avec le dérailleur. *Le dérailleur peut être commandé par le contacteur une fois le vélo déconnecté d’E-TUBE PROJECT."
    override val m2HowToPairing = "Méthode d’appariement"
    override val m2IdInputAreaHint = "Ajouter un contacteur"
    override val m2InvalidQRCodeErrorMsg =
        "Scannez le QR code sur le contacteur. Il se peut qu’un autre QR code ait été scanné."
    override val m2QRScanMsg =
        "Appuyez sur l’écran pour cibler. Si le QR code est illisible, entrez l’identifiant à 11 chiffres."
    override val m2QRConfirm = "Emplacement de l’identifiant/QR code"
    override val m2SerialManualInput = "Saisie manuelle de l’ID"
    override val m2CameraAccessGuideMsgForAndroid =
        "L’accès à l’appareil photo n’est pas autorisé. Dans les paramètres de l’application, autorisez l’accès pour permettre l’appariement du contacteur."
    override val m2CameraAccessGuideMsgForIos =
        "L’accès à l’appareil photo n’est pas autorisé. Autorisez l’accès dans les paramètres de l’application. Lorsque les paramètres sont modifiés pour autoriser l’accès, l’application redémarre."
    override val m2AddWirelessSwitches = "Ajouter un contacteur sans fil"
    //endregion

    //region M3 接続と切断
    override val m3Searching = "Recherche..."
    override val m3Connecting = "Connexion en cours..."
    override val m3Connected = "Connecté"
    override val m3Detected = "Détecté"
    override val m3SearchOff = "ARRÊT"
    override val m3Disconnect = "DÉCONNECTER"
    override val m3Disconnected = "Déconnecté"
    override val m3RecognizedUnits = "Unités reconnues"
    override val m3Continue = "Continuer"

    //region 互換性確認
    override val m3CompatibilityTable = "Tableau de compatibilité"
    override val m3CompatibilityErrorSCM9051OrSCMT800WithWirelessUnit =
        "Pour les unités suivantes, seule 1 unité de chaque unité peut être connectée."
    override val m3CompatibilityErrorUnitSpecMessage1 =
        "La combinaison reconnue d'unités n'est pas compatible. Connectez les unités conformément au tableau de compatibilité."
    override val m3CompatibilityErrorUnitSpecMessage2 =
        "Vous pouvez remplir les exigences de compatibilité en retirant les unités rouges suivantes."
    override val m3CompatibilityErrorShouldUpdateFWMessage1 =
        "Le micrologiciel des unités suivantes n'est plus à jour."
    override val m3CompatibilityErrorShouldUpdateFWMessage2 =
        "Ce problème peut parfois être résolu par une mise à jour du micrologiciel permettant une compatibilité étendue.\nEssayez de mettre à jour le micrologiciel."

    // 今後文言が変更になる可能性があるが、現状ではTextTableに合わせて実装
    override val m3CompatibilityErrorDUAndBT =
        "La combinaison reconnue d'unités n'est pas compatible. Connectez les unités conformément au tableau de compatibilité."
    //endregion
    //endregion

    //region M4 アップデート
    override val m4Latest = "Dernière"
    override val m4UpdateAvailable = "METTRE À JOUR"
    override val m4UpdateAllUnits = "TOUT METTRE À JOUR"
    override val m4UpdateAnyUnits = "MISE À JOUR | TEMPS ESTIMÉ - {0}"
    override val m4History = "HISTORIQUE"
    override val m4AllUpdateConfirmTitle = "Tout mettre à jour ?"
    override val m4AllUpdateConfirmMessage = "Temps estimé : {0}"
    override val m4SelectAll = "SÉLECTIONNER TOUT (ENTRÉE)"
    override val m4EstimatedTime = "Temps estimé : "
    override val m4Cancel = "ANNULER"
    override val m4BleVersion = "Bluetooth® LE ver.{0}"
    override val m4OK = "OK (ENTRÉE)"
    override val m4Update = "Mise à jour"
    override val m4UpdateUppercase = "MISE À JOUR"

    // TODO:TextTableには表記がないので要確認
    override val m4Giant = "GIANT"
    override val m4System = "SYSTEM"
    //endregion

    //region M5 カスタマイズTOP
    override val m5EBike = "E-BIKE"
    override val m5Assist = "Assistance"
    override val m5MaximumAssistSpeed = "vitesse maximale avec l'assistance : {0}"
    override val m5AssistPattern = "modèle d'assistance : {0}"
    override val m5RidingCharacteristic = "caractéristique de conduite : {0}"
    override val m5CategoryShift = "Changement de vitesse"
    override val m5Synchronized = "Synchronized"
    override val m5AutoShift = "Vitesse auto"
    override val m5Advice = "Conseil"
    override val m5Switch = "Interrupteur"
    override val m5ForAssist = "Pour assistance"
    override val m5Suspension = "Suspension"
    override val m5ControlSwitch = "Interrupteur de commande"
    override val m5CategoryDisplay = "Affichage"
    override val m5SystemInformation = "Informations système"
    override val m5Information = "Affichage des"
    override val m5DisplayTime = "Temps d'affichage : {0}"
    override val m5Mode = "Mode {0}"
    override val m5Other = "Autres"
    override val m5WirelessSetting = "Paramètres sans fil"
    override val m5Fox = "FOX"
    override val m5ConnectBikeToApplyChangedSettings =
        "Connectez le vélo pour appliquer la nouvelle configuration."
    override val m5CannotBeSetMessage = "Certaines fonctions ne sont pas réglables."
    override val m5GearShiftingInterval = "Intervalle de changement de vitesse : {0}"
    override val m5GearNumberLimit = "Limite du nombre de vitesses : {0}"

    /**
     * NOTE: 本来カスタマイズ画面で改行した形で表示する必要があるが、翻訳が日本語しかないため仮でラベルを二つ使う形で実装。
     * そのとき上で定義しているものとは文言が異なる可能性があるため、新しく定義
     * */
    override val m5Shift = "shift"
    override val m5Display = "informations"
    //endregion

    //region M6 バイク設定
    override val m6Bike = "Vélo"
    override val m6Nickname = "Pseudo"
    override val m6ChangeWirelessUnit = "Modifier l'unité sans fil"
    override val m6DeleteUnits = "Supprimer les unités"
    override val m6DeleteBike = "Supprimer le vélo"
    override val m6Preset = "Pré-régler"
    override val m6SaveCurrentSettings = "Enregistrer la configuration actuelle"
    override val m6WriteSettings = "Écrire la configuration"
    override val m6SettingsCouldNotBeApplied = "Impossible d'appliquer la configuration."
    override val m6DeleteUppercase = "SUPPRIMER"
    override val m6Save = "ENREGISTRER"
    override val m6ReferenceBike = "Vélo de référence"
    override val m6PreviousData = "Données précédentes"
    override val m6SavedSettings = "Configuration enregistrée"
    override val m6LatestSetting = "Dernière configuration"
    override val m6Delete = "Supprimer"
    override val m6BikeSettings = "Configuration du vélo"
    override val m6WritingSettingIsCompleted = "La configuration a été écrite."
    override val m6Other = "Autres"

    //region M7 カスタマイズ詳細
    override val m7TwoStepShiftDescription =
        "Position permettant de régler le changement de vitesse multiple"
    override val m7FirstGear = "1re vitesse"
    override val m7SecondGear = "2e vitesse"
    override val m7OtherSwitchMessage =
        "Lorsque vous actionnez le changement de vitesse multiple avec un contacteur connecté à un élément autre que SW-M9050, passez la 2e vitesse."
    override val m7NameDescription = "8 caractères maximum"
    override val m7WirelessCommunication = "Communication sans fil"
    override val m7Bike = "Vélo"
    override val m7Nickname = "Pseudo"
    override val m7ChangeWirelessUnit = "Modifier l'unité sans fil"
    override val m7DeleteUnit = "Supprimer l'unité"
    override val m7DeleteBikeRegistration = "Supprimer l'enregistrement du vélo"
    override val m7Setting = "Paramètre"
    override val m7Mode = "Mode"
    override val m7AlwaysDisplay = "Toujours afficher"
    override val m7Auto = "Auto"
    override val m7Manual = "Manuel"
    override val m7MaximumAssistSpeed = "Vitesse maximale avec l'assistance"
    override val m7ShiftingAdvice = "Changement conseillé"
    override val m7ShiftingTiming = "Moment du changement"
    override val m7StartMode = "Mode de démarrage"
    override val m7ConfirmConnectSprinterSwitch = "La commande Sprinter est-elle connectée ?"
    override val m7Reflect = "Refléter (Entrée)"
    override val m7Left = "Gauche"
    override val m7Right = "Droite"
    override val m7SettingsCouldNotBeApplied = "Impossible d'appliquer la configuration."
    override val m7Preset = "PRÉRÉGLER"
    override val m7SaveCurrentSettings = "Enregistrer la configuration actuelle"
    override val m7WriteSettings = "Écrire la configuration"
    override val m7EditSettings = "Modifier la configuration"
    override val m7ExportSavedSettings = "Exporter la configuration enregistrée"
    override val m7Name = "Nom"
    override val m7SettingWereNotApplied = "La configuration n'a pas été appliquée."
    override val m7DragAndDropFile = "Glissez-déposez le fichier de configuration ici."
    override val m7CharactersLimit = "1 à 8 caractères alphanumériques de demi-largeur"
    override val m7PasskeyDescriptionMessage =
        "Saisissez 6 caractères numériques.\n0 ne peut pas être défini comme premier caractère dans le PassKey sauf pendant l'initialisation de la clé PassKey."
    override val m7Display = "Afficher"
    override val m7CompleteSettingMessage = "Les paramètres se sont terminés normalement."

    override val m7ConfirmCancelSettingsTitle = "Configuration de la pause"
    override val m7ConfirmDefaultTitle = "Réinitialiser la configuration par défaut"
    override val m7PasskeyDescription =
        "Entrez un nombre à 6 chiffres commençant par un autre chiffre que 0"
    override val m7AssistCharacterWithValue = "Type d'assistance : Nv.{0}"
    override val m7MaxTorqueWithValue = "Couple maximal : {0} Nm"
    override val m7AssistStartWithValue = "Démarrage de l'assistance : Nv.{0}"
    override val m7CustomizeDisplayName = "Personnaliser le nom d’affichage"
    override val m7DisplayNameDescription = "Jusqu’à 10 caractères"
    override val m7AssistModeDisplay = "Affichage du mode d’assistance"
    //endregion

    //region M8 カスタマイズ-スイッチ
    override val m8OptionalSwitchTitle = "Contacteur facultatif"
    override val m8SettingColudNotBeApplied = "Impossible d'appliquer la configuration."
    override val m8MixedAssistShiftCheckMessage =
        "Augmenter assistance, réduire assistance, affichage/éclairage et dérailleur arrière haut, dérailleur arrière bas, affichage, vous ne pourrez pas régler le canal D-FLY sur un seul contacteur."
    override val m8EmptyConfigurableListMessage = "Aucun élément configurable."
    override val m8NotApplySettingMessage = "Certains boutons n'exécutent pas leurs fonctions."
    override val m8PressSwitchMessage = "Appuyez sur le contacteur de l'unité à sélectionner."
    override val m8SetToGearShifting = "Régler pour le changement de vitesse"
    override val m8UseX2Y2 = "Utiliser la 2e vitesse"
    override val m8Model = "Modèle"
    override val m8CustomizeSusSwitchPosition = "Régler sur la configuration de la position"
    override val m8SetReverseOperation = "Inverser le fonctionnement sur le contacteur Y1"
    override val m8PortA = "Port A"
    override val m8PortB = "Port B"
    override val m8NoUseMultiShift = "Ne pas utiliser le changement de vitesse à 2 rapports"
    override val m8SetReverce = "Affectez la fonction opposée à {0}"
    override val m8ConfirmReadDcasWSwitchStatusText = "Chargez le paramètre actuel. Appuyez sur un bouton sur {0} ({1})."
    override val m8ConfirmWriteDcasWSwitchStatusText = "Enregistrez les paramètres. Appuyez sur un bouton sur {0} ({1})."

    //region M9 カスタマイズ詳細-シンクロシフト
    override val m9SynchronizedShiftTitle = "Synchronized shift"
    override val m9TeethPatternSelectorHeaderTitle = "Nombre de dents"
    override val m9Cancel = "Annuler"
    override val m9Casette = "Cassette"
    override val m9Edit = "Édition"
    override val m9FC = "Manivelle"
    override val m9CS = "Cassette"
    override val m9NoSetting = "Aucune configuration"
    override val m9Table = "Tableau"
    override val m9Animation = "Animation"
    override val m9DisplayConditionConfirmationMessage =
        "Impossible de configurer Synchronized shift. Reportez-vous au site Web d'E-TUBE PROJECT."
    override val m9NotApplicableMessage =
        "Vérifiez les éléments suivants avant d'appliquer la configuration."
    override val m9NotApplicableUnselectedMessage = "S1 ou S2 n'est pas réglé."
    override val m9NotApplicableUnsettableValueMessage =
        "Le point de changement de vitesse de la configuration du Synchronized shift se situe dans une plage hors réglage."
    override val m9SettingFileUpperLimitMessage =
        "La limite de stockage du fichier de configuration a été dépassée."
    override val m9FrontUp = "Avant haut"
    override val m9FrontDown = "Avant bas"
    override val m9RearUp = "Rapport arrière haut {0}"
    override val m9RearDown = "Rapport arrière bas {0}"
    override val m9Crank = "Le pédalier"
    override val m9Option = "Option"
    override val m9NotApplicableDifferentGearPositionControlSettingsMessage =
        "Si les valeurs réglées pour le nombre de dents et le contrôle de la vitesse engagée diffèrent entre S1 et S2, la configuration ne peut pas être appliquée."

    // TODO: TextTableには表記がないので要確認
    override val m9Name = "Nom"

    //region M10 メンテナンス
    override val m10Battery = "Batterie"
    override val m10DerailleurAdjustment = "Réglage du dérailleur"
    override val m10Front = "Avant"
    override val m10Rear = "Arrière"
    override val m10NotesOnDerailleurAdjustmentMsg =
        "Le réglage du dérailleur implique de tourner le pédalier à la main. And the like mounting a bike maintenance stand, please create an environment. Veillez à ne pas rester coincé dans les engrenages."
    override val m10NotAskAgain = "Ne pas afficher cela la prochaine fois"
    override val m10Step = "Étape {0}/{1}"
    override val m10BeforePerformingElectricAdjustmentMsg =
        "Avant d'exécuter les réglages électriques avec l'application, vous devez exécuter un réglage à l'aide du boulon de réglage situé sur le dérailleur avant. Consultez l'aide pour en savoir plus."
    override val m10MovementOfPositionOfDerailleurMsg =
        "Déplacez le dérailleur comme indiqué sur l'illustration."
    override val m10ShiftStartMesssage =
        "Le changement de vitesse est actionné. Veuillez tourner le pédalier."
    override val m10SettingChainAndFrontDerailleurMsg =
        "Réglez l'écart entre la chaîne et le dérailleur avant entre 0 et 0,5mm."
    override val m10Adjust = "Réglage : {0}"
    override val m10Gear = "Position de la vitesse"
    override val m10ErrorLog = "Journal d'erreurs"
    override val m10AdjustTitle = "Réglage"

    override val m10Restrictions = "Restrictions"
    override val m10Remedy = "Solution"

    override val m10Status = "État"
    override val m10DcasWBatteryLowVoltage =
        "Le niveau de batterie est faible. Remplacez la batterie."
    override val m10DcasWBatteryUpdateInfo =
        "Vérifiez le niveau de la batterie. Appuyez sur un bouton sur {0} ({1})."
    override val m10LeverLeft = "Levier de gauche"
    override val m10LeverRight = "Levier de droite"

    // Error Message
    override val m10ErrorMessageSensorAbnormality =
        "Une anomalie du capteur a été détectée dans l'unité motrice."
    override val m10ErrorMessageSensorFailure =
        "Une défaillance du capteur a été détectée dans l'unité motrice."
    override val m10ErrorMessageMotorFailure =
        "Détection d'une défaillance du moteur dans l'unité motrice."
    override val m10ErrorMessageAbnormality = "Une anomalie a été détectée dans l'unité motrice."
    override val m10ErrorMessageNotDetectedSpeedSignal =
        "Le capteur de vitesse n'a détecté aucun signal de vitesse du véhicule."
    override val m10ErrorMessageAbnormalVehicleSpeedSignal =
        "Le capteur de vitesse a détecté un signal anormal de vitesse du véhicule."
    override val m10ErrorMessageBadCommunicationBTAndDU =
        "Erreur de communication entre la batterie et l’unité motrice."
    override val m10ErrorMessageDifferentShiftingUnit =
        "Une unité de changement de vitesse différente de celle configurée dans le système est installée."
    override val m10ErrorMessageDUFirmwareAbnormality =
        "Anomalie détectée dans le micrologiciel de l’unité motrice."
    override val m10ErrorMessageVehicleSettingsAbnormality =
        "Une anomalie a été détectée dans la configuration du véhicule."
    override val m10ErrorMessageSystemConfiguration = "Erreur due à la configuration du système."
    override val m10ErrorMessageAbnormalHighTemperature =
        "Détection d'une température anormalement élevée dans l'unité motrice."
    override val m10ErrorMessageNotCompletedSensorInitialization =
        "L'initialisation du capteur n'a pas pu se terminer normalement."
    override val m10ErrorMessageUnexpectBTDisconnected =
        "Coupure inattendue de l’alimentation détectée."
    override val m10ErrorMessagePowerOffDueToOverTemperature =
        "L'alimentation a été éteinte, car la température dépasse la plage de fonctionnement garantie."
    override val m10ErrorMessagePowerOffDueToCurrentLeakage =
        "L'alimentation a été éteinte, car une fuite de courant a été détectée dans le système."

    // Error Restrictions
    override val m10ErrorRestrictionsUnableUseAssistFunction =
        "Impossible d'utiliser la fonction d'assistance."
    override val m10ErrorRestrictionsNotStartAllFunction = "Aucune fonction système au démarrage."
    override val m10ErrorRestrictionLowerAssistPower =
        "Baisse inhabituelle de la puissance d'assistance."
    override val m10ErrorRestrictionLowerMaxAssistSpeed =
        "Baisse inhabituelle de la vitesse d'assistance maximale."
    override val m10ErrorRestrictionNoRestrictedAssistFunction =
        "Il n'y a pas de fonctions d'assistance limitées lorsque c'est affiché."
    override val m10ErrorRestrictionUnableToShiftGears = "Impossible de changer de vitesse."

    // Error Remedy
    override val m10ErrorRemedyContactPlaceOfPurchaseOrDistributor =
        "Contactez le lieu d'achat ou distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyTurnPowerOffAndThenOn =
        "Coupez, puis rétablissez l'alimentation. Si l'erreur persiste, cessez de l'utiliser et contactez le lieu d'achat ou un distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyReInstallSpeedSensorAndRestoreIfModifiedForPlaceOfPurchase =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Installer l'aimant et le capteur de vitesse aux emplacements appropriés. \n• Si le vélo a été modifié, restaurez sa configuration d'usine. \nSuivez les instructions ci-dessus et roulez quelques instants sur le vélo pour supprimer l'erreur. Si l'erreur persiste, ou si les informations ci-dessus ne sont pas pertinentes, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyCheckDUAndBTIsConnectedCorrectly =
        "Demandez à votre revendeur de \n･ Vérifier que le câble entre l'unité motrice et la batterie est correctement branché."
    override val m10ErrorRemedyCheckTheSettingForPlaceOfPurchase =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Se connecter à E-TUBE PROJECT et vérifier la configuration. Si la configuration ne correspond pas à l'état du véhicule, passez en revue l'état du véhicule. \nSi l'erreur persiste, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyRestoreFirmwareForPlaceOfPurchase =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Se connecter à E-TUBE PROJECT et restaurer le micrologiciel. \nSi l'erreur persiste, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyContactBicycleManufacturer =
        "Prenez contact avec le fabricant du vélo."
    override val m10ErrorRemedyWaitTillTheDUTemperatureDrops =
        "Ne roulez pas avec le mode d'assistance activé tant que la température de l'unité motrice n'a pas diminué. \nSi l'erreur persiste, contactez le lieu d'achat ou un distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyReinstallSpeedSensorForPlaceOfPurchase =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Installer le capteur de vitesse à l'emplacement approprié. \n• Installer l'aimant à l'emplacement approprié. (Reportez-vous à la rubrique « Frein à disque » du chapitre « Opérations générales » ou au manuel du revendeur pour connaître la procédure d'installation de l'aimant démonté.) \nSi l'erreur persiste, ou si les informations ci-dessus ne sont pas pertinentes, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyTorqueSensorOffsetW013 =
        "Appuyez sur le bouton d'alimentation de la batterie pour couper l'alimentation, puis la rétablir sans poser les pieds sur les pédales.\nSi l'erreur persiste, contactez le lieu d'achat ou un distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyTorqueSensorOffset =
        "• Si l'unité motrice est une DU-EP800 (l'ordinateur de bord affiche W103) : tournez les manivelles à l'envers deux ou trois fois. \n• Si l'unité motrice est un modèle différent (l'ordinateur de bord affiche W013) : appuyez sur le bouton d'alimentation de la batterie pour couper l'alimentation, puis la rétablir sans poser les pieds sur les pédales. \nSi l'erreur persiste après avoir fait ce qui précède, contactez le lieu d'achat ou un distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyBTConnecting =
        "Coupez, puis rétablissez l'alimentation. \nSi W105 s'affiche fréquemment, demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Éliminer le cliquetis au niveau de la fixation de la batterie et veiller à ce qu'elle soit correctement attachée. \n• Vérifier si le cordon d'alimentation est endommagé. Dans ce cas, remplacez-le avec la pièce requise.\nSi l'erreur persiste, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyOverTemperature =
        "Si la température de la batterie est supérieure à celle recommandée (risquant d'occasionner une décharge), laissez la batterie dans un endroit frais, à l'abri des rayons directs du soleil, jusqu'à ce que sa température interne diminue suffisamment. Si la température de la batterie est inférieure à celle recommandée (risquant d'occasionner une décharge), laissez la batterie à l'intérieur jusqu'à ce que sa température interne soit à la température appropriée. \nSi l'erreur persiste, contactez le lieu d'achat ou un distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyReplaceShiftingUnit =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes : \n• Vérifier l'état actuel du système dans E-TUBE PROJECT et la remplacer avec l'unité de changement de vitesse appropriée.\nSi l'erreur persiste, contactez votre distributeur pour bénéficier d'une assistance."
    override val m10ErrorRemedyCurrentLeakage =
        "Demandez au magasin où vous avez acheté votre vélo d'effectuer les opérations suivantes :  \nDéposer les composants autres que l’unité motrice un à la fois et rétablir l’alimentation.  \n• L'alimentation est coupée pendant le démarrage si le composant qui cause le problème n'a pas été retiré. • L'alimentation est démarrée et W104 continue à s'afficher si le composant qui cause le problème a été retiré. • Une fois que vous avez identifié l'élément qui cause le problème, remplacez cet élément."
    override val m10ErrorTorqueSensorOffsetAdjust =
        "Appuyez sur le bouton d'alimentation de la batterie pour couper l'alimentation, puis la rétablir sans poser les pieds sur les pédales. Si l'avertissement persiste après le premier essai, répétez la même opération jusqu'à ce qu'il disparaisse. \n*Le capteur s'initialise également lorsque le vélo est en mouvement. Si vous continuez à utiliser le vélo, la situation peut s'améliorer. \n\nSi la situation ne s'améliore pas, demandez ce qui suit auprès de votre lieu d'achat ou d'un distributeur. \n• Ajustez la tension de la chaîne.\n• Avec la roue arrière levée, faites tourner les pédales pour lancer la roue arrière. Modifiez la position d'arrêt des pédales pendant que la roue tourne jusqu'à ce que l'avertissement disparaisse."

    // TODO: フランス語訳の確認
    override val m10MotorUnitShift = "Motor Unit Shift"
    //endregion

    //region M11 パワーメーター
    override val m11Name = "Nom"
    override val m11Change = "Modifier"
    override val m11Power = "PUISSANCE"
    override val m11Cadence = "CADENCE"
    override val m11Watt = "watt"
    override val m11Rpm = "tpm"
    override val m11Waiting = "Attendez..."
    override val m11CaribrationIsCompleted = "L'étalonnage est terminé."
    override val m11CaribrationIsFailed = "Échec de l'étalonnage."
    override val m11N = "N"
    override val m11PowerMeter = "Capteur de puissance"
    override val m11ZeroOffsetCaribration = "Étalonnage de décalage zéro"
    override val m11ChangeBike = "Changer de vélo"
    override val m11CancelPairing = "Annuler l'appariement"
    override val m11Set = "Régler"
    override val m11RegisteredBike = "Vélo enregistré"
    override val m11RedLighting = "Niveau de batterie du compteur de puissance : \n1 - 5 %"
    override val m11RedBlinking = "Niveau de batterie du compteur de puissance : \nMoins de 1 ％"
    //endregion

    //region M12 アプリケーション設定
    override val m12Acconut = "Compte"
    override val m12ShimanoIDPortal = "PORTAIL D'IDENTIFICATION SHIMANO"
    override val m12Setting = "Paramètre"
    override val m12ApplicationSettings = "Paramètres de l'application"
    override val m12AutoBikeConnection = "Connexion auto du vélo"
    override val m12Language = "Langue"
    override val m12Link = "Lien"
    override val m12ETubeProjectWebsite = "Site Web d'E-TUBE PROJECT"
    override val m12ETubeRide = "E-TUBE RIDE"
    override val m12Other = "Autres"
    override val m12ETubeProjectVersion = "E-TUBE PROJECT Cyclist Ver.{0}"
    override val m12PoweredByShimano = "Propulsé par SHIMANO"
    override val m12SignUpLogIn = "Inscription/Connexion"
    override val m12CorporateClient = "Client d'entreprise"
    override val m12English = "Anglais"
    override val m12French = "Français"
    override val m12German = "Allemand"
    override val m12Dutch = "Néerlandais"
    override val m12Spanish = "Espagnol"
    override val m12Italian = "Italien"
    override val m12Chinese = "Chinois"
    override val m12Japanese = "Japonais"
    override val m12Change = "Modifier"
    override val m12AppOperationLogAgreement =
        "Accord concernant le journal des opérations de l'application"
    override val m12AgreeAppOperationLogAgreemenMsg =
        "J'accepte l'accord concernant le journal des opérations de l'application."
    override val m12DataProtecitonNotice = "Avis concernant la protection des données"
    override val m12Apache = "Apache"

    // TODO: 仮
    override val m12AppOperationLog = "アプリ操作ログ取得"

    override val m12ConfirmLogout =
        "Voulez-vous vous déconnecter ? Toutes les unités actuellement connectées seront aussi déconnectées."
    //endregion

    //region M13 ヘルプ
    override val m13Connection = "Connexion"
    override val m13Title = "Aide"
    override val m13Operation = "Fonctionnement"
    override val m13HowToConnectBike = "Comment connecter le vélo"
    override val m13SectionCustomize = "Personnaliser"
    override val m13SynchronizedShiftTitle = "Synchronized shift"
    override val m13Maintenance = "Entretien"
    override val m13FrontDerailleurAdjustment = "Réglage du dérailleur avant"
    override val m13RearDerailleurAdjustment = "Réglage du dérailleur arrière"
    override val m13ZeroOffsetCalibration = "Étalonnage de décalage zéro"
    override val m13SystemInformationDisplayTitle = "Pour l'affichage des informations"
    override val m13JunctionATitle = "Pour le raccord A"
    override val m13SCE8000Title = "Pour SC-E8000"
    override val m13CycleComputerTitle = "Pour le compteur/EW-EN100\n(hormis SC-E8000)"
    override val m13PowerMeterTitle = "Pour FC-R9100-P"
    override val m13SystemInformationDisplayDescription =
        "Maintenez enfoncé le sélecteur de mode du vélo jusqu'à ce que la lettre « c » s'affiche."
    override val m13ModeSwitchAnnotationRetention =
        "* Une fois le vélo prêt à être connecté, relâchez le sélecteur de mode ou le bouton. Si vous maintenez le sélecteur de mode ou le bouton enfoncé, le système passe à un autre mode."
    override val m13ShiftMode = "Mode de changement de vitesse"
    override val m13LongPress = "Maintenir enfoncé"
    override val m13Seconds = "{0} s ou plus"
    override val m13ConnectionMode = "Mode de connexion Bluetooth LE"
    override val m13AdjustMode = "Mode de réglage"
    override val m13RDProtectionResetMode = "Mode de réinitialisation dér arr"
    override val m13ExitModeDescription =
        "Maintenez enfoncé pendant au moins 0,5 s pour quitter le mode de réglage ou le mode de réinitialisation dér arr."
    override val m13JunctionADescription =
        "Maintenez enfoncé le bouton de raccord A jusqu'à ce que le témoin LED vert et le témoin LED rouge s'allument alternativement."
    override val m13HowToConnectWirelessUnit =
        "Lorsque vous maintenez A enfoncé avec le vélo à l'arrêt, l'écran Liste de menus apparaît sur le compteur. Appuyez sur X1 ou Y1 pour sélectionner Bluetooth LE, puis sur A pour confirmer. \nSur l'écran Bluetooth LE, sélectionnez Démarrer, puis appuyez sur A pour confirmer."
    override val m13CommunicationConditions =
        "Les communications sont autorisées aux conditions énoncées ci-dessous. \nVeuillez exécuter l'une des opérations suivantes."
    override val m13TurningOnStepsMainPower =
        "・ Pendant 15 secondes après la mise sous tension de SHIMANO STEPS."
    override val m13AnyButtonOperation =
        "・ Pendant 15 secondes après avoir actionné l'un des boutons (à l'exception du bouton d'alimentation principale de SHIMANO STEPS)."
    override val m13PowerMeterDescription = "Appuyez sur le bouton de l'unité de commande."
    override val m13SynchronizedShift = "Synchronized shift"
    override val m13SynchronizedShiftDetail =
        "Cette fonction active automatiquement la synchronisation du dérailleur avant avec le dérailleur arrière haut et le dérailleur arrière bas. "
    override val m13SemiSynchronizedShift = "Semi-Synchronized Shift"
    override val m13SemiSynchronizedShiftDetail =
        "Cette fonction actionne automatiquement le dérailleur arrière lorsque le dérailleur avant est commandé afin d'optimiser le passage des vitesses."
    override val m13Characteristics = "Caractéristiques"
    override val m13CharacteristicsFeature1 =
        "Le changement de vitesse multiple rapide est à présent activé.\n\n•Cette fonction vous permet de régler rapidement la vitesse de rotation de la manivelle (RPM) en réponse aux changements des conditions de conduite.\n•Vous permet de régler rapidement votre vitesse."
    override val m13CharacteristicsFeature2 =
        "Permet le fonctionnement du changement de vitesse multiple fiable"
    override val m13Note = "Remarque"
    override val m13OperatingPrecautionsUse1 =
        "1. Il est facile de changer une vitesse de trop.\n\n2. Si la vitesse de rotation de la manivelle (RPM) est faible, la chaîne ne pourra pas suivre le mouvement du dérailleur arrière.\nPar conséquent, la chaîne peut glisser sur les dents de pignon au lieu de s'engager dans le pignon de cassette."
    override val m13OperatingPrecautionsUse2 =
        "Le fonctionnement du changement de vitesse multiple dure un certain temps"
    override val m13Clank = "Vitesse de manivelle à utiliser"
    override val m13ClankFeature = "À une vitesse de rotation de la manivelle élevée (RPM)"
    override val m13AutoShiftOverview =
        "SHIMANO STEPS avec un moyeu à vitesses internes DI2 peut modifier automatiquement la vitesse de l'unité de changement de vitesse en fonction des conditions de déplacement."
    override val m13ShiftTiming = "Moment du changement : "
    override val m13ShiftTimingOverview =
        "Modifie la cadence standard du changement de vitesse automatique. Augmente la valeur configurée pour le pédalage rapide avec une charge légère. Réduit la valeur configurée pour le pédalage lent avec une charge modérée. "
    override val m13StartMode = "Mode de démarrage :"
    override val m13StartModeOverview =
        "Cette fonction réduit automatiquement la vitesse du rapport après arrêt à un feu de signalisation, etc., vous permettant ainsi de redémarrer sur un rapport préréglé inférieur. Cette fonction est utilisable avec le changement de vitesse automatique ou manuel."
    override val m13AssistOverview = "2 modes d'assistance sont proposés."
    override val m13Comfort = "CONFORT :"
    override val m13ComfortOverview =
        "permet une conduite plus fluide et offre vraiment la sensation d'utiliser un vélo classique avec le couple maximal de 50 Nm."
    override val m13Sportive = "SPORTIF :"
    override val m13SportiveOverview =
        "offre une assistance électrique permettant de gravir facilement les pentes avec le couple maximal de 60 Nm. (En fonction du modèle d'unité de changement de vitesse intégré, le couple maximal peut être contrôlé jusqu'à 50 Nm.)"
    override val m13RidingCharacteristicOverview =
        "3 modes de conduite sont proposés."
    override val m13Dynamic = "(1) DYNAMIQUE :"
    override val m13DynamicOverview =
        "les 3 niveaux d'assistance (ECO/TRAIL/BOOST) sont réglables à l'aide d'un contacteur. DYNAMIQUE offre la plus grande différence entre ces 3 niveaux d'assistance. Il vous assiste sur un E-MTB en mode ECO en proposant une meilleure assistance électrique que le mode ECO de la configuration EXPLORATION, TRAIL garantit un meilleur contrôle et BOOST une puissante accélération."
    override val m13Explorer = "(2) EXPLORATION : "
    override val m13ExplorerOverview =
        "EXPLORATION allie le contrôle de l'assistance électrique à une faible consommation de la batterie dans les 3 niveaux d'assistance. Cette configuration est adaptée sur les voies uniques."
    override val m13Customize = "(3) PERSONNALISER :"
    override val m13CustomizeOverview =
        "le niveau d'assistance souhaité est sélectionnable parmi LOW/MEDIUM/HIGH pour chacun des 3 niveaux d'assistance."
    override val m13AssistProfileOverview =
        "Vous pouvez créer 2 types de profils d'assistance à sélectionner. SC permet de passer d'un profil à l'autre. Un profil règle 3 paramètres pour chacun des 3 niveaux d'assistance (ECO/TRAIL/BOOST) que vous pouvez modifier avec un contacteur."
    override val m13AssistCharacter = "(1) Type d'assistance :"
    override val m13AssistCharacterOverview =
        "avec SHIMANO STEPS, le couple d'assistance est appliqué en fonction de la pression sur la pédale. Lorsque PUISSANCE est configuré, l'assistance est fournie même avec une faible pression sur la pédale. Lorsque ECO est configuré, l'équilibre entre le niveau d'assistance et la faible consommation de la batterie peut être optimisé."
    override val m13MaxTorque = "(2) Couple maximal :"
    override val m13MaxTorqueOverview =
        "la sortie du couple d'assistance maximal par l'unité motrice est modifiable."
    override val m13AssistStart = "(3) Démarrage de l'assistance :"
    override val m13AssistStartOverview =
        "Moment où l'assistance fournie est modifiable. Lorsque QUICK est configuré, l'assistance est fournie rapidement après l'activation du pédalier. Lorsque MILD est configuré, l'assistance est fournie lentement."
    override val m13DisplaySpeedAdjustment = "Afficher le réglage de la vitesse"
    override val m13DisplaySpeedAdjustmentOverview =
        "En présence d'une différence entre la vitesse affichée sur le compteur et la vitesse affichée sur votre appareil, réglez la vitesse affichée.\nLorsque vous augmentez la valeur en appuyant sur Assistance-X, la valeur de vitesse affichée augmente.\nLorsque vous diminuez la valeur en appuyant sur Assistance-Y, la valeur de vitesse affichée baisse."
    override val m13SettingIsNotAvailableMessage =
        "* Cette configuration n'est pas disponible sur certains modèles d'unité motrice."
    override val m13TopSideOfTheAdjustmentMethod = "Exécution d'un réglage supérieur"
    override val m13SmallestSprocket = "Pignon le plus petit"
    override val m13LargestGear = "Rapport le plus grand"
    override val m13Chain = "Chaîne"
    override val m13DescOfTopSideOfAdjustmentMethod2 =
        "2.\nTournez le boulon de réglage latéral supérieur avec une clé hexagonale de 2 mm. L'espace entre la chaîne et la plaque extérieure de guide-chaîne doit ensuite être ajusté entre 0, 5 et 1 mm."
    override val m13TopSideOfAdjustmentMethod =
        "Avant d'exécuter les réglages électriques avec l'application, vous devez exécuter un réglage à l'aide du boulon de réglage situé sur le dérailleur avant. Suivez les étapes ci-après pour procéder au réglage."
    override val m13BoltLocationCheck = "Vérification de l'emplacement du boulon"
    override val m13BoltLocationCheckDescription =
        "Le boulon de réglage inférieur, le boulon de réglage supérieur et le boulon de support sont situés à proximité les uns des autres.\nVeillez à régler le bon boulon."
    override val m13Bolts =
        "(A) Boulon de réglage inférieur\n(B) Boulon de réglage supérieur\n(C) Boulon de support"
    override val m13DescOfTopSideOfAdjustmentMethod1 =
        "1.\nPlacez la chaîne sur le plus grand plateau et le plus petit pignon."
    override val m13DescOfMtbFrontDerailleurMethod2 =
        "2.\nDesserrez le boulon de blocage de la course à l'aide d'une clé Allen de 2 mm.\n (A) Boulon de réglage de course\n (B) Boulon de réglage supérieur"
    override val m13DescOfMtbFrontDerailleurMethod3 =
        "3.\nTournez le boulon de réglage supérieur avec une clé Allen de 2 mm pour régler le jeu. Réglez l'espace entre la chaîne et la plaque dans le guide-chaîne entre 0 et 0, 5 mm."
    override val m13AfterAdujustmentMessge =
        "4.\nAprès le réglage, serrez fermement le boulon de blocage de la course."
    override val m13DescOfRearDerailleurAdjustmentMethod1 =
        "1.\nDéplacez la chaîne sur le 5e pignon. Déplacez le galet-guide vers l'intérieur jusqu'à ce que la chaîne touche le 4e pignon et émette un léger bruit."
    override val m13DescOfRearDerailleurAdjustmentMethod2 =
        "2.\nDéplacez le galet de guidage vers l'extérieur de 4 engrenages (5 engrenages sur un VTT) vers la position cible."
    override val m13PerformingZeroOffsetTitle = "Réalisation de l'étalonnage de décalage zéro"
    override val m13PerformingZeroOffsetMsg =
        "1.\nPlacez le vélo sur une surface plane.\n\n2.\nPositionnez la manivelle de sorte qu'elle soit perpendiculaire au sol comme dans l'illustration.\n\n3.\nAppuyez sur le bouton \"Étalonnage de décalage zéro\".\n\nNe placez pas les pieds sur les pédales ou n'appliquez aucune charge sur le pédalier."
    override val m13ShiftingAdvice = "Changement conseillé :"
    override val m13ShiftingAdviceOverview =
        "Cette fonction vous avertit sur le compteur lorsqu'il convient de changer de vitesse en mode manuel. Le moment d'affichage de la notification varie en fonction de la valeur configurée pour le moment de changement."
    override val m13PerformingLowAdjustment =
        "Comment exécuter un réglage inférieur (FD-6870/FD-9070 uniquement)"
    override val m13PerformingLowAdjustmentDescription1 =
        "1.\nRéglez la chaîne sur le plus petit plateau et le plus grand pignon."
    override val m13PerformingLowAdjustmentDescription2 =
        "2.\nTournez le boulon de réglage inférieur avec une clé hexagonale de 2 mm. Réglez l'espace entre la chaîne et la plaque extérieure de guide-chaîne entre 0 et 0,5mm."
    override val m13FrontDerailleurMethod2 =
        "2.\nDesserrez le boulon de fixation de la course à l'aide d'une clé à six pans de 2 mm.\n(A) Boulon de réglage de la course\n(B) Boulon de réglage supérieur"
    override val m13FrontDerailleurMethod3 =
        "3.\nTournez le boulon de réglage supérieur avec une clé à six pans de 2 mm pour régler le jeu. Réglez l'espace entre la chaîne et la plaque intérieure de guide-chaîne entre 0 et 0,5 mm."
    override val m13VerySlow = "très lent"
    override val m13Slow = "lent"
    override val m13Normal = "normal"
    override val m13Fast = "rapide"
    override val m13VeryFast = "très rapide"
    override val m13MtbTopSideOfAdjustmentMethod1 =
        "1.\nRéglez la chaîne sur le plateau le plus grand et le pignon le plus grand."
    override val m13AdjustmentMethod = "調整方法"
    //endregion

    //region dialog
    //region 要求仕様書ベース
    //region タイトル
    override val dialog17SwipeToSwitchTitle = "Balayer pour changer"
    override val dialog28BluetoothOffTitle = "Bluetooth désactivé"
    override val dialog29LocationInformationOffTitle = "Informations de localisation désactivée"
    override val dialog2_3AddBikeTitle = "Ajouter un vélo"
    override val dialog25PasskeyErrorTitle = "Erreur de PassKey"
    override val dialog27RegistersBikeTitle = "Unité enregistrée"
    override val dialog2_8RegisterBikeNameTitle = "Enregistrer le nom du vélo"
    override val dialog2_9PasskeyTitle = "PassKey"
    override val dialog2_11ConfirmBikeTitle = "Vérifier le vélo"
    override val dialog69SwitchBikeConnectionTitle = "Déconnecter le vélo actuel"
    override val dialog3_4UnitNotDetectdTitle = "L'unité n'est pas détectée."
    override val dialog3_5UnitRegisteredAgainTitle = "Réenregistrer l'unité"
    override val dialog4_1UpdateDetailTitle = "{0} ver. {1}"
    override val dialog4_2AllUpdateConfirmTitle = "Voulez-vous mettre à jour\ntoutes les unités ?"
    override val dialog4_4CancelUpdateTitle = "Annuler la mise à jour"
    override val dialog6_1DeleteBikeTitle = "Supprimer le vélo"
    override val dialog6_2ConfirmTitle = "Confirmer"
    override val dialog6_3DeleteTitle = "Supprimer"
    override val dialog7_1ConfirmCancelSettingTitle = "Configuration de la pause"
    override val dialog7_2ConfirmDefaultTitle = "Réinitialiser la configuration par défaut"
    override val dialog10_2PauseSetupTitle = "Réglage de la pause"
    override val dialog_jira258PushedIntoThe2ndGearTitle =
        "Opération exécutée lorsque le contacteur est placé sur le 2e rapport"
    //endregion

    //region 文章
    override val dialog01ConfirmExecuteZeroOffsetMessage = "Voulez-vous exécuter un décalage nul ?"
    override val dialog02ConnectedMessage = "Connecté."
    override val dialog02CaribrationIsCompleted = "L'étalonnage est terminé."
    override val dialog02CaribrationIsFailed = "Échec de l'étalonnage."
    override val dialog03FailedToExecuteZeroOffsetMessage = "Échec d'exécution du décalage nul."
    override val dialog03DoesNotrespondProperlyMessage =
        "Le capteur de puissance ne répond pas correctement."
    override val dialog04UpdatingFirmwareMessage = "Mise à jour du micrologiciel..."
    override val dialog05CompleteSetupMessage = "La configuration est terminée."
    override val dialog06ConfirmStartUpdateMessage =
        "Avant de lancer la mise à jour, bien lire l'Avis ci-dessous. N'effectuez aucune autre opération pendant la mise à jour."
    override val dialog06AllUpdateConfirmMessage = "Temps requis approximatif : {0}"
    override val dialog07CancelUpdateMessage =
        "Voulez-vous annuler la mise à jour du micrologiciel ?\n* Si vous choisissez « Annuler », les mises à jour ultérieures de l'unité suivante seront annulées. La mise à jour de l'unité actuellement en cours se poursuit."
    override val dialog13ConfirmReviewTheFollowingChangesMessage =
        "Passez en revue les changements suivants pour {0}, qui résulteront de l'opération de  rétablissement du micrologiciel."
    override val dialog13Changes = "Changements"
    override val dialog13MustAgreeToTermsOfUseMessage =
        "Vous devez accepter les conditions d'utilisation du logiciel pour pouvoir rétablir le micrologiciel."
    override val dialog13AdditionalSoftwareLicenseAgreementMessage =
        "Contratto di licenza software supplementare\n (Leggere scrupolosamente)"
    override val dialog15FirmwareWasUpdateMessage =
        "Le firmware de {0} fonctionne normalement.\nLe micrologiciel a été mis à jour vers la dernière version."
    override val dialog15FirmwareRestoreMessage = "Le firmware de {0} a été réparé."
    override val dialog15CanChangeTheSwitchSettingMessage =
        "Lors de l'utilisation de {0}, vous pouvez modifier l'état du réglage de suspension affiché dans SC à partir du Réglage de l'interrupteur dans le menu Personnaliser."
    override val dialiog15FirmwareOfUnitFunctionsNormallyMessage =
        "Le firmware de {0} fonctionne normalement.\nIl n'a pas besoin d'être restauré."
    override val dialog15FirmwareRestorationFailureMessage =
        "Restauration du firmware de {0} échouée."
    override val dialog15MayBeFaultyMessage = "{0} est peut-être défectueux."
    override val dialog15FirmwareRecoveryAgainMessage =
        "Un nouvel essai de récupération du micrologiciel pour {0} est en cours."
    override val dialog15ConfirmDownloadLatestVersionOfFirmwareMessage =
        "Le fichier du microprogramme de {0} est vide.\nTélécharger la dernière version du micrologiciel ?"
    override val dialog15FirstUpdateFirmwareMessage = "D'abord, mettez à jour le firmware pour {0}."
    override val diaog15CannotUpdateMessage =
        "Mise à jour impossible en raison d'une mauvaise réception du signal. Réessayez dans un endroit où la réception du signal est meilleure."
    override val dialog15ConfirmStartUpdateMessage =
        "La mise à jour du micrologiciel prendra quelques minutes. \nSi la batterie de votre appareil est faible, effectuez la mise à jour après l'avoir chargée ou connectez-la à un chargeur. \nDémarrer la mise à jour ?"
    override val dialog15ConnectAfterFirmwareUpdateFailureMessage =
        "Echec de connexion après la mise à jour du microprogramme.\nVeuillez essayer de vous connecter de nouveau."
    override val dialog15ConfirmConnectedUnitMessage =
        "{0} est reconnu.\nL'unité {0} est-elle connectée?"
    override val dialog17RegistrationIsCompletedMessage = "L'enregistrement est terminé."
    override val dialog17ConnectionIsCompletedMessage = "La connexion est terminée."
    override val dialog19TryAgainWirelessCommunicationMessage =
        "L'environnement sans fil n'est pas stable. Réessayez dans un endroit où la communication est stable."
    override val dialog20BetaVersionMsg =
        "Ce logiciel est une version bêta.\nSon évaluation n'étant pas terminée, il se peut que divers problèmes surviennent.\nSouhaitez-vous malgré tout utiliser ce logiciel ?"
    override val dialog21BetaVersionMsg =
        "Ce logiciel est une version bêta.\nSon évaluation n'étant pas terminée, il se peut que divers problèmes surviennent.\nSouhaitez-vous malgré tout utiliser ce logiciel ?\n \nDate d'expiration du logiciel : {0}"
    override val dialog22BetaVersionMsg =
        "Ce logiciel est une version bêta.\nCe logiciel a expiré.\nVeuillez réinstaller le logiciel authentique."
    override val dialog24PasskeyMessage = "Entrez votre PassKey."
    override val dialog25PasskeyErrorMessage =
        "Authentification impossible. Le PassKey n'a pas été correctement saisi ou le PassKey de l'unité sans fil a été modifié. Reconnectez-vous, puis réessayez de saisir le PassKey défini dans l'unité sans fil."
    override val dialog26ChangePasskeyMessage = "Voulez-vous modifier votre PassKey ?"
    override val dialog27RegisterdBikeMessage =
        "{0} est déjà enregistré en tant qu'unité sans fil pour {1}. Voulez-vous l'utiliser en tant qu'unité sans fil pour le nouveau vélo ?* L'enregistrement de {1} sera annulé."
    override val dialog28BluetoothOffMesssage =
        "Activez la configuration Bluetooth sur l'appareil à connecter."
    override val dialog29LocationInformationOffMessage =
        "Permet la connexion au service d'informations de localisation."
    override val dialog30DeleteBikeMessage =
        "Lorsqu'un vélo est supprimé, ses données sont également supprimées."
    override val dialog31ConfirmDeleteBikeMessage = "Voulez-vous vraiment supprimer le vélo ?"
    override val dialog32UpdatedProperlyMessage = "Des réglages ont été correctement mis à jour."
    override val dialog32UpdateCompletedMessage = "La mise à jour est terminée."
    override val dialog36PressSwitchMessage =
        "Appuyez sur le contacteur de l'unité à sélectionner."
    override val dialog37SelectTheDerectionMessage =
        "Sur quelle poignée le contacteur de commande est-il installé ?"
    override val dialog38SameMarksCannotBeAssignedMessage =
        "- Si le modèle de combinaison est différent pour les suspensions avant et arrière, il est impossible d'affecter la même marque (CTD)."
    override val dialog38DifferentMarksCannotBeAssignedMessage =
        "- Si le même modèle de combinaison est utilisé pour les suspensions avant et arrière, il est impossible d'affecter des marques (CTD) différentes."
    override val dialog39CheckTheFollowingInformationMessage = "Vérifiez le message ci-dessous."
    override val dialog39ConfirmContinueWithProgrammingMessage =
        "D'accord pour continuer la programmation ?"
    override val dialog39SameSettingMessage =
        "- Le même réglage a été choisi pour deux ou plus de positions."
    override val dialog39OnlyHasBeenSetMessage = "- Seul {1} a été réglé pour {0}."
    override val dialog40CannotSelectSwitchMessage =
        "Si vous ne pouvez pas sélectionner l'utilisation des contacteurs, vérifiez si l'un des fils électriques est déconnecté. \nSi c'est le cas, reconnectez-le. \nSi ce n'est pas le cas, il se peut qu'un contacteur présente un dysfonctionnement."
    override val dialog41ConfirmDefaultMessage =
        "Voulez-vous réinitialiser la configuration par défaut ?"
    override val dialog42ConfirmDeleteSettingMessage =
        "Voulez-vous vraiment supprimer la configuration ?"
    override val dialog43ComfirmUnpairPowerMeterMessage =
        "Voulez-vous désappairer le capteur de puissance ?"
    override val dialog44ConfirmCancelSettingMessage =
        "Voulez-vous vraiment ignorer les informations saisies ?"
    override val dialog45DisplayConditionConfirmationMessage =
        "Impossible de configurer Synchronized shift. Reportez-vous au site Web d'E-TUBE PROJECT."
    override val dialog46DisplayConditionConfirmationMessage =
        "Impossible de configurer le mode de changement de vitesse multiple. Reportez-vous au site Web d'E-TUBE PROJECT."
    override val dialog47DuplicateSwitchesMessage =
        "Plusieurs contacteurs identiques sont connectés. Impossible d'enregistrer les réglages."
    override val dialog48FailedToUpdateSettingsMessage = "Échec de mise à jour des réglages."
    override val dialog48FailedToConnectBicycleMessage = "Échec de connexion au vélo."
    override val dialog49And50CannotOperateOormallyMessage =
        "Le point de changement de vitesses se trouve dans la plage non réglable et ne peut pas fonctionner normalement."
    override val dialog51ConfirmContinueSettingMessage =
        "Les fonctions suivantes ne sont pas incluses. Voulez-vous vraiment continuer ?\n{0}"
    override val dialog55NoInformationMessage = "Informations introuvables sur ce vélo."
    override val dialog56UnpairConfirmationDialogMessage =
        "Voulez-vous vraiment supprimer le capteur de puissance ?"
    override val dialog57DisconnectBluetoothDialogMessage =
        "Le capteur de puissance a été supprimé. L'unité sans fil doit également être supprimée de la configuration Bluetooth du système d'exploitation."
    override val dialog58InsufficientStorageAvailableMessage =
        "Stockage disponible insuffisant. Impossible d'enregistrer le fichier de préréglages."
    override val dialog59ConfirmLogoutMessage = "Voulez-vous vous déconnecter ?"
    override val dialog59LoggedOutMessage = "Vous vous êtes déconnecté."
    override val dialog60BeforeApplyingSettingMessage =
        "Vérifiez les éléments suivants avant d'appliquer la configuration."
    override val dialog60S1OrS2IsNotSetMessage = "S1 ou S2 n'est pas réglé."
    override val dialog60AlreadyAppliedSettingMessage = "Ce réglage a aussi été appliqué."
    override val dialog60NotApplicableDifferentGearPositionControlSettingsMessage =
        "Si les valeurs réglées pour le nombre de dents et le contrôle de la vitesse engagée diffèrent entre S1 et S2, la configuration ne peut pas être appliquée."
    override val dialog60NotApplicableDifferentSettingsMessage =
        "Si les valeurs réglées pour l'intervalle de changement de vitesse diffèrent entre S1 et S2, la configuration ne peut pas être appliquée."
    override val dialog60ShiftPointCannotAppliedMessage =
        "Le point de changement de vitesse de la configuration du Synchronized shift se situe dans une plage hors réglage."
    override val dialog61CreateNewSettingMessage =
        "Pour créer un nouveau réglage, supprimez l'un des réglages existants."
    override val dialog62AdjustDerailleurMessage =
        "Pour régler le dérailleur, vous devez tourner le pédalier manuellement. Préparez l'opération en plaçant le vélo sur le support d'entretien ou tout autre dispositif. Veillez également à ne pas vous coincer la main dans les engrenages."
    override val dialog63ConfirmCancellAdjustmentMessage =
        "Voulez-vous vraiment quitter ?"
    override val dialog64RecoveryFirmwareMessage =
        "Le micrologiciel de {0} pourrait ne pas fonctionner correctement. La récupération du micrologiciel a été  effectuée. Temps requis approximatif : {1}"
    override val dialog64ConfirmRestoreTheFirmwareMessage =
        "Une unité sans fil défectueuse a été trouvée.\n Restaurer le firmware ?"
    override val dialog64UpdateFirmwareMessage =
        "L'unité sans fil est peut être défectueuse.\nLe micrologiciel sera mis à niveau."
    override val dialog65ApplicationOrFirmwareMayBeOutdatedMessage =
        "L'application ou le micrologiciel est peut-être obsolète. Connectez-vous à Internet et recherchez la dernière version."
    override val dialog66ConfirmConnectToPreviouslyConnectedUnitMessage =
        "Voulez-vous vous connecter à l'unité connectée précédemment ?"
    override val dialog67BluetoothOffMessage =
        "Le Bluetooth n'est pas activé sur l'appareil. Activez la configuration Bluetooth sur l'appareil à connecter."
    override val dialog68LocationInformationOffMessage =
        "Les données de localisation de l'appareil ne sont pas disponibles. Permet la connexion au service d'informations de localisation."
    override val dialog69SwitchBikeConnectionMessage =
        "Voulez-vous vraiment déconnecter le vélo actuellement connecté ?"
    override val dialog71LanguageChangeCompleteMessage =
        "Le réglage de la langue a été modifié. \nLa langue sera modifiée lorsque vous quitterez puis relancerez l'application."
    override val dialog72ConfirmBikeMessage =
        "{0} unités sont utilisées. Voulez-vous l'enregistrer en tant que vélo existant ? Voulez-vous l'enregistrer en tant que nouveau vélo ?"
    override val dialog73CannnotConnectToNetworkMessage = "Connexion au réseau impossible."
    override val dialog74AccountIsLockedMessage =
        "Ce compte est verrouillé. Essayez à nouveau de vous connecter ultérieurement."
    override val dialog75IncorrectIdOrPasswordMessage =
        "L'identifiant utilisateur ou le mot de passe entré est incorrect."
    override val dialog76PasswordHasExpiredMessage =
        "Votre mot de passe provisoire a expiré.\nRecommencez le processus d'enregistrement."
    override val dialog77UnexpectedErrorMessage = "Une erreur inattendue s'est produite."
    override val dialog78AssistModeSwitchingIsConnectedMessage =
        "{0} pour la commutation du mode d'assistance est connecté. \nLe type de vélo actuel prend uniquement en charge le réglage de changement de vitesse. \nRégler {0} pour le changement de vitesse?"
    override val dialog78ShiftModeSwitchingIsConnectedMessage =
        "{0} est connecté pour le changement de vitesse.\nLe type de vélo actuel prend uniquement en charge ce réglage pour la commutation du mode d'assistance.\nVoulez-vous configurer {0} pour la commutation du mode d'assistance ?"
    override val dialog79WirelessConnectionIsPoorMessage =
        "La connexion sans fil est pauvre. \nLa connexion Bluetooth® LE peut être interrompue."
    override val dialog79ConfirmAjustedChainTensionMessage = "Avez-vous réglé la tension de chaîne?"
    override val dialog79ConfirmAjustedChainTensionDetailMessage =
        "Lors de l'utilisation d'un moyeu à vitesses intégrées, il est nécessaire d'ajuster la tension de chaîne.\nRéglez la tension de la chaîne, puis appuyez sur le bouton Exécuter."
    override val dialog79ConfirmCrankAngleMessage = "Avez-vous vérifié l'angle du pédalier?"
    override val dialog79ConfirmCrankAngleDetailMessage =
        "La manivelle gauche doit être installée sur l'axe à l'angle approprié. Vérifiez l'angle de la manivelle installée, puis appuyez sur le bouton Exécuter."
    override val dialog79RecomendUpdateOSVersionMessage =
        "L'unité suivante ne prend pas en charge le système d'exploitation de votre smartphone ou de votre tablette.\n{0}\nIl est recommandé de mettre à jour le système d'exploitation de votre smartphone ou tablette vers la dernière version pour pouvoir l'utiliser avec cette application."
    override val dialog79CannotUseUnitMessage = "L'unité suivante ne peut pas être utilisée."
    override val dialog79RemoveUnitMessage = "Supprimer l'unité."
    override val dialog79UpdateApplicationMessage =
        "Mettez à jour l'application vers la dernière version, puis réessayez."
    override val dialog79ConnectToTheInternetMessage =
        "Impossible de vérifier une connexion avec le serveur. \nConnectez-vous à Internet, puis réessayez."
    override val dialog79NewFirmwareVersionWasFoundMessage =
        "Une nouvelle version de micrologiciel a été trouvée. \nTéléchargement de la nouvelle version…"
    override val dialog79DownloadedFileIsCorrupMessage =
        "Le fichier téléchargé est endommagé.\nTéléchargez de nouveau le fichier.\nSi le téléchargement échoue de façon répétée, il peut y avoir un problème avec la connexion internet ou le serveur web de Shimano.\nAttendez un peu et recommencez."
    override val dialog79FileDownloadFailedMessage = "Échec de téléchargement du fichier."
    override val dialog79ConfirmConnectInternetMessage =
        "Le fichier va être mis à jour. \nVotre appareil est-il connecté à Internet ?"
    override val dialog79UpdateFailedMessage = "Échec de la mise à jour."
    override val dialog80TakeYourHandOffTheSwitchMessage =
        "OK.\nRetirez votre main de l'interrupteur.\nSi la boîte de dialogue ne se ferme pas, même après l'avoir quittée, l'interrupteur a peut-être rencontré une erreur. Le cas échéant, connectez l'interrupteur et effectuez un contrôle d'erreurs."
    override val dialog81CheckWhetherThereAreAnyUpdatedMessage =
        "Connectez-vous à Internet et vérifiez s'il y a des mises à jour de E-TUBE PROJECT ou de produit disponibles. \nLa mise à jour vers la dernière version vous permettra d'utiliser de nouveaux produits et de nouvelles fonctionnalités."
    override val dialog82FailedToRegisterTheImageMessage = "Échec d'enregistrement de l'image."
    override val dialog83NotConnectedToTheInternetMessage =
        "Vous n'êtes pas connecté à Internet. Connectez-vous à Internet et réessayez."
    override val dialog84RegisterTheBicycleAgainMessage =
        "Aucun vélo applicable détecté. Connectez-vous à Internet et réenregistrez le vélo."
    override val dialog85RegisterTheImageAgainMessage =
        "Une erreur inattendue s'est produite. Réenregistrez l'image."
    override val dialog87ProgrammingErrorMessage =
        "Une erreur s'est produite pendant l'exécution des paramètres."
    override val dialog88MoveNextStepMessage =
        "Le dérailleur est déjà dans la position de pignons indiquée. Veuillez passer à l'étape suivante."
    override val dialog89ConfirmWhenGoingToETubeRide =
        "Démarrez E-TUBE RIDE. Toutes les unités actuellement connectées seront déconnectées. Voulez-vous vraiment continuer ?"
    override val dialog92Reading = "Lecture en cours."
    override val dialog103BleAutoConnectCancelMessage = "Voulez-vous annuler la connexion ?"
    override val dialog107_PairingCompleteMessage =
        "ID :{0}({1})\n\nPour terminer l’appariement, appuyez sur un bouton du contacteur dont l’ID a été saisi."
    override val dialog108_DuplicateMessage =
        "ID :{0}({1})\n\n{2} est déjà appairé. Voulez-vous désappairer le {2} précédemment appairé et passer à l’étape suivante ?"
    override val dialog108_LeftLever = "Levier de gauche"
    override val dialog108_RightLever = "Levier de droite"
    override val dialog109_UnrecognizableMessage =
        "ID :{0}\n\nID non reconnu. Vérifiez que l’ID que vous avez saisi est correct."
    override val dialog110_WriteFailureMessage = "ID :{0}({1})\n\nL’écriture de l’ID a échoué."
    override val dialog112_PairingDeleteMessage = "Voulez-vous désappairer le contacteur ?"
    override val dialog113GetSwitchFWVersionMessage =
        "Vérifiez la version du micrologiciel du contacteur. Appuyez sur un bouton sur {0} ({1})."
    override val dialog116HowToUpdateWirelessSwitchMessage =
        "Reportez-vous aux « Détails » pour connaître la procédure de mise à jour."
    override val dialog122ConfirmFirmwareLatestUpdateMessage =
        "Voulez-vous vérifier la dernière version du fichier du micrologiciel ?"
    override val dialog123AllFirmwareIsUpToDateMessage =
        "Le micrologiciel est à jour sur toutes les unités du vélo connecté."
    override val dialog123FoundNewFirmwareVersionMessage =
        "Une nouvelle version du micrologiciel est disponible pour l'unité du vélo connecté."
    override val dialog3_5UnitRegisteredAgainMessage =
        "Les informations du vélo ne sont pas synchronisées. L'unité doit être réenregistrée."
    override val dialog4_1UpdatesMessage = "Mises à jour"
    override val dialog8_2ConfirmButtonFunction =
        "Aucune fonction n'est assignée à certains boutons."
    override val dialog_jira258ConfirmSecondLevelGearShiftingMessage =
        "Voulez-vous activer le changement de vitesse à 2 rapports ?"
    override val dialog_jira258ConfirmSameOperationMessage =
        "Voulez-vous demander d'effectuer la même opération que celle exécutée lorsque le contacteur est enfoncé deux fois ?"
    override val dialog2_2NeedFirmwareUpdateMessage =
        "Pour utiliser E-TUBE PROJECT, le micrologiciel sur {0} doit être mis à jour. Temps requis approximatif : {1}"
    override val dialog3_1CompleteFirmwareUpdateMessage = "Le micrologiciel a été mis à jour."
    override val dialog3_2ConfirmCancelFirmwareUpdateMessage =
        "Voulez-vous annuler la mise à jour du micrologiciel ? \n*Les mises à jour ultérieures de l'unité suivante seront annulées. \nLa mise à jour de l'unité actuellement en cours se poursuit."
    override val dialog3_3ConfirmRewriteFirmwareMessage =
        "Le micrologiciel de {0} utilise la dernière version. Voulez-vous l'écraser ?"
    override val dialog3_4UpdateForNewFirmwareMessage =
        "Un nouveau micrologiciel pour {0} existe mais la mise à jour est impossible. La version Bluetooth® LE n'est pas prise en charge. Mettez à jour avec la version mobile."
    override val dialog4_1ConfirmDisconnectMessage =
        "Voulez-vous procéder à la déconnexion ?\nUne configuration personnalisée existe. En cas de déconnexion, la configuration ne sera pas appliquée."
    override val dialog4_2ConfirmResetAllChangedSettingMessage =
        "Voulez-vous réinitialiser toutes les modifications apportées à la configuration ?"
    override val dialog4_3ConfirmDefaultMessage =
        "Voulez-vous réinitialiser la configuration par défaut des réglages sélectionnés ?"
    override val dialog6_3DeleteMessage =
        "Le vélo a été supprimé. L'unité sans fil doit également être supprimée de la configuration Bluetooth du système d'exploitation."
    override val dialog12_2CanNotErrorCheckMessage =
        "Impossible d'exécuter le contrôle d'erreurs avec SM-BCR2."
    override val dialog12_4ConfirmStopChargeAndDissconnectMessage =
        "Voulez-vous arrêter le chargement et vous déconnecter ?"
    override val dialog13_1ConfirmDiscaresAdjustmentsMessage =
        "Voulez-vous vraiment ignorer les réglages effectués ?"
    override val dialog_jira258BrightnessSettingMessage =
        "Le réglage de la luminosité de l'affichage prendra effet après la déconnexion."
    override val dialogUsingSM_Pce02Message =
        "E-TUBE PROJECT est plus utile avec SM-PCE02.\n\n・Le contrôle de la consommation de la batterie permet de vérifier le courant de fuite au niveau des différentes unités connectées.\n・Augmentation de la stabilité des communications \n・Accélération des mises à jour"
    override val dialogLocationServiceIsNotPermittedMessage =
        "Si l'accès au service de localisation n'est pas autorisé, il n'est pas possible d'établir une connexion Bluetooth® LE en raison des restrictions d'utilisation d'Android OS. Activez le service de localisation pour cette application, puis redémarrez l'application."
    override val dialogStorageServiceIsNotPermittedMessage =
        "Si l'accès à l'espace de stockage n'est pas autorisé, les images ne peuvent pas être enregistrées en raison des restrictions d'utilisation d'Android OS. Activez l'espace de stockage pour cette application, puis redémarrez l'application."
    override val dialog114AcquisitionFailure = "La récupération a échoué."
    override val dialog114WriteFailure = "L’écriture a échoué."
    override val dialog129_DuplicateMessage = "{0}\n\nCe contacteur est déjà appairé."
    override val dialogUnsupportedFetchErrorLogMessage =
        "Les journaux d’erreur ne sont pas consultables avec la version du firmware de votre unité."
    override val dialogAcquisitionFailedMessage = "La récupération a échoué."
    //endregion

    //region 選択肢
    override val dialog06TopButtonTitle = "AVIS"
    override val dialog13AdditionalSoftwareLicenseAgreementOption1 = "Accepter et mettre à jour"
    override val dialog20And21Yes = "OUI"
    override val dialog20And21No = "NON"
    override val dialog22OK = "OK"
    override val dialog25PasskeyErrorOption1 = "Ressaisir"
    override val dialog26ChangePasskeyOption1 = "Plus tard"
    override val dialog26ChangePasskeyOption2 = "Modifier"
    override val dialog27RegisterBikeOption2 = "Utilisation"
    override val dialog28BluetoothOffOption1 = "Régler"
    override val dialog39ConfirmContinueOption2 = "Continuer"
    override val dialog41ConfirmDefaultOption2 = "Retour"
    override val dialog45SecondButtonTitle = "Plus"
    override val dialog55NoInformationOption1 = "Supprimer"
    override val dialog55NoInformationOption2 = "Enregistrer l'unité"
    override val dialog69SwitchBikeConnectionOption1 = "Déconnecter"
    override val dialog72ConfirmBikeOption1 = "Enregistrer en tant\nque nouveau vélo"
    override val dialog72ConfirmBikeOption2 = "Enregistrer en tant\nque vélo existant"
    override val dialog88MoveNextStepOption = "Suivant"
    override val dialog3_5UnitRegisteredAgainOption1 = "Enregistrer"
    override val dialog4_1UpdateDetailOption2 = "Historique des mises à jour"
    override val dialog6_3DeleteOption1 = "Régler"
    override val dialog2_1RecoveryFirmwareOption1 = "Récupérer (Entrée)"
    override val dialog2_1RecoveryFirmwareOption2 = "Ne pas récupérer"
    override val dialog2_2NeedFirmwareUpdateOption1 = "Mettre à jour (Entrée)"
    override val dialog2_2NeedFirmwareUpdateOption2 = "Ne pas mettre à jour"
    override val dialog3_1CompleteFirmwareUpdateOption = "OK (Entrée)"
    override val dialog3_3ConfirmRewriteFirmwareOption2 = "Sélectionner"
    override val dialogPhotoLibraryAuthorizationSet = "Régler"
    override val dialogPhotoLibraryAuthorizationMessage =
        "Cette application utilise la bibliothèque d'images pour télécharger des images."
    override val dialog109_Retry = "Ressaisir"
    //endregion
}
