package openCSV.combineVariables

import com.opencsv.CSVReaderBuilder
import com.opencsv.CSVReader
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.io.*
import openCSV.combineVariables.localization.*

@SpringBootApplication
class CombineVariablesApplication

fun main(args: Array<String>) {
	runApplication<CombineVariablesApplication>(*args)

	var fileReader: BufferedReader? = null
	var csvReader: CSVReader? = null
	val mtl  = AppVariables().getAppVariables()
	var mapResult = mutableListOf<Combination>()
	val fileName = "/Users/rhataji/Documents/e-tube-project/SpringBoot/combinevariables/src/main/kotlin/openCSV/combineVariables/TextTable.csv"

	try {
		println("0" + "	" + "アプリケーション変数名" + "	" + "TextTableID" + "	" + "備考" + "	" + "アプリケーション変数のテキスト" + "	" + "TextTableテキスト")
		for (combination in mtl.withIndex()) {
			fileReader =
				BufferedReader(FileReader(fileName))
			csvReader = CSVReader(fileReader)

			csvReader.readNext() // skip Header

			var record = csvReader.readNext()
			label1@ while (record != null) {
				if (record[3] == combination.value.text?.replace("\n", "n")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[4] == combination.value.text?.replace("\n", "n")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[4])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "") == combination.value.text?.replace("\\n", "")?.replace("\n", "") + "	" + record[3]) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text + "	" + "TextTableとテキストが異なります。（\\nがありません）")
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。（\\nがありません）" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。(丸カッコまわり)" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。{波カッコまわり}" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("Enter", "ENTER") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("Enter", "ENTER")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。（Enterが大文字です。）" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("：", ":").replace(" :", ":").replace(": ", ":") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("：", ":")?.replace(" :", ":")?.replace(": ", ":")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。（：が違います）" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[3].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("：", ":").replace(" :", ":").replace(": ", ":").replace("？", "?") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("：", ":")?.replace(" :", ":")?.replace(": ", ":")?.replace("？", "?")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}
				if (record[4].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("：", ":").replace(" :", ":").replace(": ", ":").replace("？", "?") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("：", ":")?.replace(" :", ":")?.replace(": ", ":")?.replace("？", "?")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[4])
					break@label1
				}
				if (record[5].replace("\\n", "").replace("\n", "").replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("：", ":").replace(" :", ":").replace(": ", ":").replace("？", "?") == combination.value.text?.replace("\\n", "")?.replace("\n", "")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("：", ":")?.replace(" :", ":")?.replace(": ", ":")?.replace("？", "?")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[5])
					break@label1
				}
				if (record[3].replace("（", "(").replace("）", ")").replace(" (", "(").replace(" {", "{").replace("} ", "}").replace("：", ":").replace(" :", ":").replace(": ", ":").replace("？", "?")  == combination.value.text?.replace("\n", "n")?.replace("（", "(")?.replace("）", ")")?.replace(" (", "(")?.replace(" {", "{")?.replace("} ", "}")?.replace("：", ":")?.replace(" :", ":")?.replace(": ", ":")?.replace("？", "?")) {
					mapResult.add(
						Combination(name = combination.value.name, textTable = record[0], text = combination.value.text)
					)
					println((combination.index + 1).toString()+ "	" + combination.value.name + "	" + record[0] + "	" + "TextTableとテキストが異なります。" + "	" + combination.value.text?.replace("\n", "\\n") + "	" + record[3])
					break@label1
				}

				record = csvReader.readNext()
				if (record == null)
					println((combination.index + 1).toString() + "	" + combination.value.name + "	" +  "" + "	" + "	" + combination.value.text?.replace("\n", "\\n"))
			}
			csvReader.close()
		}
	} catch (e: Exception) {
		println("Reading CSV Error!")
		e.printStackTrace()
	} finally {
		try {
			fileReader!!.close()
			csvReader!!.close()
		} catch (e: IOException) {
			println("Closing fileReader/csvParser Error!")
			e.printStackTrace()
		}
	}
}
